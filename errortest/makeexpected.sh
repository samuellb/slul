#!/bin/sh -eu -o pipefail
#
# makeexpected.sh - Generates expected error messages
#
# Copyright © 2022 Samuel Lidén Borell <samuel@kodafritt.se>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# This script generates a set of expected error messages from annotated
# .slul files.
#
# Each file should contain lines such as the following:
#
#    # ERROR .8: error: Example message
#    func f(() -> int
#    # ERROR +2.5: error: Example message
#    type Thing = struct {
#        3]int arr
#    }
#
# The first example error is expected at column 8 on the following line.
# The second example error is expected two lines ahead at column 5.

process_file() {
    local linenum=1
    local file="$1"
    while read -r line; do
        end="${line### ERROR }"
        if [ "x$line" != "x$end" ]; then
            if [ "x${end#.}" != "x$end" ]; then
                # Error message applies to the next line
                printf "%s:%s%s\n" "$file" $((linenum + 1)) "$end"
            elif [ "x${end#+}" != "x$end" ]; then
                end="${end#+}"
                offset="${end%%.*}"
                end="${end#*.}"
                printf "%s:%s.%s\n" "$file" $((linenum + offset)) "$end"
            elif [ "x${end#FILE }" != "x$end" ]; then
                # Error message applies to the whole file
                end="${end#FILE}"
                printf "%s:%s\n" "$file" "$end"
            fi
        fi
        linenum=$((linenum + 1))
    done < "$file"
}

while [ $# != 0 ]; do
    file=${1#.//}
    file=${file#./}
    process_file "$file"
    shift
done
