#
# Test .slul source file with invalid type definitions/usage
#
# Copyright © 2022-2024 Samuel Lidén Borell <samuel@kodafritt.se>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#


# Private types
# ERROR .6: error: Private types can only be used in references
data PrivType typechkP01 = (.x=1, .y=2)
# ERROR +2.15: error: Private types can only be used in references
# ERROR .45: error: Incompatible types
data struct { PrivType x } typechkP02 = (.x=1)
# ERROR +2.15: error: Open types cannot be used in top-level data items
# ERROR .28: error: Struct cannot be created outside its implementation module
data OpenType typechkP03 = (.x=1)
# ERROR +2.50: error: Incompatible types. Arrays have mixed variable/non-variable lengths
# ERROR .7: error: Identifier value is not known at compile-time
data [external_value]int typechk_array_extlen1 = []
# ERROR +2.50: error: Incompatible types. Arrays have mixed variable/non-variable lengths
# ERROR .7: error: Identifier value is not known at compile-time
data [external_value]int typechk_array_extlen2 = [1]
# ERROR +2.5: error: Private types can only be used in references
type TypechkP03 = struct {
    PrivType x
}
# ERROR .19: error: Private types can only be used in references
type TypechkP04 = [2]PrivType
# ERROR .19: error: Private types can only be used in references
type TypechkP05 = ref [2]PrivType
# ERROR +2.5: error: Private types can only be used in references
type TypechkP06 = ref struct {
    PrivType x
}
type TypechkP07 = ref struct {
    # ERROR .14: error: Open types can only be used inside a "ref"
    OpenType a
}

# Private types (PrivTypeVer is versioned)
# ERROR .6: error: Private types can only be used in references
data PrivTypeVer typechkPV01 = (.x=1, .y=2)
# ERROR +2.15: error: Private types can only be used in references
# ERROR .49: error: Incompatible types
data struct { PrivTypeVer x } typechkPV02 = (.x=1)
# ERROR +2.5: error: Private types can only be used in references
type TypechkPV03 = struct {
    PrivTypeVer x
}
# ERROR .20: error: Private types can only be used in references
type TypechkPV04 = [2]PrivTypeVer
# ERROR .20: error: Private types can only be used in references
type TypechkPV05 = ref [2]PrivTypeVer
# ERROR +2.5: error: Private types can only be used in references
type TypechkPV06 = ref struct {
    PrivTypeVer x
}
type TypechkPV07 = ref struct {
    # ERROR .17: error: Open types can only be used inside a "ref"
    OpenTypeVer a
}

# Incorrect usage of "func" keyword where it should be "funcref"
# ERROR .20: error: Expected a type here. (Use "funcref" for function references)
type BadFuncType = func()->bool

# ERROR .6: error: Expected a type here, but types must start with UpperCase
data t_typechk01 typechk_l01 = 123
# ERROR .23: error: Expected a type here, but types must start with UpperCase
func typechk_l02() -> t_typechk02
# ERROR .6: error: Expected a type here, but types must start with UpperCase
type t_typechk03 = struct {
    int x
}
type Typechk04 = struct {
# ERROR .5: error: Expected a type here, but types must start with UpperCase
    t_typechk04 x
}

# ERROR .22: error: Expected ( after function name
func typechk_badfunc { }
# ERROR .28: error: Bad type parameter definition
func typechk_badtypeparam1<t()
# ERROR .27: error: Bad type parameter definition
type TypechkBadTypeParam2<t = struct {
    int x
}
type TypechkBadArrLength1 = struct {
    # ERROR .6: error: Incomplete expression
    [F]byte arr
}
type TypechkBadNestedStruct = struct {
    # FIXME error should appear at end of line instead
    struct
    # ERROR .1: error: Incorrect syntax. Expected a type here.
}

# ERROR .32: error: Incorrect syntax. Expected a type here.
type TypechkBadEnumBase = enum else {
}

type TypechkBadEnumExpr = enum int {
    # ERROR .9: error: Incomplete expression
    a = else
}

type TypechkBadEnumDupl = enum {
    # ERROR .5: <-- other definition: a
    a
    # ERROR .5: error: Duplicate enum identifier
    a
}

type TypechkBadEnumValueWithoutBaseType = enum {
    # ERROR .7: error: enums can only have explicit "= values" if they have a base type
    a = 123
}

type TypechkBadEnumOverflow = enum uint64 {
    a = 18446744073709551615
    # ERROR .5: error: Number is too large (maximum for any type is 2^64)
    b
}

# ERROR .7: <-- other definition: red
func .red() -> TypechkBadEnumOutsideDupl1 { while true { } }
type TypechkBadEnumOutsideDupl1 = enum {
    # ERROR .5: error: Identifier already in use
    red
    blue
}

type TypechkBadEnumOutsideDupl2 = enum {
    red
    # ERROR .5: <-- other definition: blue
    blue
}
# ERROR .7: error: Identifier already in use
func .red() -> TypechkBadEnumOutsideDupl2 { while true { } }
# ERROR .34: error: Identifier already in use
data TypechkBadEnumOutsideDupl2 .blue = .red

# ERROR .37: error: enums cannot have type parameters
type TypechkBadEnumGeneric<ref T> = enum { a b }

# ERROR .43: error: Elementary types cannot have type parameters
type TypechkBadElementaryGeneric<ref T> = int


# ---------- Cycling typedefs ----------

# Referencing a broken type should not cause a hang or crash
data TypechkInfinity4 typechk_inf = ()

# ERROR .6: error: Cyclic type declaration without "ref" inside
type TypechkInfinity1 = TypechkInfinity1
# ERROR .6: error: Cyclic type declaration without "ref" inside
type TypechkInfinity2 = ref TypechkInfinity2
# ERROR .6: error: Cyclic type declaration without "ref" inside
type TypechkInfinity3 = ref ?ref arena ?ref ref TypechkInfinity3
# ERROR .6: error: Cyclic type declaration without "ref" inside
type TypechkInfinity4 = TypechkInfinity5
# ERROR .6: error: Cyclic type declaration without "ref" inside
type TypechkInfinity5 = TypechkInfinity4
# ERROR .6: error: Cyclic type declaration without "ref" inside
type TypechkDeepCycle = struct {
    TypechkDeepCycle nested
}
# ERROR .6: error: Cyclic type declaration without "ref" inside
type TypechkDeepCycle2 = [2,3]struct {
    [4]TypechkDeepCycle3 nested
}
# ERROR .6: error: Cyclic type declaration without "ref" inside
type TypechkDeepCycle3 = [5]TypechkDeepCycle2
type TypechkGeneric<T> = struct {
    slot T x
}
# ERROR .6: error: Cyclic type declaration without "ref" inside
type TypechkGenericCycle1<T> = ?ref TypechkGeneric<TypechkGenericCycle1>
# ERROR .6: error: Cyclic type declaration without "ref" inside
type TypechkGenericCycle2<T> = ?ref TypechkGenericCycle2<bool>
# ERROR .6: error: Cyclic type declaration without "ref" inside
type TypechkEnumCycle = enum TypechkEnumCycle {
    a
    b
}

func TypechkInfinity2.test() -> TypechkInfinity2
{
    # Referencing a broken type should not cause a hang or crash
    this.x = true
    return this
}

func TypechkDeepCycle.test(int arg) -> TypechkDeepCycle
{
    # Referencing a broken type should not cause a hang or crash
    if arg == 0 {
        return this.nested.nested.nested.nested.nested
    } else if arg == 1 {
        return (.nested=this)
    } else {
        return (.nested=(.nested=(.nested=this)))
    }
}

func TypechkGenericCycle1.test() -> TypechkGenericCycle1
{
    # Referencing a broken type should not cause a hang or crash
    this.x = true
    return this
}

func TypechkGenericCycle2.test() -> TypechkGenericCycle2
{
    # Referencing a broken type should not cause a hang or crash
    this.x = true
    return this
}

# Referencing a broken type should not cause a hang or crash
data TypechkEnumCycle typechk_enum_cycle = .a
