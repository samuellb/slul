#
# Makefile for testing syntax/semantic error checking
#
# Copyright © 2021-2024 Samuel Lidén Borell <samuel@kodafritt.se>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

ERROR_TEST_FILES = \
    $(srcdir)/errortest/mainapp/main.slul \
    $(srcdir)/errortest/mainapp/comments.slul \
    $(srcdir)/errortest/mainapp/constexpr.slul \
    $(srcdir)/errortest/mainapp/decls.slul \
    $(srcdir)/errortest/mainapp/definedness.slul \
    $(srcdir)/errortest/mainapp/exprchk.slul \
    $(srcdir)/errortest/mainapp/funccalls.slul \
    $(srcdir)/errortest/mainapp/funcchk.slul \
    $(srcdir)/errortest/mainapp/generics.slul \
    $(srcdir)/errortest/mainapp/return.slul \
    $(srcdir)/errortest/mainapp/token.slul \
    $(srcdir)/errortest/mainapp/typechk.slul \
    $(srcdir)/errortest/mainapp/typecompat.slul \
    $(srcdir)/errortest/baddeps/main.slul \
    $(srcdir)/errortest/baddeps_lib/main.slul \
    $(srcdir)/errortest/baddepsyntax_lib/main.slul \
    $(srcdir)/errortest/baddepsyntax_unstable/main.slul \
    $(srcdir)/errortest/badlib/main.slul \
    $(srcdir)/errortest/badlib/versioned_impls.slul \
    $(srcdir)/errortest/slul-interfaces/bad_header.slul \
    $(srcdir)/errortest/slul-interfaces/conflict1.slul \
    $(srcdir)/errortest/slul-interfaces/conflict2.slul \
    $(srcdir)/errortest/slul-interfaces/depends_on_baddeps24.slul \
    $(srcdir)/errortest/slul-interfaces/error_support_ver.slul \
    $(srcdir)/errortest/slul-interfaces/error_support_unver.slul \
    $(srcdir)/errortest/slul-interfaces/ifacedep_a.slul \
    $(srcdir)/errortest/slul-interfaces/ifacedeps.slul \
    $(srcdir)/errortest/slul-interfaces/too_old.slul \
    $(srcdir)/errortest/slul-interfaces/two_versions_a.slul \
    $(srcdir)/errortest/slul-interfaces/two_versions_b.slul \
    $(srcdir)/errortest/slul-interfaces/unstable_without_since.slul \
    $(srcdir)/errortest/slul-interfaces/use_nested.slul \
    $(srcdir)/errortest/slul-interfaces/use_unavailable_versions.slul \
    $(srcdir)/errortest/slul-interfaces/with_func.slul \
    $(srcdir)/errortest/slul-interfaces/with_type.slul \
    $(srcdir)/errortest/slul-interfaces/with_type_diff1.slul \
    $(srcdir)/errortest/slul-interfaces/with_type_diff2.slul \
    $(srcdir)/errortest/slul-interfaces/with_typeident1.slul \
    $(srcdir)/errortest/slul-interfaces/with_typeident2.slul \
    $(srcdir)/errortest/slul-interfaces/with_typeident3.slul \
    $(srcdir)/errortest/slul-interfaces/with_typeident_diff1.slul \
    $(srcdir)/errortest/slul-interfaces/wrongname.slul \
    $(srcdir)/errortest/slul-interfaces/wrongtype.slul \
    $(srcdir)/errortest/slul-interfaces-more/two_versions_a.slul \
    $(srcdir)/errortest/slul-interfaces-more/two_versions_b.slul
ERRORTEST_MODULES = \
    $(srcdir)/errortest/mainapp \
    $(srcdir)/errortest/baddeps \
    $(srcdir)/errortest/baddeps_lib \
    $(srcdir)/errortest/baddepsyntax_lib \
    $(srcdir)/errortest/baddepsyntax_unstable \
    $(srcdir)/errortest/badlib
ERRORTEST_IFACEDIRS = \
    -I - \
    -I $(src_runtime)/slul-interfaces \
    -I $(srcdir)/errortest/slul-interfaces \
    -I $(srcdir)/errortest/slul-interfaces-more
ERRORTEST_ARGS = \
    --no-hints --error-limit=0 --error-output=- --accept-errors \
    $(ERRORTEST_IFACEDIRS) $(ERRORTEST_MODULES)

check-error: errortest/out.expected errortest/out.actual
	diff errortest/out.expected errortest/out.actual
check-error-valgrind: errortest/out.expected errortest/out_vg.actual
	diff errortest/out.expected errortest/out_vg.actual
check-error-tcc-boundscheck: errortest/out.expected errortest/out_tcc.actual
	diff errortest/out.expected errortest/out_tcc.actual

.rawout.actual:
	sed 's/^\.\/*\///' $< | \
	        grep -vE 'error_support_.*<--' | \
	        sed -r 's/^(errortest\/[^/\\]+)\\/\1\//' | \
	        sort > $@

# Runs errortest normally
errortest/out.rawout: $(ERROR_TEST_FILES) $(build_cslul)/cslul
	LC_ALL=C.UTF-8 $(build_cslul)/cslul \
	            $(ERRORTEST_ARGS) --outdir=errortest/mainapp > $@

# Runs errortest under Valgrind
errortest/out_vg.rawout: $(ERROR_TEST_FILES) $(build_cslul)/cslul
	$(MKDIR_P) errortest/mainapp_vg
	LC_ALL=C.UTF-8 valgrind -q --leak-check=full $(VALGRIND_OPTS) \
	        $(build_cslul)/cslul \
	            $(ERRORTEST_ARGS) --outdir=errortest/mainapp_vg > $@

# Runs errortest compiled with TCC with boundschecking
errortest/out_tcc.rawout: $(ERROR_TEST_FILES) $(build_cslul)/cslul-tcc-boundscheck
	$(MKDIR_P) errortest/mainapp_tcc
	TCC_BOUNDS_WARN_POINTER_ADD=1 $(build_cslul)/cslul-tcc-boundscheck \
	            $(ERRORTEST_ARGS) --outdir=errortest/mainapp_tcc > $@

errortest/out.expected: errortest/makeexpected.sh $(ERROR_TEST_FILES)
	sh $(srcdir)/errortest/makeexpected.sh $(ERROR_TEST_FILES) | \
	    sort > errortest/out.expected

# gdb command to debug checking in a specific .slul file:
#    break check_funcbody if !(int)strcmp(func->filename, "/path/to/mainapp/funcchk.slul")
# gdb command to debug checking of a specific function:
#    break check_funcbody if !(int)strcmp(node_nameptr(func->ident), "bad_statements")
gdb-build-errortest: $(build_cslul)/cslul
	gdb --args $(build_cslul)/cslul \
	    $(ERRORTEST_ARGS) --outdir=errortest/mainapp --message-level=fatal

errortest-outdirs:
	$(MKDIR_P) errortest/mainapp errortest/mainapp_vg errortest/mainapp_tcc

.PHONY: check-error check-error-valgrind check-error-tcc-boundscheck \
        gdb-build-errortest \
        errortest-outdirs errortest-clean

errortest-clean:
	$(RM_F) errortest/mainapp/errortest \
	        errortest/mainapp/errortest_vg \
	        errortest/mainapp/errortest_tcc \
	        errortest/out.expected \
	        errortest/out.rawout \
	        errortest/out.actual \
	        errortest/out_vg.rawout \
	        errortest/out_vg.actual \
	        errortest/out_tcc.rawout \
	        errortest/out_tcc.actual
