#
# Main makefile for building SLUL
#
# Copyright © 2021-2024 Samuel Lidén Borell <samuel@kodafritt.se>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

# If you use srcdir: Please run "make srcdir=xxx outdirs" first,
# to create the necessary directory structure for output files.
srcdir ?= .
src_topdir = $(srcdir)
src_common = $(srcdir)/src-common
src_cslul = $(srcdir)/src-cslul
src_backend = $(srcdir)/src-backend
src_runtime = $(srcdir)/src-runtime
build_topdir = .
build_common = src-common
build_cslul = src-cslul
build_backend = src-backend
build_runtime = src-runtime
VPATH ?= $(srcdir)

# The final outputs are:
#   src-cslul/cslul             (the compiler)
#   src-runtime/libslulrt.so    (the runtime library)
all: cslul-all backend-all runtime-all
check: cslul-check check-exec check-error check-compile backend-check runtime-check

include $(src_common)/Makefile.inc
include $(src_backend)/Makefile.inc
include $(src_cslul)/Makefile.inc
include $(src_runtime)/Makefile.inc
include $(srcdir)/testexec/Makefile.inc
include $(srcdir)/errortest/Makefile.inc


# Run the valgrind target with VALGRIND_OPTS=--gdb-error=1
# then run "make vgdb" to inspect memory errors with GDB.
vgdb:
	gdb $(build_cslul)/cslul -ex "target remote | vgdb"

check-unit: cslul-check-unit backend-check-unit runtime-check-unit
check-oom-unit: cslul-check-oom-unit backend-check-oom-unit
check-unit-valgrind: cslul-check-unit-valgrind backend-check-unit-valgrind runtime-check-unit-valgrind
check-oom-unit-valgrind: \
    cslul-check-oom-unit-valgrind backend-check-oom-unit-valgrind
check-valgrind: check-unit-valgrind check-exec-valgrind check-compile-valgrind check-error-valgrind
tcc-boundscheck: \
    cslul-tcc-boundscheck backend-tcc-boundscheck runtime-tcc-boundscheck \
    check-exec-tcc-boundscheck check-error-tcc-boundscheck
gcc-coverage: cslul-gcc-coverage backend-gcc-coverage runtime-gcc-coverage
clang-analyze: cslul-clang-analyze backend-clang-analyze runtime-clang-analyze
# quite slow and prone to give false positives
gcc-analyze: cslul-gcc-analyze backend-gcc-analyze runtime-gcc-analyze
sparse-analyze: cslul-sparse-analyze backend-sparse-analyze runtime-sparse-analyze
# gives false positives, and suppressions need to be updated regularly
cppcheck: cslul-cppcheck backend-cppcheck runtime-cppcheck
check-deps: cslul-check-deps

# scan-all includes everything except afl-fuzz, gcc-analyze and cppcheck:
# - cppcheck gives too many false positives.
# - gcc-analyze is too slow.
# - afl-fuzz is meant to run for a long time (i.e. hours/days).
scan-all: clang-analyze check-unit-valgrind sparse-analyze \
          tcc-boundscheck check-oom-unit-valgrind \
          check-error-indices check-exec-valgrind check-error-valgrind \
          check-compile-valgrind check-deps elflint

# Creates all necessary output directories.
# Only needed when srcdir is overridden
outdirs: common-outdirs cslul-outdirs backend-outdirs runtime-outdirs \
         testexec-outdirs errortest-outdirs

# Generates the file list in CODE_OVERVIEW.md
source-overview:
	LC_ALL=C.UTF-8; \
	for dir in src-backend src-backend/outformat src-backend/codegen \
	           src-cslul src-cslul/fuzz src-cslul/testgen src-cslul/winlibc \
	           src-runtime; do \
	    printf '\nIn %s:\n\n' "$$dir"; \
	    ls "$(srcdir)/$$dir"/*.c | sort | while read file; do \
	        printf '  '; head -n 3 "$$file" | tail -n 1; \
	    done; \
    done


.PHONY: all check \
        check-unit check-unit-valgrind \
        check-oom-unit check-oom-unit-valgrind \
        vgdb tcc-boundscheck gcc-coverage \
        clean distclean install uninstall \
        clang-analyze sparse-analyze cppcheck scan-all \
        check-deps outdirs source-overview


clean: cslul-clean backend-clean runtime-clean testexec-clean errortest-clean
distclean: cslul-distclean backend-distclean runtime-distclean clean

install: cslul-install runtime-install
uninstall: cslul-uninstall runtime-uninstall
