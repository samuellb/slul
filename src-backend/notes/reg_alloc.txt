Register allocation
===================

Simple allocation
-----------------
Reserve any special-purpose register registers, e.g:
- ebx for PIC code on x86
- frame pointer register (ebp on x86)

Then allocate a register to all low var-lane indices.
- Assign the var-lane to the register.
  This is a fixed size lookup table (but the size varies per arch).
- Mark the register as used.
- Continue to the following var-lane.


Advanced allocation
-------------------
On some architectures, some registers have a certain intended purpose, and
the machine code might be shorter or more efficient if the optimal registers
are used.

For example, on x86:
- ecx = loop counter
- ebp, e*x = for struct pointers etc.

Caller/calle saving
-------------------
Temporaries should use caller-saved regs.

Variables might be better of with callee-saved regs, unless it is a leaf call.

If "tightcall" is added (less regs are saved by the caller), then such
functions should avoid using the unsaved regs in likely paths, as they
have to be saved before the first use AND before any external calls.

Optimized var-lane allocation
-----------------------------
Variables that...

1. are only used in a single EBB, and
2. whose address is never taken (not passed into a pointer)

...can be allocated in a separate set of low-numbered var-lanes.
- But: This should only be done if there are multiple variables that
  can share the same var-lane. Otherwise it makes no sense, and can
  even be counter-productive.
- If the EBB does not use all of the temporaries, then the variable
  could use one of those var-lanes.
  (this optimization could go into CSBE, perhaps as a function such as
   csbe_ebb_varlane_status(csbe, varlane, status={unused, initially_unused, used}))

Variables that are used many times and/or in many EBB's should also
be given low-numbered var-lanes.
