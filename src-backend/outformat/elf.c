/*

  elf.c -- ELF file handling

  Copyright © 2023-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include <assert.h>
#include <string.h>

#include "outformat_common.h"


#define RET_ERR(res) \
    if (UNLIKELY((e = (res)) != CSBEE_OK)) return e

#define EHSIZE_32 52
#define EHSIZE_64 64
#define PHENTSIZE_32 32
#define PHENTSIZE_64 56
#define SHENTSIZE_32 40
#define SHENTSIZE_64 64

#define ELFCLASS32 1
#define ELFCLASS64 2
#define ELFDATA2LSB 1
#define ELFDATA2MSB 2
#define EV_CURRENT 1
#define ELFOSABI_SYSV 0

#define EI_NONE 0
#define EI_REL  1
#define EI_EXEC 2
#define EI_DYN  3
#define EI_CORE 4

#define SHT_NULL     0
#define SHT_PROGBITS 1
#define SHT_SYMTAB   2
#define SHT_STRTAB   3
#define SHT_RELA     4
#define SHT_HASH     5
#define SHT_DYNAMIC  6
#define SHT_NOTE     7
#define SHT_NOBITS   8
#define SHT_REL      9
#define SHT_SHLIB   10
#define SHT_DYNSYM  11
#define SHT_GNU_HASH 0x6FFFFFF6

#define NO_FLAGS         0x0
#define SHF_WRITE        0x1
#define SHF_ALLOC        0x2
#define SHF_EXECINSTR    0x4
#define SHF_STRINGS     0x20
#define SHF_INFO_LINK   0x40

#define ST_I_BIND_MASK  0xF0
#define ST_I_LOCAL      0x00
#define ST_I_GLOBAL     0x10
#define ST_I_WEAK       0x20

#define ST_I_TYPE_MASK  0x0F
#define ST_I_NOTYPE        0
#define ST_I_OBJECT        1
#define ST_I_FUNC          2
#define ST_I_SECTION       3
#define ST_I_FILE          4

#define ST_O_DEFAULT    0
#define ST_O_INTERNAL   1
#define ST_O_HIDDEN     2
#define ST_O_PROTECTED  3

#define SHN_ABS 0xFFF1

#define DT_NULL     0
#define DT_NEEDED   1
#define DT_PLTRELSZ 2
#define DT_PLTGOT   3
#define DT_HASH     4
#define DT_STRTAB   5
#define DT_SYMTAB   6
#define DT_RELA     7
#define DT_RELASZ   8
#define DT_RELAENT  9
#define DT_STRSZ   10
#define DT_SYMENT  11
#define DT_SONAME  14
#define DT_REL     17
#define DT_RELSZ   18
#define DT_RELENT  19
#define DT_PLTREL  20
#define DT_JMPREL  23
#define DT_FLAGS   30
#define DT_GNU_HASH  0x6FFFFEF5
#define DT_FLAGS_1   0x6FFFFFFB
#define DT_RELACOUNT 0x6FFFFFF9
#define DT_RELCOUNT  0x6FFFFFFA

#define DF_BIND_NOW 0x8
#define DF_1_NOW    0x1
#define DF_1_PIE    0x8000000

#define PT_NULL     0
#define PT_LOAD     1
#define PT_DYNAMIC  2
#define PT_INTERP   3
#define PT_NOTE     4
#define PT_PHDR     6
#define PT_GNU_STACK 0x6474E551
#define PT_GNU_RELRO 0x6474E552

#define PF_X    0x1
#define PF_W    0x2
#define PF_R    0x4

static uint16 get_elf_type(const struct ArchInfo *arch)
{
    switch (arch->outformat) {
    case CSBEOF_ELF_OBJ:
        return EI_REL;
    case CSBEOF_ELF_EXE:
    case CSBEOF_ELF_DYNLIB:
        return EI_DYN;
    /* TODO static libraries? */
    CASE_EXCEPT_ELF
    default:
        assert(0);
        return EI_NONE;
    }
}

#define EM_386        3
#define EM_X86_64    62
#define EM_AARCH64  183

#define R_386_GLOB_DAT         6
#define R_X86_64_GLOB_DAT      6
#define R_AARCH64_GLOB_DAT  1025

struct MachineInfo {
    uint16 elf_machine_id;
    uint16 code_alignment;
    uint16 pltgot_alignment;
    uint32 page_alignment;
    unsigned merge_hdr_text; /**< Merge LOAD segments for text and header */
    unsigned got_rel_type;
};

static const struct MachineInfo machine_info[] = {
    /* CSBEC_TEXT_IR */ { 0,             0, 0,     0, 0, 0 },
    /* CSBEC_I386 */    { EM_386,       16, 8,  4096, 0, R_386_GLOB_DAT },
    /* CSBEC_X86_64 */  { EM_X86_64,    16, 8,  4096, 0, R_X86_64_GLOB_DAT },
    /* CSBEC_AARCH64 */ { EM_AARCH64,   16/* XXX sometimes it's 4 */, 4/* XXX with "full" pltgot entries, it should be 16 */, 65536, 1, R_AARCH64_GLOB_DAT }
};

/** C library specific details */
struct LibcInfo {
    const char *ldso_path;
    const char *libc_so1, *libc_so2;
};

static enum CsbeErr get_libc_info(const struct ArchInfo *arch, struct LibcInfo *out)
{
    struct LibcInfoEntry {
        enum CsbeCpu cpu;
        enum CsbeOsType os;
        const char ldso_path[30];
        const char libc_so[25];
        unsigned ldso_dtneeded_stroffs;
    };
    static const struct LibcInfoEntry libc_infos[] = {
        { CSBEC_AARCH64,  CSBEOS_LINUX_GLIBC, "/lib/ld-linux-aarch64.so.1", "libc.so.6", 5/* ld-linux-aarch64.so.1 */ },
        { CSBEC_AARCH64,  CSBEOS_LINUX_MUSL,  "/lib/ld-musl-aarch64.so.1",  "libc.musl-aarch64.so.1" , 0 },
        { CSBEC_I386,     CSBEOS_LINUX_GLIBC, "/lib/ld-linux.so.2",         "libc.so.6" , 0 },
        { CSBEC_I386,     CSBEOS_LINUX_MUSL,  "/lib/ld-musl-i386.so.1",     "libc.musl-x86.so.1", 0 },
        { CSBEC_X86_64,   CSBEOS_LINUX_GLIBC, "/lib64/ld-linux-x86-64.so.2", "libc.so.6", 0 },
        { CSBEC_X86_64,   CSBEOS_LINUX_MUSL,  "/lib/ld-musl-x86_64.so.1",   "libc.musl-x86-64.so.1" , 0 }
    };
    const struct LibcInfoEntry *info;
    unsigned remaining;

    if (arch->outformat != CSBEOF_ELF_EXE) {
        /* TODO add an option to force-depend on libc? */
        return CSBEE_OK;
    }

    remaining = sizeof(libc_infos)/sizeof(struct LibcInfoEntry);
    for (info = libc_infos; remaining--; info++) {
        if (info->cpu == arch->cpu && info->os == arch->os) {
            out->ldso_path = info->ldso_path;
            out->libc_so1 = info->libc_so;
            out->libc_so2 = info->ldso_dtneeded_stroffs ?
                    info->ldso_path + info->ldso_dtneeded_stroffs : NULL;
            return CSBEE_OK;
        }
    }
    return CSBEE_ARCH_NOT_SUPPORTED;
}

static enum CsbeErr add_libc_init_sym(struct CgCtx *cg,
                                      const struct LibcInfo *libc)
{
    /* TODO check how libc init works on BSDs */
    static const char libc_init_func[] = "__libc_start_main";
    struct SymInfo *sym = new_sym(cg);
    (void)libc;
    if (!sym) return CSBEE_OUT_OF_MEM;
    sym->funcdef = (void*)-1; /* must be != NULL for functions */
    sym->datadef = NULL;
    sym->mode = SYMMODE_IMPORT;
    sym->name = libc_init_func;
    sym->namelen = sizeof(libc_init_func)-1;
    cg->import_sym_count++;
    cg->import_func_count++;
    cg->libc_init = sym;
    return CSBEE_OK;
}

struct GnuHashBucket {
    unsigned count;
    unsigned first_index, next_index;
    struct SymInfo *chain_last;
};

struct GnuHash {
    unsigned symoffset;
    unsigned nbuckets, bloom_size, bloom_shift;

    struct GnuHashBucket *buckets;
    uint64 *bloomflt;
    struct SymInfo **symtab;
};

struct ElfInfo {
    uint32 phoff;           /**< File offset to program header */
    unsigned phsize;        /**< Size of program header */
    uint32 hdr_end_addr;    /**< End of "header sections",
                                i.e. before .text/.plt.got */
    unsigned rel_entsize;   /**< Size of a relocation entry */
    uint32 reltype;         /**< SHT_REL / SHT_RELA */
    size_t dynstr_soname;   /**< Offset in .dynstr to SONAME */
    size_t dynstr_libc_so1;
    size_t dynstr_libc_so2;
    struct Section interp, dynstr, dynsym, gnu_hash,
                   rel_dyn, plt_got;
    struct LibcInfo libc;
    struct GnuHash gnuhash_info;
};

static void write_strtab(struct BinWriter *bw, const struct Section *strtab,
                         const char *name, size_t namelen, size_t *offs_out);
static void write_sym(struct BinWriter *bw, size_t strtab_offs,
                      size_t offs, size_t size,
                      uint8 info, uint8 other, unsigned section);

static uint32 calc_gnuhash(const char *s0, size_t len)
{
    /*
      calc_gnuhash() is based on Musl libc code by Boris Brezillon, Rich Felker
      and Alexander Monakov, which in turn is based on a hash function by
      Dan Bernstein.
    */
    const unsigned char *s = (void *)s0;
    uint_fast32 h = 5381;
    for (; len--; s++) {
        h += h*32 + *s;
    }
    return h;
}

static void bloomflt_mark(struct GnuHash *o, const struct ArchInfo *arch,
                          uint32 h)
{
    /* bloomflt_mark() is based on the example code from Andrey Roenko here:
       https://flapenguin.me/elf-dt-gnu-hash  */
    unsigned wordbits = arch->wordbits;
    unsigned ind = (h / wordbits) % o->bloom_size;
    uint64 bits = ((uint64)1 << (h % wordbits)) |
                  ((uint64)1 << ((h >> o->bloom_shift) % wordbits));
    o->bloomflt[ind] |= bits;
}

static int prepare_gnuhash(struct CgCtx *cg, const struct ArchInfo *arch,
                           struct GnuHash *o)
{
    /* prepare_gnuhash() is based on info in this web page from Andrey Roenko:
       https://flapenguin.me/elf-dt-gnu-hash  */
    struct SymInfo *sym;
    struct GnuHashBucket *bucket;
    unsigned symidx, i;

    o->symoffset = 1 + cg->import_sym_count;
    /* TODO check which values are optimal */
    o->nbuckets = cg->export_sym_count;
    o->bloom_size = 2*cg->export_sym_count/arch->wordbits + 1;
    o->bloom_shift = 5;
    o->bloomflt = allocpa(cg->csbe, sizeof(uint64), o->bloom_size);
    if (!o->bloomflt) return 0;
    memset(o->bloomflt, 0, o->bloom_size*sizeof(uint64));

    if (!cg->export_sym_count) {
        assert(o->nbuckets == 0);
        o->buckets = NULL;
        o->symtab = NULL;
        return 1;
    }
    o->buckets = allocpa(cg->csbe, sizeof(struct GnuHashBucket), o->nbuckets);
    if (!o->buckets) return 0;
    o->symtab = allocpa(cg->csbe, sizeof(struct SymInfo *), cg->export_sym_count);
    if (!o->symtab) return 0;

    memset(o->buckets, 0, o->nbuckets*sizeof(struct GnuHashBucket));
    bucket = o->buckets;
    for (i = o->nbuckets; i--; ) {
        bucket->chain_last = NULL; /* in case NULL != 0 */
        bucket++;
    }
    /* Assign each symbol to a bucket */
    for (sym = cg->syminfos; sym; sym = sym->next) {
        uint32 h;
        if (sym->mode != SYMMODE_EXPORT) continue;
        h = calc_gnuhash(sym->name, sym->namelen);
        sym->gnuhash = h;
        sym->gnuhash_last_in_chain = 1;
        bucket = &o->buckets[h % o->nbuckets];
        if (bucket->chain_last) {
            bucket->chain_last->gnuhash_last_in_chain = 0;
        }
        bucket->chain_last = sym;
        bucket->count++;
        bloomflt_mark(o, arch, h);
    }
    /* Assign each bucket a starting index */
    symidx = 0;
    bucket = o->buckets;
    for (i = o->nbuckets; i--; ) {
        if (bucket->count) {
            bucket->first_index = bucket->next_index = symidx;
            symidx += bucket->count;
        }
        bucket++;
    }
    /* Sort/group the symbols by bucket + build chains */
    for (sym = cg->syminfos; sym; sym = sym->next) {
        if (sym->mode != SYMMODE_EXPORT) continue;
        bucket = &o->buckets[sym->gnuhash % o->nbuckets];
        o->symtab[bucket->next_index++] = sym;
    }
    return 1;
}

static void write_libname(struct BinWriter *bw, const struct Section *strtab,
                          const char *libname, size_t libname_len,
                          size_t *offs_out)
{
    typedef const unsigned char *UC;
    *offs_out = binwriter_get_position(bw) - strtab->start;
    outbytes(bw, (UC)"lib", 3);
    outbytes(bw, (UC)libname, libname_len);
    outbytes(bw, (UC)".so", 4 /* including \0 */);
}

static enum CsbeErr elf_header(struct Csbe *csbe, struct CgCtx *cg,
                               const struct ArchInfo *arch,
                               struct ElfInfo *elfinfo, struct BinWriter *bw)
{
    static const unsigned char magic[] = { 0x7F,'E','L','F' };
    unsigned elfclass, ehsize, phentsize, shentsize, phnum;
    unsigned align = arch->wordbits == 32 ? 4 : 8;
    uint64 phoff = 0;
    struct SymInfo *sym;

    (void)csbe;
    (void)cg;

    assert(arch->cpu != CSBEC_IR_DUMP);
    if (arch->wordbits == 32) {
        ehsize = EHSIZE_32;
        elfclass = ELFCLASS32;
        phentsize = PHENTSIZE_32;
        shentsize = SHENTSIZE_32;
    } else {
        ehsize = EHSIZE_64;
        elfclass = ELFCLASS64;
        phentsize = PHENTSIZE_64;
        shentsize = SHENTSIZE_64;
    }
    if (arch->elf_relocs_have_addends) {
        elfinfo->rel_entsize = arch->wordbits == 32 ? 12 : 24;
        elfinfo->reltype = SHT_RELA;
    } else {
        elfinfo->rel_entsize = arch->wordbits == 32 ? 8 : 16;
        elfinfo->reltype = SHT_REL;
    }
    switch (arch->outformat) {
    case CSBEOF_ELF_OBJ:
        phnum = 0;
        phentsize = 0;
        phoff = 0;
        break;
    case CSBEOF_ELF_DYNLIB:
        phnum = machine_info[arch->cpu].merge_hdr_text ?
                    5 : (6 + cg->rodata_present);
        phoff = ehsize;
        break;
    case CSBEOF_ELF_EXE:
        /* Like dynlib, but includes the PHDR and INTERP segments also */
        phnum = machine_info[arch->cpu].merge_hdr_text ?
                    7 : (8 + cg->rodata_present);
        phoff = ehsize;
        break;
    CASE_EXCEPT_ELF
    default:
        assert(0);
    }

    /* e_ident in File Header */
    {
        unsigned char info[12];

        outbytes(bw, magic, 4);
        info[0] = elfclass;
        info[1] = arch->big_endian ? ELFDATA2MSB : ELFDATA2LSB;
        info[2] = EV_CURRENT;
        info[3] = ELFOSABI_SYSV;
        memset(&info[4], 0, 8); /* EI_ABIVERSION and EI_PAD */
        outbytes(bw, info, 12);
    }
    /* Rest of File Header */
    out16(bw, get_elf_type(arch));
    out16(bw, machine_info[arch->cpu].elf_machine_id);
    out32(bw, EV_CURRENT); /* e_version */
    outaddr(bw, 0); /* entry point */
    outoff(bw, phoff); /* offs. to program header (phoff) */
    outoff(bw, 0); /* offs. to section header. Computed later */
    out32(bw, 0); /* e_flags */ /* XXX looks like rv64 has 0x5 here */
    out16(bw, ehsize); /* size of ELF header */
    out16(bw, phentsize); /* bytes per entry in program header */
    out16(bw, phnum); /* number of entries in progam header */
    out16(bw, shentsize); /* bytes per entry in section header */
    out16(bw, 0); /* number of entries in section header. Computed later */
    out16(bw, 0); /* index of .shstrtab in section header. Computed later */

    if (arch->outformat == CSBEOF_ELF_OBJ) {
        outalign(bw, machine_info[arch->cpu].code_alignment);
    } else {
        unsigned dynsym_entsize;
        unsigned i;
        uint64 *bloomp;
        struct GnuHashBucket *bucketp;
        struct GnuHash *gh;
        struct SymInfo **symptr;
        struct CsbeLibrary *lib;
        unsigned dynsym_id;

        /* Program Header is filled in in elf_footer() */
        elfinfo->phoff = phoff;
        elfinfo->phsize = phnum*phentsize;
        outnulls(bw, elfinfo->phsize);

        /* .interp */
        if (arch->outformat == CSBEOF_ELF_EXE) {
            section_start(&elfinfo->interp, bw, 1);
            outbytes(bw, (const unsigned char *)elfinfo->libc.ldso_path,
                     strlen(elfinfo->libc.ldso_path)+1);
            section_end(&elfinfo->interp, bw);
        }

        /* .gnu.hash */
        section_start(&elfinfo->gnu_hash, bw, align);
        gh = &elfinfo->gnuhash_info;
        ZCHK(prepare_gnuhash(cg, arch, gh));
        out32(bw, gh->nbuckets);
        out32(bw, gh->symoffset);
        out32(bw, gh->bloom_size);
        out32(bw, gh->bloom_shift);
        bloomp = gh->bloomflt;
        i = gh->bloom_size;
        if (arch->wordbits == 32) {
            while (i--) out32(bw, *(bloomp++));
        } else {
            while (i--) out64(bw, *(bloomp++));
        }
        bucketp = gh->buckets;
        for (i = gh->nbuckets; i--; bucketp++) {
            out32(bw, bucketp->count ?
                    1 + cg->import_sym_count + bucketp->first_index : 0);
        }
        symptr = gh->symtab;
        assert(symptr || !cg->export_sym_count);
        for (i = cg->export_sym_count; i--; ) {
            uint32 h;
            sym = *(symptr++);
            h = sym->gnuhash & ~0x1;
            h |= sym->gnuhash_last_in_chain;
            out32(bw, h);
        }
        section_end(&elfinfo->gnu_hash, bw);

        /* .dynsym - Filled in in elf_footer.
           But we still need to assign symbol numbers here (dynsym_id). */
        section_start(&elfinfo->dynsym, bw, align);
        dynsym_entsize = arch->wordbits == 32 ? 16 : 24;
        outnulls(bw, (1 + cg->import_sym_count + cg->export_sym_count) *
                     dynsym_entsize);
        dynsym_id = 1;
        for (sym = cg->syminfos; sym; sym = sym->next) {
            if (sym->mode != SYMMODE_IMPORT) continue;
            sym->dynsym_id = dynsym_id++;
        }
        symptr = elfinfo->gnuhash_info.symtab;
        assert(symptr || !cg->export_sym_count);
        for (i = cg->export_sym_count; i--; ) {
            sym = *(symptr++);
            if (sym->mode != SYMMODE_EXPORT) continue;
            sym->dynsym_id = dynsym_id++;
        }
        section_end(&elfinfo->dynsym, bw);

        /* .dynstr */
        section_start(&elfinfo->dynstr, bw, align);
        out8(bw, 0);
#if 0
        /* TODO skip SONAME for unstable libraries? or use the API hash as the suffix? */
        if (csbe->inputname) {
            /* use "lib<inputname>.so" as SONAME for now */
            /* TODO should there be .1 at the end or not? */
            write_libname(bw, &elfinfo->dynstr,
                          csbe->inputname, strlen(csbe->inputname),
                          &elfinfo->dynstr_soname);
        }
#endif
        if (arch->outformat == CSBEOF_ELF_EXE) {
            if (elfinfo->libc.libc_so1) {
                write_strtab(bw, &elfinfo->dynstr, elfinfo->libc.libc_so1,
                             strlen(elfinfo->libc.libc_so1),
                             &elfinfo->dynstr_libc_so1);
            }
            if (elfinfo->libc.libc_so2) {
                write_strtab(bw, &elfinfo->dynstr, elfinfo->libc.libc_so2,
                             strlen(elfinfo->libc.libc_so2),
                             &elfinfo->dynstr_libc_so2);
            }
        }
        for (lib = csbe->imported_libs; lib; lib = lib->next) {
            /* TODO allow SONAMEs of libs to be overridden */
            /* TODO skip unused libs */
            write_libname(bw, &elfinfo->dynstr,
                          lib->name, lib->namelen,
                          &lib->dynstr_offs);
        }
        for (sym = cg->syminfos; sym; sym = sym->next) {
            if (sym->mode != SYMMODE_IMPORT && sym->mode != SYMMODE_EXPORT) {
                continue;
            }
            write_strtab(bw, &elfinfo->dynstr, sym->name, sym->namelen, &sym->dynstr_offs);
        }
        section_end(&elfinfo->dynstr, bw);

        /* .rel.dyn / .rela.dyn - Filled in when .got is filled in */
        section_start(&elfinfo->rel_dyn, bw, align);
        outnulls(bw, cg->import_sym_count * elfinfo->rel_entsize);
        section_end(&elfinfo->rel_dyn, bw);

        /* Alignment padding before .plt.got and .text */
        elfinfo->hdr_end_addr = binwriter_get_position(bw);
        if (machine_info[arch->cpu].merge_hdr_text) {
            outalign(bw, machine_info[arch->cpu].code_alignment);
        } else {
            outalign(bw, machine_info[arch->cpu].page_alignment);
        }

        /* .plt.got - Filled in when .got is filled in */
        section_start(&elfinfo->plt_got, bw, align);
        outnulls(bw, cg->arch->cgen.get_elf_plt_size(cg) *
                     cg->import_func_count);
        section_end(&elfinfo->plt_got, bw);
    }
    return CSBEE_OK;
}


static void write_strtab(struct BinWriter *bw, const struct Section *strtab,
                         const char *name, size_t namelen, size_t *offs_out)
{
    size_t fileoffs;
    if (name) {
        out_nullstring(bw, &fileoffs, name, namelen);
        *offs_out = fileoffs - strtab->start;
    }
}

static void write_sym(struct BinWriter *bw, size_t strtab_offs,
                      size_t offs, size_t size,
                      uint8 info, uint8 other, unsigned section)
{
    assert(strtab_offs != 0 || info == 0);
    if (bw->addr_size == 32) {
        out32(bw, strtab_offs);
        outaddr(bw, offs);
        out32(bw, size);
        out8(bw, info);
        out8(bw, other);
        out16(bw, section);
    } else {
        out32(bw, strtab_offs);
        out8(bw, info);
        out8(bw, other);
        out16(bw, section);
        outaddr(bw, offs);
        out64(bw, size);
    }
}

static void write_reloc(struct CgCtx *cg, struct BinWriter *bw,
                        struct CgRelocEntry *re, unsigned sym_id)
{
    unsigned type = cg->arch->elf_reloc_types[re->kind];

    outaddr(bw, re->location);
    if (bw->addr_size == 32) {
        out32(bw, sym_id<<8 | type);
    } else {
        out64(bw, (uint64)sym_id<<32 | type);
    }
    if (cg->arch->elf_relocs_have_addends) {
        outaddr(bw, (int64)re->addend);
    }
}

static void write_dynreloc(struct CgCtx *cg, struct BinWriter *bw,
                           uint32 vaddr, struct SymInfo *syminfo)
{
    unsigned dynsym_id = syminfo->dynsym_id;
    unsigned type = machine_info[cg->arch->cpu].got_rel_type;

    outaddr(bw, vaddr);
    if (bw->addr_size == 32) {
        out32(bw, dynsym_id<<8 | type);
    } else {
        out64(bw, (uint64)dynsym_id<<32 | type);
    }
    if (cg->arch->elf_relocs_have_addends) {
        outaddr(bw, 0);
    }
}

static void write_dyn(struct BinWriter *bw, uint32 tag, uint32 value)
{
    if (bw->addr_size == 32) {
        out32(bw, tag);
        out32(bw, value);
    } else {
        out64(bw, tag);
        out64(bw, value);
    }
}

#define WRITE_SHSTR(sectionvar, name) \
    write_shstr(bw, &sectionvar, &shstr.sectionvar, (name), sizeof(name), \
                &shstrtab)
static void write_shstr(struct BinWriter *bw, const struct Section *section,
                        size_t *offs_out, const char *name, size_t namelen,
                        struct Section *shstrtab)
{
    if (section->present) {
        assert(*offs_out == 0);
        write_strtab(bw, shstrtab, name, namelen, offs_out);
    }
}

#define NO_LINK NULL
#define NO_INFO 0
/** The section does not contain a table of entries, so the
    entry size field is not applicable. */
#define NO_ENTSZ 0

/** Writes an entry in the Section Header */
#define WRITE_SH(varname, type, flags, virtaddr, link, info,  align, entsize) \
    do { \
        if ((varname).present) { \
            write_sh(bw, arch, &(varname), shstr.varname, (type), (flags), \
                    (virtaddr), (link), (info), (align), (entsize)); \
        } \
    } while (0)
static void write_sh(struct BinWriter *bw, const struct ArchInfo *arch,
                     const struct Section *section, size_t strtab_offs,
                     uint32 type, uint32 flags, uint32 virtaddr,
                     const struct Section *linked, uint32 info,
                     uint32 align, size_t entsize)
{
    if (!section->present) return;
    out32(bw, strtab_offs);
    out32(bw, type);
    if (arch->wordbits == 32) {
        out32(bw, flags);
        out32(bw, virtaddr);
        out32(bw, section->start);
        out32(bw, section->len);
        out32(bw, linked ? linked->number : 0);
        out32(bw, info);
        out32(bw, align);
        out32(bw, entsize);
    } else {
        out64(bw, flags);
        out64(bw, virtaddr);
        out64(bw, section->start);
        out64(bw, section->len);
        out32(bw, linked ? linked->number : 0);
        out32(bw, info);
        out64(bw, align);
        out64(bw, entsize);
    }
}

static void write_ph(struct BinWriter *bw, uint32 type,
                     uint32 offs, uint32 vaddr, uint32 paddr,
                     uint32 filesz, uint32 memsz,
                     uint32 flags, uint32 align)
{
    if (bw->addr_size == 32) {
        out32(bw, type);
        outoff(bw, offs);
        outaddr(bw, vaddr);
        outaddr(bw, paddr);
        out32(bw, filesz);
        out32(bw, memsz);
        out32(bw, flags);
        out32(bw, align);
    } else {
        out32(bw, type);
        out32(bw, flags);
        outoff(bw, offs);
        outaddr(bw, vaddr);
        outaddr(bw, paddr);
        out64(bw, filesz);
        out64(bw, memsz);
        out64(bw, align);
    }
}

static enum CsbeErr elf_footer(struct Csbe *csbe, struct CgCtx *cg,
                               const struct ArchInfo *arch,
                               struct ElfInfo *elfinfo, struct BinWriter *bw,
                               struct Section *text)
{
    static const struct Section blank_sh = { 1, 0, 0, 0 };
    struct {
        /* Section header names */
        size_t text, plt_got,
               rel_text, rel_dyn,
               data, bss, rodata,
               interp, dynstr, dynsym, gnu_hash,
               dynamic, got,
               strtab, symtab,
               note_stack,
               shstrtab;
    } shstr;
    size_t symstr_file=0, symstr_text=0, symstr_rodata=0, symstr_start=0,
           symstr_dynamic=0, symstr_got=0, symstr_textend=0;
    struct Section plt_got, rel_text = ABSENTSCN, rel_dyn,
                   data = ABSENTSCN, bss = ABSENTSCN, rodata = ABSENTSCN,
                   interp, dynstr, dynsym, gnu_hash,
                   dynamic = ABSENTSCN, got = ABSENTSCN,
                   strtab = ABSENTSCN, symtab = ABSENTSCN,
                   note_stack = ABSENTSCN,
                   shstrtab = ABSENTSCN;
    unsigned align = arch->wordbits == 32 ? 4 : 8;
    unsigned palign = machine_info[arch->cpu].page_alignment;
    unsigned rel_entsize, got_entsize, pltgot_entsize = 0;
    unsigned reltype; /* SHT_REL / SHT_RELA */
    unsigned highest_local_sym, got_sym_id = 0;
    uint32 pltgotvaddr; /**< Virtual address of .plt.got */
    uint32 textvaddr;   /**< Virtual address of .text section */
    uint32 rodatavaddr = 2*palign; /**< V-addr of .rodata section */
    uint32 datavaddr = 3*palign; /**< V-addr of .data section */
    uint32 dynbase = 4*palign; /**< Base v-addr of dynamic segment */
    size_t shstart, fileend;
    struct SymInfo *sym;

    memset(&shstr, 0, sizeof(shstr));
    interp = elfinfo->interp;
    dynstr = elfinfo->dynstr;
    dynsym = elfinfo->dynsym;
    gnu_hash = elfinfo->gnu_hash;
    rel_dyn = elfinfo->rel_dyn;
    plt_got = elfinfo->plt_got;

    rel_entsize = elfinfo->rel_entsize;
    reltype = elfinfo->reltype;
    if (arch->outformat == CSBEOF_ELF_OBJ) {
        pltgotvaddr = 0;
        textvaddr = 0;
    } else {
        pltgotvaddr = plt_got.start;
        textvaddr = text->start;
    }

    /* End of text segment */
    section_end(text, bw);
    outalign(bw, align);

    /* Assign symbol IDs early (needed for relocations) */
    {
        unsigned sym_id = 1;
        if (csbe->inputname) sym_id++;
        if (text->present) sym_id++;
        if (rodata.present) sym_id++;
        if (arch->outformat == CSBEOF_ELF_EXE) sym_id++; /* _start */
        if (arch->outformat != CSBEOF_ELF_OBJ) {
            sym_id++; /* _DYNAMIC */
            if (cg->import_sym_count) { /* _GLOBAL_OFFSET_TABLE_ */
                got_sym_id = sym_id++;
            }
            if (text->present) sym_id++; /* _text_end */
        }
        for (sym = cg->syminfos; sym; sym = sym->next) {
            if (sym->mode != SYMMODE_LOCAL || !sym->name) continue;
            sym->sym_id = sym_id++;
        }
        for (sym = cg->syminfos; sym; sym = sym->next) {
            if (sym->mode == SYMMODE_LOCAL) continue;
            sym->sym_id = sym_id++;
        }
    }

    if (arch->outformat == CSBEOF_ELF_OBJ) {
        struct CgRelocEntry *re;
        /* .rel.text / .rela.text */
        section_start(&rel_text, bw, align);
        for (re = cg->relocs; re; re = re->next) {
            if (CG_IS_CODE_RELOC(re)) {
                sym = cg->sym_by_func_id[re->target_id];
                write_reloc(cg, bw, re, sym->sym_id);
            } else if (re->kind == CG_RK_GOTPC) {
                write_reloc(cg, bw, re, got_sym_id);
            }
        }
        section_end(&rel_text, bw);
    }

    /* .rodata */
    if (machine_info[arch->cpu].merge_hdr_text) {
        outalign(bw, align);
        rodatavaddr = binwriter_get_position(bw);
    } else {
        outalign(bw, palign);
    }
    section_start(&rodata, bw, align);
    out_datasection(cg, bw);
    /* TODO any ADDRGLOBAL/LOADGLOBAL that reference non-external
            items will require relocation. *GLOBAL ir-ops should
            add in-memory/ephemeral relocation entries ("addrslots"),
            which will should be processed here.

            The symbols already have the intra-section offset,
            so the relocated addresses should be something like:
              pcrel = (rodatavaddr + sym->offset) - (textvaddr + pc_offs)
    */
    section_end(&rodata, bw);

    if (arch->outformat != CSBEOF_ELF_OBJ) {
        unsigned dt_reltype;
        size_t dt_pltgot_offset = 0;
        struct CsbeLibrary *lib;

        /* .dynamic */
        section_start(&dynamic, bw, arch->wordbits == 32 ? 8 : 16);
        if (arch->outformat == CSBEOF_ELF_EXE) {
            if (elfinfo->libc.libc_so1) {
                write_dyn(bw, DT_NEEDED, elfinfo->dynstr_libc_so1);
            }
            if (elfinfo->libc.libc_so2) {
                write_dyn(bw, DT_NEEDED, elfinfo->dynstr_libc_so2);
            }
        }
        for (lib = csbe->imported_libs; lib; lib = lib->next) {
            /* TODO allow SONAMEs of libs to be overridden */
            /* TODO skip unused libs */
            write_dyn(bw, DT_NEEDED, lib->dynstr_offs);
        }
        if (elfinfo->dynstr_soname) {
            /* TODO allow SONAME to be overridden */
            write_dyn(bw, DT_SONAME, elfinfo->dynstr_soname);
        }
        write_dyn(bw, DT_GNU_HASH, gnu_hash.start); /* vaddr == file addr */
        write_dyn(bw, DT_STRTAB, dynstr.start);
        write_dyn(bw, DT_SYMTAB, dynsym.start);
        write_dyn(bw, DT_STRSZ, dynstr.len);
        write_dyn(bw, DT_SYMENT, arch->wordbits == 32 ? 16 : 24);
        if (got_sym_id) {
            dt_pltgot_offset = binwriter_get_position(bw);
            write_dyn(bw, DT_PLTGOT, 0 /* Filled in later */);
        }
        dt_reltype = arch->elf_relocs_have_addends ? DT_RELA : DT_REL;
        if (rel_dyn.present) {
            write_dyn(bw, dt_reltype/* DT_RELA */, rel_dyn.start);
            write_dyn(bw, dt_reltype+1/* DT_RELASZ */, rel_dyn.len);
            write_dyn(bw, dt_reltype+2/* DT_RELAENT */, rel_entsize);
        }
        write_dyn(bw, DT_FLAGS, DF_BIND_NOW);
        write_dyn(bw, DT_FLAGS_1, DF_1_NOW | (
                arch->outformat == CSBEOF_ELF_EXE ? DF_1_PIE : 0));
        write_dyn(bw, (arch->elf_relocs_have_addends ?
                       DT_RELACOUNT : DT_RELCOUNT), 0 /* TODO */);
        write_dyn(bw, DT_NULL, 0);
        section_end(&dynamic, bw);

        /* .got */
        section_start(&got, bw, align);
        cg->got_vaddr = dynbase+got.start;
        for (sym = cg->syminfos; sym; sym = sym->next) {
            if (sym->mode != SYMMODE_IMPORT) continue;
            sym->got_offs = binwriter_get_position(bw) - got.start;
            outaddr(bw, 0);
        }
        section_end(&got, bw);
        if (got.present) {
            size_t prevpos = binwriter_get_position(bw);
            uint32 pc;
            struct CgCode c_text;
            struct CgRelocEntry *re;

            /* Set reference from .dynamic */
            binwriter_seekto(bw, dt_pltgot_offset);
            write_dyn(bw, DT_PLTGOT, dynbase + got.start);
            /* Fill in .rel.dyn */
            binwriter_seekto(bw, elfinfo->rel_dyn.start);
            for (sym = cg->syminfos; sym; sym = sym->next) {
                if (sym->mode != SYMMODE_IMPORT) continue;
                write_dynreloc(cg, bw,
                               dynbase + got.start + sym->got_offs,
                               sym);
            }
            /* Fill in .plt.got */
            binwriter_seekto(bw, elfinfo->plt_got.start);
            c_text = cg->code;
            cgcode_reset(&cg->code); /* contents will go here */
            pltgot_entsize = cg->arch->cgen.get_elf_plt_size(cg);
            pc = pltgotvaddr;
            for (sym = cg->syminfos; sym; sym = sym->next) {
                if (sym->mode != SYMMODE_IMPORT) continue;
                /* TODO Functions only! */
                sym->plt_vaddr = pc;
                cg->arch->cgen.gen_elf_plt(cg, dynbase + got.start,
                                           sym->got_offs, pc);
                pc += pltgot_entsize;
            }
            cg_lastchunk(cg);
            out_machinecode(bw, &cg->code);
            cg->code = c_text;
            /* Resolve all temporary relocations
               (i.e. relocations that will not appear in the final binary) */
            for (re = cg->relocs; re; re = re->next) {
                uint32 newaddr;
                if (CG_IS_CODE_RELOC(re)) {
                    sym = re->target_id != FUNCID_LIBC_INIT ?
                            cg->sym_by_func_id[re->target_id] : cg->libc_init;
                    if (sym->mode == SYMMODE_IMPORT) {
                        /* Function call via .plt.got */
                        newaddr = sym->plt_vaddr;
                    } else {
                        /* Internal function call */
                        newaddr = textvaddr + sym->offset;
                    }
                } else if (CG_IS_DATA_RELOC(re)) {
                    assert(re->target_id < csbe->max_datadef_count);
                    sym = cg->sym_by_data_id[re->target_id];
                    if (sym->mode == SYMMODE_IMPORT) {
                        /* Address of external data item */
                        /* XXX untested */
                        newaddr = dynbase + got.start + sym->got_offs;
                    } else {
                        /* Address of internal data item */
                        newaddr = rodatavaddr + sym->offset;
                    }
                } else if (re->kind == CG_RK_GOTPC) {
                    /* i386 only: Offset between PC and GOT.
                       Used to initialize ebx as a pointer to GOT. */
                    newaddr = dynbase + got.start;
                } else assert(0);
                ZCHK(cg->arch->cgen.process_reloc(cg, bw, re,
                                                  text->start + re->location,
                                                  textvaddr + re->location,
                                                  newaddr));
            }
            binwriter_seekto(bw, prevpos);
        } else {
            assert(!plt_got.present);
        }
        /* .got.plt seems to be used with lazy binding only.
           So it is not needed. */
    }
    assert(got.present == !!got_sym_id);
    assert(!!got.present == !!plt_got.present);

    /* .data */
    section_start(&data, bw, align);
    /* TODO */
    /* XXX once this is added, then the relocation loop above will have
           to be moved down, to after the .data section processing here. */
    section_end(&data, bw);

    /* .bss */
    section_start(&bss, bw, align);
    section_end(&bss, bw);

    /* .strtab */
    section_start(&strtab, bw, align);
    out8(bw, 0);
    if (csbe->inputname) {
        write_strtab(bw, &strtab, csbe->inputname, strlen(csbe->inputname),
                     &symstr_file);
    }
    if (text->present) {
        write_strtab(bw, &strtab, ".text", sizeof(".text")-1,
                     &symstr_text);
    }
    if (rodata.present) {
        write_strtab(bw, &strtab, ".rodata", sizeof(".rodata")-1,
                     &symstr_rodata);
    }
    if (arch->outformat == CSBEOF_ELF_EXE) {
        write_strtab(bw, &strtab, "_start", sizeof("_start")-1,
                     &symstr_start);
    }
    if (arch->outformat != CSBEOF_ELF_OBJ) {
        write_strtab(bw, &strtab, "_DYNAMIC",
                     sizeof("_DYNAMIC")-1, &symstr_dynamic);
        if (got_sym_id) {
            write_strtab(bw, &strtab, "_GLOBAL_OFFSET_TABLE_",
                         sizeof("_GLOBAL_OFFSET_TABLE_")-1, &symstr_got);
        }
        if (text->present) {
            write_strtab(bw, &strtab, "_text_end",
                         sizeof("_text_end")-1, &symstr_textend);
        }
    }
    for (sym = cg->syminfos; sym; sym = sym->next) {
        if (sym->name == NULL) {
            assert(sym->mode == SYMMODE_LOCAL);
            continue;
        }
        assert(sym->name[0] != '\0');
        write_strtab(bw, &strtab, sym->name, sym->namelen, &sym->strtab_offs);
    }
    section_end(&strtab, bw);

    /* .symtab */
    section_start(&symtab, bw, align);
    write_sym(bw, 0, 0, 0, 0, 0, 0);
    highest_local_sym = 0;
    if (csbe->inputname) {
        write_sym(bw, symstr_file, 0, 0,
                  ST_I_FILE|ST_I_LOCAL, ST_O_DEFAULT, SHN_ABS);
        highest_local_sym++;
    }
    if (text->present) {
        write_sym(bw, symstr_text, textvaddr, 0,
                  ST_I_SECTION|ST_I_LOCAL, ST_O_DEFAULT, text->number);
        highest_local_sym++;
    }
    if (rodata.present) {
        write_sym(bw, symstr_rodata, rodatavaddr, 0,
                  ST_I_SECTION|ST_I_LOCAL, ST_O_DEFAULT, rodata.number);
        highest_local_sym++;
    }
    if (arch->outformat == CSBEOF_ELF_EXE) {
        write_sym(bw, symstr_start, textvaddr, cg->startcode_len,
                  ST_I_FUNC|ST_I_LOCAL, ST_O_HIDDEN, text->number);
        highest_local_sym++;
    }
    if (arch->outformat != CSBEOF_ELF_OBJ) {
        write_sym(bw, symstr_dynamic, dynbase+dynamic.start, dynamic.len,
                  ST_I_OBJECT|ST_I_LOCAL, ST_O_HIDDEN, dynamic.number);
        highest_local_sym++;
        if (got_sym_id) {
            write_sym(bw, symstr_got, dynbase+got.start, got.len,
                      ST_I_OBJECT|ST_I_LOCAL, ST_O_HIDDEN, got.number);
            highest_local_sym++;
        }
        if (text->present) {
            write_sym(bw, symstr_textend, textvaddr+text->len, 0,
                      ST_I_NOTYPE|ST_I_LOCAL, ST_O_HIDDEN, text->number);
            highest_local_sym++;
        }
    }
    for (sym = cg->syminfos; sym; sym = sym->next) {
        uint32 info, other, base;
        unsigned section;
        if (!sym->name) continue;
        if (sym->mode != SYMMODE_LOCAL) continue;
        other = ST_O_HIDDEN;
        if (sym->funcdef) {
            info = ST_I_FUNC|ST_I_LOCAL;
            section = text->number;
            base = textvaddr;
        } else {
            info = ST_I_OBJECT|ST_I_LOCAL;
            section = rodata.number;
            base = rodatavaddr;
        }
        write_sym(bw, sym->strtab_offs, base+sym->offset, sym->size,
                  info, other, section);
        highest_local_sym++;
    }
    for (sym = cg->syminfos; sym; sym = sym->next) {
        uint32 info, other, base;
        unsigned section;

        if (!sym->name) continue;
        if (sym->funcdef) {
            info = ST_I_FUNC;
            section = text->number;
            base = textvaddr;
        } else {
            info = ST_I_OBJECT;
            section = rodata.number;
            base = rodatavaddr;
        }
        switch (sym->mode) {
        case SYMMODE_LOCAL:
            continue;
        case SYMMODE_OBJGLOBAL:
            info |= ST_I_GLOBAL;
            other = ST_O_HIDDEN;
            break;
        case SYMMODE_IMPORT:
            info |= ST_I_GLOBAL;
            other = ST_O_DEFAULT;
            section = 0;
            break;
        case SYMMODE_EXPORT:
            info |= ST_I_GLOBAL;
            /* It's tempting to use PROTECTED visibility here, as a way to
               indicate that symbol interposition is not supported by
               libraries compiled by CSBE.

               But apparently that can cause dynamic loaders to do inefficient
               scans for duplicate symbols, in order to allow the equality
               operation to work correctly on function pointers.

               Source:
               - https://www.airs.com/blog/archives/307
               - http://www.akkadia.org/drepper/dsohowto.pdf (page 20)
             */
            other = ST_O_DEFAULT;
            break;
        default:
            assert(0);
        }
        write_sym(bw, sym->strtab_offs, base+sym->offset, sym->size,
                  info, other, section);
    }
    section_end(&symtab, bw);

    /* .note.GNU-stack (marks stack as non-executable) */
    if (arch->outformat != CSBEOF_ELF_DYNLIB
            && arch->outformat != CSBEOF_ELF_EXE) {
        section_start(&note_stack, bw, 1);
        note_stack.present = 1;
        section_end(&note_stack, bw);
    }

    /* .shstrtab. The write_shstr function requires that this comes last */
    section_start(&shstrtab, bw, align);
    out8(bw, 0);
    WRITE_SHSTR(interp, ".interp");
    WRITE_SHSTR(gnu_hash, ".gnu.hash");
    WRITE_SHSTR(dynsym, ".dynsym");
    WRITE_SHSTR(dynstr, ".dynstr");
    WRITE_SHSTR(plt_got, ".plt.got");
    write_shstr(bw, text, &shstr.text, ".text", sizeof(".text"), &shstrtab);
    if (arch->elf_relocs_have_addends) {
        WRITE_SHSTR(rel_text, ".rela.text");
        WRITE_SHSTR(rel_dyn, ".rela.dyn");
    } else {
        WRITE_SHSTR(rel_text, ".rel.text");
        WRITE_SHSTR(rel_dyn, ".rel.dyn");
    }
    WRITE_SHSTR(data, ".data");
    WRITE_SHSTR(bss, ".bss");
    WRITE_SHSTR(rodata, ".rodata");
    WRITE_SHSTR(dynamic, ".dynamic");
    WRITE_SHSTR(got, ".got");
    WRITE_SHSTR(strtab, ".strtab");
    WRITE_SHSTR(symtab, ".symtab");
    WRITE_SHSTR(note_stack, ".note.GNU-stack");
    shstrtab.present = 1;
    WRITE_SHSTR(shstrtab, ".shstrtab");
    section_end(&shstrtab, bw);

    /* Section Header */
    outalign(bw, align);
    shstart = binwriter_get_position(bw);
    write_sh(bw, arch, &blank_sh, 0, SHT_NULL, 0, 0x0, NULL, 0, 0, 0);
    WRITE_SH(interp, SHT_PROGBITS, SHF_ALLOC, interp.start, NO_LINK,
             NO_INFO, 1, 0);
    WRITE_SH(gnu_hash, SHT_GNU_HASH, SHF_ALLOC, gnu_hash.start, &dynsym,
             NO_INFO, align, arch->wordbits == 32 ? 4 : 0/* zero on 64b */);
    WRITE_SH(dynsym, SHT_DYNSYM, SHF_ALLOC, dynsym.start, &dynstr,
             1/* num local symbols */, align, arch->wordbits == 32 ? 16 : 24);
    WRITE_SH(dynstr, SHT_STRTAB, SHF_ALLOC, dynstr.start, NO_LINK,
             NO_INFO, 1, NO_ENTSZ);
    WRITE_SH(rel_dyn, reltype, SHF_ALLOC, rel_dyn.start, &dynsym,
             NO_INFO, align, rel_entsize);
    WRITE_SH(plt_got, SHT_PROGBITS, SHF_ALLOC|SHF_EXECINSTR, pltgotvaddr, NO_LINK,
             NO_INFO, machine_info[arch->cpu].pltgot_alignment, pltgot_entsize);
    if (text->present) {
        write_sh(bw, arch, text, shstr.text, SHT_PROGBITS,
                 SHF_ALLOC|SHF_EXECINSTR, textvaddr, NO_LINK,
                 NO_INFO, machine_info[arch->cpu].code_alignment, NO_ENTSZ);
    }
    WRITE_SH(rel_text, reltype, SHF_INFO_LINK, 0x0, &symtab,
             text->number, align, rel_entsize);
    WRITE_SH(rodata, SHT_PROGBITS, SHF_ALLOC,
             rodatavaddr, NO_LINK,
             NO_INFO, align/* TODO should be maximum alignment needed by any variable */, NO_ENTSZ);
    WRITE_SH(dynamic, SHT_DYNAMIC, SHF_ALLOC|SHF_WRITE,
             dynbase + dynamic.start, &dynstr,
             NO_INFO, align, arch->wordbits == 32 ? 8 : 16);
    got_entsize = arch->wordbits == 32 ? 4 : 8;
    WRITE_SH(got, SHT_PROGBITS, SHF_ALLOC|SHF_WRITE,
             dynbase + got.start, NO_LINK, NO_INFO, align, got_entsize);
    WRITE_SH(data, SHT_PROGBITS, SHF_ALLOC|SHF_WRITE,
             datavaddr + data.start, NO_LINK,
             NO_INFO, align/* TODO should be maximum alignment needed by any variable */, NO_ENTSZ);
    WRITE_SH(bss, SHT_NOBITS, SHF_ALLOC|SHF_WRITE,
             datavaddr + bss.start, NO_LINK,
             NO_INFO, align, NO_ENTSZ);
    WRITE_SH(strtab, SHT_STRTAB, NO_FLAGS,0x0,NO_LINK,NO_INFO, 1, NO_ENTSZ);
    WRITE_SH(symtab, SHT_SYMTAB, NO_FLAGS,0x0, &strtab,
             highest_local_sym+1, align, arch->wordbits == 32 ? 16 : 24);
    WRITE_SH(note_stack, SHT_NOTE, NO_FLAGS,0x0,NO_LINK,NO_INFO, 1, NO_ENTSZ);
    WRITE_SH(shstrtab, SHT_STRTAB, NO_FLAGS,0x0,NO_LINK,NO_INFO, 1, NO_ENTSZ);
    fileend = binwriter_get_position(bw);

    /* Fill in .dynsym */
    {
        struct SymInfo **symptr;
        int i;

        binwriter_seekto(bw, elfinfo->dynsym.start);
        write_sym(bw, 0, 0, 0, 0, 0, 0);
        for (sym = cg->syminfos; sym; sym = sym->next) {
            uint32 info;
            if (sym->mode != SYMMODE_IMPORT) continue;

            /* Imported symbol */
            if (sym->funcdef) {
                info = ST_I_FUNC|ST_I_GLOBAL;
            } else {
                info = ST_I_OBJECT|ST_I_GLOBAL;
            }
            write_sym(bw, sym->dynstr_offs, 0, 0, info, ST_O_DEFAULT, 0);
        }
        symptr = elfinfo->gnuhash_info.symtab;
        assert(symptr || !cg->export_sym_count);
        for (i = cg->export_sym_count; i--; ) {
            uint32 info, base;
            unsigned section;

            sym = *(symptr++);
            if (sym->mode != SYMMODE_EXPORT) continue;

            /* Exported symbol */
            if (sym->funcdef) {
                info = ST_I_FUNC|ST_I_GLOBAL;
                section = text->number;
                base = textvaddr;
            } else {
                info = ST_I_OBJECT|ST_I_GLOBAL;
                section = rodata.number;
                base = rodatavaddr;
            }
            write_sym(bw, sym->dynstr_offs, base+sym->offset, sym->size,
                      info, ST_O_DEFAULT, section);
        }
    }

    /* Update fields in File Header */
    binwriter_seekto(bw, 24);
    outaddr(bw, textvaddr); /* entry point */
    binwriter_skip(bw, bw->off_size/8);
    outaddr(bw, shstart); /* sh offset */
    binwriter_skip(bw, 4+2+2+2+2);
    out16(bw, bw->section_number); /* sh num */
    out16(bw, shstrtab.number); /* index of string table (for section names) */

    /* Fill in the Program Header */
    if (arch->outformat != CSBEOF_ELF_OBJ) {
        uint32 dyn_addr = dynbase + dynamic.start;
        uint32 dyn_len = got.present ?
                got.start - dynamic.start + got.len :
                dynamic.len;

        binwriter_seekto(bw, elfinfo->phoff);
        if (arch->outformat == CSBEOF_ELF_EXE) {
            uint32 phoff = elfinfo->phoff;
            write_ph(bw, PT_PHDR, phoff, phoff, phoff,
                 elfinfo->phsize, elfinfo->phsize, PF_R, align);
            write_ph(bw, PT_INTERP, interp.start, interp.start, interp.start,
                 interp.len, interp.len, PF_R, 1);
        }
        if (machine_info[arch->cpu].merge_hdr_text) {
            /* Code segment includes the file header (and .rodata) */
            uint32 segm_len;
            const struct Section *last = (rodata.present ? &rodata : text);
            segm_len = UINT_ALIGN(last->start + last->len, align);
            write_ph(bw, PT_LOAD, 0x0, 0x0, 0x0, segm_len, segm_len,
                     text->present ? PF_R|PF_X : PF_R, palign);
        } else {
            uint32 hdr_end = elfinfo->hdr_end_addr;
            uint32 code_start = plt_got.start;
            uint32 text_len = UINT_ALIGN(
                    text->start - plt_got.start + text->len, align);
            write_ph(bw, PT_LOAD, 0x0, 0x0, 0x0,
                     hdr_end, hdr_end, PF_R, palign);
            if (text->present) {
                /* TODO map text as non-readable on platforms that support it? */
                write_ph(bw, PT_LOAD, code_start, code_start, code_start,
                         text_len, text_len, PF_R|PF_X, palign);
            }
            if (rodata.present) {
                write_ph(bw, PT_LOAD, rodata.start, rodatavaddr, rodatavaddr,
                         rodata.len, rodata.len, PF_R, palign);
            }
        }

        write_ph(bw, PT_LOAD, dynamic.start, dyn_addr, dyn_addr,
                 dyn_len, dyn_len, PF_R|PF_W, palign);
        write_ph(bw, PT_DYNAMIC, dynamic.start, dyn_addr, dyn_addr,
                 dynamic.len, dynamic.len, PF_R|PF_W, align);
        write_ph(bw, PT_GNU_RELRO, dynamic.start, dyn_addr, dyn_addr,
                 dyn_len, dyn_len, PF_R, 1);
        write_ph(bw, PT_GNU_STACK, 0,0,0, 0,0, PF_R|PF_W, 16);
    }
    binwriter_seekto(bw, fileend);
    return CSBEE_OK;
}

enum CsbeErr elf_output(struct Csbe *csbe, struct CgCtx *cg,
                        const struct ArchInfo *arch, struct BinWriter *bw)
{
    struct ElfInfo elfinfo = {
        0, 0, 0, 0, 0, 0, 0, 0,
        ABSENTSCN, /* .interp */
        ABSENTSCN, ABSENTSCN, ABSENTSCN, /* .dynstr, .dynsym, .gnu.hash */
        ABSENTSCN, ABSENTSCN, /* .rel.dyn, .plt.got */
        { NULL, NULL, NULL }, /* libc info */
        { 0, 0, 0, 0, NULL, NULL, NULL } /* gnuhash info */
    };
    struct Section text = ABSENTSCN;
    enum CsbeErr e;

    RET_ERR(get_libc_info(arch, &elfinfo.libc));
    if (arch->outformat == CSBEOF_ELF_EXE) {
        RET_ERR(add_libc_init_sym(cg, &elfinfo.libc));
    }
    RET_ERR(elf_header(csbe, cg, arch, &elfinfo, bw));
    section_start(&text, bw, 1); /* already aligned in elf_header */
    out_machinecode(bw, &cg->code);

    RET_ERR(elf_footer(csbe, cg, arch, &elfinfo, bw, &text));
    return CSBEE_OK;
}
