/*

  Unit tests of elf.c

  Copyright © 2023 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "../elf.c"
#include "unittest.h"
#include "alltests.h"

static void test_calc_gnuhash(void)
{
    /* Test vectors come from flapenguin.me */
    tsoftassert(calc_gnuhash("", 0) == 0x00001505);
    tsoftassert(calc_gnuhash("printf", 6) == 0x156b2bb8);
    tsoftassert(calc_gnuhash("exit", 4) == 0x7c967e3f);
}

static void test_gnuhash_bloomflt(void)
{
    /* Test vectors come from flapenguin.me */
    struct GnuHash gh;
    struct ArchInfo arch;
    uint64 flt[2];
    gh.bloom_size = 2;
    gh.bloom_shift = 5;
    gh.bloomflt = flt;
    arch.wordbits = 64;

    memset(flt, 0, sizeof(flt));
    bloomflt_mark(&gh, &arch, 0x0fabfd7e);
    tsoftassert(flt[0] == 0x0);
    tsoftassert(flt[1] == ((((uint64)1)<<62) | (((uint64)1)<<43)));
}

const TestFunctionInfo tests_elf[] = {
    TEST_INFO(test_calc_gnuhash)
    TEST_INFO(test_gnuhash_bloomflt)
    TEST_END
};
