/*

  Unit tests of outformat_common.c

  Copyright © 2023 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "../outformat_common.c"
#include "unittest.h"
#include "alltests.h"


static void test_outformat_endianness(void)
{
    struct BinWriter bw;
    unsigned char buff[2*(2+4+8)];
    static const unsigned char correct[2*(2+4+8)] = {
        0x22,0x11,
        0x66,0x55,0x44,0x33,
        0x77,0x76,0x75,0x74,0x73,0x72,0x71,0x70,
        0x88,0x99,
        0xAA,0xBB,0xCC,0xDD,
        0xE0,0xE1,0xE2,0xE3,0xE4,0xE5,0xE6,0xE7
    };
    uint64 u64;

    bw.buffer = buff;
    bw.remaining = sizeof(buff);
    bw.addr_size = 32;
    bw.off_size = 32;

    bw.little_endian = 1;
    out16(&bw, 0x1122);
    out32(&bw, 0x33445566);
    u64 = 0x70717273;
    u64 <<= 32;
    u64 |= 0x74757677;
    out64(&bw, u64);
    tsoftassert(bw.remaining == 2+4+8);

    bw.little_endian = 0;
    out16(&bw, 0x8899);
    out32(&bw, 0xAABBCCDD);
    u64 = 0xE0E1E2E3;
    u64 <<= 32;
    u64 |= 0xE4E5E6E7;
    out64(&bw, u64);

    tassert(bw.remaining == 0);
    tassert(!memcmp(buff, correct, sizeof(buff)));
}


const TestFunctionInfo tests_outformat_common[] = {
    TEST_INFO(test_outformat_endianness)
    TEST_END
};
