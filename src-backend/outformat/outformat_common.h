/*

  outformat_common.h -- Functions and definitions related to ELF/PE output

  Copyright © 2023-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef CSBE_OUTFORMAT_COMMON_H
#define CSBE_OUTFORMAT_COMMON_H

#include "../csbe_internal.h"
#include "../codegen/codegen_common.h"


struct BinWriter {
    unsigned char *buffer;
    size_t size;
    size_t remaining;
    unsigned section_number;
    /* Architecture information */
    unsigned little_endian : 1;
    unsigned addr_size : 8;
    unsigned off_size : 8;
};

#define CASE_ELF \
    case CSBEOF_ELF_OBJ: \
    case CSBEOF_ELF_EXE: \
    case CSBEOF_ELF_DYNLIB:
#define CASE_PE \
    case CSBEOF_PE_OBJ:
#define CASE_EXCEPT_ELF \
    case CSBEOF_RAW: \
    CASE_PE
#define CASE_EXCEPT_PE \
    case CSBEOF_RAW: \
    CASE_ELF

/**
 * Initializes a BinWriter. buffer may be NULL, if the purpose is only to
 * compute the size (in that case, max_size is ignored).
 */
void binwriter_init(struct BinWriter *bw, const struct ArchInfo *arch,
                    unsigned char *buffer, size_t max_size);
size_t binwriter_get_position(const struct BinWriter *bw);
void binwriter_reset(struct BinWriter *bw);
void binwriter_seekto(struct BinWriter *bw, size_t offs);
void binwriter_skip(struct BinWriter *bw, size_t amount);
void outbytes(struct BinWriter *bw, const unsigned char *bytes, size_t len);
void outnulls(struct BinWriter *bw, unsigned len);
void out8(struct BinWriter *bw, uint8 value);
void out16(struct BinWriter *bw, uint16 value);
void out32(struct BinWriter *bw, uint32 value);
void out32_or(struct BinWriter *bw, uint32 value);
void out64(struct BinWriter *bw, uint64 value);
void outaddr(struct BinWriter *bw, uint64 value);
void outoff(struct BinWriter *bw, uint64 value);
void outalign(struct BinWriter *bw, unsigned alignment);
#define OUT_NULLSTRING(offs_out, s) out_nullstring(bw, (offs_out), (s), sizeof(s))
void out_nullstring(struct BinWriter *bw, size_t *offs_out,
                    const char *s, size_t len);
void out_machinecode(struct BinWriter *bw, const struct CgCode *code);
void out_datasection(const struct CgCtx *cg,
                     struct BinWriter *bw);

/* Section handling */
struct Section {
    unsigned present;
    unsigned number;
    size_t start, len;
};
#define ABSENTSCN { 0,0,0,0 }
void section_start(struct Section *section, struct BinWriter *bw,
                   unsigned align);
void section_end(struct Section *section, struct BinWriter *bw);

enum SymMode {
    SYMMODE_LOCAL,     /**< Available only inside the current object file */
    SYMMODE_OBJGLOBAL, /**< Available to other object files but not exported */
    SYMMODE_IMPORT,    /**< Imported from another library */
    SYMMODE_EXPORT     /**< Exported (this is a library) */
};
struct SymInfo {
    struct SymInfo *next;
    struct CsbeFuncdef *funcdef;
    struct CsbeDatadef *datadef;
    size_t offset; /**< Offset from start of section */
    size_t size, align;
    const char *name;
    size_t namelen;
    enum SymMode mode;
    /* ELF specific */
    unsigned sym_id;    /**< Index in .symtab */
    unsigned dynsym_id; /**< Index in .dynsym*/
    size_t strtab_offs; /**< Name in .strtab (byte offset in section) */
    size_t dynstr_offs; /**< Name in .dynstr (byte offset in section) */
    size_t got_offs;    /**< Entry in .got (byte offset in section) */
    unsigned plt_vaddr; /**< Virtual address of entry in .plt.got */
    unsigned gnuhash_last_in_chain; /**< 1 if last in .gnu.hash chain */
    uint32 gnuhash;     /**< Full 32 bit gnu_hash value */
};

struct SymInfo *new_sym(struct CgCtx *cg);

/** Outputs an ELF file. Called from output.c */
enum CsbeErr elf_output(struct Csbe *csbe, struct CgCtx *cg,
                        const struct ArchInfo *arch, struct BinWriter *bw);
/** Outputs a "raw" file. Called from output.c */
enum CsbeErr raw_output(struct Csbe *csbe, struct CgCtx *cg,
                        const struct ArchInfo *arch, struct BinWriter *bw);

#endif
