/*

  csbe_ops.h -- Definition of intermediate language operands

  Copyright © 2022-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

/* NOTE: IMMED and PTR cannot be combined in the same operation! */
CSBE_OPDEF0(CSBEO_NOP)
CSBE_OPDEF1(CSBEO_JUMP, EBB(target))
CSBE_OPDEF4(CSBEO_CONDJUMP, EBB(target), VAR_IN_OR_IMMED(cond), ENUM(enum CsbeBranchType), ENUM(enum CsbeBranchBalance))
CSBE_OPDEF5(CSBEO_COMPAREJUMP, EBB(target), VAR_IN_OR_IMMED(a), VAR_IN_OR_IMMED(b), ENUM(enum CsbeBranchType), ENUM(enum CsbeBranchBalance))
CSBE_OPDEF0(CSBEO_TRAP)
CSBE_OPDEF2(CSBEO_CONDTRAP, VAR_IN_OR_IMMED(cond), ENUM(enum CsbeBranchType))
CSBE_OPDEF0(CSBEO_RETURN_VOID)
CSBE_OPDEF1(CSBEO_RETURN_ARG, VAR_IN_OR_IMMED(retval))
/** Marks the start of a function call. A function call consists of the
    following ops, in this order:
    1. CSBEO_CALL_START
    2. CSBEO_CALL_ARG... (from left to right)
    3. CSBEO_CALL_FUNCDEF / CSBEO_CALL_FUNCVAR
    4. CSBEO_CALL_GET_RETURN / CSBEO_CALL_DISCARD_RETURN

    NOTE: Use csbe_call_start to generate function start ops. */
CSBE_OPDEF2(CSBEO_CALL_START, PTR(struct Op *call_op), INDEX(num_args))
/** A function argument (passed from left to right). Only variables and
    immediates can be passed, so it may be necessary to use temporaries.

    NOTE: Use csbe_call_arg_start/end to generate arguments ops. */
CSBE_OPDEF2(CSBEO_CALL_ARG, PTR(struct Op *prev_arg), VAR_IN_OR_IMMED(arg))
/** An direct function call, to a global function definition.
    Use csbe_operand_callinfo() to produce the first parameter. */
CSBE_OPDEF3(CSBEO_CALL_FUNCDEF, PTR(struct Op *last_arg), ENUM(enum CsbeCallMode callmode), FUNCDEF(funcdef))
/** An indirect function call, via a pointer.
    Use csbe_operand_callinfo() to produce the first parameter. */
CSBE_OPDEF4(CSBEO_CALL_FUNCVAR, PTR(struct Op *last_arg), ENUM(enum CsbeCallMode callmode), TYPE(typdef), VAR_IN(funcptr))
/** Where to put the function return value.
    Used when the function has a return value AND it is used. */
CSBE_OPDEF1(CSBEO_CALL_GET_RETURN, VAR_OUT(res))
/** Used where is no return value OR it is not used.
    Not necessary for tail calls. */
CSBE_OPDEF0(CSBEO_CALL_DISCARD_RETURN)
#define CSBE_OPDEF_SIMPLE(op) CSBE_OPDEF3(op, VAR_IN_OR_IMMED(a), VAR_IN_OR_IMMED(b), VAR_OUT(res))
/* _TRAP variants use the result type to determine bounds.
   MUL/DIV/MOD use the result type to determine signedness.
   TODO add operations for floating point */
CSBE_OPDEF2(CSBEO_MOVE, VAR_IN_OR_IMMED(src), VAR_OUT(res))
CSBE_OPDEF2(CSBEO_MOVE_TRAP, VAR_IN_OR_IMMED(src), VAR_OUT(res))
CSBE_OPDEF2(CSBEO_NEG, VAR_IN_OR_IMMED(src), VAR_OUT(res))
CSBE_OPDEF2(CSBEO_NEG_TRAP, VAR_IN_OR_IMMED(src), VAR_OUT(res))
CSBE_OPDEF_SIMPLE(CSBEO_ADD)
CSBE_OPDEF_SIMPLE(CSBEO_ADD_TRAP)
CSBE_OPDEF_SIMPLE(CSBEO_SUB)
CSBE_OPDEF_SIMPLE(CSBEO_SUB_TRAP)
CSBE_OPDEF_SIMPLE(CSBEO_MUL)
CSBE_OPDEF_SIMPLE(CSBEO_MUL_TRAP)
CSBE_OPDEF_SIMPLE(CSBEO_DIV)
CSBE_OPDEF_SIMPLE(CSBEO_DIV_TRAP)
CSBE_OPDEF_SIMPLE(CSBEO_MOD)
CSBE_OPDEF_SIMPLE(CSBEO_MOD_TRAP)
CSBE_OPDEF_SIMPLE(CSBEO_BAND)
CSBE_OPDEF_SIMPLE(CSBEO_BOR)
CSBE_OPDEF_SIMPLE(CSBEO_BXOR)
CSBE_OPDEF2(CSBEO_BNOT, VAR_IN_OR_IMMED(src), VAR_OUT(res))
CSBE_OPDEF_SIMPLE(CSBEO_SHL)
CSBE_OPDEF_SIMPLE(CSBEO_SHR)
CSBE_OPDEF_SIMPLE(CSBEO_SHRA)
CSBE_OPDEF_SIMPLE(CSBEO_LAND)
CSBE_OPDEF_SIMPLE(CSBEO_LOR)
CSBE_OPDEF_SIMPLE(CSBEO_LXOR) /* XXX remove? this is equivalent to NEQ */
CSBE_OPDEF2(CSBEO_LNOT, VAR_IN_OR_IMMED(src), VAR_OUT(res))
CSBE_OPDEF_SIMPLE(CSBEO_EQ)
CSBE_OPDEF_SIMPLE(CSBEO_NEQ)
CSBE_OPDEF_SIMPLE(CSBEO_LT)
CSBE_OPDEF_SIMPLE(CSBEO_GT)
CSBE_OPDEF_SIMPLE(CSBEO_LE)
CSBE_OPDEF_SIMPLE(CSBEO_GE)
#undef CSBE_OPDEF_SIMPLE
/** Marks a variable as no longer in use. */
CSBE_OPDEF1(CSBEO_DISCARD, VAR_IN(var))
CSBE_OPDEF5(CSBEO_CHOICE, VAR_IN_OR_IMMED(sel), ENUM(enum CsbeSelectBalance), VAR_IN_OR_IMMED(a), VAR_IN_OR_IMMED(b), VAR_OUT(out))
CSBE_OPDEF3(CSBEO_SIZEOF, ENUM(enum CsbeSizeofKind), TYPE(type), VAR_OUT(out))
CSBE_OPDEF3(CSBEO_COPY, TYPE(type), VAR_IN(destptr), VAR_IN(srcptr))
CSBE_OPDEF2(CSBEO_CLEAR, TYPE(type), VAR_IN(destptr))
CSBE_OPDEF2(CSBEO_MOVETOPTR, VAR_IN_OR_IMMED(src), VAR_OUTPTR(destptr))
CSBE_OPDEF2(CSBEO_DEREF, VAR_IN(destptr), VAR_OUT(out))
CSBE_OPDEF5(CSBEO_ADDRELEM, TYPE(type), VAR_IN(baseptr), VAR_IN_OR_IMMED(index), INDEX(fieldnum), VAR_OUT(out))
CSBE_OPDEF5(CSBEO_LOADELEM, TYPE(type), VAR_IN(baseptr), VAR_IN_OR_IMMED(index), INDEX(fieldnum), VAR_OUT(out))
CSBE_OPDEF2(CSBEO_ADDRLOCAL, VAR_IN(localvar), VAR_OUT(out))
CSBE_OPDEF2(CSBEO_ADDRGLOBAL, DATADEF(datadef), VAR_OUT(out))
CSBE_OPDEF2(CSBEO_LOADGLOBAL, DATADEF(datadef), VAR_OUT(out))
CSBE_OPDEF2(CSBEO_ADDRFUNC, FUNCDEF(func), VAR_OUT(out))
CSBE_OPDEF_LAST

#undef CSBE_OPDEF_LAST
#undef CSBE_OPDEF5
#undef CSBE_OPDEF4
#undef CSBE_OPDEF3
#undef CSBE_OPDEF2
#undef CSBE_OPDEF1
#undef CSBE_OPDEF0
#undef CSBE_OPDEF_SIMPLE
#undef VAR_IN
#undef VAR_OUT
#undef VAR_OUTPTR
#undef VAR_IN_OR_IMMED
#undef INDEX
#undef EBB
#undef FUNCDEF
#undef DATADEF
#undef FLAGS
#undef ENUM
#undef TYPE
#undef PTR
