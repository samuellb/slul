/*

  oommain.c -- Main entry point for out-of-memory tests

  Copyright © 2022-2023 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include <stdlib.h>
#include <stdio.h>

#define TEST_MAIN
#include "unittest.h"
#include "testcommon.h"
#include "alltests.h"
TESTCALLS_START
#undef TEST_D
#define TEST_D TEST_D2
#include "alltests.h"
TESTCALLS_END

static int num_allocs, failing_alloc, ignored_allocs;
static int has_early_failures = 0;

void test_handler(void)
{
    int total_allocs;
    /* Determine the number of allocations */
    failing_alloc = 0;
    num_allocs = 0;
    errors_testcase = 0;
    run_test(); /* First run */
    if (testcase_aborted) {
        if (!quiet) {
            fprintf(stderr, "SKIPPING FAILING TEST %s\n", current_function);
        }
        has_early_failures = 1;
        return;
    }
    total_allocs = num_allocs;
    /* Test with allocation failure after x allocs */
    for (failing_alloc = ignored_allocs+1;
            failing_alloc <= total_allocs; failing_alloc++) {
        if (verbose == 2) {
            fprintf(stderr, "--- testing OOM at allocation %2d (of %d) ---\n", failing_alloc, total_allocs);
        } else if (verbose && (failing_alloc & 0x3f) == 0) {
            fputc('.', stderr);
        }
        num_allocs = 0;
        errors_testcase = 0;
        run_test(); /* Run with limited allocations */
        freeall();
    }
    if (verbose == 1 && ignored_allocs+63 <= total_allocs) {
        fputc('\n', stderr);
    }
}


static int simulate_alloc_failure(void)
{
    num_allocs++;
    if (!failing_alloc) return 0;
    return num_allocs >= failing_alloc;
}

void *allocfunc(void *ctx, size_t size, size_t align)
{
    if (simulate_alloc_failure()) return NULL;
    return tracked_alloc(ctx, size, align);
}


const char *error_extra_text(void)
{
    return NULL;
}

const char *help_extra_text(void)
{
    return
        "This program tests out-of-memory handling. Each test will be run in\n"
        "several iterations; in the first one the first allocation fails, in\n"
        "the second one the second allocation fails, etc.\n";
}

const char *verbose_extra_text(void)
{
    return "OoM-backend-test";
}


int main(int argc, char **argv)
{
    int status;
    crash_test_only = 1;
    status = test_main(argc, argv);
    return status != EXIT_SUCCESS ? status :
            (has_early_failures ? EXIT_FAILURE : EXIT_SUCCESS);
}
