/*

  Unit tests of init.c

  Copyright © 2022 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "../init.c"
#include "unittest.h"
#include "testcommon.h"
#include "alltests.h"

static void test_init(void)
{
    tassert(tnewcsbe(tnewcfg(CSBEMT_EXEC)) != NULL);
}

static void test_config_badversion(void)
{
    tsoftassert(csbe_config_new(CSBE_LATEST_API_VERSION+1,
                                NULL, NULL) == NULL);
}

static void test_addarch_good(void)
{
    struct CsbeConfig *cfg = tnewcfg(CSBEMT_EXEC);
    tassert(cfg != NULL);
    SOFTCHK(csbe_config_add_arch(cfg, CSBEC_I386, CSBEEN_LITTLE_ENDIAN,
                                 CSBEOS_LINUX_GLIBC, CSBEOF_ELF_OBJ));
    SOFTCHK(csbe_config_add_arch(cfg, CSBEC_AARCH64, CSBEEN_LITTLE_ENDIAN,
                                 CSBEOS_WINDOWS, CSBEOF_PE_OBJ));
    SOFTCHK(csbe_config_add_arch(cfg, CSBEC_AARCH64, CSBEEN_BIG_ENDIAN,
                                 CSBEOS_LINUX_GLIBC, CSBEOF_ELF_OBJ));
}

static void test_addarch_bad(void)
{
    struct CsbeConfig *cfg = tnewcfg(CSBEMT_EXEC);
    tassert(cfg != NULL);
    tsoftassert(csbe_config_add_arch(cfg, 9999, CSBEEN_LITTLE_ENDIAN,
                                     CSBEOS_LINUX_GLIBC, CSBEOF_ELF_OBJ) ==
                CSBEE_UNKNOWN_ARCH);
    tsoftassert(csbe_config_add_arch(cfg, CSBEC_I386, CSBEEN_LITTLE_ENDIAN,
                                     9999, CSBEOF_ELF_OBJ) ==
                CSBEE_UNKNOWN_ARCH);
    tsoftassert(csbe_config_add_arch(cfg, CSBEC_I386, CSBEEN_LITTLE_ENDIAN,
                                     CSBEOS_LINUX_GLIBC, 9999) ==
                CSBEE_UNKNOWN_ARCH);
    CHK_INVALID(csbe_config_add_arch(cfg, CSBEC_I386, CSBEEN_BIG_ENDIAN,
                                     CSBEOS_LINUX_GLIBC, CSBEOF_ELF_OBJ));
    CHK_INVALID(csbe_config_add_arch(cfg, CSBEC_I386, 999,
                                     CSBEOS_LINUX_GLIBC, CSBEOF_ELF_OBJ));
}

static void test_archfeature_good(void)
{
    struct CsbeConfig *cfg = tnewcfg(CSBEMT_EXEC);
    tassert(cfg != NULL);
    CHK(csbe_config_add_arch(cfg, CSBEC_I386, CSBEEN_LITTLE_ENDIAN,
                             CSBEOS_LINUX_GLIBC, CSBEOF_ELF_OBJ));
    tsoftassert(csbe_config_arch_has_feature_state(cfg,
            CSBEF_COMMON_HARDFLOAT, 1));
    tsoftassert(csbe_config_arch_has_feature_state(cfg,
            CSBEF_COMMON_HARDFLOAT, 0));
    SOFTCHK(csbe_config_arch_feature(cfg, CSBEF_COMMON_HARDFLOAT, 1));
    SOFTCHK(csbe_config_arch_feature(cfg, CSBEF_COMMON_HARDFLOAT, 0));
}

static void test_archfeature_bad(void)
{
    struct CsbeConfig *cfg = tnewcfg(CSBEMT_EXEC);
    tassert(cfg != NULL);
    CHK(csbe_config_add_arch(cfg, CSBEC_I386, CSBEEN_LITTLE_ENDIAN,
                             CSBEOS_LINUX_GLIBC, CSBEOF_ELF_OBJ));
    tsoftassert(!csbe_config_arch_has_feature_state(cfg, -999, 0));
    tsoftassert(!csbe_config_arch_has_feature_state(cfg, 999, 0));
    tsoftassert(!csbe_config_arch_has_feature_state(cfg,
            MAX_ARCH_FEATURES, 0));
    tsoftassert(csbe_config_arch_feature(cfg, -999, 0) ==
            CSBEE_UNSUPPORTED_FEATURE_PARAM);
    tsoftassert(csbe_config_arch_feature(cfg, 999, 0) ==
            CSBEE_UNSUPPORTED_FEATURE_PARAM);
}


const TestFunctionInfo tests_init[] = {
    TEST_INFO(test_init)
    TEST_INFO(test_config_badversion)
    TEST_INFO(test_addarch_good)
    TEST_INFO(test_addarch_bad)
    TEST_INFO(test_archfeature_good)
    TEST_INFO(test_archfeature_bad)
    TEST_END
};
