/*

  testcommon.h -- Common functions for the backend tests

  Copyright © 2022 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "testcommon.h"
#include "../csbe_internal.h"
#include "unittest.h"

struct CsbeConfig *tnewcfg(enum CsbeModuleType modtype)
{
    struct CsbeConfig *cfg;
    struct CsbeFuncPtrs funcptrs = { allocfunc };

    cfg = csbe_config_new(CSBE_API_VERSION_0, &funcptrs, NULL);
    tassert(cfg != NULL);
    csbe_config_set_moduletype(cfg, modtype);
    return cfg;
}

struct Csbe *tnewcsbe(struct CsbeConfig *cfg)
{
    struct Csbe *csbe = csbe_new(cfg);
    tassert(csbe != NULL);
    tassert(csbe_get_error(csbe) == CSBEE_OK);
    return csbe;
}


struct AllocInfo {
    struct AllocInfo *next;
};
static struct AllocInfo *allocs = NULL;


void *tracked_alloc(void *ctx, size_t size, size_t align)
{
    size_t headersize = (sizeof(struct AllocInfo) >= align ?
        sizeof(struct AllocInfo) : align);
    void *p;
    struct AllocInfo *ap;

    (void)ctx;
    p = malloc(headersize + size);
    if (!p) {
        fputs("Out of memory!", stderr);
        return NULL;
    }
    ap = (struct AllocInfo *)p;
    ap->next = allocs;
    allocs = ap;
    return ((char *)p) + headersize;
}

void freeall(void)
{
    /* The backend code expects the client of it to free the memory */
    while (allocs) {
        struct AllocInfo *next = allocs->next;
        free(allocs);
        allocs = next;
    }
}
