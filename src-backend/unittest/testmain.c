/*

  testmain.c -- Main entry point for unit tests for the backend

  Copyright © 2022 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include <stdlib.h>
#include <stdio.h>

#define TEST_MAIN
#include "unittest.h"
#include "testcommon.h"
#include "alltests.h"
TESTCALLS_START
#undef TEST_D
#define TEST_D TEST_D2
#include "alltests.h"
TESTCALLS_END


void test_handler(void)
{
    run_test();
    freeall();
}

void *allocfunc(void *ctx, size_t size, size_t align)
{
    return tracked_alloc(ctx, size, align);
}


const char *error_extra_text(void)
{
    return "";
}

const char *help_extra_text(void)
{
    return "This executable contains the unit tests for the backend.\n";
}

const char *verbose_extra_text(void)
{
    return "backend-test";
}


int main(int argc, char **argv)
{
    return test_main(argc, argv);
}

