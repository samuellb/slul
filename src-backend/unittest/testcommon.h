/*

  testcommon.h -- Definitions of various common functions for backend tests

  Copyright © 2022 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/
#ifndef CSBE_TESTCOMMON_H
#define CSBE_TESTCOMMON_H

#include <stdlib.h>

#include "../include/csbe.h"


#define CHK(rv) tassert((rv) == CSBEE_OK)
#define SOFTCHK(rv) tsoftassert((rv) == CSBEE_OK)
#define CHK_INVALID(rv) tsoftassert((rv) == CSBEE_INVALID_PARAM)

/* This functions are called from test cases */
struct CsbeConfig *tnewcfg(enum CsbeModuleType modtype);
struct Csbe *tnewcsbe(struct CsbeConfig *cfg);
void *allocfunc(void *ctx, size_t size, size_t align);

/* These are called from testmain.c / oommain.c */
void *tracked_alloc(void *ctx, size_t size, size_t align);
void freeall(void);

#endif

