/*

  Unit tests of analyze.c

  Copyright © 2023-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "../analyze.c"
#include "unittest.h"
#include "testcommon.h"
#include "alltests.h"

static struct Csbe *basic_func_start(unsigned num_ebb, unsigned num_vars)
{
    struct Csbe *csbe;
    csbe = tnewcsbe(tnewcfg(CSBEMT_EXEC));

    CHK(csbe_set_num_funcdefs(csbe, 1));
    CHK(csbe_funcdef_start(csbe, 0, 0, CSBEABI_C_NORMAL, 0));
    csbe_type_void(csbe);
    csbe_funcdef_end(csbe);
    /* End of defs */
    CHK(csbe_defs_done(csbe));

    CHK(csbe_funcbody_start(csbe, 0, NULL, NULL, num_ebb, num_vars));
    tsoftassert(csbe->error == CSBEE_OK);
    return csbe;
}

static struct FuncIR *basic_func_end(struct Csbe *csbe)
{
    struct FuncIR *fb = csbe->funcbody;
    tassert(fb != NULL);
    CHK(csbe_funcbody_end(csbe));
    tsoftassert(csbe->error == CSBEE_OK);
    return fb;
}

static void op_using_var(struct Csbe *csbe, unsigned var_id)
{
    CHK(csbe_op_start(csbe, CSBEO_CONDTRAP));
    csbe_operand_var_in(csbe, var_id, 0);
    csbe_operand_enum(csbe, CSBEBT_Z);
    csbe_op_end(csbe);
}

static void vardef_unused(struct Csbe *csbe, unsigned var_id, unsigned varlane)
{
    csbe_funcbody_vardef_start(csbe, CSBEVD_SIMPLE, var_id, varlane);
    csbe_type_simple(csbe, CSBET_UINT);
    csbe_funcbody_vardef_end(csbe);
}

static void vardef_used(struct Csbe *csbe, unsigned var_id, unsigned varlane)
{
    vardef_unused(csbe, var_id, varlane);
    op_using_var(csbe, var_id);
}

#define ASSERT_VARLANE(var_id, expected_varlane) \
    tsoftassert(fb->vars[(var_id)].lane == (expected_varlane))

/** Checks allocation of varlanes (abstract registers) */
static void test_varlanes_simple(void)
{
    struct Csbe *csbe;
    struct FuncIR *fb;

    csbe = basic_func_start(1, 7);

    CHK(csbe_ebb_start(csbe, 0, CSBEEF_DEFAULT));
    vardef_used(csbe, 0, 2);
    vardef_used(csbe, 1, 2);
    vardef_used(csbe, 2, 0);
    vardef_unused(csbe, 3, 0);
    vardef_used(csbe, 4, 0);
    vardef_used(csbe, 5, 1);
    vardef_used(csbe, 6, 0);

    fb = basic_func_end(csbe);

    ASSERT_VARLANE(0, 2);
    ASSERT_VARLANE(1, 2);
    ASSERT_VARLANE(2, 3);
    ASSERT_VARLANE(3, 0);
    ASSERT_VARLANE(4, 4);
    ASSERT_VARLANE(5, 1);
    ASSERT_VARLANE(6, 5);
}

/**
 * Checks that variables that are only used in a single EBB
 * get allocated shared varlanes (to avoid wasting registers)
 */
static void test_varlanes_per_ebb(void)
{
    struct Csbe *csbe;
    struct FuncIR *fb;
    unsigned ebb_varlane, dedicated_varlane;

    csbe = basic_func_start(3, 9);

    CHK(csbe_ebb_start(csbe, 0, CSBEEF_DEFAULT));
                             /* can use/share a per-EBB varlane? */
    vardef_used(csbe, 0, 2); /* no. manually allocated */
    vardef_used(csbe, 1, 2); /* no. manually allocated */
    vardef_used(csbe, 2, 0); /* yes */
    vardef_used(csbe, 3, 0); /* no. used in the last varlane */

    CHK(csbe_ebb_start(csbe, 1, CSBEEF_DEFAULT));
    vardef_unused(csbe, 4, 0); /* yes. used twice in the same varlane */
    vardef_used(csbe, 5, 0); /* yes */
    vardef_used(csbe, 6, 0); /* yes */
    op_using_var(csbe, 4);

    CHK(csbe_ebb_start(csbe, 2, CSBEEF_DEFAULT));
    vardef_used(csbe, 7, 1); /* no. manually allocated */
    vardef_used(csbe, 8, 0); /* yes */
    op_using_var(csbe, 3);

    fb = basic_func_end(csbe);

    ebb_varlane = 3;
    dedicated_varlane = ebb_varlane+3;
    ASSERT_VARLANE(0, 2);
    ASSERT_VARLANE(1, 2);
    ASSERT_VARLANE(2, ebb_varlane);
    ASSERT_VARLANE(3, dedicated_varlane);

    ASSERT_VARLANE(4, ebb_varlane);
    ASSERT_VARLANE(5, ebb_varlane+1);
    ASSERT_VARLANE(6, ebb_varlane+2);

    ASSERT_VARLANE(7, 1);
    ASSERT_VARLANE(8, ebb_varlane);
}

const TestFunctionInfo tests_analyze[] = {
    TEST_INFO(test_varlanes_simple)
    TEST_INFO(test_varlanes_per_ebb)
    TEST_END
};
