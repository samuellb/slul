#
# Makefile definitions for the backend ("CSBE")
#
# Copyright © 2022-2024 Samuel Lidén Borell <samuel@kodafritt.se>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

BACKEND_TEST_OBJECTS = \
	$(BACKEND_EXTRA_TEST_OBJECTS) \
	$(build_backend)/unittest/testcommon.o \
	$(build_backend)/unittest/test_analyze.o \
	$(build_backend)/unittest/test_datastruct.o \
	$(build_backend)/unittest/test_init.o \
	$(build_backend)/unittest/test_opinfo.o \
	$(build_backend)/unittest/test_output.o \
	$(build_backend)/codegen/unittest/codegen_testcommon.o \
	$(build_backend)/codegen/unittest/test_codegen_common.o \
	$(build_backend)/codegen/unittest/test_aarch64.o \
	$(build_backend)/codegen/unittest/test_x86.o \
	$(build_backend)/codegen/unittest/test_irdump.o \
	$(build_backend)/outformat/unittest/test_outformat_common.o \
	$(build_backend)/outformat/unittest/test_elf.o \
	$(build_backend)/outformat/unittest/test_raw.o \
	$(COMMON_TEST_OBJECTS)
BACKEND_UNITTEST_OBJECTS = \
    $(BACKEND_TEST_OBJECTS) \
	$(build_backend)/unittest/testmain.o
BACKEND_OOMTEST_OBJECTS = \
    $(BACKEND_TEST_OBJECTS) \
	$(build_backend)/unittest/oommain.o
BACKEND_ALL_TEST_OBJECTS = \
    $(BACKEND_TEST_OBJECTS) \
	$(build_backend)/unittest/testmain.o \
	$(build_backend)/unittest/oommain.o
BACKEND_UNITTEST_C_FILES_WITHOUT_EXTERNALS = \
	$(src_backend)/unittest/testcommon.c \
	$(src_backend)/unittest/test_analyze.c \
	$(src_backend)/unittest/test_datastruct.c \
	$(src_backend)/unittest/test_init.c \
	$(src_backend)/unittest/test_opinfo.c \
	$(src_backend)/unittest/test_output.c \
	$(src_backend)/codegen/unittest/codegen_testcommon.c \
	$(src_backend)/codegen/unittest/test_codegen_common.c \
	$(src_backend)/codegen/unittest/test_aarch64.c \
	$(src_backend)/codegen/unittest/test_x86.c \
	$(src_backend)/codegen/unittest/test_irdump.c \
	$(src_backend)/outformat/unittest/test_outformat_common.c \
	$(src_backend)/outformat/unittest/test_elf.c \
	$(src_backend)/outformat/unittest/test_raw.c \
	$(src_backend)/unittest/testmain.c
BACKEND_UNITTEST_C_FILES = \
    $(BACKEND_EXTRA_UNITTEST_C_FILES) \
    $(BACKEND_UNITTEST_C_FILES_WITHOUT_EXTERNALS) \
    $(COMMON_TEST_C_FILES)
BACKEND_OBJECTS = \
	$(BACKEND_EXTRA_OBJECTS) \
	$(build_backend)/analyze.o \
	$(build_backend)/datastruct.o \
	$(build_backend)/init.o \
	$(build_backend)/opinfo.o \
	$(build_backend)/output.o \
	$(build_backend)/codegen/codegen_common.o \
	$(build_backend)/codegen/aarch64.o \
	$(build_backend)/codegen/x86.o \
	$(build_backend)/codegen/irdump.o \
	$(build_backend)/outformat/outformat_common.o \
	$(build_backend)/outformat/elf.o \
	$(build_backend)/outformat/raw.o
BACKEND_C_FILES = \
	$(BACKEND_EXTRA_C_FILES) \
	$(src_backend)/analyze.c \
	$(src_backend)/datastruct.c \
	$(src_backend)/init.c \
	$(src_backend)/opinfo.c \
	$(src_backend)/output.c \
	$(src_backend)/codegen/codegen_common.c \
	$(src_backend)/codegen/aarch64.c \
	$(src_backend)/codegen/x86.c \
	$(src_backend)/codegen/irdump.c \
	$(src_backend)/outformat/outformat_common.c \
	$(src_backend)/outformat/elf.c \
	$(src_backend)/outformat/raw.c
BACKEND_PUBLIC_HEADERS = \
    $(src_backend)/include/csbe.h \
    $(src_backend)/include/csbe_ops.h
BACKEND_HEADERS = \
    $(BACKEND_PUBLIC_HEADERS) \
    $(src_backend)/csbe_internal.h \
    $(src_backend)/codegen/codegen_common.h \
    $(src_backend)/outformat/outformat_common.h \
    $(src_backend)/unittest/alltests.h \
    $(src_backend)/codegen/unittest/codegen_testcommon.h \
    $(src_common)/unittest/unittest.h \
    $(BACKEND_EXTRA_HEADERS)
CODEGEN_DEPS = \
    $(src_backend)/include/csbe.h \
    $(src_backend)/include/csbe_ops.h \
    $(src_backend)/csbe_internal.h \
    $(src_backend)/codegen/codegen_common.h \
    $(src_backend)/outformat/outformat_common.h
CODEGEN_TEST_DEPS = \
	$(src_backend)/codegen/unittest/codegen_testcommon.h \
	$(CODEGEN_DEPS)
OUTFORMAT_DEPS = \
    $(src_backend)/include/csbe.h \
    $(src_backend)/include/csbe_ops.h \
    $(src_backend)/csbe_internal.h \
    $(src_backend)/codegen/codegen_common.h \
    $(src_backend)/outformat/outformat_common.h
OUTFORMAT_TEST_DEPS = \
	$(OUTFORMAT_DEPS)


backend-all: $(build_backend)/libcsbe.a

$(build_backend)/analyze.o: $(src_backend)/include/csbe.h $(src_backend)/csbe_internal.h
$(build_backend)/datastruct.o: $(src_backend)/include/csbe.h $(src_backend)/csbe_internal.h $(src_backend)/include/csbe_ops.h
$(build_backend)/init.o: $(src_backend)/include/csbe.h $(src_backend)/csbe_internal.h
$(build_backend)/opinfo.o: $(src_backend)/include/csbe.h $(src_backend)/csbe_internal.h $(src_backend)/include/csbe_ops.h
$(build_backend)/output.o: $(OUTFORMAT_DEPS)
$(build_backend)/codegen/codegen_common.o: $(CODEGEN_DEPS)
$(build_backend)/codegen/aarch64.o: $(CODEGEN_DEPS)
$(build_backend)/codegen/x86.o: $(CODEGEN_DEPS)
$(build_backend)/codegen/irdump.o: $(CODEGEN_DEPS)
$(build_backend)/outformat/outformat_common.o: $(OUTFORMAT_DEPS)
$(build_backend)/outformat/elf.o: $(OUTFORMAT_DEPS)
$(build_backend)/outformat/raw.o: $(OUTFORMAT_DEPS)

$(build_backend)/unittest/testcommon.o: $(src_backend)/include/csbe.h $(src_backend)/csbe_internal.h $(src_backend)/unittest/testcommon.h $(src_common)/unittest/unittest.h
$(build_backend)/unittest/test_analyze.o: $(src_backend)/analyze.c $(src_backend)/include/csbe.h $(src_backend)/csbe_internal.h $(src_backend)/include/csbe_ops.h $(src_common)/unittest/unittest.h
$(build_backend)/unittest/test_datastruct.o: $(src_backend)/datastruct.c $(src_backend)/include/csbe.h $(src_backend)/csbe_internal.h $(src_backend)/include/csbe_ops.h $(src_common)/unittest/unittest.h
$(build_backend)/unittest/test_init.o: $(src_backend)/init.c $(src_backend)/include/csbe.h $(src_backend)/csbe_internal.h $(src_common)/unittest/unittest.h
$(build_backend)/unittest/test_opinfo.o: $(src_backend)/opinfo.c $(src_backend)/include/csbe.h $(src_backend)/csbe_internal.h $(src_backend)/include/csbe_ops.h $(src_common)/unittest/unittest.h
$(build_backend)/unittest/test_output.o: $(src_backend)/output.c $(OUTFORMAT_DEPS) $(src_common)/unittest/unittest.h
$(build_backend)/codegen/unittest/codegen_testcommon.o: $(CODEGEN_TEST_DEPS) $(src_common)/unittest/unittest.h
$(build_backend)/codegen/unittest/test_codegen_common.o: $(src_backend)/codegen/codegen_common.c $(CODEGEN_TEST_DEPS) $(src_common)/unittest/unittest.h
$(build_backend)/codegen/unittest/test_aarch64.o: $(src_backend)/codegen/aarch64.c $(CODEGEN_TEST_DEPS) $(src_common)/unittest/unittest.h
$(build_backend)/codegen/unittest/test_x86.o: $(src_backend)/codegen/x86.c  $(CODEGEN_TEST_DEPS) $(src_common)/unittest/unittest.h
$(build_backend)/codegen/unittest/test_irdump.o: $(src_backend)/codegen/irdump.c $(CODEGEN_TEST_DEPS) $(src_common)/unittest/unittest.h
$(build_backend)/outformat/unittest/test_outformat_common.o: $(src_backend)/outformat/outformat_common.c $(OUTFORMAT_TEST_DEPS) $(src_common)/unittest/unittest.h
$(build_backend)/outformat/unittest/test_elf.o: $(src_backend)/outformat/elf.c $(OUTFORMAT_TEST_DEPS) $(src_common)/unittest/unittest.h
$(build_backend)/outformat/unittest/test_raw.o: $(src_backend)/outformat/raw.c $(OUTFORMAT_TEST_DEPS) $(src_common)/unittest/unittest.h
$(build_backend)/unittest/testmain.o: $(src_backend)/unittest/alltests.h $(src_common)/unittest/unittest.h
$(build_backend)/unittest/oommain.o: $(src_backend)/unittest/alltests.h $(src_common)/unittest/unittest.h

$(build_backend)/libcsbe.a: $(BACKEND_OBJECTS)
	$(RM_F) $@
	$(AR) rc $@ $(BACKEND_OBJECTS)

$(build_backend)/unittest-normal: $(BACKEND_UNITTEST_OBJECTS)
	$(CC) $(INTERNCFLAGS) $(CFLAGS) $(LDFLAGS) $(DEBUG_DEFINES) $(BACKEND_UNITTEST_OBJECTS) $(BACKEND_EXTRA_LIBS) -o $@
$(build_backend)/unittest-oom: $(BACKEND_OOMTEST_OBJECTS)
	$(CC) $(INTERNCFLAGS) $(CFLAGS) $(LDFLAGS) $(DEBUG_DEFINES) $(BACKEND_OOMTEST_OBJECTS) $(BACKEND_EXTRA_LIBS) -o $@
$(build_backend)/unittest-tcc-boundscheck: $(BACKEND_UNITTEST_C_FILES) $(BACKEND_C_FILES) $(BACKEND_HEADERS)
	tcc -b -bt24 $(INTERNCFLAGS) $(LDFLAGS) $(DEBUG_DEFINES) $(BACKEND_UNITTEST_C_FILES) $(BACKEND_EXTRA_LIBS) -o $@
$(build_backend)/unittest-gcov: $(BACKEND_UNITTEST_C_FILES) $(BACKEND_C_FILES) $(BACKEND_HEADERS)
	gcc --coverage $(INTERNCFLAGS) $(LDFLAGS) $(DEBUG_DEFINES) $(BACKEND_UNITTEST_C_FILES) $(BACKEND_EXTRA_LIBS) -o $@
$(build_backend)/unittest-gprof: $(BACKEND_UNITTEST_C_FILES) $(BACKEND_C_FILES) $(BACKEND_HEADERS)
	gcc -pg $(INTERNCFLAGS) $(LDFLAGS) $(DEBUG_DEFINES) $(BACKEND_UNITTEST_C_FILES) $(BACKEND_EXTRA_LIBS) -o $@

backend-check: backend-check-unit

backend-check-unit: $(build_backend)/unittest-normal
	$(build_backend)/unittest-normal $(UNITTEST_OPTS)

backend-check-oom-unit: $(build_backend)/unittest-oom
	$(build_backend)/unittest-oom $(UNITTEST_OPTS)

backend-check-unit-valgrind: $(build_backend)/unittest-normal
	valgrind -q --leak-check=full $(VALGRIND_OPTS) $(build_backend)/unittest-normal $(UNITTEST_OPTS)

backend-check-oom-unit-valgrind: $(build_backend)/unittest-oom
	valgrind -q --leak-check=full $(VALGRIND_OPTS) $(build_backend)/unittest-oom $(UNITTEST_OPTS)

backend-tcc-boundscheck: $(build_backend)/unittest-tcc-boundscheck
	TCC_BOUNDS_WARN_POINTER_ADD=1 $(build_backend)/unittest-tcc-boundscheck $(UNITTEST_OPTS)

backend-gcc-coverage: $(build_backend)/unittest-gcov
	$(build_backend)/unittest-gcov $(UNITTEST_OPTS)

backend-clang-analyze:
	clang $(DEFINES) $(INCLUDES) --analyze $(BACKEND_C_FILES)
	clang $(DEFINES) $(INCLUDES) --analyze $(BACKEND_UNITTEST_C_FILES)

backend-gcc-analyze:
	gcc $(GCC_FANALYZE_FLAGS) $(DEFINES) $(INCLUDES) \
	        $(BACKEND_UNITTEST_C_FILES) \
	        -o $(build_backend)/fanalyzer_dummy

backend-sparse-analyze:
	sparse $(DEFINES) $(INCLUDES) $(BACKEND_C_FILES)
	sparse $(DEFINES) $(INCLUDES) $(BACKEND_UNITTEST_C_FILES)

# For now, we run all tests for a specific platform only,
# in order to reduce the number of permutations.
# The "threadsafety" addon is used to find missing/incorrect
# usage of static and/or const keywords.
backend-cppcheck:
	cppcheck $(CPPCHECK_FLAGS) -rp $(src_backend) \
	    --suppressions-list=$(src_backend)/misc/cppcheck_suppressions.txt \
	    $(BACKEND_C_FILES) $(BACKEND_UNITTEST_C_FILES)

gdb-backend-unit: $(build_backend)/unittest-normal
	gdb --args $(build_backend)/unittest-normal $(UNITTEST_OPTS)
gdb-backend-oom-unit: $(build_backend)/unittest-oom
	gdb --args $(build_backend)/unittest-oom $(UNITTEST_OPTS)

backend-outdirs:
	$(MKDIR_P) $(build_backend)/codegen
	$(MKDIR_P) $(build_backend)/outformat
	$(MKDIR_P) $(build_backend)/unittest
	$(MKDIR_P) $(build_backend)/codegen/unittest
	$(MKDIR_P) $(build_backend)/outformat/unittest

backend-clean:
	$(RM_F) $(BACKEND_OBJECTS) $(BACKEND_ALL_TEST_OBJECTS) \
	        $(build_backend)/libcsbe.a \
	        $(build_backend)/unittest-normal \
	        $(build_backend)/unittest-oom \
	        $(build_backend)/unittest-tcc-boundscheck \
	        $(build_backend)/unittest-gcov $(build_backend)/unittest-gprof \
	        $(build_backend)/fanalyzer_dummy
backend-distclean: backend-clean

.PHONY: backend-all backend-clean backend-distclean backend-outdirs \
        backend-check backend-check-unit backend-check-oom-unit \
        backend-check-unit-valgrind backend-check-oom-unit-valgrind \
        backend-tcc-boundscheck backend-gcc-coverage backend-clang-analyze \
        backend-sparse-analyze backend-cppcheck \
        gdb-backend-unit gdb-backend-oom-unit
