#!/bin/sh -eu
#
# Script for printing encodings of instructions from Aarch64_ops.csv
#
# Copyright © 2023 Samuel Lidén Borell <samuel@kodafritt.se>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

# For use with AArch64_ops.csv
file=$(dirname "$0")/AArch64_ops.csv
if [ ! -e "$file" ]; then
    cat <<EOF

ERROR: $file is missing. It can be downloaded here:

https://github.com/CAS-Atlantic/AArch64-Encoding/raw/master/AArch64_ops.csv

EOF
    exit 1
fi

op="$1"
LF="$(printf 'x\nx')"
LF="${LF#x}"
LF="${LF%x}"

add_cell() {
    if [ $is_range = 1 ]; then
        if [ $i -gt 0 ]; then
            i_last=$(($i + 1))
        else
            i_last=0
        fi
        cell_title="$cell_title-$(printf '%d' $i_last)"
    fi
    title_width=${#cell_title}
    content_width=${#cell_content}
    if [ $title_width -gt $content_width ]; then
        cell_width=$title_width
    else
        cell_width=$content_width
    fi
    cell_width=$(($cell_width + 1))
    title="$title"$(printf "%${cell_width}s" "$cell_title")
    content="$content"$(printf "%${cell_width}s" "$cell_content")
    while [ $cell_width != 0 ]; do
        border="$border-"
        cell_width=$(($cell_width - 1))
    done
    cell_title=""
    cell_content=""
}

parse_line() {
    IFS=",$LF"
    read num in_use dummy01 dummy02 opcode prependage appendage specific variant comments dummy03 bits
    [ -n "$opcode" ] || return 1
    specvarcom="$specific"
    if [ -n "$specvarcom" ] && [ -n "$variant" ]; then
        specvarcom="; $variant"
    fi
    if [ -n "$specvarcom" ] && [ -n "$comments" ]; then
        specvarcom="; $comments"
    fi
    printf "\nOpcode: $opcode\n"
    is_range=0
    border="+-"
    title="| "
    content="| "
    cell_title=""
    cell_content=""
    for i in $(seq 31 -1 0 | tr ' ' ,); do
        bit=$(printf '%s' "$bits" | cut -d , -f 1)
        bits=$(printf '%s' "$bits" | cut -d , -f 2-)
        if [ -z "$bit" ]; then
            is_range=1
            continue
        fi
        if [ -n "$cell_title" ]; then
            add_cell
        fi
        cell_title=$(printf '%d' $i)
        cell_content=$bit
        is_range=0
    done
    add_cell
    title="$title |"
    content="$content |"
    border="$border-+"
    printf '%s\n' "$border"
    last_cell_size=$((${#content} - 28))
    printf "| %11s| %10s| %${last_cell_size}s|\n" "$prependage" "$appendage" "$specvarcom"
    printf '%s\n%s\n%s\n%s\n%s\n' "$border" "$title" "$border" "$content" "$border"
    IFS="$LF"
}

parse_lines() {
    while parse_line; do
        true
    done
}

grep -iE "^[0-9]+,[^,]*,,,$1," "$file" | parse_lines
