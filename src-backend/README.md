cslul backend (csbe)
====================

This is a compiler backend written in C89 / "ANSI C" (but with uint64_t).

The goal is to be able to output (without external tools):

- dynamically linked ELF and PE executables
- dynamically linked ELF and PE libraries
- object files for use in the formats above

And the goal is to add CPU ISA's in approximately this order of
priority (not decided yet, and subject to change):

- x86-64, aarch64
- x86, riscv64gc, ppc64el
- arm, armel, mips, sh4, openrisc
- others

It is created for the "cslul" compiler, and does NOT support features that
are not used in the SLUL programming language such as global variables,
initializers, etc.

It is in a very early development stage and is not yet usable.
