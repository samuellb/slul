/*

  codegen_undef.h -- Undefines all arch-specific macros

  Copyright © 2022-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#undef ADD_IMM_X
#undef ADD_R16_I8
#undef ADD_R8_I16
#undef ADD_R8_I32
#undef ADD_R8_I8
#undef ADDR_OVR
#undef ADDRSLOT_BCOND_IMM19
#undef ADDRSLOT_IMM26
#undef AX
#undef B
#undef B_COND
#undef BP
#undef BX
#undef CB
#undef CBNZ
#undef CBZ
#undef CMP
#undef CMP_I
#undef COMPLEX
#undef CSET
#undef CX
#undef DI
#undef DX
#undef FP
#undef H3
#undef H4
#undef HI
#undef IEMIT_IMM12
#undef IEMIT_IMM16
#undef IEMIT_IMM19
#undef IEMIT_IMM26
#undef IEMIT_R
#undef IEMIT_RR
#undef INT3
#undef INVERSE_COND
#undef LDR_IMM_X
#undef LDR_POST_X
#undef LDR_X
#undef LINKREG_BYTES
#undef LO
#undef LR
#undef MODRM
#undef MOV_IMM16
#undef NO_REG
#undef NOP
#undef NUM_REGSETS
#undef OP_OVR
#undef OPTY_IMM
#undef OPTY_REG
#undef OPTY_W
#undef OPTY_X
#undef PREALLOC_INSN
#undef R0
#undef R8
#undef R16
#undef R17
#undef RET
#undef RETN
#undef REXB
#undef REXR
#undef REXW
#undef REXX
#undef RS_BYTE
#undef RS_DWORD
#undef RS_FLOAT
#undef RS_INTEGER
#undef RS_QWORD
#undef RS_WORD
#undef SI
#undef SIMPLE
#undef SIMPLE_INPUTS
#undef SP
#undef STACKALIGN
#undef STR_IMM_X
#undef STR_PRE_X
#undef STR_X
#undef SUB
#undef SUB_IMM_X
#undef SUB_R16_I8
#undef SUB_R8_I16
#undef SUB_R8_I32
#undef SUB_R8_I8
#undef TEMPREG1
#undef TEMPREG2
#undef ZR
