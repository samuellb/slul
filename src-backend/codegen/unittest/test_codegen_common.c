/*

  Unit tests of codegen_common.c

  Copyright © 2022-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "../codegen_common.c"
#include "unittest.h"
#include "codegen_testcommon.h"
#include "alltests.h"


static void test_cg_chunks(void)
{
    struct Csbe *csbe;
    struct CgCtx c;
    struct CgCtx *cg = &c;
    unsigned char *prevcode;
    struct CgCodeChunk *prevchunk;

    csbe = tnewcsbe(tnewcfg(CSBEMT_EXEC));
    init_cg(&c, csbe);

    tassert(cg_morespace(&c));
    tassert(c.code.ptr != NULL);
    tassert(c.code.end != NULL);
    tsoftassert(c.code.end - c.code.ptr == CG_CHUNK_SIZE);
    tsoftassert(c.code.size == 0);
    tassert(c.code.last_chunk != NULL);
    tassert(c.code.chunks == c.code.last_chunk);
    tsoftassert(c.code.last_chunk->next == NULL);
    prevcode = c.code.ptr;
    prevchunk = c.code.last_chunk;

    IEMIT1(0x11);
    tsoftassert(prevcode[0] == 0x11);
    IEMIT2(0x22, 0x33);
    tsoftassert(prevcode[0] == 0x11);
    tsoftassert(prevcode[1] == 0x22);
    tsoftassert(prevcode[2] == 0x33);

    tassert(cg_morespace(&c));
    tassert(c.code.ptr != NULL);
    tassert(c.code.end != NULL);
    tassert(c.code.ptr != prevcode);
    tsoftassert(c.code.end - c.code.ptr == CG_CHUNK_SIZE);
    tsoftassert(c.code.size == 3);
    tassert(c.code.last_chunk != NULL);
    tsoftassert(c.code.last_chunk != prevchunk);
    tsoftassert(c.code.chunks == prevchunk);
    tsoftassert(c.code.chunks->next == c.code.last_chunk);
    tsoftassert(c.code.chunks->size == 3);
    tsoftassert(c.code.last_chunk->next == NULL);
    prevcode = c.code.ptr;

    IEMIT1(0x44);
    tsoftassert(prevcode[0] == 0x44);

    cg_lastchunk(&c);
    tsoftassert(c.code.last_chunk->size == 1);
    tsoftassert(c.code.chunks->size == 3);
    tsoftassert(c.code.size == 4);
    return;
  oom:
    tsoftfail("Should not run out of memory");
}

static void test_align_stacksize(void)
{
    struct CgCtx cg;

    cg.func_stacksize = 0;
    cg_align_stacksize(&cg, 0, 16);
    tsoftassert(cg.func_stacksize == 0);

    cg.func_stacksize = 0;
    cg_align_stacksize(&cg, 3, 4);
    tsoftassert(cg.func_stacksize == 4);

    cg.func_stacksize = 3;
    cg_align_stacksize(&cg, 2, 4);
    tsoftassert(cg.func_stacksize == 8);

    cg.func_stacksize = 8;
    cg_align_stacksize(&cg, 8, 8);
    tsoftassert(cg.func_stacksize == 16);
}

static void test_stackalloc(void)
{
    struct Csbe *csbe;
    struct CgCtx c;
    struct FuncIR *fb;
    enum VariablesInTest {
        v_arg0_uint,
        v_arg1_byte,
        v_tmp,
        v_bool,
        v_struct,
        v_array,
        v_structarray
    };
    int i;

    csbe = tnewcsbe(tnewcfg(CSBEMT_EXEC));

    CHK(csbe_set_num_funcdefs(csbe, 1));
    /* func f(uint,byte) */
    CHK(csbe_funcdef_start(csbe, 0, 2, CSBEABI_C_NORMAL, 0));
    csbe_type_void(csbe);
    csbe_type_simple(csbe, CSBET_UINT);
    csbe_type_simple(csbe, CSBET_U8);
    csbe_funcdef_end(csbe);
    CHK(csbe_defs_done(csbe));
    CHK(csbe_funcbody_start(csbe, 0, NULL, NULL, 1, /*num_vars=*/7));
    CHK(csbe_ebb_start(csbe, 0, CSBEEF_DEFAULT));

    /* Variable definitions to be stack-allocated or not.
       Note: Sizes/alignments are for "WeirdCPU". See codegen_testcommon.c */
    csbe_funcbody_paramdef(csbe, 0, v_arg0_uint);
    csbe_funcbody_paramdef(csbe, 0, v_arg1_byte);

    csbe_funcbody_vardef_start(csbe, 0, v_tmp, 1); /* This one fits in a reg */
    csbe_type_simple(csbe, CSBET_DPTR);
    csbe_funcbody_vardef_end(csbe);

    csbe_funcbody_vardef_start(csbe, CSBEVD_INTERN_HAS_ADDRESS, v_bool, CSBE_VARLANE_NONE);
    csbe_type_simple(csbe, CSBET_BOOL); /* size=3, align=1 */
    csbe_funcbody_vardef_end(csbe);

    csbe_funcbody_vardef_start(csbe, 0, v_struct, CSBE_VARLANE_NONE);
    CHK(csbe_type_struct_start(csbe, 3, CSBEPM_CPACK));
    {
        csbe_type_simple(csbe, CSBET_BOOL); /* size=3, align=1 */
        CHK(csbe_type_struct_start(csbe, 2, CSBEPM_UNION)); /* s=8, a=8 */
        {
            csbe_type_simple(csbe, CSBET_LONG);
            csbe_type_simple(csbe, CSBET_U16);
        }
        CHK(csbe_type_struct_end(csbe));
        csbe_type_simple(csbe, CSBET_U8);   /* size=1, align=1 */
    }
    CHK(csbe_type_struct_end(csbe));
    csbe_funcbody_vardef_end(csbe);

    csbe_funcbody_vardef_start(csbe, 0, v_array, CSBE_VARLANE_NONE);
    CHK(csbe_type_array(csbe, 3)); /* size=9, align=1 */
    csbe_type_simple(csbe, CSBET_BOOL); /* s=3, a=1 */
    csbe_funcbody_vardef_end(csbe);

    csbe_funcbody_vardef_start(csbe, 0, v_structarray, CSBE_VARLANE_NONE);
    CHK(csbe_type_array(csbe, 5)); /* size=20, align=2 */
    {
        CHK(csbe_type_struct_start(csbe, 2, CSBEPM_UNION)); /* size=4, align=2 */
        {
            csbe_type_simple(csbe, CSBET_U16);  /* s=2, a=2 */
            csbe_type_simple(csbe, CSBET_BOOL); /* s=3, a=1 */
        }
        CHK(csbe_type_struct_end(csbe));
    }
    csbe_funcbody_vardef_end(csbe);

    /* Use variables to prevent them from being optimized out */
    for (i = v_bool; i <= v_structarray; i++) {
        CHK(csbe_op_start(csbe, CSBEO_ADDRLOCAL));
        csbe_operand_var_in(csbe, i, 0);
        csbe_operand_var_out(csbe, v_tmp, 0);
        csbe_op_end(csbe);
    }
    init_cg(&c, csbe);
    fb = csbe->funcbody;

    /* First, check that the struct FuncIR is sane */
    CHK(csbe_funcbody_end(csbe));
    tassert(cg_stack_alloc_vars(&c, fb) == 1);
    tassert(fb != NULL);
    tassert(fb->num_vars == 7);
    tassert((fb->vars[v_arg0_uint].flags & CSBEVD_INTERN_FUNCPARAM) != 0);
    tassert((fb->vars[v_arg1_byte].flags & CSBEVD_INTERN_FUNCPARAM) != 0);
    tassert((fb->vars[v_tmp].flags & CSBEVD_INTERN_FUNCPARAM) == 0);
    tassert(fb->vars[v_tmp].type.simple_kind == CSBET_DPTR);
    tassert(fb->vars[v_bool].type.simple_kind == CSBET_BOOL);
    tassert(fb->vars[v_struct].type.is_struct);
    tassert(fb->vars[v_array].type.is_array);
    tassert(fb->vars[v_structarray].type.is_struct);
    tassert(fb->vars[v_structarray].type.is_array);

    /* Check the stack allocations are correct.
       Note: This is for "WeirdCPU". See codegen_testcommon.c */
    tsoftassert(c.vars[v_arg0_uint].size  == 4);
    tsoftassert(c.vars[v_arg0_uint].align == 2);

    tsoftassert(c.vars[v_arg1_byte].size  == 1);
    tsoftassert(c.vars[v_arg1_byte].align == 1);

    tsoftassert(c.vars[v_tmp].size      == 4);
    tsoftassert(c.vars[v_tmp].align     == 4);
    tsoftassert(!c.vars[v_tmp].has_stack_space);

    tsoftassert(c.vars[v_bool].size      == 3);
    tsoftassert(c.vars[v_bool].align     == 1);
    if (tsoftassert(c.vars[v_bool].has_stack_space)) {
        tsoftassert(c.vars[v_bool].stackoffs == 0);
    }

    tsoftassert(c.vars[v_struct].size      == 24);
    tsoftassert(c.vars[v_struct].align     == 8);
    if (tsoftassert(c.vars[v_struct].has_stack_space)) {
        tsoftassert(c.vars[v_struct].stackoffs == 8);
    }

    tsoftassert(c.vars[v_array].size      == 9);
    tsoftassert(c.vars[v_array].align     == 1);
    if (tsoftassert(c.vars[v_array].has_stack_space)) {
        tsoftassert(c.vars[v_array].stackoffs == 32);
    }

    tsoftassert(c.vars[v_structarray].size      == 20);
    tsoftassert(c.vars[v_structarray].align     == 2);
    if (tsoftassert(c.vars[v_structarray].has_stack_space)) {
        tsoftassert(c.vars[v_structarray].stackoffs == 42);
    }
}

const TestFunctionInfo tests_codegen_common[] = {
    TEST_INFO(test_cg_chunks)
    TEST_INFO(test_align_stacksize)
    TEST_INFO(test_stackalloc)
    TEST_END
};
