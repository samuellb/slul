/*

  Unit tests of x86.c

  Copyright © 2022 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "../x86.c"
#include "unittest.h"
#include "codegen_testcommon.h"
#include "alltests.h"

static void test_x86_iemit(void)
{
    IEMIT_TEST_START
    IEMIT_CHK(OP_R_I8(SUB, SP, 32), "\x83\xEC\x20");
    IEMIT_CHK(OP_R_I8(ADD, SP, 32), "\x83\xC4\x20");
    IEMIT_CHK(OP_R_I32(SUB, SP, 0x210), "\x81\xEC\x10\x02\x00\x00");
    IEMIT_CHK(OP_R_I32(ADD, SP, 0x210), "\x81\xC4\x10\x02\x00\x00");
    IEMIT_CHK(OP_R_I8(ADD, AX, 5), "\x83\xC0\x05");
    IEMIT_CHK(OP_R_I8(AND, SP, 0xF0), "\x83\xE4\xF0");
    IEMIT_CHK(OP_R_I8(XOR, BP, 0x1F), "\x83\xF5\x1F");
    IEMIT_CHK(OP_R_I8(CMP, AX, 0xFF), "\x83\xF8\xFF");
    IEMIT_CHK(MOV_FROM_PTR_DISP8(AX, BP, +0x8), "\x8B\x45\x08");
    IEMIT_CHK(MOV_FROM_PTR_DISP8(BX, BP, -0x4), "\x8B\x5D\xFC");
    IEMIT_CHK(MOV_FROM_PTR_SIB(BX, SCALE_1X,NO_INDEX,SP), "\x8B\x1C\x24");
    IEMIT_CHK(MOV_FROM_PTR_SIB(DX, SCALE_1X,NO_INDEX,SP), "\x8B\x14\x24");
    IEMIT_CHK(MOV_FROM_PTR_SIB(BX, SCALE_1X,BX,AX), "\x8B\x1C\x18");
    IEMIT_CHK(MOV_FROM_PTR_SIB(BX, SCALE_2X,AX,SP), "\x8B\x1C\x44");
    IEMIT_CHK(MOV_R_R(CX, SP), "\x89\xE1");
    IEMIT_CHK(XOR_R_R(AX, AX), "\x31\xC0");
    IEMIT_CHK(XOR_R_R(CX, AX), "\x31\xC1");
    IEMIT_CHK(XOR_R_R(DX, BP), "\x31\xEA");
    IEMIT_CHK(XOR_R_R(DI, DI), "\x31\xFF");
    IEMIT_CHK(PUSH_R(CX), "\x51");
    IEMIT_CHK(PUSH_I8(0), "\x6A\x00");
    IEMIT_CHK(PUSH_I8(0x78), "\x6A\x78");
    IEMIT_CHK(POP(SI), "\x5E");
    IEMIT_CHK(RET, "\xC3");
    IEMIT_TEST_END
}

static void test_x86_64_iemit(void)
{
    IEMIT_TEST_START
    IEMIT_CHK(XOR_R_R(R9, R9), /*prefix+*/"\x31\xC9");
    IEMIT_TEST_END
}

const TestFunctionInfo tests_x86[] = {
    TEST_INFO(test_x86_iemit)
    TEST_INFO(test_x86_64_iemit)
    TEST_END
};
