/*

  Common definitions for codegen unit tests

  Copyright © 2022-2023 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef CODEGEN_TESTCOMMON_H
#define CODEGEN_TESTCOMMON_H

#include "../../unittest/testcommon.h"
#include "../codegen_common.h"


#define IEMIT_TEST_BUFSIZE 17 /* max size for a single instruction + 1 */
#define IEMIT_TEST_START                                 \
        unsigned char buffer[IEMIT_TEST_BUFSIZE];        \
        struct CgCtx cg_data;                            \
        struct CgCtx *cg = &cg_data;
#define IEMIT_CHK(emit_expr, expected) do {              \
        static const unsigned char correct[] = expected; \
        iemit_testinit(cg, buffer);                      \
        emit_expr;                                       \
        tassert(cg->code.end == buffer+IEMIT_TEST_BUFSIZE); \
        tassert(cg->code.end[-1] == 0xAA);               \
        iemit_chk(cg, correct, sizeof(correct)-1,        \
                __FILE__, __LINE__, IEMIT_TEST_BUFSIZE); \
    } while (0)
#define IEMIT_TEST_END       \
    return;                  \
  oom:                       \
    tfail("Unexpected OoM");

#define CODEGEN_CHK(expected) \
    iemit_chk(cg, (unsigned char *)(expected), sizeof(expected)-1, \
              __FILE__, __LINE__, CG_CHUNK_SIZE)

void init_cg(struct CgCtx *cg, struct Csbe *csbe);
void iemit_testinit(struct CgCtx *cg, unsigned char *buffer);
void iemit_chk(struct CgCtx *cg,
               const unsigned char *correct, int correct_len,
               const char *testfile, int testline,
               int chunksize);
void dump_to_objfile(struct Csbe *csbe, const char *filename);

#endif
