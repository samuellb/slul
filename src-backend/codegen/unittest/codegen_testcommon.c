/*

  Common definitions for codegen unit tests

  Copyright © 2022-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "unittest.h"
#include "codegen_testcommon.h"
#include <stdio.h>
#include <string.h>


static const unsigned short weirdcpu_typesizes[NUM_TYPEKINDS] = {
    /* CSBET_BOOL  */   3,
    /* CSBET_INT   */   4,
    /* CSBET_UINT  */   4,
    /* CSBET_LONG  */   8,
    /* CSBET_ULONG */   8,
    /* CSBET_I8    */   1,
    /* CSBET_U8    */   1,
    /* CSBET_I16   */   2,
    /* CSBET_U16   */   2,
    /* CSBET_I32   */   4,
    /* CSBET_U32   */   4,
    /* CSBET_I64   */   8,
    /* CSBET_U64   */   8,
    /* CSBET_F32   */   4,
    /* CSBET_F64   */   8,
    /* CSBET_SSIZE */   6,
    /* CSBET_USIZE */   6,
    /* CSBET_DPTR  */   4,
    /* CSBET_DPTR_ALIASED */ 4,
    /* CSBET_DPTR_THREADED */ 4,
    /* CSBET_FPTR  */ 5
};

static const unsigned short weirdcpu_typealign[NUM_TYPEKINDS] = {
    /* CSBET_BOOL  */   1,
    /* CSBET_INT   */   2,
    /* CSBET_UINT  */   2,
    /* CSBET_LONG  */   8,
    /* CSBET_ULONG */   8,
    /* CSBET_I8    */   1,
    /* CSBET_U8    */   1,
    /* CSBET_I16   */   2,
    /* CSBET_U16   */   2,
    /* CSBET_I32   */   2,
    /* CSBET_U32   */   2,
    /* CSBET_I64   */   8,
    /* CSBET_U64   */   8,
    /* CSBET_F32   */   4,
    /* CSBET_F64   */   8,
    /* CSBET_SSIZE */   2,
    /* CSBET_USIZE */   2,
    /* CSBET_DPTR  */   4,
    /* CSBET_DPTR_ALIASED */ 4,
    /* CSBET_DPTR_THREADED */ 4,
    /* CSBET_FPTR  */ 8
};

void init_cg(struct CgCtx *cg, struct Csbe *csbe)
{
    struct ArchInfo *arch;
    cg->csbe = csbe;
    if (csbe->cfg.num_arch) {
        /* Arch-specific test */
        tassert(csbe->cfg.last_arch != NULL);
        arch = &csbe->cfg.last_arch->archinfo;
    } else {
        /* Arch-generic test. Use strange (but allowed) values.
           "DeathStation 9000" */
        arch = allocp(cg->csbe, sizeof(struct ArchInfo));
        tassert(arch != NULL);
        arch->reg_size = 4;
        arch->tempreg_count = 3;
        arch->savedreg_count = 1;
        arch->paramreg_count = 2;
        memcpy(arch->types_sizes, weirdcpu_typesizes, sizeof(arch->types_sizes));
        memcpy(arch->types_align, weirdcpu_typealign, sizeof(arch->types_align));
    }
    cg->arch = arch;
    cg->code.chunks = NULL;
    cg->code.last_chunk = NULL;
    cg->code.ptr = NULL;
    cg->code.end = NULL;
    cg->code.size = 0;
    cg->func_stacksize = 0;
}

void iemit_testinit(struct CgCtx *cg, unsigned char *buffer)
{
    cg->code.ptr = buffer;
    cg->code.end = buffer + IEMIT_TEST_BUFSIZE;
    memset(buffer, 0xAA, IEMIT_TEST_BUFSIZE);
}

void iemit_chk(struct CgCtx *cg,
               const unsigned char *correct, int correct_len,
               const char *testfile, int testline, int chunksize)
{
    const unsigned char *codestart;
    if (cg->code.end - cg->code.ptr != chunksize - correct_len) {
        tsoftfail_(testfile, testline, "Wrong length of instruction(s)", 0);
        if (!quiet) {
            tprintf("Expected: %d, but was %d\n", correct_len,
                    chunksize-(cg->code.end - cg->code.ptr));
        }
        return;
    }
    codestart = cg->code.end - chunksize;
    if (memcmp(codestart, correct, correct_len)) {
        tsoftfail_(testfile, testline, "Emitted bytes are wrong", 0);
        if (!quiet) {
            const unsigned char *c = correct;
            int left;
            tprintf("Expected: ");
            for (left = correct_len; left; left--) {
                tprintf("%02x ", *(c++));
            }
            tprintf("\nActual:   ");
            c = codestart;
            for (left = correct_len; left; left--) {
                tprintf("%02x ", *(c++));
            }
            tprintf("\n");
        }
    }
}

void dump_to_objfile(struct Csbe *csbe, const char *filename)
{
    unsigned char *bytes;
    size_t size, numwritten;
    int res;
    enum CsbeErr e;
    FILE *dumpfile;

    e = csbe_output(csbe, 0, &bytes, &size, NULL, NULL);
    tassert(e == CSBEE_OK);

    dumpfile = fopen(filename, "wb");
    if (dumpfile == NULL) {
        free(bytes);
        tfail("Failed to generate output");
    }
    numwritten = fwrite(bytes, size, 1, dumpfile);
    free(bytes);
    res = fclose(dumpfile);
    tassert(numwritten == 1);
    tassert(res == 0);
}
