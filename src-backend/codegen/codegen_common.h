/*

  codegen_common.h -- Definitions for the code generators

  Copyright © 2022-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef CSBE_CODEGEN_COMMON_H
#define CSBE_CODEGEN_COMMON_H

#include "../csbe_internal.h"
#include "codegen_case.h"


struct SymInfo;

#define CG_CHUNK_SIZE 1024
#define CG_CHUNK_SIZEBITS 10

struct CgCodeChunk {
    struct CgCodeChunk *next;
    unsigned short size;
    unsigned char code[CG_CHUNK_SIZE];
};

/** An address/offset that is not known yet, and will be filled in later.
    This is used for jumps within the compilation unit. */
struct CgAddrSlot {
    struct CgAddrSlot *next;
    struct CgCodeChunk *chunk;
    unsigned offset_in_chunk : CG_CHUNK_SIZEBITS;
    unsigned addrtype : 4;
    uint32 location;
};
#define INVALID_OFFSET ((uint32)-1L)
#define INVALID_OFFSET_BYTE 0xFFU

enum CgRelocKind {
    CG_RK_FUNCCALL,
    CG_RK_FUNCADDR,
    CG_RK_DATAADDR,
    CG_RK_DATALOAD,
    CG_RK_GOTPC
    /* Note: The defines below need to be adjusted when addign values */
};
#define CG_RELOC_KIND_BITS 3
#define CG_RELOC_KIND_MASK 0x7
#define CG_NUM_RELOC_KINDS (CG_RK_GOTPC+1)
#define CG_TARGET_ID_BITS (UINTSIZE-CG_RELOC_KIND_BITS)

#define CG_IS_CODE_RELOC(re) \
        ((re)->kind == CG_RK_FUNCCALL || (re)->kind == CG_RK_FUNCADDR)
#define CG_IS_DATA_RELOC(re) \
        ((re)->kind == CG_RK_DATAADDR || (re)->kind == CG_RK_DATALOAD)

/** A relocation that can't be resolved at code-generation time.
    Used for external symbols. */
struct CgRelocEntry {
    struct CgRelocEntry *next;
    /** Byte offset in the machine code */
    uint32 location;
    /** A CgRelocKind value */
    unsigned kind : CG_RELOC_KIND_BITS;
    /**< ID of the CsbeFuncdef or CsbeDatadef */
    unsigned target_id : CG_TARGET_ID_BITS;
    /**< Addend, if the target platform uses addends */
    int addend;
};

/**< Special values for target_id for built-in functions */
#define CG_SPECIALFUNC(n) \
    ((((unsigned)-1) & ~(CG_RELOC_KIND_MASK<<CG_TARGET_ID_BITS) & ~0xF) | (n))
#define FUNCID_LIBC_INIT CG_SPECIALFUNC(0)
#define CG_IS_SPECIALFUNC(n) ((n) >= FUNCID_LIBC_INIT)

struct CgCode {
    struct CgCodeChunk *chunks, *last_chunk;
    unsigned char *ptr, *end;
    uint32 size;
};

/* XXX consider doing per-architecture register allocation */
struct CgVar {
    unsigned size;
    unsigned stackoffs;
    unsigned align;
    unsigned has_stack_space : 1;
};

struct CgCtx {
    struct Csbe *csbe;
    struct ArchInfo *arch;
    struct CgCode code;
    /* Forward references that will be filled in later: */
    struct CgAddrSlot *jumps_to_epilogue; /**< Jumps to the func epilogue */
    struct CgAddrSlot **relative_jumps; /**< Jumps within the current func */
    struct CgAddrSlot **internal_calls; /**< Jumps to other(later) funcs */
    struct CgAddrSlot *start_helper_calls; /**< Calls to helper from _start */
    /* Offsets in the machine code: */
    uint32 *ebb_offsets;  /**< Offsets to EBB's in current function */
    uint32 *func_offsets; /**< Offsets to each function */
    uint32 startcode_len;
    uint32 helper_offset;
    uint32 got_vaddr; /**< Virtual address of Global Offset Table */
    /* Relocations */
    struct CgRelocEntry *relocs, **reloc_nextptr;
    struct SymInfo *syminfos, **syminfo_nextptr;
    struct SymInfo **sym_by_func_id, *libc_init;
    struct SymInfo **sym_by_data_id;
    unsigned import_sym_count, import_func_count, import_data_count;
    unsigned export_sym_count;
    unsigned rodata_present;
    unsigned num_relocs;

    /* Per-function information */
    struct CgVar *vars;
    uint32 func_stacksize;
    uint32 regsave_stackoffset, regsave_stackend;
    unsigned alloced_calleesaved; /**< Number of alloc'd callee-saved regs */
    unsigned current_param_reg;
    const struct CsbeType *current_paramtype;
    /** 1 if processing the last IR op in a function */
    unsigned is_last_op : 1;
};

#define EBB_ITER(it, f) \
    for (((it).func = (f), (it).ebb = (f)->ebbs, (it).remaining = (f)->num_ebb); \
     (it).remaining--; \
     (it).ebb++)

struct EbbIter {
    const struct FuncIR *func;
    struct Ebb *ebb;
    unsigned remaining;
};

#define IR_OP_ITER(it, ebb_iter) \
    for (cg_ir_op_iter_init(&(it), &(ebb_iter)); \
     cg_ir_op_iter_valid(&(it)); \
     cg_ir_op_iter_next(&(it)))

struct IrOpIter {
    const struct FuncIR *func;
    const struct Op *op;
    unsigned current_num;
    unsigned end_num;
};

#define IS_LAST_OP(ebb_iter, ir_op_iter) \
    (cg_ir_op_iter_is_last(&(ir_op_iter)) && !(ebb_iter).remaining)


#define PRE_EMIT(n) \
    unsigned char *code = cg->code.ptr; \
    if (UNLIKELY(!code || cg->code.end - code < (n))) { \
        code = cg_morespace(cg); \
        if (UNLIKELY(!code)) goto oom; \
    }
#define POST_EMIT(n)        \
    cg->code.ptr = code;

#define IEMIT1(a) do {      \
        PRE_EMIT(1);        \
        *(code++) = (a);    \
        POST_EMIT(1);       \
    } while (0)
#define IEMIT2(a,b) do {    \
        PRE_EMIT(2);        \
        *(code++) = (a);    \
        *(code++) = (b);    \
        POST_EMIT(2);       \
    } while (0)
#define IEMIT3(a,b,c) do {  \
        PRE_EMIT(3);        \
        *(code++) = (a);    \
        *(code++) = (b);    \
        *(code++) = (c);    \
        POST_EMIT(3);       \
    } while (0)
#define IEMIT4(a,b,c,d) do { \
        PRE_EMIT(4);        \
        *(code++) = (a);    \
        *(code++) = (b);    \
        *(code++) = (c);    \
        *(code++) = (d);    \
        POST_EMIT(4);       \
    } while (0)
#define IEMIT5(a,b,c,d,e) do { \
        PRE_EMIT(5);        \
        *(code++) = (a);    \
        *(code++) = (b);    \
        *(code++) = (c);    \
        *(code++) = (d);    \
        *(code++) = (e);    \
        POST_EMIT(5);       \
    } while (0)
#define IEMIT6(a,b,c,d,e,f) do { \
        PRE_EMIT(6);        \
        *(code++) = (a);    \
        *(code++) = (b);    \
        *(code++) = (c);    \
        *(code++) = (d);    \
        *(code++) = (e);    \
        *(code++) = (f);    \
        POST_EMIT(6);       \
    } while (0)
/* Typically used in a "PREALLOC_INSN" macro for each codegen arch */
#define IPREALLOC(size) do { \
        PRE_EMIT(size);     \
    } while (0)

#ifdef CSBE_TRAP_ON_ERROR
#    ifdef NDEBUG
#        error "Can't mix CSBE_TRAP_ON_ERROR and NDEBUG"
#    endif
#    define ZCHK(res) assert(res)
#    define CG_INTERNAL_ERROR() cg_internal_error(cg, __FILE__, __LINE__)
#else
#    define ZCHK(res) if (UNLIKELY(!(res))) return 0
#    define CG_INTERNAL_ERROR() assert(0)
#endif

void cg_ir_op_iter_init(struct IrOpIter *it, struct EbbIter *ebb_iter);
int cg_ir_op_iter_valid(struct IrOpIter *it);
int cg_ir_op_iter_is_last(struct IrOpIter *it);
void cg_ir_op_iter_next(struct IrOpIter *it);
const struct Op *cg_lookahead(struct IrOpIter *it);

void cgcode_reset(struct CgCode *code);
unsigned char *cg_morespace(struct CgCtx *cg);
void cg_lastchunk(struct CgCtx *cg);
uint32 cg_get_position(const struct CgCtx *cg);

void cg_align_stacksize(struct CgCtx *cg, unsigned add, unsigned stackalign);
int cg_stack_alloc_vars(struct CgCtx *cg, const struct FuncIR *f);
int cg_get_type_size(const struct CgCtx *cg, const struct CsbeType *type,
                     unsigned *size_out, unsigned *align_out);
int cg_get_elemsize(const struct CgCtx *cg, const struct CsbeType *type,
                    unsigned *size_out, unsigned *align_out);
int cg_get_elem_offset(const struct CgCtx *cg, const struct CsbeType *type,
                       unsigned index, unsigned fieldnum,
                       unsigned *offset_out);
/** Adds an "address slot", i.e. an address offset that is not known yet.
    This is used for jumps within the compilation unit.
    The "address slot" is added to the following instructions.
    NOTE: PREALLOC_INSN must be called first to pre-alloc the instruction. */
int cg_add_addrslot(struct CgCtx *cg, struct CgAddrSlot **aslist,
                    unsigned addrtype);
/** Edit bytes (little endian) in the machine code */
void cg_edit_bytes_le(struct CgCtx *cg, struct CgAddrSlot *as, int offset,
                      uint64 data, uint32 length);
/** Adds a relocation to the following instructions.
    NOTE: PREALLOC_INSN must be called first to pre-alloc the instruction. */
int cg_add_reloc(struct CgCtx *cg, enum CgRelocKind reloc_kind,
                 unsigned target_sym_id, int addend);
struct SymInfo *cg_helper_func_start(struct CgCtx *cg, const char *symname);
void cg_helper_func_end(struct CgCtx *cg, struct SymInfo *sym);

void cg_internal_error(struct CgCtx *cg, const char *sourcefile, int line);

void irdump_get_codegen(struct Codegen *cgen);
void x86_get_codegen(struct Codegen *cgen);
void riscv_get_codegen(struct Codegen *cgen);
void arm_get_codegen(struct Codegen *cgen);
void aarch64_get_codegen(struct Codegen *cgen);
void mips_get_codegen(struct Codegen *cgen);
void openrisc_get_codegen(struct Codegen *cgen);
void ppc64_get_codegen(struct Codegen *cgen);
void sh4_get_codegen(struct Codegen *cgen);

#endif
