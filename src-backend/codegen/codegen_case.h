/*

  codegen_case.h -- Switch/case definitions for the code generators

  Copyright © 2023-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef CSBE_CODEGEN_CASE_H
#define CSBE_CODEGEN_CASE_H

#define CODEGEN_CASE_SIMPLE_OPS \
    case CSBEO_ADD: \
    case CSBEO_ADD_TRAP: \
    case CSBEO_SUB: \
    case CSBEO_SUB_TRAP: \
    case CSBEO_MUL: \
    case CSBEO_MUL_TRAP: \
    case CSBEO_DIV: \
    case CSBEO_DIV_TRAP: \
    case CSBEO_MOD: \
    case CSBEO_MOD_TRAP: \
    case CSBEO_BAND: \
    case CSBEO_BOR: \
    case CSBEO_BXOR: \
    case CSBEO_SHL: \
    case CSBEO_SHR: \
    case CSBEO_SHRA: \
    case CSBEO_MOVE: \
    case CSBEO_MOVE_TRAP: \
    case CSBEO_NEG: \
    case CSBEO_NEG_TRAP: \
    case CSBEO_BNOT: \
    case CSBEO_EQ: \
    case CSBEO_NEQ: \
    case CSBEO_LT: \
    case CSBEO_GT: \
    case CSBEO_LE: \
    case CSBEO_GE:

#define CODEGEN_CASE_SIMPLE_INPUTS_OPS \
    case CSBEO_LAND: \
    case CSBEO_LOR: \
    case CSBEO_LXOR: \
    case CSBEO_LNOT:

#define CODEGEN_CASE_COMPLEX_OPS \
    case CSBEO_NOP: \
    case CSBEO_JUMP: \
    case CSBEO_CONDJUMP: \
    case CSBEO_COMPAREJUMP: \
    case CSBEO_TRAP: \
    case CSBEO_CONDTRAP: \
    case CSBEO_RETURN_VOID: \
    case CSBEO_RETURN_ARG: \
    case CSBEO_CALL_START: \
    case CSBEO_CALL_ARG: \
    case CSBEO_CALL_FUNCDEF: \
    case CSBEO_CALL_FUNCVAR: \
    case CSBEO_CALL_GET_RETURN: \
    case CSBEO_CALL_DISCARD_RETURN: \
    case CSBEO_DISCARD: \
    case CSBEO_CHOICE: \
    case CSBEO_SIZEOF: \
    case CSBEO_COPY: \
    case CSBEO_CLEAR: \
    case CSBEO_ADDRELEM: \
    case CSBEO_LOADELEM: \
    case CSBEO_ADDRLOCAL: \
    case CSBEO_LOADLOCAL: \
    case CSBEO_ADDRGLOBAL: \
    case CSBEO_LOADGLOBAL: \
    case CSBEO_ADDRFUNC:


#endif
