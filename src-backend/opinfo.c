/*

  opinfo.c -- Information about all IR operations.

  Copyright © 2022-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "csbe_internal.h"

#define VAR_IN(ident)           OPERAND_VAR_IN
#define VAR_OUT(ident)          (OPERAND_VAR_OUT | OPERAND_IS_DEST)
#define VAR_OUTPTR(ident)       (OPERAND_VAR_IN | OPERAND_IS_DEST)
#define VAR_IN_OR_IMMED(ident)  OPERAND_VAR_IN_OR_IMMED
#define INDEX(ident)            OPERAND_INDEX
#define EBB(ident)              OPERAND_EBB
#define FUNCDEF(ident)          OPERAND_FUNCDEF
#define DATADEF(ident)          OPERAND_DATADEF
#define FLAGS(ident)            OPERAND_FLAGS
#define ENUM(ident)             OPERAND_ENUM
#define TYPE(ident)             OPERAND_TYPE
#define PTR(ident)              OPERAND_PTR
#define CSBE_OPDEF0(op)             { 0, { 0, 0, 0, 0, 0 } },
#define CSBE_OPDEF1(op,a)           { 1, { a, 0, 0, 0, 0 } },
#define CSBE_OPDEF2(op,a,b)         { 2, { a, b, 0, 0, 0 } },
#define CSBE_OPDEF3(op,a,b,c)       { 3, { a, b, c, 0, 0 } },
#define CSBE_OPDEF4(op,a,b,c,d)     { 4, { a, b, c, d, 0 } },
#define CSBE_OPDEF5(op,a,b,c,d,e)   { 5, { a, b, c, d, e } },
#define CSBE_OPDEF_LAST             { 0, { 0, 0, 0, 0, 0 } }
const struct OpArgInfo op_argdefs[] = {
#   include "include/csbe_ops.h"
};
const char op_names[][MAX_OP_NAMELEN] = {
#define CSBE_OPDEF0(op)             #op,
#define CSBE_OPDEF1(op,a)           #op,
#define CSBE_OPDEF2(op,a,b)         #op,
#define CSBE_OPDEF3(op,a,b,c)       #op,
#define CSBE_OPDEF4(op,a,b,c,d)     #op,
#define CSBE_OPDEF5(op,a,b,c,d,e)   #op,
#define CSBE_OPDEF_LAST             ""
#   include "include/csbe_ops.h"
};
