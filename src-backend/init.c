/*

  init.c -- Initialization for CSBE

  Copyright © 2022-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "include/csbe.h"
#include "csbe_internal.h"
#include "codegen/codegen_common.h"
#include "outformat/outformat_common.h"

#define CSBE_LATEST_API_VERSION 0

#define PTRSIZE sizeof(void *)
#define FUNCPTRSIZE sizeof(void*(*)(void))

static void *dumb_alloc(void *userctx, size_t size, size_t align)
{
    (void)userctx;
    (void)align; /* malloc uses maximum alignment */
    return malloc(size);
}

void *allocp(struct Csbe *csbe, size_t size)
{
    void *p = csbe->cfg.funcptrs.fptr_alloc(csbe->cfg.userctx, size,
                                            sizeof(void *));
    if (p) {
        return p;
    } else {
        SET_ERROR(CSBEE_OUT_OF_MEM);
        return NULL;
    }
}

void *allocpa(struct Csbe *csbe, size_t size, size_t count)
{
    void *p;
    size_t totalsize = size*count;
    if (totalsize/size != count) {
        SET_ERROR(CSBEE_OUT_OF_MEM);
        return NULL;
    }
    p = csbe->cfg.funcptrs.fptr_alloc(csbe->cfg.userctx, totalsize,
                                      sizeof(void *));
    if (p) {
        return p;
    } else {
        SET_ERROR(CSBEE_OUT_OF_MEM);
        return NULL;
    }
}

void *alloc_dptr_list(struct Csbe *csbe, size_t count)
{
    void **list, **entry;
    list = allocp(csbe, sizeof(void *)*count);
    if (!list) return NULL;
    for (entry = list; count--; ) {
        *(entry++) = NULL;
    }
    return list;
}

struct CsbeConfig *csbe_config_new(int api_version,
                                   const struct CsbeFuncPtrs *funcptrs,
                                   void *userctx)
{
    struct CsbeConfig *cfg;
    if (api_version > CSBE_LATEST_API_VERSION) return NULL;
    if (funcptrs && funcptrs->fptr_alloc) {
        cfg = funcptrs->fptr_alloc(userctx, sizeof(struct CsbeConfig),
                                   MAX(PTRSIZE, FUNCPTRSIZE));
    } else {
        cfg = malloc(sizeof(struct CsbeConfig));
    }
    if (!cfg) return NULL;
    cfg->api_version = api_version;
    cfg->userctx = userctx;
    cfg->funcptrs.fptr_alloc = dumb_alloc;
    cfg->num_arch = 0;
    cfg->last_arch = NULL;
    if (funcptrs) {
        if (funcptrs->fptr_alloc) {
            cfg->funcptrs.fptr_alloc = funcptrs->fptr_alloc;
        }
    }
    return cfg;
}

enum CsbeErr csbe_config_add_arch(struct CsbeConfig *cfg, enum CsbeCpu cpu,
                                  enum CsbeEndianness endianness,
                                  enum CsbeOsType ostype,
                                  enum CsbeOutputFormat outformat)
{
    struct ArchInfoEntry *entry;

    entry = cfg->funcptrs.fptr_alloc(cfg->userctx,
            sizeof(struct ArchInfoEntry), MAX(PTRSIZE, FUNCPTRSIZE));
    if (!entry) return CSBEE_OUT_OF_MEM;
    memset(entry->archinfo.features, 0, sizeof(entry->archinfo.features));
    /* Check endianness and CPU */
    if (endianness == CSBEEN_LITTLE_ENDIAN) entry->archinfo.big_endian = 0;
    else if (endianness == CSBEEN_BIG_ENDIAN) entry->archinfo.big_endian = 1;
    else return CSBEE_INVALID_PARAM;
    entry->archinfo.cpu = cpu;
    entry->archinfo.os = ostype;
    entry->archinfo.outformat = outformat;
    if (ostype >= CSBEOS_NUM_OSTYPES || outformat >= CSBEOS_NUM_OUTFORMATS) {
        return CSBEE_UNKNOWN_ARCH;
    }
/*
 TODO add more architectures:
    riscv
    - little endian
    arm
    mips
    or1k
    ppc64
    sh4
    - bi-endian (although or1k is generally big endian)
*/
    switch (cpu) {
    case CSBEC_IR_DUMP:
        if (endianness || ostype) return CSBEE_INVALID_PARAM;
        irdump_get_codegen(&entry->archinfo.cgen);
        break;
    case CSBEC_I386:
    case CSBEC_X86_64:
        x86_get_codegen(&entry->archinfo.cgen);
        break;
/*    case CSBEC_RV32:
    case CSBEC_RV64:
        riscv_get_codegen(&entry->archinfo.cgen);
        break;
    case CSBEC_ARM:
        arm_get_codegen(&entry->archinfo.cgen);
        break;*/
    case CSBEC_AARCH64:
        aarch64_get_codegen(&entry->archinfo.cgen);
        break;
/*    case CSBEC_MIPS:
        mips_get_codegen(&entry->archinfo.cgen);
        break;
    case CSBEC_OR1K:
        openrisc_get_codegen(&entry->archinfo.cgen);
        break;
    case CSBEC_PPC64:
        ppc64_get_codegen(&entry->archinfo.cgen);
        break;
    case CSBEC_SH4:
        sh4_get_codegen(&entry->archinfo.cgen);
        break;*/
    default:
        return CSBEE_UNKNOWN_ARCH;
    }
    if (!entry->archinfo.cgen.has_endianness(endianness))
        return CSBEE_INVALID_PARAM;
    switch ((int)outformat) {
    case CSBEOF_RAW:
        entry->archinfo.symbytes = 0;
        entry->archinfo.symnamebytes = 0;
        break;
    CASE_ELF
        entry->archinfo.symbytes = /* Maximum possible sizes below: */
            24 + /* .dynsym entry (64b) */
            24 + /* .realy.dyn entry (64b) */
            16 + /* .plt.got jump slot (aarch64) */
             8 + /* .got entry (64b) */
            24 + /* .symtab entry (64b) */
            16 + /* .gnu.hash entry (worst case: 8+4+4) */
             2;  /* 2x null terminators (strtab + dynstr) */
        entry->archinfo.symnamebytes = 2; /* strtab + dynstr */
        entry->archinfo.libbytes =
            16 + /* .dynamic DT_NEEDED entry */
             1;  /* Null terminator */;
        entry->archinfo.libnamebytes = 1;
        break;
    CASE_PE
        /* TODO */
        entry->archinfo.symbytes = 123;
        entry->archinfo.symnamebytes = 1;
        entry->archinfo.libbytes = 123;
        entry->archinfo.libnamebytes = 1;
        break;
    }
    /* Add entry */
    entry->next = cfg->last_arch;
    cfg->last_arch = entry;
    cfg->num_arch++;
    return entry->archinfo.cgen.initialize(cfg);
}

int csbe_config_arch_has_feature_state(struct CsbeConfig *cfg,
                                       int feature, int state)
{
    struct ArchInfo *arch;

    assert(cfg->last_arch != NULL);

    if (feature >= MAX_ARCH_FEATURES)
        return 0;
    arch = &cfg->last_arch->archinfo;
    return arch->cgen.has_feature_state(arch->cpu, feature, state);
}

enum CsbeErr csbe_config_arch_feature(struct CsbeConfig *cfg,
                                      int feature, int state)
{
    struct ArchInfo *arch;

    assert(cfg->last_arch != NULL);
    arch = &cfg->last_arch->archinfo;
    if (!arch->cgen.has_feature_state(arch->cpu, feature, state))
        return CSBEE_UNSUPPORTED_FEATURE_PARAM;

    if (feature < 0) { /* common features */
        switch (feature) {
        case CSBEF_COMMON_HARDFLOAT:
            arch->hardfloat = state;
            break;
        default:
            return CSBEE_UNSUPPORTED_FEATURE_PARAM;
        }
    } else {
        unsigned f = (unsigned)feature;
        unsigned bit = 1 << (f & (UINTSIZE-1));
        unsigned bs = arch->features[f/UINTSIZE];
        bs &= ~bit;
        if (state) bs |= bit;
        arch->features[f/UINTSIZE] = bs;
    }
    return CSBEE_OK;
}

void csbe_config_set_moduletype(struct CsbeConfig *cfg,
                                enum CsbeModuleType modtype)
{
    (void)cfg;
    (void)modtype;
    /* TODO */
}

void csbe_config_set_symver_export(struct CsbeConfig *cfg,
                                   enum CsbeSymverExportMode mode)
{
    (void)cfg;
    (void)mode;
    /* TODO */
}


struct Csbe *csbe_new(const struct CsbeConfig *cfg)
{
    struct Csbe *csbe = cfg->funcptrs.fptr_alloc(cfg->userctx,
            sizeof(struct Csbe), MAX(PTRSIZE, FUNCPTRSIZE));
    if (!csbe) return NULL;
    memcpy(&csbe->cfg, cfg, sizeof(struct CsbeConfig));
    csbe->error = CSBEE_OK;
    csbe->inputname = NULL;
    csbe->typedefs = NULL;
    csbe->datadefs = NULL;
    csbe->funcdefs = NULL;
    csbe->td = NULL;
    csbe->dd = NULL;
    csbe->fd = NULL;
    csbe->imported_libs = NULL;
    csbe->exported_symvers = NULL;
    csbe->exported_unversioned_syms = NULL;
    csbe->main_func = NULL;
    csbe->type = NULL;
    csbe->funcbody = NULL;
    csbe->defs_done = 0;
    csbe->num_funcs = 0;
    csbe->num_data = 0;
    csbe->max_typedef_count = 0;
    csbe->max_funcdef_count = 0;
    csbe->max_datadef_count = 0;
    if (cfg->num_arch) {
        struct ArchInfo *archs, *cur_arch;
        const struct ArchInfoEntry *archentry;
        csbe->archs = archs = allocpa(csbe, sizeof(struct ArchInfo),
                                      cfg->num_arch);
        if (!archs) return NULL;
        cur_arch = &archs[cfg->num_arch];
        for (archentry = cfg->last_arch; archentry; archentry = archentry->next) {
            cur_arch--;
            memcpy(cur_arch, &archentry->archinfo, sizeof(struct ArchInfo));
        }
    }
    return csbe;
}

/** Checks if there is an error */
enum CsbeErr csbe_get_error(const struct Csbe *csbe)
{
    return csbe->error;
}

void csbe_set_input_name(struct Csbe *csbe, const char *name)
{
    csbe->inputname = name;
}
