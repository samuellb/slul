SLUL Test Executable
====================

This is the SLUL Test Executable, which is a set of modules including an
executable application. It serves as a "black box" test of the compiler.
It is a work in progress (like the rest of SLUL).

To run the test executable, run this command from the top level directory:

    make -s check-exec

The reason why it is implemented as a single executable, is to keep the
test cycle really fast, even on slow hardware and even as the depth and
breadth of the testing increases.

There is also a unit test suite, that does inspect compiler internals,
in src-cslul/unittest/
