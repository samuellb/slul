#
# Makefile for building a few test modules
#
# Copyright © 2021-2024 Samuel Lidén Borell <samuel@kodafritt.se>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

# main.slul must come first
TESTEXEC_FILES = \
    $(srcdir)/testexec/mainapp/main.slul \
    $(srcdir)/testexec/mainapp/comments.slul \
    $(srcdir)/testexec/mainapp/definedness.slul \
    $(srcdir)/testexec/mainapp/for_loops.slul \
    $(srcdir)/testexec/mainapp/funccalls.slul \
    $(srcdir)/testexec/mainapp/generics.slul \
    $(srcdir)/testexec/mainapp/return.slul \
    $(srcdir)/testexec/mainapp/statements.slul \
    $(srcdir)/testexec/mainapp/typecompat.slul \
    $(srcdir)/testexec/mainapp/typedefs.slul \
    $(srcdir)/testexec/slul-interfaces/otherlib.slul \
    $(srcdir)/testexec/slul-interfaces/somelib.slul \
    $(srcdir)/testexec/slul-interfaces/nesteddep.slul
SOMELIB_FILES = \
    $(srcdir)/testexec/somelib/main.slul \
    $(srcdir)/testexec/somelib/some_impl_file.slul
OTHERLIB_FILES = \
    $(srcdir)/testexec/otherlib/main.slul \
    $(srcdir)/testexec/otherlib/some_impl_file.slul
NESTEDDEP_FILES = \
    $(srcdir)/testexec/nesteddep/main.slul \
    $(srcdir)/testexec/nesteddep/some_impl_file.slul
TESTEXEC_ARCH_EXECUTABLES = \
    testexec/mainapp/bin/aarch64-linux-gnu/mainapp \
    testexec/mainapp/bin/i386-linux-gnu/mainapp \
    testexec/mainapp/bin/x86_64-linux-gnu/mainapp
TESTEXEC_ARCH_LIBRARIES = \
    testexec/somelib/lib/aarch64-linux-gnu/libsomelib.so \
    testexec/somelib/lib/i386-linux-gnu/libsomelib.so \
    testexec/somelib/lib/x86_64-linux-gnu/libsomelib.so \
    testexec/otherlib/lib/aarch64-linux-gnu/libotherlib.so \
    testexec/otherlib/lib/i386-linux-gnu/libotherlib.so \
    testexec/otherlib/lib/x86_64-linux-gnu/libotherlib.so \
    testexec/nesteddep/lib/aarch64-linux-gnu/libnesteddep.so \
    testexec/nesteddep/lib/i386-linux-gnu/libnesteddep.so \
    testexec/nesteddep/lib/x86_64-linux-gnu/libnesteddep.so
TESTEXEC_BINARIES = \
    testexec/mainapp/mainapp \
    testexec/somelib/libsomelib.so \
    testexec/otherlib/libotherlib.so \
    testexec/nesteddep/libnesteddep.so \
    $(TESTEXEC_ARCH_EXECUTABLES) \
    $(TESTEXEC_ARCH_LIBRARIES)
TESTEXEC_INTERFACES = \
    -I $(srcdir)/testexec/slul-interfaces \
    -I $(src_runtime)/slul-interfaces

testexec/mainapp/mainapp: $(TESTEXEC_FILES) $(build_cslul)/cslul
	$(build_cslul)/cslul $(TESTEXEC_INTERFACES) $(srcdir)/testexec/mainapp --outdir=testexec/mainapp

testexec/somelib/libsomelib.so: $(SOMELIB_FILES) $(build_cslul)/cslul
	$(build_cslul)/cslul $(TESTEXEC_INTERFACES) $(srcdir)/testexec/somelib --outdir=testexec/somelib
testexec/otherlib/libotherlib.so: $(OTHERLIB_FILES) $(build_cslul)/cslul
	$(build_cslul)/cslul $(TESTEXEC_INTERFACES) $(srcdir)/testexec/otherlib --outdir=testexec/otherlib
testexec/nesteddep/libnesteddep.so: $(NESTEDDEP_FILES) $(build_cslul)/cslul
	$(build_cslul)/cslul $(TESTEXEC_INTERFACES) $(srcdir)/testexec/nesteddep --outdir=testexec/nesteddep

$(TESTEXEC_ARCH_EXECUTABLES): check-compile
$(TESTEXEC_ARCH_LIBRARIES): check-compile-libs

check-compile: $(TESTEXEC_FILES) $(build_cslul)/cslul
	$(build_cslul)/cslul $(TESTEXEC_INTERFACES) $(srcdir)/testexec/mainapp --outdir=testexec/mainapp --target=aarch64-linux-gnu,i386-linux-gnu,x86_64-linux-gnu
check-compile-somelib: $(SOMELIB_FILES) $(build_cslul)/cslul
	$(build_cslul)/cslul $(TESTEXEC_INTERFACES) $(srcdir)/testexec/somelib --outdir=testexec/somelib --target=aarch64-linux-gnu,i386-linux-gnu,x86_64-linux-gnu
check-compile-otherlib: $(OTHERLIB_FILES) $(build_cslul)/cslul
	$(build_cslul)/cslul $(TESTEXEC_INTERFACES) $(srcdir)/testexec/otherlib --outdir=testexec/otherlib --target=aarch64-linux-gnu,i386-linux-gnu,x86_64-linux-gnu
check-compile-nesteddep: $(NESTEDDEP_FILES) $(build_cslul)/cslul
	$(build_cslul)/cslul $(TESTEXEC_INTERFACES) $(srcdir)/testexec/nesteddep --outdir=testexec/nesteddep --target=aarch64-linux-gnu,i386-linux-gnu,x86_64-linux-gnu
check-compile-libs: check-compile-somelib check-compile-otherlib check-compile-nesteddep

check-compile-valgrind: $(TESTEXEC_FILES) $(build_cslul)/cslul
	$(MKDIR_P) testexec/mainapp_vg
	valgrind -q --leak-check=full $(VALGRIND_OPTS) $(build_cslul)/cslul $(TESTEXEC_INTERFACES) $(srcdir)/testexec/mainapp --outdir=testexec/mainapp_vg --target=aarch64-linux-gnu,i386-linux-gnu,x86_64-linux-gnu
check-compile-somelib-valgrind: $(SOMELIB_FILES) $(build_cslul)/cslul
	$(MKDIR_P) testexec/somelib_vg
	valgrind -q --leak-check=full $(VALGRIND_OPTS) $(build_cslul)/cslul $(TESTEXEC_INTERFACES) $(srcdir)/testexec/somelib --outdir=testexec/somelib_vg --target=aarch64-linux-gnu,i386-linux-gnu,x86_64-linux-gnu
check-compile-otherlib-valgrind: $(OTHERLIB_FILES) $(build_cslul)/cslul
	$(MKDIR_P) testexec/otherlib_vg
	valgrind -q --leak-check=full $(VALGRIND_OPTS) $(build_cslul)/cslul $(TESTEXEC_INTERFACES) $(srcdir)/testexec/otherlib --outdir=testexec/otherlib_vg --target=aarch64-linux-gnu,i386-linux-gnu,x86_64-linux-gnu
check-compile-nesteddep-valgrind: $(NESTEDDEP_FILES) $(build_cslul)/cslul
	$(MKDIR_P) testexec/nesteddep_vg
	valgrind -q --leak-check=full $(VALGRIND_OPTS) $(build_cslul)/cslul $(TESTEXEC_INTERFACES) $(srcdir)/testexec/nesteddep --outdir=testexec/nesteddep_vg --target=aarch64-linux-gnu,i386-linux-gnu,x86_64-linux-gnu
check-compile-libs-valgrind: check-compile-somelib-valgrind check-compile-otherlib-valgrind check-compile-nesteddep-valgrind

check-exec: testexec/mainapp/mainapp testexec/somelib/libsomelib.so testexec/otherlib/libotherlib.so testexec/nesteddep/libnesteddep.so check-ir
	# TODO actually run the executable
	#testexec/mainapp

check-exec-valgrind: $(TESTEXEC_FILES) $(build_cslul)/cslul
	$(MKDIR_P) testexec/mainapp_vg
	valgrind -q --leak-check=full $(VALGRIND_OPTS) $(build_cslul)/cslul $(TESTEXEC_INTERFACES) $(srcdir)/testexec/mainapp --outdir=testexec/mainapp_vg
	# TODO actually run the executable
	#valgrind -q --leak-check=full testexec/mainapp

check-exec-tcc-boundscheck: $(TESTEXEC_FILES) $(build_cslul)/cslul-tcc-boundscheck
	$(MKDIR_P) testexec/mainapp_tcc
	TCC_BOUNDS_WARN_POINTER_ADD=1 $(build_cslul)/cslul-tcc-boundscheck \
	    $(TESTEXEC_INTERFACES) --outdir=testexec/mainapp_tcc \
	     $(srcdir)/testexec/mainapp

check-ir: $(TESTEXEC_FILES) $(build_cslul)/cslul
	rm -f testexec/mainapp/mainapp.ir
	$(build_cslul)/cslul \
	    $(TESTEXEC_INTERFACES) --outdir=testexec/mainapp --target=ir \
	    $(srcdir)/testexec/mainapp
	grep -vE '^ *#( [^=]|$$)' $(srcdir)/testexec/mainapp/mainapp_expected.ir > testexec/mainapp/mainapp_expected.ir1
	grep -vE '^ *#( [^=]|$$)' testexec/mainapp/mainapp.ir > testexec/mainapp/mainapp.ir1
	sed -E -e 's/ *#( [^=].*)?$$//g' testexec/mainapp/mainapp_expected.ir1 > testexec/mainapp/mainapp_expected.ir2
	sed -E -e 's/ *#( [^=].*)?$$//g' testexec/mainapp/mainapp.ir1 > testexec/mainapp/mainapp.ir2
	diff testexec/mainapp/mainapp_expected.ir2 testexec/mainapp/mainapp.ir2

elflint: $(TESTEXEC_BINARIES)
	file=''; \
	$(ELFLINT) --strict -q $(TESTEXEC_BINARIES) 2>&1 | while read line; do \
	    if [ "$${line%:}" != "$$line" ]; then \
	        file="$$line"; \
        elif [ -n "$$line" ]; then \
            if [ -n "$$file" ]; then printf "\n%s\n" "$$file" >&2; file=''; fi; \
            printf "%s\n" "$$line" >&2; \
        fi; \
    done
elflint-verbose: $(TESTEXEC_BINARIES)
	$(ELFLINT) --strict -q $(TESTEXEC_BINARIES)

# gdb command to debug checking in a specific .slul file:
#    break check_funcbody if !(int)strcmp(func->filename, "/path/to/mainapp/funcchk.slul")
# gdb command to debug checking of a specific function:
#    break check_funcbody if !(int)strcmp(node_nameptr(func->ident), "bad_statements")
gdb-build-testexec: $(build_cslul)/cslul
	gdb --args $(build_cslul)/cslul $(TESTEXEC_INTERFACES) $(srcdir)/testexec/mainapp --outdir=testexec/mainapp
gdb-build-somelib: $(build_cslul)/cslul
	gdb --args $(build_cslul)/cslul $(TESTEXEC_INTERFACES) $(srcdir)/testexec/somelib --outdir=testexec/somelib
gdb-build-otherlib: $(build_cslul)/cslul
	gdb --args $(build_cslul)/cslul $(TESTEXEC_INTERFACES) $(srcdir)/testexec/otherlib --outdir=testexec/otherlib
gdb-irdump: $(build_cslul)/cslul
	gdb --args $(build_cslul)/cslul $(TESTEXEC_INTERFACES) $(srcdir)/testexec/mainapp --outdir=testexec/mainapp --target=ir

testexec-outdirs:
	$(MKDIR_P) testexec/mainapp testexec/mainapp_vg testexec/mainapp_tcc \
	           testexec/somelib testexec/somelib_vg \
	           testexec/otherlib testexec/otherlib_vg \
	           testexec/nesteddep testexec/nesteddep_vg

.PHONY: check-exec check-exec-valgrind check-exec-tcc-boundscheck \
        check-compile check-compile-valgrind \
        check-compile-somelib check-compile-otherlib check-compile-nesteddep \
        check-compile-somelib-valgrind check-compile-otherlib-valgrind check-compile-nesteddep-valgrind \
        check-compile-libs check-compile-libs-valgrind \
        check-ir \
        elflint elflint-verbose \
        testexec/mainapp/mainapp \
        gdb-build-testexec \
        testexec-outdirs testexec-clean

testexec-clean:
	$(RM_F) $(TESTEXEC_BINARIES) \
	        testexec/mainapp_tcc/mainapp \
	        testexec/mainapp_vg/bin/aarch64-linux-gnu/mainapp \
	        testexec/mainapp_vg/bin/i386-linux-gnu/mainapp \
	        testexec/mainapp_vg/bin/x86_64-linux-gnu/mainapp \
	        testexec/mainapp/mainapp_expected.ir1 testexec/mainapp/mainapp.ir1 \
	        testexec/mainapp/mainapp_expected.ir2 testexec/mainapp/mainapp.ir2
