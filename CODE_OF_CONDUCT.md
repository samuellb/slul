Code of Conduct
===============

No matter how small one's involvement is in the SLUL project,
this code of conduct always applies.

Good behaviors
--------------
Examples of good behaviors:

* Being respectful to others
* Assuming good faith in others
* Using public communications, rather than private e-mail/phone/DM's
  (unless requested)
* Accepting differing opinions and criticism from others
* Giving concrete criticism and explain the reasons
* Criticizing code/behaviors/things rather than people

Unacceptable behaviors
----------------------
Examples of unacceptable behaviors:

* Sexualized language or behaviors
* Harassment
* Intentionally trying to provoke anger
* Spreading prejudices
* Publishing others' private information, such as home addresses
* Using someone else's identity

Scope
-----
Unacceptable behaviors are still unacceptable even if conducted in private
and/or outside of the scope of the project.

Violations
----------
Violations will result a in action deemed appropriate. This will depend on
severity, frequency and intent.

The maintainer(s) of the project will act when they see a violation. But you
can also send reports to samuel@kodafritt.se . You will remain anonymous.

Copyright of this document
--------------------------
This document may be used under the Creative Commons CC-0 license
and/or the MIT-0 license. If you use it in your own project, then please
replace the following:

1. The project name in the very first paragraph
2. The e-mail address in the "Violations" section
