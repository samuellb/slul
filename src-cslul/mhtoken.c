/*

  mhtoken.c -- Tokenization of module headers

  Copyright © 2021-2023 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "internal.h"
#include "hash.h"
#include <assert.h>
#include <string.h>
#define INTERR_MHTOKEN(errnum) MAKE_INTERR(errnum, INTERRBASE_MHTOKEN)

static void mh_token_start(struct CSlul *ctx, const char *bp)
{
    if (ctx->tokcolumn == 1) { /* Assume it is an \attribute */
        ctx->toklen += 1; /* for the '\' */
    }
    token_start(ctx, bp);
}

enum CSlulModuleHeaderToken cslul_ll_next_mh_token(struct CSlul *ctx)
{
    const char *bp = ctx->buffer;
    const char *bend = ctx->bufferend;
    unsigned char ch;
    enum CSlulModuleHeaderToken ret;
    enum MHTokenState tokstate = MHTDone;

    if (ctx->reused_token.mh) {
        enum CSlulModuleHeaderToken tok = ctx->reused_token.mh;
        ctx->reused_token.mh = 0;
        return tok;
    }
    ctx->linestart = bp;
    ctx->mbtrailerbytes = 0;

    if (UNLIKELY(ctx->tokstate.mh != MHTDone)) {
        switch (ctx->tokstate.mh) {
            case MHTInNewline:      goto in_newline;
            case MHTInComment:      goto in_comment;
            case MHTInMaybeMLCommentStart1: goto in_maybe_ml_comment_start1;
            case MHTInMaybeMLCommentStart2: goto in_maybe_ml_comment_start2;
            case MHTInMaybeMLCommentEnd0: goto in_maybe_ml_comment_end0;
            case MHTInMaybeMLCommentEnd1: goto in_maybe_ml_comment_end1;
            case MHTInMaybeMLCommentEnd2: goto in_maybe_ml_comment_end2;
            case MHTAttrStart:      goto in_attribute_start;
            case MHTAttrName:       goto in_attribute_name;
            case MHTAttrSpaceStart: goto in_attribute_spacestart;
            case MHTAttrSpace:      goto in_attribute_space;
            case MHTAttrValue:      goto in_attribute_value;
            case MHTInIndentation:  goto in_indentation;
            case MHTBadCharCheck:   goto in_badchar_check;
            case MHTDone:;
        }
    }
    if (UNLIKELY(ctx->utf8state)) {
        if (bp == bend) {
            tokstate = MHTDone;
            goto buffer_end;
        }
        bp = skip_utf8(ctx, bp, bend);
    }
  nextchar:
    if (UNLIKELY(bp == bend)) {
        tokstate = MHTDone;
        goto buffer_end;
    }
    ch = *bp;
    if (UNLIKELY(ch > 127)) {
        bp = unexpected_utf8(ctx, bp, bend);
        goto in_comment;
    }
    switch (ch) {
    case '\r':
    case '\n':
      newline_start:
        bp++;
        if (LIKELY(ch == '\n')) {
          newline_end:
            ctx->line++;
            ctx->startcolumn = 1;
            ctx->mbtrailerbytes = 0;
            ctx->linestart = bp;
            if (ctx->in_multiline_comment) goto in_maybe_ml_comment_end0;
            goto nextchar;
        }
      in_newline:
        if (UNLIKELY(bp == bend)) { /* \r <end> */
            if (ctx->last_buffer) {
                error_char_offs(ctx, bp, -1, CSLUL_E_CRNEWLINE);
            }
            tokstate = MHTInNewline;
            goto buffer_end;
        } else {
            if (LIKELY(*bp == '\n')) bp++; /* \r\n */
            else error_char_offs(ctx, bp, -1, CSLUL_E_CRNEWLINE);
            goto newline_end;
        }
        break; /* unreachable */
      in_maybe_ml_comment_end0:
        if (UNLIKELY(bp == bend)) {
            tokstate = MHTInMaybeMLCommentEnd0;
            goto buffer_end;
        }
        if (*bp != '#') goto in_comment;
        bp++;
      in_maybe_ml_comment_end1:
        if (UNLIKELY(bp == bend)) {
            tokstate = MHTInMaybeMLCommentEnd1;
            goto buffer_end;
        }
        if (*bp != '}') goto in_maybe_ml_comment_start1;
        bp++;
      in_maybe_ml_comment_end2:
        if (UNLIKELY(bp == bend)) {
            tokstate = MHTInMaybeMLCommentEnd2;
            goto buffer_end;
        }
        if (*bp == '}') ctx->in_multiline_comment--;
        bp++;
        goto in_comment; /* skip to end of line */
    case ' ':
    case '\t':
        error_char(ctx, bp, CSLUL_E_INDENTEDATTR);
        bp++;
      in_indentation:
        for (;;) {
            if (bp != bend) {
                /* Tabs count as 1 char because we can't know their size.
                   GCC and clang also count tab as 1 character. */
                if (*bp == ' ' || *bp == '\t') bp++;
                else break;
            } else {
                tokstate = MHTInIndentation;
                goto buffer_end;
            }
        }
        goto nextchar;
    case '\\': {
        /* Start of an attribute */
        int status;
        mh_token_start(ctx, bp);
        bp++;
      in_attribute_start:
        if (UNLIKELY(bp == bend)) {
            tokstate = MHTAttrStart;
            goto buffer_end_noeof;
        }
        ident_start(ctx);
      in_attribute_name:
        bp = tokenize_ident(ctx, bp, bend, &status, ParseAttrName);
        if (LIKELY(status)) {
            ret = CSLUL_MHT_AttrName;
            tokstate = MHTAttrSpaceStart;
            ctx->tmplen = 0;
            goto have_token;
        }
        tokstate = MHTAttrName;
        goto buffer_end; }
    case '#':
        bp++;
        ctx->in_multiline_comment = 0;
      in_maybe_ml_comment_start1:
        if (UNLIKELY(bp == bend)) {
            tokstate = MHTInMaybeMLCommentStart1;
            goto buffer_end;
        }
        if (*bp != '{') goto in_comment;
        bp++;
      in_maybe_ml_comment_start2:
        if (UNLIKELY(bp == bend)) {
            tokstate = MHTInMaybeMLCommentStart2;
            goto buffer_end;
        }
        if (*bp == '{') {
            if (ctx->startcolumn + (bp-ctx->linestart) != 3) {
                error_linecol(ctx, CSLUL_E_MLCOMMENTNOTLINESTART, ctx->line,
                              ctx->startcolumn + (bp-ctx->linestart) - 2);
                goto in_comment;
            }
            if (!ctx->in_multiline_comment) {
                ctx->multilinecomment_startline = ctx->line;
            }
            ctx->in_multiline_comment++;
        }
        bp++;
      in_comment: /* also used for skipping to end of line on error */
        for (;;) {
            while (bp != bend && *bp >= 0x20 && *bp < 127) bp++;
            if (UNLIKELY(bp == bend)) {
                tokstate = MHTInComment;
                goto buffer_end;
            }
            ch = *bp;
            if (ch == '\n' || ch == '\r') break;
            if (ch >= 128) {
                bp = skip_utf8(ctx, bp, bend);
            } else {
                error_char(ctx, bp, CSLUL_E_INVALIDCHAR);
                bp++;
            }
        }
        goto newline_start;
    case 0: /* = invalid character */
    default:
        if (LIKELY(ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') ||
             (ch >= '0' && ch <= '9')) {
            int status;
            /* Either an attribute that is missing '\\'
               or the end of the module header */
            mh_token_start(ctx, bp);
            ident_start(ctx);
          in_badchar_check:
            bp = tokenize_ident(ctx, bp, bend, &status, ParseAttrName);
            if (LIKELY(status)) {
                ctx->tmplen = 0;
                ret = CSLUL_MHT_BareIdentifier;
                tokstate = MHTAttrSpaceStart;
                goto have_token;
            }
            tokstate = MHTBadCharCheck;
            goto buffer_end;
        } else {
            error_char(ctx, bp, CSLUL_E_INVALIDCHAR);
            bp++;
            goto in_comment;
        }
    } /* unreachable */

    /* Non-initial states */
  in_attribute_spacestart:
    if (UNLIKELY(bp == bend)) {
        tokstate = MHTAttrSpaceStart;
        goto buffer_end;
    }
    ch = *bp;
    if (UNLIKELY(ctx->utf8state)) { /* Oversized value with truncated UTF-8 */
        if (ch > 127) goto in_comment;
        else ctx->utf8state = UTF8ST_NONE;
    }
    if (UNLIKELY(ch == '\n' || ch == '\r')) goto newline_start;
    else if (UNLIKELY(ch != ' ' && ch != '\t')) {
        error_char(ctx, bp, CSLUL_E_BADATTRCHAR);
        goto in_comment;
    }
  in_attribute_space:
    for (;;) {
        if (UNLIKELY(bp == bend)) {
            tokstate = MHTAttrSpace;
            goto buffer_end;
        }
        ch = *bp;
        if (UNLIKELY(ch == '\t')) {
            error_char(ctx, bp, CSLUL_E_TAB);
        } else if (UNLIKELY(ch == '\n' || ch == '\r')) {
            /* There is at least one space character */
            error_char(ctx, bp, CSLUL_E_TRAILINGSPACE);
            goto newline_start;
        } else if (ch != ' ') break;
        bp++;
    }
    mh_token_start(ctx, bp);
    ident_start(ctx);
  in_attribute_value: {
        int status;
        bp = tokenize_ident(ctx, bp, bend, &status, ParseAttrValue);
        if (LIKELY(status)) {
            ret = CSLUL_MHT_AttrValue;
            tokstate = MHTAttrSpaceStart;
            goto have_token;
        }
        tokstate = MHTAttrValue;
        goto buffer_end;
    }

    /* These are the possible function exit states */
  buffer_end_noeof:
    if (LIKELY(ctx->last_buffer)) {
        error_char(ctx, bp, CSLUL_E_UNEXPECTEDEOF);
        ret = CSLUL_MHT_EOF;
        goto have_token;
    }
  buffer_end:
    if (ctx->last_buffer) {
        token_eof(ctx, bp);
        ret = CSLUL_MHT_EOF;
    } else {
        ret = CSLUL_MHT_NEEDDATA;
    }
  have_token:
    ctx->buffer = bp;
    ctx->startcolumn += (bp - ctx->linestart - ctx->mbtrailerbytes);
    ctx->tokstate.mh = tokstate;
    return ret;
}
