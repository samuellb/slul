/*

  platform.c -- Platform dependent code

  Copyright © 2021-2023 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#define _CSLULWINLIBC_SOURCE 1 /* for cslulwinlibc_getappdir() on Windows */

#include "internal.h"
#include "defaults.h"
#define INTERR_PLATFORM(errnum) MAKE_INTERR(errnum, INTERRBASE_PLATFORM)

#if defined(PLATFORM_NIX) || defined(PLATFORM_FAKEPOSIX)
#    include <errno.h>
#    include <stdio.h>
#    include <stdlib.h>
#    include <string.h>
#    include <sys/stat.h>
#    include <sys/types.h>
#else
#    include <stdio.h>
#    include <stdlib.h>
#    include <string.h>
#endif

#ifdef PLATFORM_NIX
#    include <fcntl.h>  /* for creat() */
#    ifndef NO_GETPWUID
#        include <unistd.h> /* for getuid() */
#        include <pwd.h>    /* for getpwuid(), struct passwd */
#    endif
#endif

static int add_system_iface_dirs(struct CSlulConfig *cfg)
{
#ifdef NO_DEFAULT_IFACEDIRS
    return cslul_config_add_iface_dir(cfg, SLULIFACEDIR);
#elif defined(PLATFORM_WIN)
    /* TODO add %APPDATA%\\slul-interfaces */
    /* TODO add %CommonProgramFiles%\\slul-interfaces */
    char *appdir = cslulwinlibc_getappdir("bin", "slul-interfaces");
    if (appdir) {
        if (!cslul_config_add_iface_dir(cfg, appdir)) {
            free(appdir);
            return 0;
        }
        cfg->alloced_appdir = appdir;
    }
    return 1;
#else
    static const char subdir_xdg[] = "/slul-interfaces";
    static const char subdir_home[] = "/.local/share/slul-interfaces";
    const char *env, *add;
    char *userpath;
    size_t envlen, addlen;
    if (strlen(USR_SLULIFACEDIR) > 0 &&
            strcmp(USR_SLULIFACEDIR, SLULIFACEDIR) &&
            !cslul_config_add_iface_dir(cfg, USR_SLULIFACEDIR))
        return 0;
    if (strlen(USRLOCAL_SLULIFACEDIR) > 0 &&
            strcmp(USRLOCAL_SLULIFACEDIR, SLULIFACEDIR) &&
            !cslul_config_add_iface_dir(cfg, USRLOCAL_SLULIFACEDIR))
        return 0;
    if (!cslul_config_add_iface_dir(cfg, SLULIFACEDIR))
        return 0;
    env = getenv("XDG_DATA_HOME");
    if (env && *env) {
        add = subdir_xdg;
        addlen = sizeof(subdir_xdg);
    } else {
        env = getenv("HOME");
#if defined(PLATFORM_NIX) && !defined(NO_GETPWUID)
        if (!env || !*env) {
            /* Fall back to looking in passwd file */
            struct passwd *pwd = getpwuid(getuid());
            if (!pwd) return 1;
            env = pwd->pw_dir;
        }
#endif
        if (env && *env) {
            add = subdir_home;
            addlen = sizeof(subdir_home);
        } else {
            return 1;
        }
    }
    envlen = strlen(env);
    userpath = cfgalloc(cfg, envlen + addlen + 1);
    if (!userpath) return 0;
    memcpy(userpath, env, envlen);
    memcpy(userpath+envlen, add, addlen);
    userpath[envlen+addlen] = '\0';
    return cslul_config_add_iface_dir(cfg, userpath);
#endif
}

static int add_iface_dirs_from_envvar(struct CSlulConfig *cfg, const char *env)
{
    char *start, *s;
    size_t len = strlen(env);
    start = cfgalloc(cfg, len+1);
    if (!start) return 0;
    memcpy(start, env, len+1);
    s = start + len;
    /* Environment variable is processed backwards (leftmost entries
       have the highest priority), and path separators are replaced
       with '\0' and the entry to the left use it as a null terminator. */
    for (;;) {
        if (*s == PATHSEP || *s == ';') {
            if (!cslul_config_add_iface_dir(cfg, s+1)) return 0;
            *s = '\0';
        } else if (s == start) {
            return cslul_config_add_iface_dir(cfg, s);
        }
        s--;
    }
}

int cslul_config_add_default_iface_dirs(struct CSlulConfig *cfg)
{
    const char *env;
    if (!add_system_iface_dirs(cfg)) return 0;
    env = getenv("SLUL_INTERFACE_PATH");
    if (env && *env) {
        return add_iface_dirs_from_envvar(cfg, env);
    }
    return 1;
}

/** Creates an executable file */
CSlulFile slul_createexec(const char *filename)
{
#if defined(PLATFORM_NIX)
    int fd = creat(filename, S_IRWXU|S_IRWXG|S_IRWXO);
    if (fd == -1) return NULL;
    return fdopen(fd, "wb");
#else
    return fopen(filename, "wb");
#endif
}

int slul_mkdir(const char *filename)
{
#if defined(PLATFORM_NIX) || defined(PLATFORM_FAKEPOSIX)
    if (mkdir(filename, S_IRWXU|S_IRWXG|S_IRWXO) == -1) {
        /* This can also happen the filename is in use by a non-directory.
           In that case, an error will be reported and handled later on. */
        return errno == EEXIST;
    } else {
        return 1;
    }
#else
    return 0;
#endif
}
