This directory contains files for fuzzing CSLUL, in order to detect bugs.

Directory contents:

* aflmain.c          - For fuzzing CSLUL in persistent mode under afl++
* base_dict.txt      - Base fuzzing dictionary
* tmp-in/            - Generated files (created by Makefile)

All test cases are licensed under the MIT license, just like the source code.
