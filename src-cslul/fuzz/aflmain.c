/*

  aflmain.c -- Special entry point for fuzzing C-SLUL

  Copyright © 2021-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "cslul.h"
#include "defaults.h"
#include <assert.h>
#include <setjmp.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

/* Looks like AFL needs these */
#include <stdio.h>
#include <unistd.h>
#include <signal.h>

__AFL_FUZZ_INIT()

/* TODO fuzzing with other files than just main.slul */
#define MAIN_SLUL ((CSlulFile)-1)
#define OUTFILE ((CSlulFile)-2)

static const unsigned char *fuzzbuff;
static const unsigned char *in_buffptr;
static size_t in_left;

/* 640K should be enough for anyone ;) */
#define MAX_MEM (640*1024)
static unsigned char cfg_memory[MAX_MEM]; /**< Not cleared between runs */
static unsigned char memory[MAX_MEM];     /**< Cleared after each run */
static unsigned char *nextalloc;
static int num_alloced;

static void *mocked_malloc(size_t size)
{
    void *p;
    size_t align;
    if (!size) return NULL;
    align = (sizeof(void*)-1) - ((((uintptr_t)nextalloc)-1) & (sizeof(void*)-1));
    if (nextalloc > &memory[MAX_MEM-size]) return NULL;
    num_alloced++;
    nextalloc += align;
    p = nextalloc;
    nextalloc += size;
    return p;
}

static void mocked_free(void *p)
{
    /* does (almost) nothing */
    if (p) num_alloced--;
}

static void *mocked_realloc(void *p, size_t size)
{
    void *newp;
    (void)p;
    /* stupid implementation that just allocates a new block */
    newp = mocked_malloc(size);
    if (!newp) return NULL;
    if (p) {
        num_alloced--; /* or it grows on each call */
        memmove(newp, p, size);
    }
    return newp;
}

static CSlulFile mocked_fopen(const char *filename, const char *mode)
{
    if (!strcmp(filename, "moduledir/main.slul")) {
        return MAIN_SLUL;
    } else if (!strcmp(mode, "rb")) {
        return (CSlulFile)NULL;
    } else {
        return OUTFILE;
    }
}

static CSlulFile mocked_createexec(const char *filename)
{
    (void)filename;
    return OUTFILE;
}

static int mocked_fclose(CSlulFile file)
{
    (void)file;
    return 0;
}

static int mocked_ferror(CSlulFile file)
{
    (void)file;
    return 0;
}

static int mocked_remove(const char *filename)
{
    (void)filename;
    return 0;
}

static size_t mocked_fwrite(const void *buffer, size_t size, size_t nmemb, CSlulFile file)
{
    (void)buffer;
    (void)size;
    (void)file;
    return nmemb;
}

static size_t mocked_fread(void *buffer, size_t size, size_t nmemb, CSlulFile file)
{
    size_t amount;
    assert(size == 1);
    assert(file == MAIN_SLUL);

    amount = nmemb <= in_left ? nmemb : in_left;
    memcpy(buffer, in_buffptr, amount);
    in_buffptr += amount;
    in_left -= amount;
    return amount;
}

static int mocked_mkdir(const char *filename)
{
    (void)filename;
    return 1;
}

static void messagetrap(const struct CSlulState *st)
{
    /* Abort on Internal Compiler Errors etc. */
    if (CSLUL_IS_INTERNAL_ERR(st->errorcode)) {
        abort();
    }
}

/*static void dry_run(struct CSlulConfig *cfg)
{
    struct CSlul *ctx;
    static const unsigned char code[] =
        "\\slul 0.0.0\n"
        "\\name test\n"
        "\n"
        "func test() -> int\n"
        "{\n"
        "    return 1+2\n"
        "}\n";
    in_buffptr = code;
    in_left = sizeof(code);

    ctx = cslul_create(cfg);
    cslul_build(ctx, "moduledir");
    cslul_free(ctx);
}*/

extern jmp_buf antioptimize;
jmp_buf antioptimize;

int main(int argc, char **argv)
{
    struct CSlulInitParams prm;
    struct CSlulConfig *cfg;

    (void)argc;
    (void)argv;
    __AFL_INIT();

    nextalloc = cfg_memory;
    prm.size = sizeof(prm);
    prm.dirsep = '/';
    prm.fptr_malloc = mocked_malloc;
    prm.fptr_free = mocked_free;
    prm.fptr_realloc = mocked_realloc;
    prm.fptr_fopen = mocked_fopen;
    prm.fptr_createexec = mocked_createexec;
    prm.fptr_fclose = mocked_fclose;
    prm.fptr_ferror = mocked_ferror;
    prm.fptr_remove = mocked_remove;
    prm.fptr_fwrite = mocked_fwrite;
    prm.fptr_fread = mocked_fread;
    prm.fptr_mkdir = mocked_mkdir;
    prm.fptr_dropprivs = NULL;

    cfg = cslul_config_create(&prm);
    assert(cfg);
    cslul_config_set_message_level(cfg, CSLUL_L_FATAL);
    cslul_config_set_message_handler(cfg, messagetrap);
    /* TODO this should probably add either all supported architectures or none */
    if (!cslul_config_add_host_arch(cfg)) assert(0);

    fuzzbuff = __AFL_FUZZ_TESTCASE_BUF;
    memset(memory, 0x7E, sizeof(memory));
    while (!setjmp(antioptimize) && __AFL_LOOP(2147483647) && !setjmp(antioptimize)) {
        struct CSlul *ctx;

        in_buffptr = fuzzbuff;
        in_left = __AFL_FUZZ_TESTCASE_LEN;
        nextalloc = memory;
        num_alloced = 0;

        ctx = cslul_create(cfg);
        cslul_build(ctx, "moduledir");
        cslul_free(ctx);

        assert(num_alloced == 0);
        memset(memory, 0x7E, nextalloc-memory);

    }
    cslul_config_free(cfg);
    return EXIT_SUCCESS;
}
