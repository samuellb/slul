#!/bin/sh -eu
#
# Script that counts the number of lines in source files
#
# Copyright © 2021-2022 Samuel Lidén Borell <samuel@kodafritt.se>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

cd $(dirname "$0")/..

getmakevar() {
    grep -F -m1 -A9999 "$1 = " src-cslul/Makefile.inc | grep -E -m1 -B9999 '[^\\]$' | \
        grep -oE '[^ ]+\.c' | \
        sed -e 's/\$[(]src_cslul[)]/src-cslul/g'
}

mainsource() {
    getmakevar CSLUL_C_FILES_EXCEPT_MAIN
    echo src-cslul/main.c
}

testsource() {
    getmakevar CSLUL_UNITTEST_C_FILES_WITHOUT_EXTERNALS
    echo src-cslul/unittest/oommain.c
}

headers="src-cslul/ast.h src-cslul/cslul.h src-cslul/defaults.h src-cslul/errors.h src-cslul/hash.h src-cslul/internal.h src-cslul/tokencase.h"
testheaders="src-cslul/unittest/alltests.h src-cslul/unittest/parsecommon.h src-cslul/unittest/testalloc.h"
#grep -F -A9999 "CSLUL_UNITTEST_C_FILES = " Makefile | grep -E -m1 -B9999 '[^\\]$'
wc -l $headers $(mainsource) $testheaders $(testsource)
printf "Whereof main source: "
wc -l $(mainsource) | tail -n 1 | tr -dc '[0-9]'
printf "\nWhereof headers:     "
wc -l $headers | tail -n 1 | tr -dc '[0-9]'
printf "\nWhereof unit tests:  "
wc -l $(testsource) $testheaders | tail -n 1 | tr -dc '[0-9]'
printf "\n - FIXME - This script counts lines in the src-cslul directory only"
echo
