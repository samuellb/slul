/*

  exprchk.c -- Expression checker

  Copyright © 2022-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "internal.h"
#include "ast.h"
#include <assert.h>
#include <string.h>
#define INTERR_EXPRCHK(errnum) MAKE_INTERR(errnum, INTERRBASE_EXPRCHK)
#define INTERR_BADEXPRTYPE      INTERR_EXPRCHK(0x01)
#define INTERR_BADUNARYOP       INTERR_EXPRCHK(0x02)
#define INTERR_BADBINARYOP      INTERR_EXPRCHK(0x03)
#define INTERR_MISSINGECERROR   INTERR_EXPRCHK(0x04)

static int check_subexpr(struct CSlul *ctx, const struct TypeRef *target_tr,
                         struct ExprNode *e, enum ExprLocation loc);

static void fixup_rpn(struct ExprRoot *root)
{
    struct ExprNode *node = root->rpn, **sourceptr = &root->rpn;
    while (node) {
        if (node->exprtype) {
            /* Node remains. Move to next node */
            sourceptr = &node->rpnnext;
        } else {
            /* Node was eliminated by constant evaluation.
               Fix up, and don't point sourceptr to the eliminated node */
            *sourceptr = node->rpnnext;
        }
        node = node->rpnnext;
    }
    sourceptr = NULL;
}

/**
 * Checks an expression.
 *
 * \param ctx        Compilation context
 * \param target_tr  Expected result type of expression. May be shallow-copied
 * \param expr       Expression to check
 * \param loc        Location of the expresion (LE* constants)
 * \return 1 if it could be checked (even on error), or 0 on fatal errors
 *         such as out of memory.
 */
int check_expr(struct CSlul *ctx, const struct TypeRef *target_tr,
               struct ExprRoot *expr, enum ExprLocation loc)
{
    int ret;
    if (UNLIKELY(expr->root == NULL)) {
        assert(ctx->has_errors);
        return 1; /* not a fatal error */
    }

    ctx->current_exprroot = expr; /* saved in ensure_identdecl_checked */
    ret = check_subexpr(ctx, target_tr, expr->root, loc);
    fixup_rpn(expr);
    return ret;
}

static int recursion_level_deeper(struct CSlul *ctx, struct ExprNode *e)
{
    if (UNLIKELY(++ctx->recursion_level >= MAX_VFY_RECURSION_DEPTH)) {
        /* TODO report starting declaration */
        error_expr(ctx, CSLUL_E_VERIFYTOODEEP, ctx->current_exprroot, e);
        return 0;
    }
    return 1;
}

/**
 * Checks that a TypeRef refers to a numeric type, and reports an error
 * if not.
 *
 * A zero return means that there is an error, but NOT a fatal error.
 */
static int require_numeric(struct CSlul *ctx, const struct TypeRef *tr,
                           struct ExprNode *e)
{
    enum CSlulErrorCode errcode;
    const struct Type *type;
    if (UNLIKELY(!tr)) {
        errcode = CSLUL_E_NUMREQUIREDBUTUNKNOWN;
        goto report_error;
    }
    type = real_type_tr(ctx, tr);
    if (UNLIKELY(!type || type->type == T_INVALID)) {
        assert(ctx->has_errors);
        return 0;
    }
    if (UNLIKELY(
            !(type->type == T_ELMNTRY &&
                BI_IS_NUMERIC(builtin_infos[type->u.builtin].type)) &&
            !(type->type == T_INTERNAL &&
                type->u.internal == IT_UnsizedInt))) {
        errcode = CSLUL_E_NUMREQUIRED;
        goto report_error;
    }
    return 1;
  report_error:
    message_set_expr(ctx, 0, CSLUL_LT_MAIN, ctx->current_exprroot, e);
    message_final(ctx, errcode);
    return 0;
}

/**
 * Checks whether two subexpressions can be compared.
 * Returns 1 if comparable.
 * Returns 0 and reports error if not.
 */
static int require_comparable(struct CSlul *ctx,
                              const struct ExprNode *a,
                              const struct ExprNode *b,
                              enum OpNum op)
{
    /* TODO */
    (void)ctx;
    (void)a->u.tr;
    (void)b->u.tr;
    (void)op; /* TODO require refs for OP_REF_IS/OP_REF_IS_NOT, and non-refs for OP_EQ/OP_NOT_EQ */
    return 1;
}

static int ensure_identdecl_checked(struct CSlul *ctx,
                                    struct ExprNode *e,
                                    struct IdentDecl *decl,
                                    enum ExprLocation loc)
{
    int ok = 1;
    if (UNLIKELY(decl->ident.state == TNS_PROCESSING)) {
        message_set_ident(ctx, 0, CSLUL_LT_MAIN, &decl->ident);
        message_final(ctx, CSLUL_E_CYCLICDECL);
        return 0;
    } else if (decl->ident.state != TNS_DONE) {
        /* Definition is not yet defined. This can only happen for
           globals (constant data) and enum values, since locals cannot
           be referenced on lines before their definition. */
        struct ExprRoot *save_exprroot = ctx->current_exprroot;
        assert(IS_TOPLEVEL_EXPRLOC(loc) || ctx->has_errors);
        if (!recursion_level_deeper(ctx, e)) return 0;
        ok = verify_tlident(ctx, decl);
        ctx->recursion_level--;
        ctx->current_exprroot = save_exprroot;
    }
    return ok;
}

/**
 * Invalidates an RPN node, so it will be substituted when the RPN gets
 * rebuilt after the expression has been checked (by fixup_rpn()).
 */
static void invalidate_rpn(struct ExprNode *invalid, struct ExprNode *substitute)
{
    invalid->exprtype = 0;
    invalid->a.expr = substitute;
}

static void replace_with(struct ExprNode *target, struct ExprNode *subnode)
{
    invalidate_rpn(target, subnode);
    target->exprtype = subnode->exprtype;
    target->op = subnode->op;
    target->is_called = subnode->is_called;
    target->misc = subnode->misc;
    target->a = subnode->a;
    target->b = subnode->b;
    target->u = subnode->u;
}

static int check_unary_op(struct CSlul *ctx, const struct TypeRef *target_tr,
                          struct ExprNode *e, enum ExprLocation loc,
                          const struct TypeRef **subexpr_tr)
{
    if (!recursion_level_deeper(ctx, e)) return 0;
    switch (e->op) {
    case OP_POS:
        if (!require_numeric(ctx, target_tr, e)) return 1;
        if (!check_subexpr(ctx, target_tr, e->a.expr, loc)) return 0;
        /* This operation is always a no-op */
        if (e->a.expr) {
            replace_with(e, e->a.expr);
        }
        break;
    case OP_NEG:
        if (!require_numeric(ctx, target_tr, e)) return 1;
        if (!check_subexpr(ctx, target_tr, e->a.expr, loc)) return 0;
        if (e->a.expr && e->a.expr->exprtype == E_INTEGER) {
            uint64 intval = e->a.expr->a.intval;
            invalidate_rpn(e->a.expr, e);
            e->exprtype = E_INTEGER;
            e->b.intflags = (intval && !INTLITERAL_IS_NEGATIVE(*e->a.expr) ?
                                EIF_NEGATIVE : 0);
            e->a.intval = intval;
        }
        /* TODO check range */
        break;
    case OP_NOT: {
        const struct TypeRef *bool_tr = &ctx->basedefs->builtin_tr[BT_Bool];
        if (!check_subexpr(ctx, bool_tr, e->a.expr, loc)) return 0;
        if (e->a.expr && e->a.expr->exprtype == E_BOOL) {
            /* Evaluate constant expr */
            int b = e->a.expr->a.intval;
            invalidate_rpn(e->a.expr, e);
            e->exprtype = E_BOOL;
            e->a.intval = !b;
        }
        *subexpr_tr = bool_tr;
        break; }
    case OP_DEREF: {
        if (target_tr) {
            struct Type *ptrtype;
            ptrtype = aallocp(ctx, sizeof(struct Type));
            if (!ptrtype) return 0;
            PROTECT_STRUCT(*ptrtype);
            ptrtype->defflags = 0;
            ptrtype->quals = 0;
            ptrtype->type = T_REF;
            ptrtype->misc = M_ANYREF;
            ptrtype->column = 0;
            ptrtype->line = 0;
            ptrtype->u.nested = target_tr->type;
            {
                const struct TypeRef tr = nested_tr_const(ptrtype, target_tr);
                if (!check_subexpr(ctx, &tr, e->a.expr, loc)) return 0;
            }
        } else {
            const struct Type *t;
            if (!check_subexpr(ctx, NULL, e->a.expr, loc)) return 0;
            t = real_type_tr(ctx, &e->a.expr->u.tr);
            SOFTCHK(t);
            if (UNLIKELY(t->type != T_REF)) {
                error_expr(ctx, CSLUL_E_DEREFNONREF, ctx->current_exprroot, e);
                break;
            } else if (IS_REF_OPTIONAL(*t)) {
                /* This triggers a check that the value can't be 'none' */
                struct TypeRef dummytr;
                real_deref_tr(ctx, &e->a.expr->u.tr, e->a.expr, &dummytr);
            }
            e->u.tr = nested_tr_const(t->u.nested, &e->a.expr->u.tr);
            *subexpr_tr = &e->u.tr;
        }
        break; }
    case OP_REFTO: {
        /* TODO this should have a limited lifetime! */
        if (target_tr) {
            const struct Type *t = real_type_tr(ctx, target_tr);
            SOFTCHK(t);
            if (UNLIKELY(t->type != T_REF)) {
                error_expr(ctx, CSLUL_E_TARGETNOTREF, ctx->current_exprroot, e);
                break;
            }
            SOFTCHK(t->u.nested);
            {
                struct TypeRef tr = nested_tr_const(t->u.nested,
                                                    target_tr);
                tr.quals |= Q_TAKEADDR;
                if (!check_subexpr(ctx, &tr, e->a.expr, loc)) return 0;
            }
            {
                struct Type *ptrtype;
                ptrtype = aallocp(ctx, sizeof(struct Type));
                if (!ptrtype) return 0;
                PROTECT_STRUCT(*ptrtype);
                /* TODO can't this just use "*ptrtype = *t;" + clear misc ? */
                ptrtype->defflags = 0;
                ptrtype->quals = 0;
                ptrtype->type = T_REF;
                ptrtype->misc = M_NORMAL;
                ptrtype->column = 0;
                ptrtype->line = 0;
                ptrtype->u.nested = e->a.expr->u.tr.type;
                e->u.tr = root_tr_const(ptrtype);
            }
            *subexpr_tr = &e->u.tr;
        } else {
            error_expr(ctx, CSLUL_E_TARGETNOTREF, ctx->current_exprroot, e);
        }
        break; }
    case OP_OPTIONAL:
        /* TODO */
        break;
    default:
        internal_error(ctx, INTERR_BADUNARYOP);
        return 0;
    }
  out:
    ctx->recursion_level--;
    return 1;
  soft_assert_error:
    assert(ctx->has_errors);
    goto out;
}

/**
 * Performs constant evaluation of a binary expr with constant operands.
 *
 * Returns 1 if successful or 0 on error. Evalulation error is NOT
 * a fatal error.
 */
static int eval_integer_op(struct CSlul *ctx, struct ExprNode *e)
{
    uint64 ia = e->a.expr->a.intval;
    uint64 ib = e->b.expr->a.intval;
    uint64 res;
    unsigned na = INTLITERAL_IS_NEGATIVE(*e->a.expr);
    unsigned nb = INTLITERAL_IS_NEGATIVE(*e->b.expr);
    unsigned resflags;
    enum CSlulErrorCode errcode;
    if (e->op == OP_ADD || e->op == OP_SUB) {
        if (e->op == OP_SUB) nb = !nb;
        if (na == nb) {
            res = ia + ib;
            if (res < ia) goto too_large;
        } else if (ia >= ib) {
            res = ia - ib;
        } else {
            res = ib - ia;
            na = !na;
        }
        resflags = na ? EIF_NEGATIVE : 0;
    } else if (e->op == OP_MUL) {
        res = ia * ib;
        if (ia != 0 && res/ia != ib) goto too_large;
        resflags = (na == nb ? 0 : EIF_NEGATIVE);
    } else if (e->op == OP_DIV) {
        if (ib == 0) goto div_by_zero;
        res = ia / ib;
        resflags = (na == nb ? 0 : EIF_NEGATIVE);
    } else if (e->op == OP_MOD) {
        if (ib == 0) goto div_by_zero;
        res = ia % ib;
        if (na != nb && res) {
            res = ib - res;
        }
        resflags = (!nb ? 0 : EIF_NEGATIVE);
    } else assert(0);
    /* Negative zero is not allowed for integers */
    if (!res && resflags) resflags = 0;
    e->exprtype = E_INTEGER;
    e->a.intval = res;
    e->b.intflags = resflags;
    return 1;
  too_large:
    errcode = CSLUL_E_NUMBERTOOLARGE;
    goto report_error;
  div_by_zero:
    errcode = CSLUL_E_DIVBYZERO;
  report_error:
    error_expr(ctx, errcode, ctx->current_exprroot, e);
    return 0;
}

static int check_binary_op(struct CSlul *ctx, const struct TypeRef *target_tr,
                           struct ExprNode *e, enum ExprLocation loc,
                           const struct TypeRef **subexpr_tr)
{
    if (!recursion_level_deeper(ctx, e)) return 0;
    switch (e->op) {
    case OP_ADD:
    case OP_SUB:
    case OP_MUL:
    case OP_DIV:
    case OP_MOD:
        if (!require_numeric(ctx, target_tr, e)) return 1;
        if (target_tr) {
            const struct Type *t;
            CHK(check_subexpr(ctx, target_tr, e->a.expr, loc));
            CHK(check_subexpr(ctx, target_tr, e->b.expr, loc));

            t = real_type_tr(ctx, target_tr);
            SOFTCHK(t);
            if (((t->type==T_ELMNTRY && BT_IS_INTEGER(t->u.builtin)) ||
                    (t->type==T_INTERNAL && t->u.internal==IT_UnsizedInt)) &&
                e->a.expr->exprtype == E_INTEGER &&
                e->b.expr->exprtype == E_INTEGER) {
                /* Evaluate constant expr */
                struct ExprNode *orig_a = e->a.expr, *orig_b = e->b.expr;
                if (eval_integer_op(ctx, e)) {
                    invalidate_rpn(orig_a, e);
                    invalidate_rpn(orig_b, e);
                }
            }
            /* TODO constant evaluation of floating point */
            /* TODO check range */
        }
        break;
    case OP_AND:
    case OP_OR: {
        const struct TypeRef *bool_tr = &ctx->basedefs->builtin_tr[BT_Bool];
        CHK(check_subexpr(ctx, bool_tr, e->a.expr, loc));
        CHK(check_subexpr(ctx, bool_tr, e->b.expr, loc));
        if (e->a.expr->exprtype == E_BOOL &&
            e->b.expr->exprtype == E_BOOL) {
            /* Evaluate constant expr */
            int ba = e->a.expr->a.intval;
            int bb = e->b.expr->a.intval;
            int res = e->op == OP_AND ? (ba&bb) : (ba|bb);
            invalidate_rpn(e->a.expr, e);
            invalidate_rpn(e->b.expr, e);
            e->exprtype = E_BOOL;
            e->a.intval = res;
        }
        /* TODO evaluate "x or false" etc. */
        *subexpr_tr = bool_tr;
        break; }
    case OP_REF_IS:
    case OP_REF_IS_NOT:
    case OP_EQ:
    case OP_NOTEQ: {
        int const_equal;
        SOFTCHK(e->a.expr);
        if (e->b.expr && e->b.expr->exprtype == E_NONE) {
            /* Allow e.g. "v != none" and "v == none".
               Without this special handling, it would have given an error
               because "v" lacks a target type (because it's on the left
               side), and in that case it can't be checked that the subexpr
               allows 'none' values (because the type isn't known). */
            ctx->current_nonecheck_expr = e->a.expr;
        }
        CHK(check_subexpr(ctx, NULL, e->a.expr, loc));
        *subexpr_tr = &ctx->basedefs->builtin_tr[BT_Bool];
        SOFTCHK(e->b.expr);
        if (LIKELY(e->a.expr->u.tr.type)) { /*or check_subexpr reports error*/
            /* TODO check that:
                    - ref_is/ref_is_not are only used with refs/funcrefs
                    - ==/!= are only used with non-refs/funcrefs*/
            CHK(check_subexpr(ctx, &e->a.expr->u.tr, e->b.expr, loc));
            require_comparable(ctx, e->a.expr, e->b.expr, e->op);
            /* TODO check that the condition can be true, or report warning */
        }
        /* Evaluate constant expr */
        if (e->a.expr->exprtype == E_INTEGER &&
            e->b.expr->exprtype == E_INTEGER) {
            unsigned aneg = INTLITERAL_IS_NEGATIVE(*e->a.expr);
            unsigned bneg = INTLITERAL_IS_NEGATIVE(*e->b.expr);
            if (aneg != bneg) {
                const_equal = 0;
                goto replace_with_bool;
            }
            goto compare_absval;
        }
        if (e->a.expr->exprtype == E_BOOL &&
            e->b.expr->exprtype == E_BOOL) {
            uint64 a, b;
          compare_absval:
            a = e->a.expr->a.intval;
            b = e->b.expr->a.intval;
            const_equal = (a == b);
          replace_with_bool:
            invalidate_rpn(e->a.expr, e);
            invalidate_rpn(e->b.expr, e);
            e->exprtype = E_BOOL;
            e->a.intval = (const_equal == (e->op == OP_EQ));
        }
        break; }
    case OP_LESS:
    case OP_LEQ:
    case OP_GREATER:
    case OP_GEQ: {
        struct TypeRef tr = root_tr((struct Type *)
                &INTERNAL_TYPE(IT_UnsizedInt)); /* FIXME could be a float also... */
        /* TODO allow things like a+b<x without specifying a type for a+b
                how?
                - can promote to a larger type (except int64 vs uint64)
                  if highbound(a)+highbound(b)<int64_max
                - can use overflow detection if supported by the CPU
                - if the terms are unsigned:
                    tmp <- a+b,  tmp<0 || tmp>
                Note: regardsless of which method is used, it is
                necessary(?) to put negative terms on one side
                and positive terms on the other side.
                If a term can be either positive or negative, then
                it is necessary to check which side it should go to
                at runtime!
                - overflow at the "greater side" = true
                - overflow at the "less side" = error (->trap)
                */
        SOFTCHK(e->a.expr);
        CHK(check_subexpr(ctx, &tr, e->a.expr, loc));
        /* FIXME this won't let typescopes be used in the right-hand side, like in == and != expressions  */
        SOFTCHK(e->b.expr);
        CHK(check_subexpr(ctx, &tr, e->b.expr, loc));
        /* TODO check that the condition can be true, or report warning */
        if (e->a.expr->exprtype == E_INTEGER &&
            e->b.expr->exprtype == E_INTEGER) {
            /* Evaluate constant expr */
            uint64 a = e->a.expr->a.intval;
            uint64 b = e->b.expr->a.intval;
            unsigned aneg = INTLITERAL_IS_NEGATIVE(*e->a.expr);
            unsigned bneg = INTLITERAL_IS_NEGATIVE(*e->b.expr);
            int res;
            if (aneg != bneg) {
                a = bneg; /* Make "a" larger (non-zero) if b<0 */
                b = aneg;
            } else if (aneg) {
                a = ~a; /*  2 > 1,  but -2 < -1 */
                b = ~b;
            }
            if (e->op == OP_LESS)          { res = a <  b; }
            else if (e->op == OP_LEQ)      { res = a <= b; }
            else if (e->op == OP_GREATER)  { res = a >  b; }
            else if (e->op == OP_GEQ)      { res = a >= b; }
            else assert(0);
            invalidate_rpn(e->a.expr, e);
            invalidate_rpn(e->b.expr, e);
            e->exprtype = E_BOOL;
            e->a.intval = res;
        }
        *subexpr_tr = &ctx->basedefs->builtin_tr[BT_Bool];
        break; }
    case OP_ASSIGN:
        if (UNLIKELY(IS_CONST_EXPRLOC(loc))) {
            error_expr(ctx, CSLUL_E_CONSTEXPRBADOP, ctx->current_exprroot, e);
            break;
        }
        /* Left operand decides the type */
        SOFTCHK(e->a.expr);
        /* TODO left subexpr must be a writable l-value */
        e->a.expr->u.tr = root_tr(NULL); /* in case of errors */
        ctx->current_assign_lvalue = e->a.expr;
        CHK(check_subexpr(ctx, NULL, e->a.expr, loc));
        ctx->current_assign_lvalue = NULL;
        *subexpr_tr = &e->a.expr->u.tr;
        CHK(check_subexpr(ctx, *subexpr_tr, e->b.expr, loc));
        /* AFTER the assignment, the left side can be marked as assigned */
        if (e->a.expr && e->a.expr->exprtype == E_IDENT) {
            struct IdentDecl *decl;
            decl = (struct IdentDecl *)e->a.expr->a.ident;
            SOFTCHK(decl);
            if (IS_IDENT_LOCAL(decl)) {
                /* TODO assigning a non-var local variable may only happen once */
                CHK(var_assigned(ctx, (struct VarDef *)decl, e->b.expr));
            }
        }
        break;
    case OP_ADDASSIGN:
    case OP_SUBASSIGN:
    case OP_MULASSIGN:
    case OP_DIVASSIGN:
        if (UNLIKELY(IS_CONST_EXPRLOC(loc))) {
            error_expr(ctx, CSLUL_E_CONSTEXPRBADOP, ctx->current_exprroot, e);
            break;
        }
        /* Left operand decides the type */
        SOFTCHK(e->a.expr);
        /* TODO left subexpr must be a writable l-value */
        if (!check_subexpr(ctx, NULL, e->a.expr, loc)) return 0;
        if (!require_numeric(ctx, &e->a.expr->u.tr, e)) return 1;
        *subexpr_tr = &e->a.expr->u.tr;
        if (!check_subexpr(ctx, *subexpr_tr, e->b.expr, loc)) return 0;
        break;
    case OP_MEMBER: /* stored in AST as E_FIELD, not E_BINARYOP */
    default:
        internal_error(ctx, INTERR_BADBINARYOP);
        return 0;
    }
    ctx->recursion_level--;
    return 1;
  soft_assert_error:
    if (!ctx->has_errors) goto missing_error;
    return 1;
  assert_error:
    if (!ctx->has_errors) goto missing_error;
    return 0;
  missing_error:
    internal_error(ctx, INTERR_MISSINGECERROR);
    return 0;
}

static int is_taking_address(const struct TypeRef *tr)
{
    return tr && (tr->quals & Q_TAKEADDR) != 0;
}

static int is_const_evaluated(struct IdentDecl *decl, enum ExprLocation loc)
{
    unsigned exprtype;

    if (!decl->u.initval) return 0;
    CHECK_STRUCT(*decl->u.initval);

    /* The identifier must not be modifiable */
    if (!IS_CONST_QUALS(decl->type.quals)) return 0;

    /* The initial value must be a compile-time constant */
    if (!decl->u.initval->rpn) return 0;
    if (decl->u.initval->is_computed) return 0;

    /* Constant array/struct values become references to a
       "datadef" at the IR generation stage */
    exprtype = decl->u.initval->root->exprtype;
    if (exprtype == E_ARRAY || exprtype == E_STRUCT) {
        /* Array/struct values in const locations MUST be
           const-expr eliminated. */
        /* TODO also allow array/struct values to be
                eliminated in non-const locations */
        return IS_CONST_EXPRLOC(loc);
    }

    return 1;
}

static int check_identexpr(struct CSlul *ctx, struct ExprNode *e,
                           struct IdentDecl *decl, enum ExprLocation loc,
                           const struct TypeRef **tr_out,
                           const struct TypeRef *target_tr)
{
    /* TODO get constraints */
    if (decl->type.type == T_IMPORTED) {
        decl = (struct IdentDecl *)decl->type.u.ident;
        assert(decl->type.type != T_IMPORTED);
    }
    if (!ensure_identdecl_checked(ctx, e, decl, loc)) return 1;
    CHECK_STRUCT(*decl);
    e->u.tr = root_tr(&decl->type);
    if (tr_out) *tr_out = &e->u.tr;
    if (UNLIKELY(!IS_IDENT_DEFINED(decl))) {
        e->u.tr = root_tr(NULL);
        goto soft_assert_error;
    }
    /* Perform constant evaluation.
       Functions (pure & inlineable) could be evaluated in check_subexpr */
    if (!IS_FUNC_DECL(decl)) { /* Data */
        if (!is_taking_address(target_tr) && is_const_evaluated(decl, loc)) {
            /* FIXME will array expressions have multiple RPN nodes or not? */
            struct ExprNode *initval, *save_rpnnext;
            unsigned save_lineoffs, save_column;
            struct TypeRef save_tr;
            /*assert(!decl->u.initval->rpn->rpnnext);*/ /* TODO enable this once constexpr is fully done */
            initval = decl->u.initval->root;
            SOFTCHK(initval);
            save_lineoffs = e->line_offset;
            save_column = e->column;
            save_rpnnext = e->rpnnext;
            save_tr = e->u.tr; /* for enum values */
            CHECK_STRUCT(*initval);
            *e = *initval;
            /* The line/column offset is relative to the ExprRoot */
            e->line_offset = save_lineoffs;
            e->column = save_column;
            e->rpnnext = save_rpnnext;
            e->u.tr = save_tr;
            SOFTCHK(initval->u.tr.type != NULL);
        } else if (UNLIKELY(IS_CONST_EXPRLOC(loc))) {
            message_set_expr(ctx, 0, CSLUL_LT_MAIN, ctx->current_exprroot, e);
            message_set_ident(ctx, 1, CSLUL_LT_DEFINITION, &decl->ident);
            message_final(ctx, CSLUL_E_IDENTNOTCOMPILETIME);
        } else {
            ctx->current_exprroot->is_computed = 1;
        }
    }
    return 1;
  soft_assert_error:
    if (!ctx->has_errors) {
        internal_error(ctx, INTERR_MISSINGECERROR);
    }
    return 1;
}

static int check_fieldinit(struct CSlul *ctx, const struct ExprNode *e,
                           const struct FieldOrParamEntry *field,
                           const struct FieldOrParamList *fieldlist,
                           enum CSlulErrorCode err_wrong_order,
                           enum CSlulErrorCode err_not_found)
{
    const struct TreeNode *fieldident;
    const char *exprstr;
    size_t len;

    if (UNLIKELY(!e->b.expr)) return 0;
    fieldident = &field->f.vardef.decl.ident;
    exprstr = e->b.unbound_ident;
    len = e->misc;
#ifdef SLUL_DEBUG
    assert(len == strlen(exprstr));
#endif
    /* The ident is supposed to be equal, so don't bother with the hash */
    if (UNLIKELY(!ident_equals(fieldident, exprstr, len))) {
        HashCode hash = hash_str(exprstr, len);
        enum CSlulErrorCode errcode =
            (tree_search(ctx, fieldlist->fields_root,
                         hash, len, exprstr) != NULL ?
                err_wrong_order : err_not_found);

        message_set_expr_text(ctx, 0, CSLUL_LT_MAIN,
                              ctx->current_exprroot, e, exprstr, len);
        message_final(ctx, errcode);
        return 0;
    }
    return 1;
}

static int expr_is_const(struct ExprNode *e)
{
    switch ((int)e->exprtype) {
    case E_INTEGER:
    case E_FLOAT:
    case E_BOOL:
    case E_NONE:
        return 1;
    case E_ARRAY:
    case E_STRUCT:
        if (IS_COMPOUNDEXPR_CONST(e)) {
            e->misc |= COMPOUNDEXPR_NESTEDCONST;
            return 1;
        } else {
            return 0;
        }
    default:
        return 0;
    }
}

/* FIXME what if the target is a variable with a constraint? */
static int check_subexpr(struct CSlul *ctx, const struct TypeRef *target_tr,
                         struct ExprNode *e, enum ExprLocation loc)
{
    const struct TypeRef *subexpr_tr = NULL;
    const struct VarStateEntry *varstate = NULL;
    enum TypeCompatCheck assign_mode = TC_ASSIGNABLE;
    e->u.tr = root_tr(NULL);
    PROTECT_STRUCT(e->u.tr);
    switch (e->exprtype) {
    case E_UNARYOP:
        CHK(check_unary_op(ctx, target_tr, e, loc, &subexpr_tr));
        break;
    case E_BINARYOP:
        CHK(check_binary_op(ctx, target_tr, e, loc, &subexpr_tr));
        break;
    case E_CONDITIONAL:
        /* XXX */
        break;
    case E_CONDCHOICES:
        /* XXX */
        break;
    case E_IDENT: {
        struct IdentDecl *decl = (struct IdentDecl *)e->a.ident;
        SOFTCHK(decl);
        CHK(check_identexpr(ctx, e, decl, loc, &subexpr_tr, target_tr));
        SOFTCHK(subexpr_tr);
        SOFTCHK(subexpr_tr->type);
        if (IS_IDENT_LOCAL(decl) &&
                    e != ctx->current_assign_lvalue &&
                    e != ctx->current_nonecheck_expr) {
            require_assigned(ctx, (struct VarDef *)decl, e, &varstate);
        }
        break; }
    case E_TYPEIDENT: {
        struct IdentDecl *typeident;
        struct TypeDecl *tsdecl;
        if (UNLIKELY(!target_tr)) {
            error_expr(ctx, CSLUL_E_TYPESCOPENOTARGET,
                       ctx->current_exprroot, e);
            return 1;
        }
        tsdecl = get_typescope(ctx, e, target_tr); /* reports errors */
        SOFTCHK(tsdecl);
        typeident = find_typeident(ctx, tsdecl, e->a.unbound_hash, e->misc,
                                   e->b.unbound_ident,
                                   ctx->current_exprroot, e,
                                   CSLUL_E_TYPEIDENTNOTFOUND);
        if (UNLIKELY(!typeident)) break;
        CHK(check_identexpr(ctx, e, typeident, loc, &subexpr_tr, target_tr));
        if (e->exprtype == E_TYPEIDENT) { /* if not const-evaluated */
            e->a.ident = &typeident->ident;
        }
        assign_mode = TC_ASSIGNABLE_TYPEIDENT;
        break; }
    case E_INTEGER: {
        const struct Type *t = real_type_tr(ctx, target_tr);
        if (UNLIKELY(!t)) break;
        /* TODO determine type (not strictly needed, but it is good to show an error if integer literals are too large) */
        /* TODO check range */
        subexpr_tr = &INTERNAL_TR(IT_UnsizedInt);
        break; }
    case E_FLOAT: {
        const struct Type *t = real_type_tr(ctx, target_tr);
        if (UNLIKELY(!t)) break;
        /* TODO check range */
        /*subexpr_tr = &ctx->basedefs->builtin_tr[BT_Double];*/
        break; }
    case E_STRING:
        subexpr_tr = &ctx->basedefs->builtin_tr[BT_String];
        break;
    case E_BOOL:
        subexpr_tr = &ctx->basedefs->builtin_tr[BT_Bool];
        break;
    case E_NONE: {
        /* Target type must be an optional ref (or string) type */
        const struct Type *t = real_type_tr(ctx, target_tr);
        if (UNLIKELY(!t)) break;
        if (UNLIKELY((t->type != T_REF || !IS_REF_OPTIONAL(*t)) &&
                     (t->type != T_ELMNTRY || !IS_BT_OPTIONAL(*t)) &&
                     (t->type != T_FUNC || !IS_FUNCREF_OPTIONAL(*t)))) {
            error_expr(ctx, CSLUL_E_CANTTAKENONE, ctx->current_exprroot, e);
        }
        break; }
    case E_UNDEF:
        /* TODO */
        break;
    case E_ARRAY: {
        size_t numelems, elemsleft;
        const struct Type *arrtype, *elemtype;
        unsigned flags = COMPOUNDEXPR_CONST;
        e->misc = 0;
        if (UNLIKELY(!target_tr)) break;
        /* TODO generic types */
        arrtype = real_type_tr(ctx, target_tr);
        if (UNLIKELY(!arrtype)) break;
        if (UNLIKELY(arrtype->type != T_ARRAY)) {
            error_expr(ctx, CSLUL_E_TARGETTYPENOTARRAY, ctx->current_exprroot, e);
            break;
        }
        /* Verify elements */
        SOFTCHK(e->a.exprlist); /* elements */
        numelems = e->a.exprlist->length;
        elemsleft = numelems;
        elemtype = (arrtype->misc == M_LONG_LENGTH ?
                &arrtype->u.arr->elemtype :
                arrtype->u.nested);
        SOFTCHK(elemtype);
        {
            const struct TypeRef elemtr = nested_tr_const(elemtype, target_tr);
            struct ExprNode **elemexpr = e->a.exprlist->elems;
            while (elemsleft--) {
                struct ExprNode *elem = *(elemexpr++);
                CHK(check_subexpr(ctx, &elemtr, elem, loc));
                flags &= expr_is_const(elem) ? COMPOUNDEXPR_CONST : 0;
            }
        }
        e->misc = flags;
        if (IS_COMPOUNDEXPR_CONST(e)) {
            ctx->num_literals++;
        }
        /* Subexpression type. This is basically the target type, but
           with the length being the number of elements. This way, the
           length can be checked by require_type_compat */
        {
            struct TypeRef *vtr; /* v = value */
            struct Type *vtype;
            struct ArrayType *varr;
            vtr = aallocp(ctx, sizeof(struct TypeRef));
            SOFTCHK(vtr);
            PROTECT_STRUCT(*vtr);
            vtype = aallocp(ctx, sizeof(struct Type));
            SOFTCHK(vtype);
            PROTECT_STRUCT(*vtype);
            varr = aallocp(ctx, sizeof(struct ArrayType));
            SOFTCHK(varr);
            PROTECT_STRUCT(*varr);
            vtype->defflags = 0;
            vtype->quals = 0;
            vtype->misc = M_LONG_LENGTH;
            vtype->type = T_ARRAY;
            vtype->column = 0;
            vtype->line = 0;
            vtype->u.arr = varr;
            varr->lengthexpr = make_number_expr(ctx, numelems, e);
            SOFTCHK(varr->lengthexpr);
            varr->elemtype = *elemtype;
            *vtr = nested_tr_const(vtype, target_tr);
            subexpr_tr = vtr;
        }
        break; }
    case E_STRUCT: {
        size_t elemsleft;
        struct TypeRef structtr;
        const struct Type *structtype;
        unsigned flags = COMPOUNDEXPR_CONST;
        e->misc = 0;
        if (UNLIKELY(!target_tr)) break;
        structtype = real_tr(ctx, target_tr, &structtr);
        if (UNLIKELY(!structtype)) break;
        if (UNLIKELY(structtype->type != T_STRUCT)) {
            if (structtype->type == T_PRIVATE) {
                ctx->must_have_errors = 1;
                break;
            }
            error_expr(ctx, CSLUL_E_TARGETTYPENOTSTRUCT,
                       ctx->current_exprroot, e);
            break;
        }
        if (UNLIKELY((structtype->quals & Q_CLOSED) == 0)) {
            error_expr(ctx, CSLUL_E_INITOPENSTRUCT,
                       ctx->current_exprroot, e);
            break;
        }
        /* Verify contents */
        SOFTCHK(e->a.exprlist);
        SOFTCHK(structtype->u.fields);
        elemsleft = e->a.exprlist->length;
        {
            struct ExprNode **elemexpr = e->a.exprlist->elems;
            struct FieldOrParamEntry *field =
                structtype->u.fields->first;
            while (elemsleft--) {
                struct ExprNode *subexpr = *(elemexpr++);
                if (UNLIKELY(!field)) {
                    error_expr(ctx, CSLUL_E_STRUCTINITTOOMANY,
                               ctx->current_exprroot, subexpr);
                    goto struct_out;
                }
                if (UNLIKELY(subexpr->exprtype != E_FIELDINIT)) {
                    error_expr(ctx, CSLUL_E_NOTAFIELDINIT,
                               ctx->current_exprroot, subexpr);
                    goto struct_out;
                }
                if (UNLIKELY(!check_fieldinit(ctx, subexpr, field,
                                              structtype->u.fields,
                                              CSLUL_E_STRUCTINITWRONGORDER,
                                              CSLUL_E_FIELDNOTFOUND))) {
                    assert(ctx->has_errors);
                    goto struct_out;
                }
                {
                    const struct TypeRef fieldtr =
                            nested_tr_const(&field->f.vardef.decl.type,
                                            &structtr);
                    CHK(check_subexpr(ctx, &fieldtr, subexpr->a.expr, loc));
                    flags &= expr_is_const(subexpr->a.expr) ?
                            COMPOUNDEXPR_CONST : 0;
                }
                field = field->next;
            }
            e->misc = flags;
            if (IS_COMPOUNDEXPR_CONST(e)) {
                ctx->num_literals++;
            }
            if (UNLIKELY(field)) {
                error_expr(ctx, CSLUL_E_STRUCTINITTOOFEW,
                           ctx->current_exprroot, e);
            }
          struct_out: ;
        }
        break; }
    case E_INDEX: {
        size_t numidx;
        /* Verify array expr */
        SOFTCHK(e->a.exprlist); /* index expr(s) */
        numidx = e->a.exprlist->length;
        SOFTCHK(numidx >= 1);
        SOFTCHK(e->b.expr);     /* array itself */
        /* Note: it is possible to access a full row in a
                 multi-dimensional array, e.g. row=matrix[0] */
        if (target_tr) {
            const struct Type *innertype = target_tr->type;
            struct Type *arrtype;
            size_t idxleft = numidx;
            do {
                arrtype = aallocp(ctx, sizeof(struct Type));
                SOFTCHK(arrtype);
                PROTECT_STRUCT(*arrtype);
                arrtype->defflags = 0;
                arrtype->quals = 0;
                arrtype->misc = M_ANY_LENGTH;
                arrtype->type = T_ARRAY;
                arrtype->column = 0;
                arrtype->line = 0;
                arrtype->u.nested = innertype;
                innertype = arrtype;
            } while (--idxleft);
            /* outermost array */
            {
                const struct TypeRef arrtr = nested_tr_const(arrtype,
                                                             target_tr);
                CHK(check_subexpr(ctx, &arrtr, e->b.expr, loc));
            }
        } else {
            CHK(check_subexpr(ctx, NULL, e->b.expr, loc));
        }
        /* The expression type (subexpr_tr) is the element type.
           The index expressions are checked at the same time. */
        {
            const struct TypeRef *usize_tr = &ctx->basedefs->builtin_tr[BT_USize];
            struct ExprNode **indexexpr = e->a.exprlist->elems;
            struct TypeRef *arrtr = aallocp(ctx, sizeof(struct TypeRef));
            SOFTCHK(arrtr);
            PROTECT_STRUCT(*arrtr);
            SOFTCHK(real_deref_tr(ctx, &e->b.expr->u.tr, e->b.expr, arrtr));
            while (numidx--) {
                const struct Type *t = arrtr->type;
                SOFTCHK(t && t->type == T_ARRAY);
                CHK(check_subexpr(ctx, usize_tr, *(indexexpr++), loc));
                /* TODO check bounds */
                /* TODO use get_array_info(t, &arrinfo, &state) */
                if (t->misc == M_LONG_LENGTH) {
                    SOFTCHK(t->u.arr);
                    arrtr->type = &t->u.arr->elemtype;
                } else {
                    arrtr->type = t->u.nested;
                }
                SOFTCHK(real_deref_tr(ctx, arrtr, NULL, arrtr));
            }
            subexpr_tr = arrtr;
            target_tr = NULL;
        }
        if (e->b.expr && e->b.expr->exprtype == E_ARRAY) {
            /* TODO Evaluate constant expr (with support for multidimensional arrays) */
        }
        break; }
    case E_CALL: {
        struct ExprNode *funcexpr = e->b.expr;
        const struct Type *functype;
        struct TypeRef func_tr;
        size_t num_args;
        ctx->current_exprroot->is_computed = 1;
        /* Check function expression */
        SOFTCHK(funcexpr);
        if (funcexpr->exprtype == E_FIELD) {
            /* Method call */
            struct ExprNode *objexpr = funcexpr->a.expr;
            const struct Type *classref;
            const struct TypeDecl *classdecl;
            const struct IdentDecl *method;
            const char *name;
            size_t namelen;
            HashCode hashcode;

            PROTECT_STRUCT(funcexpr->u.tr);
            funcexpr->u.tr.type = NULL;
            SOFTCHK(objexpr);
            CHK(check_subexpr(ctx, NULL, objexpr, loc));
            classref = objexpr->u.tr.type;
            SOFTCHK(classref);
            if (classref->type == T_REF) {
                classref = classref->u.nested;
            }
            func_tr = root_tr_const(classref);
            if (classref->type == T_GENERICSPEC) {
                SOFTCHK(bind_type_params(ctx, classref, &func_tr.prm));
                classref = classref->u.gprm->generictype;
                func_tr.type = classref;
            }
            if (UNLIKELY(classref->type != T_IDENT)) {
                SOFTCHK(classref->type != T_INVALID);
                error_expr(ctx, CSLUL_E_NOMETHODNAMESPACE,
                           ctx->current_exprroot, objexpr);
                return 1;
            }
            classdecl = get_identtype_decl(classref);
            SOFTCHK(classdecl);
            name = funcexpr->b.unbound_ident;
            namelen = funcexpr->misc;
            hashcode = hash_str(name, namelen);
            method = find_typeident(ctx, classdecl, hashcode, namelen, name,
                                    ctx->current_exprroot, funcexpr,
                                    CSLUL_E_METHODNOTFOUND);
            if (UNLIKELY(!method)) return 1;
            else if (UNLIKELY(method->type.type != T_METHOD)) {
                SOFTCHK(method->type.type != T_INVALID);
                error_expr(ctx, method->type.type == T_FUNC ?
                            CSLUL_E_METHODCONSTR : CSLUL_E_NOTAMETHOD,
                           ctx->current_exprroot, funcexpr);
                return 1;
            }
            functype = &method->type;
            funcexpr->b.bound_method = method;
        } else if (funcexpr->exprtype == E_TYPEIDENT) {
            /* Constructor call */
            struct IdentDecl *typeident;
            struct TypeDecl *tsdecl;
            const char *name;
            size_t namelen;
            HashCode hashcode;

            if (UNLIKELY(!target_tr)) {
                PROTECT_STRUCT(e->u.tr);
                e->u.tr.type = NULL;
                error_expr(ctx, CSLUL_E_TYPESCOPENOTARGET,
                           ctx->current_exprroot, funcexpr);
                return 1;
            }
            tsdecl = get_typescope(ctx, funcexpr, target_tr);
            SOFTCHK(tsdecl);
            name = funcexpr->b.unbound_ident;
            namelen = funcexpr->misc;
            hashcode = funcexpr->a.unbound_hash;
            typeident = find_typeident(ctx, tsdecl, hashcode, namelen, name,
                                       ctx->current_exprroot, funcexpr,
                                       CSLUL_E_TYPEIDENTNOTFOUND);
            if (UNLIKELY(!typeident)) return 1;
            CHK(check_identexpr(ctx, funcexpr, typeident, loc, NULL, NULL));
            func_tr = funcexpr->u.tr;
            functype = func_tr.type;
            funcexpr->a.ident = &typeident->ident;
            /* TODO get type parameters from target type */
        } else if (funcexpr->exprtype == E_INTEGER ||
                   funcexpr->exprtype == E_FLOAT ||
                   funcexpr->exprtype == E_STRING ||
                   funcexpr->exprtype == E_NONE ||
                   funcexpr->exprtype == E_UNDEF ||
                   funcexpr->exprtype == E_ARRAY ||
                   funcexpr->exprtype == E_STRUCT ||
                   funcexpr->exprtype == E_BOOL) {
           goto not_callable; /* avoid "Type of expression is not clear" */
        } else {
            /* Plain function call */
            CHK(check_subexpr(ctx, NULL, funcexpr, loc));
            functype = real_deref_tr(ctx, &funcexpr->u.tr, funcexpr, &func_tr);
            SOFTCHK(functype);
            if (UNLIKELY(functype->type != T_FUNC)) {
                SOFTCHK(functype->type != T_INVALID);
              not_callable:
                error_expr(ctx, CSLUL_E_NOTCALLABLE, ctx->current_exprroot,
                           funcexpr);
                return 1;
            }
        }
        SOFTCHK(functype->u.func);
        /* Check argument count */
        num_args = e->a.exprlist->length;
        if (UNLIKELY(num_args != functype->u.func->params.count)) {
            enum CSlulErrorCode errcode;
            if (num_args > functype->u.func->params.count) {
                errcode = CSLUL_E_TOOMANYFUNCARGS;
                num_args = functype->u.func->params.count;
            } else {
                errcode = CSLUL_E_TOOFEWFUNCARGS;
            }
            /* TODO it would be nice to show the count and/or paramter lists here */
            error_expr(ctx, errcode, ctx->current_exprroot, funcexpr);
        }
        /* Check function arguments */
        if (num_args) {
            const struct FieldOrParamEntry *param;
            struct ExprNode **argptr;
            SOFTCHK(e->a.exprlist->elems);
            argptr = &e->a.exprlist->elems[0];
            param = functype->u.func->params.first;
            while (num_args--) {
                struct TypeRef param_tr;
                struct ExprNode *arg = *argptr;
                /* Handle .param=value syntax */
                if (arg->exprtype == E_FIELDINIT) {
                    if (UNLIKELY(!check_fieldinit(ctx, arg, param,
                                                  &functype->u.func->params,
                                                  CSLUL_E_PARAMWRONGORDER,
                                                  CSLUL_E_PARAMNOTFOUND)))
                        break;
                    arg = arg->a.expr;
                }
                /* Check argument value */
                /* XXX how should parametric types be handled here? */
                param_tr = nested_tr_const(&param->f.vardef.decl.type,
                                           &func_tr);
                CHK(check_subexpr(ctx, &param_tr, arg, loc));
                param = param->next;
                argptr++;
            }
        }
        /* Expr type = return type */
        e->u.tr = nested_tr(&functype->u.func->returntype, &func_tr);
        /* TODO do we need to substitute params? (could perhaps be done in nested_tr?) */
        subexpr_tr = &e->u.tr;
        break; }
    case E_FIELD: {
        struct FieldOrParam *field;
        struct Type *type;
        struct TypeRef struct_tr;
        const char *name;
        size_t namelen;
        HashCode hashcode;
        /* Verify struct subexpr */
        SOFTCHK(e->a.expr);
        CHK(check_subexpr(ctx, NULL, e->a.expr, loc));
        type = real_deref_tr(ctx, &e->a.expr->u.tr, e->a.expr, &struct_tr);
        SOFTCHK(type);
        if (UNLIKELY(type->type != T_STRUCT)) {
            enum CSlulErrorCode structerr = type->type == T_PRIVATE ?
                CSLUL_E_ACCESSPRIVATETYPE : CSLUL_E_FIELDNONSTRUCT;
            error_expr(ctx, structerr, ctx->current_exprroot,
                       e->a.expr);
            return 1;
        }
        /* Look up field in struct type */
        /* TODO keep the hash in the expr without increasing the struct size? */
        name = e->b.unbound_ident;
        namelen = e->misc;
        hashcode = hash_str(name, namelen);
        SOFTCHK(type->u.fields);
        SOFTCHK(type->u.fields->fields_root);
        field = (struct FieldOrParam *)tree_search(ctx,
                type->u.fields->fields_root, hashcode, namelen, name);
        if (UNLIKELY(field == NULL)) {
            message_set_expr_text(ctx, 0, CSLUL_LT_MAIN,
                                  ctx->current_exprroot, e, name, namelen);
            message_final(ctx, CSLUL_E_FIELDNOTFOUND);
            return 1;
        }
        e->b.bound_field = field;

        /* Check since-version.
           Assumption: The reference comes from the implementation
           of the main module. (Interfaces can only access interfaces,
           and interfaces can only contain closed types, and closed
           types cannot have versioned fields.) */
        if (field->sinceversions) {
            const struct CSlulDependency *dep;
            const struct Module *depmod;
            struct ClosestSincever closest = { NULL,NULL,NULL,NULL };
            const struct TypeDecl *sdecl;
            /* Dependency to check the version of */
            enum TypeOrigin origin = find_dep_of_type(ctx, &e->a.expr->u.tr,
                                                      &sdecl, &dep);
            if (origin == TYORG_MAIN_MODULE) goto skip_version_check;
            if (UNLIKELY(origin == TYORG_NOT_FOUND)) {
                assert(ctx->has_errors);
                return 1;
            }
            assert(origin == TYORG_OTHER_MODULE);
            depmod = dep->module;
            if (UNLIKELY(!depmod)) {
                assert(ctx->has_errors);
                return 1;
            }
            if (!check_decl_sinceversion(dep, depmod, field->sinceversions,
                                         &closest)) {
                enum CSlulErrorCode errcode;
                assert(closest.too_old_dep);
                message_set_expr_text(ctx, 0, CSLUL_LT_MAIN,
                                      ctx->current_exprroot, e, name, namelen);
                message_set_type(ctx, 1, CSLUL_LT_DEFINITION,
                                 sdecl, &sdecl->type);
                message_set_module(ctx, 2, CSLUL_LT_DEPENDENCY,
                                   closest.too_old_module);
                message_set_textlen(ctx, 3, CSLUL_LT_DEPENDENCY,
                        closest.too_old_dep->min_version, /*requested version*/
                        closest.too_old_dep->minverlen);
                if (closest.best_sincever) {
                    errcode = CSLUL_E_FIELDTOONEW;
                } else {
                    errcode = CSLUL_E_FIELDLATERLOWER;
                    closest.best_sincever = closest.first_sincever;
                    assert(closest.first_sincever);
                }
                message_set_ident(ctx, 4, CSLUL_LT_MINIMUM,
                          &closest.best_sincever->verstr); /*required version*/
                message_final(ctx, errcode);
                /* Continue with the too high/recent version */
            }
          skip_version_check: ;
        }

        assert(field->vardef.decl.type.type);
        /* TODO store the field pointer somewhere? */
        e->u.tr = nested_tr(&field->vardef.decl.type, &struct_tr);
        /* TODO substitute params (perhaps in nested_tr?) */
        /* TODO require that the type is not a type parameter? */
        /* TODO require that there are no unbound params? */
        subexpr_tr = &e->u.tr;
        break; }
    case E_FIELDINIT:
        error_expr(ctx, CSLUL_E_FIELDINITCONTEXT, ctx->current_exprroot, e);
        break;
    case E_THIS:
        SOFTCHK(ctx->this_tr.type);
        ctx->current_exprroot->is_computed = 1;
        subexpr_tr = &ctx->this_tr;
        break;
    default:
        SOFTCHK(e->exprtype);
        internal_error(ctx, INTERR_BADEXPRTYPE);
        return 0;
    }
    if (subexpr_tr) {
        if (target_tr) {
            ctx->current_targettype_ident =
                    ctx->current_sourcetype_ident = NULL;
            require_type_compat(ctx, target_tr, subexpr_tr,
                                e, varstate, assign_mode);
            if (subexpr_tr->type && subexpr_tr->type->type == T_INTERNAL) {
                /* e.g. IT_UnsizedInt */
                e->u.tr = *target_tr;
                SOFTCHK(e->u.tr.type);
                return 1;
            }
        }
        e->u.tr = *subexpr_tr;
        SOFTCHK(e->u.tr.type);
    } else if (target_tr) {
        e->u.tr = *target_tr;
        SOFTCHK(e->u.tr.type);
    } else {
        /* Ambiguous expression type */
        if (UNLIKELY(ctx->current_exprroot->root == e)) {
            assert(ctx->has_errors);
        } else {
            error_expr(ctx, CSLUL_E_AMBIGUOUSEXPRTYPE, ctx->current_exprroot, e);
        }
    }
    return 1;
  soft_assert_error:
    if (!ctx->has_errors) goto missing_error;
    return 1; /* 0 means out of memory / internal error */
  assert_error:
    if (!ctx->has_errors) goto missing_error;
    return 0;
  missing_error:
    internal_error(ctx, INTERR_MISSINGECERROR);
    return 0;
}
