/*

  defaults.h -- Default paths

  Copyright © 2021-2023 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef CSLUL_DEFAULTS_H
#define CSLUL_DEFAULTS_H


#ifndef PREFIX
#define PREFIX "/usr/local"
#endif

#ifndef DATADIR
#define DATADIR PREFIX "/share"
#endif

#ifndef LOCALEDIR
#define LOCALEDIR DATADIR "/locale"
#endif

#ifndef SLULIFACEDIR
#define SLULIFACEDIR DATADIR "/slul-interfaces"
#endif

#ifndef NO_DEFAULT_IFACEDIRS
    #ifndef USR_SLULIFACEDIR
    /** Searched if SLULIFACEDIR is set to something different. */
    #define USR_SLULIFACEDIR "/usr/share/slul-interfaces"
    #endif

    #ifndef USRLOCAL_SLULIFACEDIR
    /** Searched if SLULIFACEDIR is set to something different. */
    #define USRLOCAL_SLULIFACEDIR "/usr/local/share/slul-interfaces"
    #endif
#else
    #define USR_SLULIFACEDIR ""
    #define USRLOCAL_SLULIFACEDIR ""
#endif

#endif
