/*

  print_hashes.c -- Generates pre-computed hashcodes for hash.h

  Copyright © 2022-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "cslul.h"
#include "internal.h"
#include "hash.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

/** Prints a #define for a hashcode */
static void ph_ex(const char *str, const char *name)
{
    uint32 hashcode = 0;
    const char *sp;
    for (sp = str; *sp; sp++) {
        hashcode = HASH(hashcode, *sp);
    }
    printf("#define H_%s %uU\n", name, hashcode);
}

/** Prints a #define for a hashcode with automatic uppercasing */
static void ph(const char *str)
{
    const char *sp;
    char *up;
    char uppercase[100];
    assert(strlen(str) < 100);
    up = uppercase;
    for (sp = str; *sp; sp++) {
        char c = *sp;
        *(up++) = (c >= 'a' && c <= 'z' ? c-'a'+'A' : c);
    }
    *up = '\0';
    ph_ex(str, uppercase);
}

int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;

    printf("/* from mhparse.c */\n");
    ph("slul");
    ph("name");
    ph("version");
    ph("type");
    ph("repo");
    ph_ex("repo.mirror", "REPO_DOT_MIRROR");
    ph("website");
    ph("license");
    ph_ex("license.text", "LICENSE_DOT_TEXT");
    ph_ex("license.list", "LICENSE_DOT_LIST");
    ph("depends");
    ph("interface_depends");
    ph("api_def");
    ph("source");
    /* Keywords inside module attributes */
    ph("impl");
    ph("upto");
    /* "since" appears below */
    /* Module types */
    ph("internal");
    ph("library");
    ph("plugin");
    ph("libraryspec");
    ph("pluginspec");
    ph("app");
    /* Langauge versions */
    ph_ex("0.0.0", "0_0_0");
    /* Dependency flags */
    ph("optional");
    ph("nestedonly");
    ph("unstable_api");
    ph("bundled");
    /* api_def flags */
    ph("retracted");
    /* Start of non-moduleheader lines */
    ph("data");
    ph("func");
    /* "type" appears below */

    printf("/* from parse.c */\n");
    ph("not");
    ph("and");
    ph("or");
    ph("mod");
    ph("deref");
    ph("refto");
    ph("ref_is");
    ph("ref_is_not");
    /* data, func, type are already handled above */
    ph("bool");
    ph("usize");
    ph("ssize");
    ph("fileoffs");
    ph("string");
    ph("int8");
    ph("byte");
    ph("wuint8");
    ph("int16");
    ph("uint16");
    ph("wuint16");
    ph("int");
    ph("uint");
    ph("wuint");
    ph("int32");
    ph("uint32");
    ph("wuint32");
    ph("int64");
    ph("uint64");
    ph("wuint64");
    ph("ref");
    ph("own");
    ph("arena");
    ph("slot");
    ph("funcref");
    ph("noreturn");
    ph("struct");
    ph("enum");
    ph("lifetime");
    ph("since");
    ph("var");
    ph("writeonly");
    ph("aliased");
    ph("threaded");
    ph("closed");
    ph("none");
    ph("this");
    ph("undef");
    ph("false");
    ph("true");
    ph("if");
    ph("else");
    ph("while");
    ph("do");
    ph("for");
    ph("in");
    ph("loopend");
    ph("loopempty");
    ph("switch");
    ph("case");
    ph("with");
    ph("default");
    ph("subcase");
    ph("assert");
    ph("break");
    ph("continue");
    ph("goto");
    ph("return");
    printf("/* not a keyword. used in error check */\n");
    ph("void");
    printf("/* not keywords. Used to match against SlulApp.main */\n");
    ph("SlulApp");
    ph("main");
    ph("SlulExitStatus");
    ph("slulrt");

    printf("/* from unittest/test_tlverify.c */\n");
    ph("somefunc");
    ph("somefunc2");
    ph("someconst");
    ph("SomeType");
    ph("somedata");
    ph("InternalType");
    ph("PrivType");

    return EXIT_SUCCESS;
}
