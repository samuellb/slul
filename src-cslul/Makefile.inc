#
# Makefile for the cslul compiler
#
# Copyright © 2021-2024 Samuel Lidén Borell <samuel@kodafritt.se>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

# For fuzzing with afl++
# TODO CSLUL_ variable name prefix
DEFINES_AFL += --std=gnu99 $(DEFINES) $(INCLUDES) \
    -DNO_GETTEXT -DPARSEBUFFER=92 -DFUZZ_TIGHT_MODE -DARENA_SIZE=92 \
    -DCSLUL_NO_STDIO_MOCKS -fvisibility=hidden -Wall -Wextra
FUZZ_INPUT_DIR ?= $(src_cslul)/fuzz/input

# test_parse.c/parse.c/test_token.c/token.c are the slowest files to parse.
# so they goes first, to speed up parallel builds a bit.
CSLUL_TEST_OBJECTS = \
	$(CSLUL_EXTRA_TEST_OBJECTS) \
	$(build_cslul)/unittest/test_parse.o \
	$(build_cslul)/unittest/test_token.o \
	$(build_cslul)/unittest/chkcommon.o \
	$(build_cslul)/unittest/parsecommon.o \
	$(build_cslul)/unittest/test_arch.o \
	$(build_cslul)/unittest/test_arena.o \
	$(build_cslul)/unittest/test_build.o \
	$(build_cslul)/unittest/test_builtins.o \
	$(build_cslul)/unittest/test_bwrapper.o \
	$(build_cslul)/unittest/test_chkutil.o \
	$(build_cslul)/unittest/test_config.o \
	$(build_cslul)/unittest/test_context.o \
	$(build_cslul)/unittest/test_errors.o \
	$(build_cslul)/unittest/test_exprchk.o \
	$(build_cslul)/unittest/test_funcchk.o \
	$(build_cslul)/unittest/test_ir.o \
	$(build_cslul)/unittest/test_mhparse.o \
	$(build_cslul)/unittest/test_mhtoken.o \
	$(build_cslul)/unittest/test_misc.o \
	$(build_cslul)/unittest/test_platform.o \
	$(build_cslul)/unittest/test_tree.o \
	$(build_cslul)/unittest/test_tlverify.o \
	$(build_cslul)/unittest/test_typechk.o \
	$(build_cslul)/unittest/test_typecompat.o \
	$(COMMON_TEST_OBJECTS)
CSLUL_UNITTEST_OBJECTS = \
    $(CSLUL_TEST_OBJECTS) \
	$(build_cslul)/unittest/testmain.o
CSLUL_OOMTEST_OBJECTS = \
    $(CSLUL_TEST_OBJECTS) \
	$(build_cslul)/unittest/oommain.o
CSLUL_ALL_TEST_OBJECTS = \
    $(CSLUL_TEST_OBJECTS) \
	$(build_cslul)/unittest/testmain.o \
	$(build_cslul)/unittest/oommain.o \
	$(build_cslul)/dummy_cei.o
CSLUL_UNITTEST_C_FILES_WITHOUT_EXTERNALS = \
	$(src_cslul)/unittest/test_parse.c \
	$(src_cslul)/unittest/test_token.c \
	$(src_cslul)/unittest/chkcommon.c \
	$(src_cslul)/unittest/parsecommon.c \
	$(src_cslul)/unittest/test_arch.c \
	$(src_cslul)/unittest/test_arena.c \
	$(src_cslul)/unittest/test_build.c \
	$(src_cslul)/unittest/test_builtins.c \
	$(src_cslul)/unittest/test_bwrapper.c \
	$(src_cslul)/unittest/test_chkutil.c \
	$(src_cslul)/unittest/test_config.c \
	$(src_cslul)/unittest/test_context.c \
	$(src_cslul)/unittest/test_errors.c \
	$(src_cslul)/unittest/test_exprchk.c \
	$(src_cslul)/unittest/test_funcchk.c \
	$(src_cslul)/unittest/test_ir.c \
	$(src_cslul)/unittest/test_mhparse.c \
	$(src_cslul)/unittest/test_mhtoken.c \
	$(src_cslul)/unittest/test_misc.c \
	$(src_cslul)/unittest/test_platform.c \
	$(src_cslul)/unittest/test_tree.c \
	$(src_cslul)/unittest/test_tlverify.c \
	$(src_cslul)/unittest/test_typechk.c \
	$(src_cslul)/unittest/test_typecompat.c \
	$(src_cslul)/unittest/testmain.c
CSLUL_UNITTEST_C_FILES = \
    $(CSLUL_EXTRA_UNITTEST_C_FILES) \
    $(CSLUL_UNITTEST_C_FILES_WITHOUT_EXTERNALS) \
    $(COMMON_TEST_C_FILES)
CSLUL_OBJECTS = \
	$(CSLUL_EXTRA_OBJECTS) \
	$(build_cslul)/parse.o \
	$(build_cslul)/token.o \
	$(build_cslul)/arch.o \
	$(build_cslul)/arena.o \
	$(build_cslul)/build.o \
	$(build_cslul)/builtins.o \
	$(build_cslul)/bwrapper.o \
	$(build_cslul)/chkutil.o \
	$(build_cslul)/config.o \
	$(build_cslul)/context.o \
	$(build_cslul)/errors.o \
	$(build_cslul)/exprchk.o \
	$(build_cslul)/funcchk.o \
	$(build_cslul)/ir.o \
	$(build_cslul)/mhparse.o \
	$(build_cslul)/mhtoken.o \
	$(build_cslul)/misc.o \
	$(build_cslul)/platform.o \
	$(build_cslul)/tree.o \
	$(build_cslul)/tlverify.o \
	$(build_cslul)/typechk.o \
	$(build_cslul)/typecompat.o \
	$(build_backend)/libcsbe.a \
	$(build_cslul)/main.o
CSLUL_C_FILES_EXCEPT_MAIN = \
	$(src_cslul)/parse.c \
	$(src_cslul)/token.c \
	$(src_cslul)/arch.c \
	$(src_cslul)/arena.c \
	$(src_cslul)/build.c \
	$(src_cslul)/builtins.c \
	$(src_cslul)/bwrapper.c \
	$(src_cslul)/chkutil.c \
	$(src_cslul)/config.c \
	$(src_cslul)/context.c \
	$(src_cslul)/errors.c \
	$(src_cslul)/exprchk.c \
	$(src_cslul)/funcchk.c \
	$(src_cslul)/ir.c \
	$(src_cslul)/mhparse.c \
	$(src_cslul)/mhtoken.c \
	$(src_cslul)/misc.c \
	$(src_cslul)/platform.c \
	$(src_cslul)/tree.c \
	$(src_cslul)/tlverify.c \
	$(src_cslul)/typechk.c \
	$(src_cslul)/typecompat.c \
	$(CSLUL_EXTRA_C_FILES)
CSLUL_C_FILES = \
    $(CSLUL_C_FILES_EXCEPT_MAIN) \
	$(src_cslul)/main.c
CSLUL_HEADERS = \
    $(src_cslul)/ast.h \
    $(src_cslul)/backend.h \
    $(src_cslul)/cslul.h \
    $(src_cslul)/defaults.h \
    $(src_cslul)/errors.h \
    $(src_cslul)/hash.h \
    $(src_cslul)/internal.h \
    $(src_cslul)/tokencase.h \
    $(src_cslul)/unittest/alltests.h \
    $(src_cslul)/unittest/chkcommon.h \
    $(src_cslul)/unittest/parsecommon.h \
    $(src_common)/unittest/unittest.h \
    $(CSLUL_EXTRA_HEADERS)
AFL_C_FILES = \
    $(BACKEND_C_FILES) \
    $(CSLUL_C_FILES_EXCEPT_MAIN) \
    $(src_cslul)/fuzz/aflmain.c
UNITY_BUILD_IQUOTE = \
    -iquote $(src_cslul) -iquote $(src_backend) \
    -iquote $(src_backend)/codegen -iquote $(src_backend)/outformat \


CSLUL_TEXTDOMAIN = cslul
CSLUL_MOFILES = $(build_cslul)/lang/sv.mo
CSLUL_LANGUAGES = sv

# Files with translatable text
CSLUL_POTFILES = $(src_cslul)/main.c $(src_cslul)/errors.h

cslul-all: $(build_cslul)/cslul $(build_cslul)/template.pot $(MOFILES)

$(build_cslul)/arch.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h
$(build_cslul)/arena.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h
$(build_cslul)/build.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h
$(build_cslul)/builtins.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/ast.h
$(build_cslul)/bwrapper.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/backend.h $(src_backend)/include/csbe.h $(src_backend)/include/csbe_ops.h
$(build_cslul)/chkutil.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/ast.h $(src_cslul)/hash.h
$(build_cslul)/config.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h
$(build_cslul)/context.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/ast.h
$(build_cslul)/errors.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/errors.h
$(build_cslul)/exprchk.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/ast.h
$(build_cslul)/funcchk.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/ast.h
$(build_cslul)/ir.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/ast.h $(src_cslul)/backend.h $(src_backend)/include/csbe.h $(src_backend)/include/csbe_ops.h
$(build_cslul)/mhparse.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/hash.h
$(build_cslul)/mhtoken.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/hash.h
$(build_cslul)/misc.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/hash.h
$(build_cslul)/parse.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/hash.h $(src_cslul)/tokencase.h $(src_cslul)/ast.h
$(build_cslul)/platform.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/defaults.h
$(build_cslul)/token.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/hash.h $(src_cslul)/tokencase.h
$(build_cslul)/tree.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h
$(build_cslul)/tlverify.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/ast.h
$(build_cslul)/typechk.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/ast.h
$(build_cslul)/typecompat.o: $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/ast.h
$(build_cslul)/main.o: $(src_cslul)/cslul.h $(src_cslul)/defaults.h

$(build_cslul)/unittest/chkcommon.o: $(src_cslul)/unittest/parsecommon.h $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/unittest/chkcommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/parsecommon.o: $(src_cslul)/unittest/parsecommon.h $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/ast.h $(src_cslul)/hash.h $(src_cslul)/unittest/testcommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_arch.o: $(src_cslul)/arch.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/unittest/testcommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_arena.o: $(src_cslul)/arena.c $(src_cslul)/unittest/testalloc.h $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/unittest/testcommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_build.o: $(src_cslul)/build.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/unittest/testcommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_builtins.o: $(src_cslul)/builtins.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/ast.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_bwrapper.o: $(src_cslul)/bwrapper.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/backend.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_chkutil.o: $(src_cslul)/chkutil.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/ast.h $(src_cslul)/hash.h $(src_cslul)/unittest/chkcommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_config.o: $(src_cslul)/config.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/unittest/testcommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_context.o: $(src_cslul)/context.c $(src_cslul)/unittest/testalloc.h $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/ast.h $(src_cslul)/unittest/testcommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_errors.o: $(src_cslul)/errors.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/errors.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_exprchk.o: $(src_cslul)/exprchk.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/ast.h $(src_cslul)/unittest/parsecommon.h $(src_cslul)/unittest/chkcommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_funcchk.o: $(src_cslul)/funcchk.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/ast.h $(src_cslul)/unittest/parsecommon.h $(src_cslul)/unittest/chkcommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_ir.o: $(src_cslul)/ir.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/ast.h $(src_cslul)/backend.h $(src_cslul)/unittest/parsecommon.h $(src_cslul)/unittest/chkcommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_mhparse.o: $(src_cslul)/mhparse.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/hash.h $(src_cslul)/unittest/parsecommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_mhtoken.o: $(src_cslul)/mhtoken.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/hash.h $(src_cslul)/unittest/parsecommon.h $(src_cslul)/unittest/testcommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_misc.o: $(src_cslul)/misc.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/hash.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_parse.o: $(src_cslul)/parse.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/hash.h $(src_cslul)/tokencase.h $(src_cslul)/ast.h $(src_cslul)/unittest/parsecommon.h $(src_cslul)/unittest/testcommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_platform.o: $(src_cslul)/platform.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/defaults.h $(src_cslul)/unittest/testcommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_token.o: $(src_cslul)/token.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/hash.h $(src_cslul)/tokencase.h $(src_cslul)/unittest/parsecommon.h $(src_cslul)/unittest/testcommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_tree.o: $(src_cslul)/tree.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/unittest/testcommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_tlverify.o: $(src_cslul)/tlverify.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/ast.h $(src_cslul)/hash.h $(src_cslul)/unittest/testcommon.h $(src_cslul)/unittest/parsecommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_typechk.o: $(src_cslul)/typechk.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/ast.h $(src_cslul)/unittest/parsecommon.h $(src_cslul)/unittest/chkcommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/test_typecompat.o: $(src_cslul)/typecompat.c $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/ast.h $(src_cslul)/unittest/chkcommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/testmain.o: $(src_cslul)/unittest/alltests.h $(src_cslul)/unittest/testalloc.h $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/unittest/testcommon.h $(src_common)/unittest/unittest.h
$(build_cslul)/unittest/oommain.o: $(src_cslul)/unittest/alltests.h $(src_cslul)/unittest/testalloc.h $(src_cslul)/internal.h $(src_cslul)/cslul.h $(src_cslul)/unittest/testcommon.h $(src_common)/unittest/unittest.h

$(build_cslul)/cslul: $(CSLUL_OBJECTS)
	$(CC) $(INTERNCFLAGS) $(CFLAGS) $(LDFLAGS) $(CSLUL_OBJECTS) $(CSLUL_LIBS) $(CSLUL_EXTRA_LIBS) -o $@
$(build_cslul)/cslul-tcc-boundscheck: $(CSLUL_C_FILES) $(CSLUL_HEADERS) $(BACKEND_C_FILES) $(BACKEND_HEADERS)
	tcc -b -bt24 $(INTERNCFLAGS) $(LDFLAGS) $(DEBUG_DEFINES) \
	    $(CSLUL_C_FILES) $(BACKEND_C_FILES) \
	    $(CSLUL_LIBS) $(CSLUL_EXTRA_LIBS) -o $@
$(build_cslul)/cslul-gprof: $(CSLUL_UNITTEST_C_FILES) $(CSLUL_C_FILES) $(CSLUL_HEADERS)
	gcc -O3 -pg $(INTERNCFLAGS) $(LDFLAGS) $(CSLUL_C_FILES) $(CSLUL_LIBS) $(CSLUL_EXTRA_LIBS) -o $@
$(build_cslul)/cslul-gcov: $(CSLUL_UNITTEST_C_FILES) $(CSLUL_C_FILES) $(CSLUL_HEADERS)
	gcc -O2 --coverage $(INTERNCFLAGS) $(LDFLAGS) $(CSLUL_C_FILES) $(CSLUL_LIBS) $(CSLUL_EXTRA_LIBS) -o $@


$(build_cslul)/template.pot: $(CSLUL_POTFILES)
	xgettext -k_ -kTR -kMSGDEF:3 --from-code=UTF-8 -d cslul \
			--package-name=cslul --copyright-holder='YOUR NAME' \
			--msgid-bugs-address=samuel@kodafritt.se -o $@ $(CSLUL_POTFILES)

$(build_cslul)/unittest-normal: $(CSLUL_UNITTEST_OBJECTS)
	$(CC) $(INTERNCFLAGS) $(CFLAGS) $(LDFLAGS) $(DEBUG_DEFINES) $(CSLUL_UNITTEST_OBJECTS) $(CSLUL_EXTRA_LIBS) -o $@
$(build_cslul)/unittest-oom: $(CSLUL_OOMTEST_OBJECTS)
	$(CC) $(INTERNCFLAGS) $(CFLAGS) $(LDFLAGS) $(DEBUG_DEFINES) $(CSLUL_OOMTEST_OBJECTS) $(CSLUL_EXTRA_LIBS) -o $@
$(build_cslul)/unittest-tcc-boundscheck: $(CSLUL_UNITTEST_C_FILES) $(CSLUL_C_FILES) $(CSLUL_HEADERS)
	tcc -b -bt24 $(INTERNCFLAGS) $(LDFLAGS) $(DEBUG_DEFINES) $(CSLUL_UNITTEST_C_FILES) $(CSLUL_EXTRA_LIBS) -o $@
$(build_cslul)/unittest-gcov: $(CSLUL_UNITTEST_C_FILES) $(CSLUL_C_FILES) $(CSLUL_HEADERS)
	gcc --coverage $(INTERNCFLAGS) $(LDFLAGS) $(DEBUG_DEFINES) $(CSLUL_UNITTEST_C_FILES) $(CSLUL_EXTRA_LIBS) -o $@
$(build_cslul)/unittest-gprof: $(CSLUL_UNITTEST_C_FILES) $(CSLUL_C_FILES) $(CSLUL_HEADERS)
	gcc -pg $(INTERNCFLAGS) $(LDFLAGS) $(DEBUG_DEFINES) $(CSLUL_UNITTEST_C_FILES) $(CSLUL_EXTRA_LIBS) -o $@

cslul-check: cslul-check-unit

cslul-check-unit: $(build_cslul)/unittest-normal
	$(build_cslul)/unittest-normal $(UNITTEST_OPTS)

cslul-check-oom-unit: $(build_cslul)/unittest-oom
	$(build_cslul)/unittest-oom $(UNITTEST_OPTS)

cslul-check-unit-valgrind: $(build_cslul)/unittest-normal
	valgrind -q --leak-check=full $(VALGRIND_OPTS) $(build_cslul)/unittest-normal $(UNITTEST_OPTS)

cslul-check-oom-unit-valgrind: $(build_cslul)/unittest-oom
	valgrind -q --leak-check=full $(VALGRIND_OPTS) $(build_cslul)/unittest-oom $(UNITTEST_OPTS)

cslul-tcc-boundscheck: $(build_cslul)/unittest-tcc-boundscheck
	TCC_BOUNDS_WARN_POINTER_ADD=1 $(build_cslul)/unittest-tcc-boundscheck $(UNITTEST_OPTS)

cslul-gcc-coverage: $(build_cslul)/unittest-gcov
	$(build_cslul)/unittest-gcov $(UNITTEST_OPTS)

cslul-clang-analyze:
	clang $(DEFINES) $(INCLUDES) --analyze $(CSLUL_C_FILES)
	clang $(DEFINES) $(INCLUDES) --analyze $(CSLUL_UNITTEST_C_FILES)

cslul-gcc-analyze:
	gcc $(GCC_FANALYZE_FLAGS) $(DEFINES) $(INCLUDES) \
	        $(CSLUL_UNITTEST_C_FILES) \
	        -o $(build_cslul)/fanalyzer_dummy

cslul-sparse-analyze:
	sparse $(DEFINES) $(INCLUDES) $(CSLUL_C_FILES)
	sparse $(DEFINES) $(INCLUDES) $(CSLUL_UNITTEST_C_FILES)

# For now, we run all tests for a specific platform only,
# in order to reduce the number of permutations.
# The "threadsafety" addon is used to find missing/incorrect
# usage of static and/or const keywords.
cslul-cppcheck:
	cppcheck $(CPPCHECK_FLAGS) -rp $(src_cslul) -I $(src_backend)/include \
	    -itmp-in -itmp-crash.c -iaflmain.c \
	    --suppressions-list=$(src_cslul)/misc/cppcheck_suppressions.txt \
	    -DDEFAULT_CPU=I386 -DDEFAULT_SYSCALLABI=LINUX -DDEFAULT_USERABI=GNU \
	    -D__AFL_FUZZ_INIT= \
	    $(CSLUL_C_FILES) $(CSLUL_UNITTEST_C_FILES) $(BACKEND_PUBLIC_HEADERS)

$(build_cslul)/fuzz/tmp-in/all.c: $(AFL_C_FILES)
	$(MKDIR_P) $(build_cslul)/fuzz/tmp-in
	( \
	    cd $(srcdir); \
	    cat $(AFL_C_FILES); \
	) > $(build_cslul)/fuzz/tmp-in/all.c

# This used to have AFL_LLVM_INSTRIM=1 set,
# but that seems to be broken on Debian 12 and 13
$(build_cslul)/cslul-afl: $(build_cslul)/fuzz/tmp-in/all.c
	AFL_LLVM_LAF_ALL=1 AFL_USE_UBSAN=1 afl-cc \
	    $(CFLAGS) $(LDFLAGS) $(DEFINES_AFL) $(UNITY_BUILD_IQUOTE) \
	    $(build_cslul)/fuzz/tmp-in/all.c \
	    -o $(build_cslul)/cslul-afl $(CSLUL_EXTRA_LIBS)

$(build_cslul)/fuzz/tmp-in/generated_dict.txt: $(src_cslul)/mhparse.c $(src_cslul)/token.c $(src_cslul)/fuzz/base_dict.txt
	$(MKDIR_P) $(src_cslul)/fuzz/tmp-in
	{ \
	    grep -vE '^#' $(src_cslul)/fuzz/base_dict.txt; \
	    grep -oE 'TOK_EQ_RETURN\("[^"]+' $(src_cslul)/token.c | cut -d '"' -f 2 | sed 's/^/"/' | sed 's/$$/"/'; \
	    grep -oE 'TOK_EQ_RETURN\("[^"]+' $(src_cslul)/mhparse.c | cut -d '"' -f 2 | sed 's/^/"\\\\/' | sed 's/$$/"/'; \
	    grep -oE 'memcmp\([^,]+, *"[^"]+' $(src_cslul)/mhparse.c | cut -d '"' -f 2 | sed 's/^/"/' | sed 's/$$/"/'; \
	} | LC_ALL=C.UTF-8 sort | { echo '# Generated file. Do not edit'; uniq; } > $@

# For quick fuzzing: FUZZ_FLAGS_AFL='-p fast -d'
afl-fuzz: $(build_cslul)/cslul-afl $(build_cslul)/fuzz/tmp-in/generated_dict.txt
	set -eu; \
	date=$$(date +%Y%m%d_%Hh%M); \
	dir=$$(mktemp --tmpdir -d fuzz-cslul-$$date.XXX); \
	[ -d "$$dir" ] || exit 1; \
	cp $(build_cslul)/cslul-afl "$$dir/"; \
	AFL_SKIP_CPUFREQ=1 AFL_HANG_TMOUT=30ms AFL_NO_ARITH=1 \
	    afl-fuzz -m none $(FUZZ_FLAGS_AFL) \
	    -x $(build_cslul)/fuzz/tmp-in/generated_dict.txt -i $(FUZZ_INPUT_DIR) \
	    -o "$$dir/findings" "$$dir/cslul-afl"

$(build_cslul)/fuzz/tmp-crash.c: $(BACKEND_C_FILES) $(CSLUL_C_FILES) $(CSLUL_HEADERS)
	 cat $(BACKEND_C_FILES) $(CSLUL_C_FILES) > $(build_cslul)/fuzz/tmp-crash.c

$(build_cslul)/cslul-fuzz-crash: $(build_cslul)/fuzz/tmp-crash.c
	 $(CC) -fsanitize=undefined $(INTERNCFLAGS) \
	    -DNO_GETTEXT -DPARSEBUFFER=92 -DFUZZ_TIGHT_MODE -DARENA_SIZE=92 \
        -DCSLUL_NO_STDIO_MOCKS -fvisibility=hidden $(UNITY_BUILD_IQUOTE) \
        $(CFLAGS) $(LDFLAGS) $(build_cslul)/fuzz/tmp-crash.c \
        -o $(build_cslul)/cslul-fuzz-crash

prepare-run-crash:
	$(MKDIR_P) $(build_cslul)/fuzz/tmp-repro $(build_cslul)/fuzz/tmp-dummyout
	ln -sf /dev/stdin $(build_cslul)/fuzz/tmp-repro/main.slul
	echo 'type Tabc = struct { int x }' > $(build_cslul)/fuzz/tmp-repro/abc.slul

# run as: make crashfile="xxx/findings/crashes/id:xxx" afl-run-crash
afl-run-crash: $(build_cslul)/cslul-fuzz-crash prepare-run-crash
	$(build_cslul)/cslul-fuzz-crash --accept-errors $(build_cslul)/fuzz/tmp-repro < $(crashfile)
afl-run-crash-silent: $(build_cslul)/cslul-fuzz-crash prepare-run-crash
	$(build_cslul)/cslul-fuzz-crash --accept-errors --message-level=fatal $(build_cslul)/fuzz/tmp-repro < $(crashfile)
# run as: make crashdir="xxx/findings/crashes" afl-run-crashdir
afl-run-crashdir: $(build_cslul)/cslul-fuzz-crash prepare-run-crash
	for f in $(crashdir)/id*; do \
	    echo "--- $$f ---"; \
	    $(build_cslul)/cslul-fuzz-crash --accept-errors --message-level=fatal --outdir=$(build_cslul)/fuzz/tmp-dummyout $(build_cslul)/fuzz/tmp-repro < "$$f"; \
	done

gdb-cslul-unit: $(build_cslul)/unittest-normal
	gdb --args $(build_cslul)/unittest-normal $(UNITTEST_OPTS)
gdb-cslul-oom-unit: $(build_cslul)/unittest-oom
	gdb --args $(build_cslul)/unittest-oom $(UNITTEST_OPTS)

check-error-indices:
	$(CC) $(DEFINES) -DCHECK_ERROR_INDICES=1 -c $(src_cslul)/errors.c -o $(build_cslul)/dummy_cei.o
	$(RM_F) $(build_cslu)/dummy_cei.o

print-hashes: $(build_cslul)/print_hashes
	$(build_cslul)/print_hashes

$(build_cslul)/print_hashes: $(src_cslul)/print_hashes.c $(src_cslul)/hash.h $(src_cslul)/internal.h
	$(CC) $(CFLAGS) $(LDFLAGS) $(DEFINES) $< -o $(build_cslul)/print_hashes $(CSLUL_EXTRA_LIBS)

cslul-check-deps:
	( cd $(src_cslul); \
	    $(CC) $(DEFINES) $(CFLAGS) -MM -I../src-common/unittest -I../src-backend/include \
	    $(CSLUL_C_FILES:$(src_cslul)/%=%) \
	    $(CSLUL_UNITTEST_C_FILES_WITHOUT_EXTERNALS:$(src_cslul)/%=%) \
	    unittest/oommain.c | \
	    LC_ALL=C.UTF-8 sed -e 's/unittest\/\.\.\///g' | \
	    while read line; do echo "$$line"; done | \
	    grep -v '^unittest\.o:' | \
	    LC_ALL=C.UTF-8 sed -E \
	        -e 's/  / /g' \
	        -e 's/^/\$$\(build_cslul\)\//g' \
	        -e 's/ ([^ ]+\/unittest\.h)//g' \
	        -e 's/ ([^ ]+\.h)/ $$(src_cslul)\/\1/g' \
	        -e 's/ [a-z_/]+\.c//g' \
	        -e 's/^\$$\(build_cslul\)\/(test_)([a-z_]+)\.o:/\$$(build_cslul)\/unittest\/\1\2.o: $$(src_cslul)\/\2.c/g' \
	        -e 's/^\$$\(build_cslul\)\/(chkcommon|parsecommon|testmain|oommain)/\$$(build_cslul)\/unittest\/\1/g' \
	        -e 's/ \$$\(src_cslul\)\/unittest\/alltests\.h//g' \
	        -e 's/ \$$\(src_cslul\)\/..\/src-backend\// $$(src_backend)\//g' \
	        -e 's/^\$$\(build_cslul\)\/unittest\/.*$$/\0 $$(src_common)\/unittest\/unittest.h/g' \
	        -e 's/(testmain\.o:|oommain\.o:)/\1 \$$(src_cslul)\/unittest\/alltests\.h/g' \
	        -e 's/(\$$\(src_cslul\)\/internal.h )(.*) \$$\(src_cslul\)\/internal.h/\1\2/g'; ) | \
        LC_ALL=C.UTF-8 sort > $(build_cslul)/deps_expected.out
	grep '^$$(build_cslul)/.*\.o' $(src_cslul)/Makefile.inc | LC_ALL=C.UTF-8 sort > $(build_cslul)/deps_actual.out
	diff $(build_cslul)/deps_expected.out $(build_cslul)/deps_actual.out

cslul-outdirs:
	$(MKDIR_P) $(build_cslul)/unittest $(build_cslul)/winlibc \
	           $(build_cslul)/winlibc/unittest $(build_cslul)/fuzz \
	           $(build_cslul)/testgen $(build_cslul)/lang

cslul-clean:
	$(RM_F) $(CSLUL_OBJECTS) $(build_cslul)/cslul \
	        $(build_cslul)/cslul-tcc-boundscheck \
	        $(CSLUL_ALL_TEST_OBJECTS) $(build_cslul)/unittest-normal \
	        $(build_cslul)/unittest-oom $(build_cslul)/unittest-tcc-boundscheck \
	        $(build_cslul)/unittest-gcov $(build_cslul)/unittest-gprof \
	        $(build_cslul)/cslul-gcov $(build_cslul)/cslul-gprof \
	        $(build_cslul)/cslul-afl $(build_cslul)/print_hashes \
	        $(build_cslul)/fuzz/tmp-in/all.c \
	        $(build_cslul)/fuzz/tmp-in/generated_dict.txt \
	        $(build_cslul)/deps_expected.out $(build_cslul)/deps_actual.out \
	        $(build_cslul)/fanalyzer_dummy \
	        $(MOFILES) $(build_cslul)/template.pot
cslul-distclean: cslul-clean

cslul-install: cslul-install-bin cslul-install-lang
cslul-install-bin: $(build_cslul)/cslul
	$(INSTALL_DIR) $(DESTDIR)$(bindir)
	$(INSTALL_PROGRAM) $(build_cslul)/cslul $(DESTDIR)$(bindir)/cslul
cslul-install-lang: $(CSLUL_MOFILES)
	for lang in $(CSLUL_LANGUAGES); do \
		$(INSTALL_DIR) $(DESTDIR)$(localedir)/$$lang/LC_MESSAGES; \
		$(INSTALL_DATA) $(build_cslul)/lang/$$lang.mo \
				$(DESTDIR)$(localedir)/$$lang/LC_MESSAGES/$(CSLUL_TEXTDOMAIN).mo \
				|| exit 1; \
	done
cslul-uninstall: cslul-uninstall-bin cslul-uninstall-lang
cslul-uninstall-bin:
	$(RM) $(DESTDIR)$(bindir)/cslul
cslul-uninstall-lang:
	for lang in $(CSLUL_LANGUAGES); do \
		$(RM_F) $(DESTDIR)$(localedir)/$$lang/LC_MESSAGES/$(CSLUL_TEXTDOMAIN).mo \
				|| exit 1; \
	done

.PHONY: cslul-all cslul-clean cslul-distclean cslul-install cslul-uninstall \
        cslul-install-bin cslul-install-lang \
        cslul-uninstall-bin cslul-uninstall-lang \
        print-hashes cslul-outdirs \
        prepare-run-crash afl-run-crash afl-run-crash-silent afl-run-crashdir \
        afl-fuzz check-error-indices \
        gdb-cslul-unit gdb-cslul-oom-unit \
        cslul-check cslul-check-unit cslul-check-oom-unit \
        cslul-check-unit-valgrind cslul-check-oom-unit-valgrind \
        cslul-tcc-boundscheck cslul-gcc-coverage cslul-clang-analyze \
        cslul-sparse-analyze cslul-cppcheck
