/*

  hash.h -- Fast and simple hash function + pre-computed hashes

  Copyright © 2020-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/
#ifndef CSLUL_HASH_H
#define CSLUL_HASH_H

#define TOK_EQ_RETURN(name, token) \
    if (LIKELY(toklen == sizeof(name)-1 && \
        (sizeof(name)-1 <= 4 || /* these have unique hashes */ \
            !memcmp(tokval, (name), sizeof(name)-1)))) { \
        return (token); \
    } else break;

/* The hash function itself. It is used for fast keyword/identifier lookup.
   It guarantees unique hashes for ASCII strings up to 4 chars, but is NOT
   cryptographically secure, and for longer keywords/identifiers, it is only
   used as an optimization. */
#define HASH(hash, ch) (((hash) << 7) + ((hash) >> 22) + (uint32)(ch))

/* Pre-computed hashes (generated with print_hashes.c) */
/* from mhparse.c */
#define H_SLUL 242957036U
#define H_NAME 232290021U
#define H_VERSION 780800615U
#define H_TYPE 245266533U
#define H_REPO 240744559U
#define H_REPO_DOT_MIRROR 641392931U
#define H_WEBSITE 780809430U
#define H_LICENSE 1019926102U
#define H_LICENSE_DOT_TEXT 1374721158U
#define H_LICENSE_DOT_LIST 1358008838U
#define H_DEPENDS 214486251U
#define H_INTERFACE_DEPENDS 306467307U
#define H_API_DEF 2617601818U
#define H_SOURCE 4273786716U
#define H_IMPL 222001260U
#define H_UPTO 247216751U
#define H_INTERNAL 1702579999U
#define H_LIBRARY 778540522U
#define H_PLUGIN 3468300580U
#define H_LIBRARYSPEC 3318857817U
#define H_PLUGINSPEC 3055172600U
#define H_APP 1603696U
#define H_0_0_0 97261384U
#define H_OPTIONAL 2782514977U
#define H_NESTEDONLY 2639978629U
#define H_UNSTABLE_API 650421210U
#define H_BUNDLED 3970421083U
#define H_RETRACTED 3273412658U
#define H_DATA 211319393U
#define H_FUNC 215840611U
/* from parse.c */
#define H_NOT 1816564U
#define H_AND 1603428U
#define H_OR 14322U
#define H_MOD 1800164U
#define H_DEREF 1287435032U
#define H_REFTO 750369448U
#define H_REF_IS 1557664293U
#define H_REF_IS_NOT 2328347144U
#define H_BOOL 207353836U
#define H_USIZE 1585085855U
#define H_SSIZE 1048214942U
#define H_FILEOFFS 1690825126U
#define H_STRING 1314559201U
#define H_INT8 222018104U
#define H_BYTE 207518309U
#define H_WUINT8 1564203442U
#define H_INT16 2648512746U
#define H_UINT16 2648520490U
#define H_WUINT16 2649544490U
#define H_INT 1734516U
#define H_UINT 247101300U
#define H_WUINT 2126149551U
#define H_INT32 2648512998U
#define H_UINT32 2648520742U
#define H_WUINT32 2649544742U
#define H_INT64 2648513384U
#define H_UINT64 2648521128U
#define H_WUINT64 2649545128U
#define H_REF 1880806U
#define H_OWN 1833966U
#define H_ARENA 509179793U
#define H_SLOT 242956276U
#define H_FUNCREF 3968487773U
#define H_NORETURN 1718433441U
#define H_STRUCT 1314754414U
#define H_ENUM 213629677U
#define H_LIFETIME 1713943064U
#define H_SINCE 1027322398U
#define H_VAR 1945842U
#define H_WRITEONLY 1296692427U
#define H_ALIASED 2622042392U
#define H_THREADED 1674006551U
#define H_CLOSED 3455913050U
#define H_NONE 232519525U
#define H_THIS 244987123U
#define H_UNDEF 1574515488U
#define H_FALSE 1815820824U
#define H_TRUE 245152485U
#define H_IF 13542U
#define H_ELSE 213596645U
#define H_WHILE 2098886304U
#define H_DO 12911U
#define H_FOR 1685490U
#define H_IN 13550U
#define H_LOOPEND 4264072923U
#define H_LOOPEMPTY 630978250U
#define H_SWITCH 2101170019U
#define H_CASE 209222117U
#define H_WITH 251296360U
#define H_DEFAULT 1816826087U
#define H_SUBCASE 747059286U
#define H_ASSERT 1048154605U
#define H_BREAK 777613596U
#define H_CONTINUE 1402698271U
#define H_GOTO 217840239U
#define H_RETURN 1587386016U
/* not a keyword. used in error check */
#define H_VOID 249296100U
/* not keywords. Used to match against SlulApp.main */
#define H_SLULAPP 1570453162U
#define H_MAIN 230192366U
#define H_SLULEXITSTATUS 386621707U
#define H_SLULRT 3468383978U
/* from unittest/test_tlverify.c */
#define H_SOMEFUNC 1681733078U
#define H_SOMEFUNC2 513469634U
#define H_SOMECONST 3990549763U
#define H_SOMETYPE 1610495703U
#define H_SOMEDATA 1677211860U
#define H_INTERNALTYPE 754819958U
#define H_PRIVTYPE 1873614048U

#endif
