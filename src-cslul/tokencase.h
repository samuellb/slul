/*

  tokencase.h -- Groups of tokens for use in switch-case blocks

  Copyright © 2021-2023 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

/* There are two types of control statement keywords:
   Those that require an opening { after if/while/etc, and those that don't */
#define CASE_CONTROL_NEEDCURLY \
    case CSLUL_T_GotoTarget: \
    case CSLUL_T_KW_If: \
    case CSLUL_T_KW_Else: \
    case CSLUL_T_KW_While: \
    case CSLUL_T_KW_Do: \
    case CSLUL_T_KW_For: \
    case CSLUL_T_KW_In: \
    case CSLUL_T_KW_LoopEnd: \
    case CSLUL_T_KW_LoopEmpty: \
    case CSLUL_T_KW_Switch: \
    case CSLUL_T_KW_Case: \
    case CSLUL_T_KW_With: \
    case CSLUL_T_KW_Default: \
    case CSLUL_T_KW_SubCase: \
    case CSLUL_T_KW_Assert:
#define CASE_CONTROL_NOCURLY \
    case CSLUL_T_KW_Break: \
    case CSLUL_T_KW_Continue: \
    case CSLUL_T_KW_Goto: \
    case CSLUL_T_KW_Return:


/* The following list contains all tokens, and only once */
#define CASE_TOPLEVELS \
    case CSLUL_T_KW_Data: \
    case CSLUL_T_KW_Func: \
    case CSLUL_T_KW_Type:
#define CASE_LITERALS \
    case CSLUL_T_Integer: \
    case CSLUL_T_Float: \
    case CSLUL_T_LowerIdent: \
    case CSLUL_T_String: \
    case CSLUL_T_KW_None: \
    case CSLUL_T_KW_This: \
    case CSLUL_T_KW_False: \
    case CSLUL_T_KW_True:
#define CASE_PARENS \
    case CSLUL_T_LParen: \
    case CSLUL_T_RParen: \
    case CSLUL_T_LSquare: \
    case CSLUL_T_RSquare: \
    case CSLUL_T_LCurly: \
    case CSLUL_T_RCurly: \
    case CSLUL_T_Dot:
#define CASE_SEPARATORS \
    case CSLUL_T_Comma: \
    case CSLUL_T_Colon: \
    case CSLUL_T_Semicolon:
#define CASE_OPERATORS_EXCEPT_LINESTART \
    case CSLUL_T_Plus: \
    case CSLUL_T_Minus: \
    case CSLUL_T_Asterisk: \
    case CSLUL_T_Slash: \
    case CSLUL_T_Less: \
    case CSLUL_T_Assign: \
    case CSLUL_T_Greater: \
    case CSLUL_T_PlusAssign: \
    case CSLUL_T_MinusAssign: \
    case CSLUL_T_MultiplyAssign: \
    case CSLUL_T_DivideAssign: \
    case CSLUL_T_LessEqual: \
    case CSLUL_T_Equal: \
    case CSLUL_T_GreaterEqual: \
    case CSLUL_T_NotEqual: \
    case CSLUL_T_KW_Not: \
    case CSLUL_T_KW_And: \
    case CSLUL_T_KW_Or: \
    case CSLUL_T_KW_Mod: \
    case CSLUL_T_KW_RefTo: \
    case CSLUL_T_KW_RefIs: \
    case CSLUL_T_KW_RefIsNot:
#define CASE_OPERATORS \
    CASE_OPERATORS_EXCEPT_LINESTART \
    case CSLUL_T_KW_Deref:
#define CASE_SIMPLETYPES \
    case CSLUL_T_KW_Bool: \
    case CSLUL_T_KW_USize: \
    case CSLUL_T_KW_SSize: \
    case CSLUL_T_KW_FileOffs: \
    case CSLUL_T_KW_String: \
    case CSLUL_T_KW_Int8: \
    case CSLUL_T_KW_Byte: \
    case CSLUL_T_KW_WUInt8: \
    case CSLUL_T_KW_Int16: \
    case CSLUL_T_KW_UInt16: \
    case CSLUL_T_KW_WUInt16: \
    case CSLUL_T_KW_Int: \
    case CSLUL_T_KW_UInt: \
    case CSLUL_T_KW_WUInt: \
    case CSLUL_T_KW_Int32: \
    case CSLUL_T_KW_UInt32: \
    case CSLUL_T_KW_WUInt32: \
    case CSLUL_T_KW_Int64: \
    case CSLUL_T_KW_UInt64: \
    case CSLUL_T_KW_WUInt64:
#define CASE_COMPOSITETYPES \
    case CSLUL_T_KW_Ref: \
    case CSLUL_T_KW_Own: \
    case CSLUL_T_KW_Arena: \
    case CSLUL_T_KW_Slot: \
    case CSLUL_T_KW_FuncRef: \
    case CSLUL_T_KW_Struct: \
    case CSLUL_T_KW_Enum: \
    case CSLUL_T_UpperIdent: \
    case CSLUL_T_Question:
#define CASE_INSIDEDEF \
    case CSLUL_T_KW_NoReturn: \
    case CSLUL_T_KW_Lifetime: \
    case CSLUL_T_RArrow: \
    case CSLUL_T_Version:
#define CASE_TYPEQUALS \
    case CSLUL_T_KW_Var: \
    case CSLUL_T_KW_WriteOnly: \
    case CSLUL_T_KW_Aliased: \
    case CSLUL_T_KW_Threaded: \
    case CSLUL_T_KW_Closed:
#define CASE_CONTROL \
    CASE_CONTROL_NEEDCURLY \
    CASE_CONTROL_NOCURLY
#define CASE_SPECIALS \
    case CSLUL_T_KW_Undef:
#define CASE_INTERNAL \
    case CSLUL_T_Newline: \
    case CSLUL_INT_Whitespace: \
    case CSLUL_INT_Comment: \
    case CSLUL_T_Exclamation:


/*
#define CASE_TOPLEVELS \
#define CASE_LITERALS \
#define CASE_PARENS \
#define CASE_SEPARATORS \
#define CASE_OPERATORS \
#define CASE_SIMPLETYPES \
#define CASE_COMPOSITETYPES \
#define CASE_TYPEQUALS \
#define CASE_CONTROL \
#define CASE_SPECIALS \
#define CASE_INTERNAL \
*/

/* The following contains all except a certain type */
#define CASE_EXCEPT_TOPLEVELS \
    CASE_LITERALS \
    CASE_PARENS \
    CASE_SEPARATORS \
    CASE_OPERATORS \
    CASE_SIMPLETYPES \
    CASE_COMPOSITETYPES \
    CASE_INSIDEDEF \
    CASE_TYPEQUALS \
    CASE_CONTROL \
    CASE_SPECIALS \
    CASE_INTERNAL

#define CASE_EXCEPT_TYPES /* This includes type qualifiers */ \
    case CSLUL_T_KW_Data: \
    case CSLUL_T_KW_Func: \
    case CSLUL_T_KW_Type: \
    case CSLUL_T_LowerIdent: \
    case CSLUL_T_Integer: \
    case CSLUL_T_Float: \
    case CSLUL_T_String: \
    case CSLUL_T_KW_None: \
    case CSLUL_T_KW_This: \
    case CSLUL_T_KW_False: \
    case CSLUL_T_KW_True: \
    case CSLUL_T_LParen: \
    case CSLUL_T_RParen: \
    case CSLUL_T_LCurly: \
    case CSLUL_T_RCurly: \
    case CSLUL_T_RSquare: \
    case CSLUL_T_Dot: \
    case CSLUL_T_RArrow: \
    case CSLUL_T_KW_NoReturn: \
    case CSLUL_T_KW_Lifetime: \
    case CSLUL_T_KW_Since: \
    case CSLUL_T_Version: \
    CASE_SEPARATORS \
    CASE_OPERATORS \
    CASE_TYPEQUALS \
    CASE_CONTROL \
    CASE_SPECIALS \
    CASE_INTERNAL


/* The following contains all keyword tokens */
#define CASE_ALL_KEYWORDS \
    case CSLUL_T_KW_Data: \
    case CSLUL_T_KW_Func: \
    case CSLUL_T_KW_Type: \
    case CSLUL_T_KW_Not: \
    case CSLUL_T_KW_And: \
    case CSLUL_T_KW_Or: \
    case CSLUL_T_KW_Mod: \
    case CSLUL_T_KW_Deref: \
    case CSLUL_T_KW_RefTo: \
    case CSLUL_T_KW_RefIs: \
    case CSLUL_T_KW_RefIsNot: \
    CASE_SIMPLETYPES \
    case CSLUL_T_KW_Ref: \
    case CSLUL_T_KW_Own: \
    case CSLUL_T_KW_Arena: \
    case CSLUL_T_KW_Slot: \
    case CSLUL_T_KW_FuncRef: \
    case CSLUL_T_KW_Struct: \
    case CSLUL_T_KW_Enum: \
    case CSLUL_T_KW_NoReturn: \
    case CSLUL_T_KW_Lifetime: \
    case CSLUL_T_KW_Since: \
    case CSLUL_T_KW_Var: \
    case CSLUL_T_KW_WriteOnly: \
    case CSLUL_T_KW_Aliased: \
    case CSLUL_T_KW_Threaded: \
    case CSLUL_T_KW_Closed: \
    CASE_CONTROL \
    case CSLUL_T_KW_None: \
    case CSLUL_T_KW_This: \
    case CSLUL_T_KW_Undef: \
    case CSLUL_T_KW_False: \
    case CSLUL_T_KW_True:

/* All operators with more than 1 character */
#define CASE_MULTICHAR_OPS \
    case CSLUL_T_PlusAssign: \
    case CSLUL_T_MinusAssign: \
    case CSLUL_T_MultiplyAssign: \
    case CSLUL_T_DivideAssign: \
    case CSLUL_T_LessEqual: \
    case CSLUL_T_Equal: \
    case CSLUL_T_GreaterEqual: \
    case CSLUL_T_NotEqual: \
    case CSLUL_T_RArrow:
