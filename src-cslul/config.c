/*

  config.c -- Functions for configuring compilation contexts.

  Copyright © 2021-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "internal.h"
#include <string.h>
#include <stdlib.h>
#define INTERR_CONFIG(errnum) MAKE_INTERR(errnum, INTERRBASE_CONFIG)

struct CfgAlloc {
    struct CfgAlloc *next;
    /* ...data... */
};

void *cfgalloc(struct CSlulConfig *cfg, size_t size)
{
    struct CfgAlloc *alloc = lowlvl_alloc(cfg, sizeof(struct CfgAlloc)+size);
    if (UNLIKELY(!alloc)) return NULL;
    alloc->next = cfg->allocs;
    cfg->allocs = alloc;
    return (void *)&alloc[1];
}

struct CSlulConfig *cslul_config_create(const struct CSlulInitParams *params)
{
    struct CSlulConfig *cfg;
    cfg = params &&
          params->size >= offsetof(struct CSlulInitParams, fptr_free) &&
          params->fptr_malloc ?
        params->fptr_malloc(sizeof(struct CSlulConfig)) :
        malloc(sizeof(struct CSlulConfig));
    if (UNLIKELY(!cfg)) return NULL;
    PROTECT_STACK_STRUCT(*cfg); /* no ctx here, so use "stack" variant */

    cfg->requested_outputtype = CSLUL_OT_AUTO;
    cfg->use_arch_dirs = 0;
    cfg->targets = NULL;
    cfg->codegen = CG_INTERNAL;
    cfg->outdir = NULL;
    cfg->iface_dirs = NULL;

    cfg->msglevel = CSLUL_L_STYLE;
    cfg->msghandler = NULL;

    cfg->params.dirsep = '\0';
    cfg->params.fptr_malloc = NULL;
    cfg->params.fptr_free = NULL;
    cfg->params.fptr_realloc = NULL;
    cfg->params.fptr_fopen = NULL;
    cfg->params.fptr_createexec = NULL;
    cfg->params.fptr_fclose = NULL;
    cfg->params.fptr_ferror = NULL;
    cfg->params.fptr_remove = NULL;
    cfg->params.fptr_fwrite = NULL;
    cfg->params.fptr_fread = NULL;
    cfg->params.fptr_mkdir = NULL;
    cfg->params.fptr_dropprivs = NULL;
    if (params) {
        memcpy(&cfg->params, params, MIN(sizeof(cfg->params), params->size));
    }
    if (!cfg->params.dirsep) {
        cfg->params.dirsep = DIRSEP;
    }
    cfg->skip_implicit_slulrt_dep = 0;
    cfg->has_errors = 0;
    cfg->allocs = NULL;
    cfg->alloced_appdir = NULL;
    return cfg;
}

void cslul_config_free(struct CSlulConfig *cfg)
{
    struct CfgAlloc *alloc;

    if (!cfg) return;
    for (alloc = cfg->allocs; alloc; ) {
        struct CfgAlloc *next = alloc->next;
        lowlvl_free(cfg, alloc);
        alloc = next;
    }
    if (cfg->alloced_appdir) { /* free(NULL) causes failure in arena tests */
        lowlvl_free(cfg, cfg->alloced_appdir);
    }
    lowlvl_free(cfg, cfg);
}


void cslul_config_set_message_level(struct CSlulConfig *cfg,
                                    enum CSlulMessageLevel level)
{
    cfg->msglevel = level;
}

void cslul_config_set_message_handler(struct CSlulConfig *cfg,
                                      CSlulMessageHandler *mh)
{
    cfg->msghandler = mh;
}


int cslul_config_add_arch(struct CSlulConfig *cfg, const char *archtriple)
{
    return add_arch(cfg, archtriple, strlen(archtriple));
}

int cslul_config_add_arches(struct CSlulConfig *cfg, const char *archtriples)
{
    const char *comma;
    const char *arch = archtriples;
    do {
        size_t len;
        comma = strchr(arch, ',');
        len = comma ? (size_t)(comma-arch) : strlen(arch);
        if (!add_arch(cfg, arch, len)) return 0;
        if (comma) {
            cfg->use_arch_dirs = 1;
            arch++;
        }
        arch += len;
        arch += strspn(arch, " ");
    } while (comma && *arch);
    return 1;
}

/**
 * Sets the output type. By default, it is auto-detected for the module
 * being compiled.
 */
void cslul_config_set_output_type(struct CSlulConfig *cfg,
                                  enum CSlulOutputType outtype)
{
    cfg->requested_outputtype = outtype;
}

/**
 * Sets the output directory. By default, the current directory is used.
 *
 * When compiling for multiple architectures, a separate directory structure
 * will be used:  bin/<archtriple>/ for executables and lib/<archtriple>/
 * for libraries.
 */
void cslul_config_set_output_directory(struct CSlulConfig *cfg,
                                       const char *dir)
{
    cfg->outdir = dir;
}

int cslul_config_add_iface_dir(struct CSlulConfig *cfg, const char *path)
{
    /* Note: This adds the iface dirs in reverse order,
       so last added = searched first */
    struct InterfaceDir *ifacedir = cfgalloc(cfg, sizeof(struct InterfaceDir));
    if (!ifacedir) return 0;
    ifacedir->path = path;
    ifacedir->pathlen = strlen(path);
    ifacedir->next = cfg->iface_dirs;
    cfg->iface_dirs = ifacedir;
    return 1;
}

int cslul_config_has_errors(struct CSlulConfig *cfg)
{
    return cfg->has_errors;
}

static void cfg_message_final(struct CSlulConfig *cfg, struct CSlulState *st,
                              enum CSlulErrorCode errorcode)
{
    st->level = lookup_error_level(errorcode);
    if (st->level <= CSLUL_L_ERROR) {
        cfg->has_errors = 1;
    }
    st->cfg = cfg;
    st->ctx = NULL;
    st->phase = 0;
    st->errorcode = errorcode;
    if (st->level <= cfg->msglevel && cfg->msghandler) {
        cfg->msghandler(st);
    }
}

static void set_cfgerror_loc(struct CSlulLocation *loc,
                             const char *text, int len)
{
    loc->filename = NULL;
    loc->type = CSLUL_LT_MAINTEXT;
    loc->line = 0;
    loc->column = 0;
    loc->length = len;
    loc->text = text;
}

void cfgerror(struct CSlulConfig *cfg, enum CSlulErrorCode errorcode)
{
    struct CSlulState st;
    reset_msgstate(&st);
    cfg_message_final(cfg, &st, errorcode);
}

void cfgerror_textlen(struct CSlulConfig *cfg, enum CSlulErrorCode errorcode,
                   const char *text, int len)
{
    struct CSlulState st;
    reset_msgstate(&st);
    set_cfgerror_loc(&st.locs[0], text, len);
    cfg_message_final(cfg, &st, errorcode);
}
