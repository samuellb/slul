This is a small test tool that generates very large source files for
testing the compiler.

It can operate in two modes:

1. Generate complete garbage source code to test the parser.
   It is expected to get "Internal tree data structure too large"
   for some of the generated source code.
2. Generate source code that compiles (but is meaningless).
   This mode is called the happy-path mode.

There is a script called `runmany.sh` that runs the garbage
source code generation 4096 times with different parameters.
This will take a long time to run.
