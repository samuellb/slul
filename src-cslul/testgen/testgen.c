/*

  testgen.c -- Generates a very large source file with random functions

  Copyright © 2021-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const char specials[] = "\"()*+,-./01:<=>?[\\]{}\n";

/*
    Extracted from match_keyword in parse.c by replacing the matches
    of the following regex with ', ':

    , [A-ZWa-zw_0-9]+\)\n    case [A-ZW0-9]+\([a-zw0-9',]+\): [A-ZW_]+\(

    W is needed, due to a bug in GNU systems, in locales where V and W
    are variants of the same letter.

    Some keywords have been commented out because they are almost identical
    (in regards of code paths taken in cslul) to another keyword.
*/
static const char *keywords[] = {
    "not", "and", /*"or",*/ "data", "func", "type", "bool", "usize",
    /*"ssize", "fileoffs",*/ "string", /*"int8", "byte", "wuint8", "int16",
    "uint16", "wuint16", "int", "uint", "wuint", "int32", "uint32",
    "wuint32",*/ "int64", /*"uint64",*/ "wuint64", "ref", "own", "arena",
    "noreturn", "struct", "enum", "lifetime", "var", /*"writeonly",*/
    /*"aliased",*/ "threaded", "closed", "none", "undef", "if", "else",
    "while", "do", "for", "in", "loopend", "loopempty", "loopend",
    "loopempty", "switch", "case", "default", "subcase",
    "assert", "break", "continue", "goto", "return"
};

static const char *start_templates[] = {
    "\nfunc f_%04x() {\n    ",  /* type/stmt, 1 */
    "\ntype t_%04x = ",         /* type, 2 */
    "\ndata ",                  /* type, 3 */
    "\ndata int d_%04x = ",     /* expression, 1 */
    "\nfunc f_%04x()\n{"        /* expression, 2 */
        "\n    int i = ",
    "\nfunc f_%04x()\n{"        /* expression, 3 */
        "\n    for int i in "
};

static const char *end_templates[] = {
    "\n}\n",    /* type/stmt, 1 */
    "\n",       /* type, 2 */
    "d_%04x\n", /* type, 3 */
    "\n",       /* expression, 1 */
    "\n}\n",    /* expression, 2 */
    "\n}\n"     /* expression, 3 */
};

#define NUM_TEMPLATES ((int)((sizeof(start_templates)/sizeof(char*))))
#define NUM_SPECIALS ((int)(sizeof(specials)-1))
#define NUM_SYMBOLS ((int)(NUM_SPECIALS + (sizeof(keywords)/sizeof(char*))))
#define MAX_IDENTS 16777216 /* MAX_DEPTH==25 -> max tree size = 2^24 */

#define NUM_HAPPYPATH_FUNCS 40000

static void print_symbol(int sym)
{
    if (sym < 0) return;
    else if (sym < NUM_SPECIALS) {
        if (putchar(specials[sym]) < 0) goto err;
    } else {
        if (putchar(' ') < 0) goto err;
        if (fputs(keywords[sym-NUM_SPECIALS], stdout) < 0) goto err;
        if (putchar(' ') < 0) goto err;
    }
    return;
  err:
    perror("Failed to write test data");
    abort();
}

static int parse_num_arg(const char *arg)
{
    int n = atoi(arg);
    if (n >= NUM_SYMBOLS) {
        fprintf(stderr, "Parameter too large. Maximum is %d\n", NUM_SYMBOLS);
        abort();
    }
    return n;
}

/* TODO test arrays also */
static void bool_expr(int num, int num_ints, int num_bools, int use_globals)
{
    static const char ops[][3] = {
        "==", "!=", "<", "<=", ">", ">="
    };

    int type = (num % 11) & 0x1;
    if (num_ints && use_globals) {
        int a = (num % 61) % num_ints;
        int op = num % 6;
        printf("arg %s i%d_%d", ops[op], num,a);
    } else if (use_globals) {
        int op = num % 6;
        printf("arg %s %d", ops[op], (num%71)/8 - 5);
    } else if (num_ints && type == 0) {
        int a = (num % 3) % num_ints;
        int b = (num % 7) % num_ints;
        int op = num % 6;
        printf("i%d_%d %s i%d_%d", num,a, ops[op], num,b);
    } else if (num_bools && type == 1) {
        int a = (num % 3) % num_bools;
        int b = (num % 7) % num_bools;
        const char ops[][8] = {
            "and", "or", "and not", "or not", "==", "!="
        };
        int op = num % 6;
        printf("b%d_%d %s b%d_%d", num,a, ops[op], num,b);
    } else {
        printf("false");
    }
}

static void some_stmt(int num, int num_ints, int num_bools, int use_globals)
{
    int type = (num % 13) & 0x1;
    if (num_ints && num > 0 && type == 0) {
        int a = (num % 17) % num_ints;
        int b = (num % 19) % num_ints;
        printf("return f%d(i%d_%d - (i%d_%d + arg))", num-1, num,a, num,b);
    } else if (num_bools && !use_globals) {
        int a = (num % 23) % num_bools;
        int b = (num % 29) % num_bools;
        printf("b%d_%d = not b%d_%d", num,a, num,b);
    } else if (num_ints && !use_globals) {
        int a = (num % 31) % num_ints;
        int b = (num % 37) % num_ints;
        printf("i%d_%d += i%d_%d - arg", num,a, num,b);
    } else if (num_ints && num_bools && !use_globals) {
        int a = (num % 41) % num_bools;
        int b = (num % 43) % num_ints;
        printf("b%d_%d = i%d_%d < 0", num,a, num,b);
    } else if (num_ints) {
        int a = (num % 47) % num_ints;
        printf("return i%d_%d + 123", num,a);
    } else if (num_bools) {
        int a = (num % 51) % num_bools;
        int b = (num % 53) % num_bools;
        printf("assert (not b%d_%d) != b%d_%d", num,a, num,b);
    } else if (num >= 2) {
        printf("return f%d(f%d(%d) - f%d(7))", num-1, num-2, num, num-1);
    } else {
        printf("return 0x12345678");
    }
}

#define INDENT1 "    "
#define INDENT2 "        "
#define INDENT3 "            "

int main(int argc, char **argv)
{
    int num, i, j, k, l;
    int m = -1, n = -1;
    int happy_path = 0;

    /* The output would get really large (several gigabytes) if we
       just ran all combinations at once. So the l and m variables
       are instead command-line parameters, so we can run a reasonable
       chunk (around 67 MB) at once. */
    if (argc >= 2) {
        if (!strcmp(argv[1], "--num-templates")) {
            printf("%d\n", NUM_TEMPLATES);
            return 0;
        } else if (!strcmp(argv[1], "--num-symbols")) {
            printf("%d\n", NUM_SYMBOLS);
            return 0;
        } else if (!strcmp(argv[1], "--happy-path")) {
            /* Generates source code that compiles */
            happy_path = 1;
        } else {
            m = parse_num_arg(argv[1]);
            if (argc >= 3) {
                n = parse_num_arg(argv[2]);
            }
        }
    }

    puts("\\slul 0.0.0\n"
         "\\name test\n"
         "\n");

    num = 0;
    if (happy_path) {
        for (i = 0; i < NUM_HAPPYPATH_FUNCS; i++) {
            unsigned u = i;
            int num_ints, num_bools, use_globals, stmt1, stmt2;

            num_ints = (u & 0x3);
            u >>= 2;
            num_bools = (u & 0x3);
            u >>= 2;
            use_globals = (u & 0x1);
            u >>= 1;
            stmt1 = (u & 0x2);
            u >>= 1;
            stmt2 = (u & 0x2);
            u >>= 1;

            if (use_globals) {
                for (j = 0; j < num_ints; j++) {
                    printf("data int i%d_%d = %d\n", num, j, num);
                }
                for (j = 0; j < num_bools; j++) {
                    printf("data bool b%d_%d = %s\n", num, j,
                           (num&0x1) ? "true" : "false");
                }
                printf("data [3]int16 arr%d = [0x1111,0x2222,0x3333]\n", num);
            }

            printf(
                "func f%d(int arg) -> int\n"
                "{\n",
                num);

            if (!use_globals) {
                for (j = 0; j < num_ints; j++) {
                    printf(INDENT1 "var int i%d_%d = %d + arg\n",
                           num, j, num+j);
                }
                for (j = 0; j < num_bools; j++) {
                    printf(INDENT1 "var bool b%d_%d = %s\n", num, j,
                           ((num+j)&0x1) ? "true" : "false");
                }
                /* TODO test arrays also */
                /*printf(INDENT1 "var [3]int16 arr%d = [0x1111,0x2222,0x3333]\n",
                       num);*/
            }

            switch (stmt1) {
            case 0:
                printf(INDENT1 "if ");
                bool_expr(num, num_ints, num_bools, use_globals);
                printf(" {\n");
                break;
            case 1:
                printf(INDENT1 "if ");
                bool_expr(num, num_ints, num_bools, use_globals);
                printf(
                    " {\n"
                    INDENT2 "return false\n"
                    INDENT1 "} else {\n");
                break;
            case 2:
                printf(INDENT1 "while ");
                bool_expr(num, num_ints, num_bools, use_globals);
                printf(" {\n");
                break;
            case 3:
                printf(INDENT1 "do {\n");
                break;
            }

            switch (stmt2) {
            case 0:
                printf(INDENT2 "if ");
                bool_expr(num, num_ints, num_bools, use_globals);
                printf(" {\n" INDENT3);
                some_stmt(num, num_ints, num_bools, use_globals);
                printf("\n" INDENT2 "}\n");
                break;
            case 1:
                printf(INDENT2 "if ");
                bool_expr(num, num_ints, num_bools, use_globals);
                printf(" {\n" INDENT3);
                some_stmt(num, num_ints, num_bools, use_globals);
                printf("\n" INDENT2 "} else {\n" INDENT3);
                some_stmt(num, num_ints, num_bools, use_globals);
                printf("\n" INDENT2 "}\n");
                break;
            case 2:
                printf(INDENT2 "while ");
                bool_expr(num, num_ints, num_bools, use_globals);
                printf(" {\n" INDENT3);
                some_stmt(num, num_ints, num_bools, use_globals);
                printf("\n" INDENT2 "}\n");
                break;
            case 3:
                printf(INDENT2 "do {\n" INDENT3);
                some_stmt(num, num_ints, num_bools, use_globals);
                printf("\n" INDENT2 "} while ");
                bool_expr(num, num_ints, num_bools, use_globals);
                printf("\n");
                break;
            }

            if (stmt1 == 3) {
                printf(INDENT1 "while ");
                bool_expr(num, num_ints, num_bools, use_globals);
                printf("\n");
            } else {
                printf(INDENT1 "}\n");
            }

            if (num_ints) {
                int a = (num % 57) % num_ints;
                printf(INDENT1 "return i%d_%d\n", num,a);
            } else if (num_bools && !use_globals) {
                int a = (num % 59) % num_bools;
                printf(INDENT1 "if b%d_%d return -5678\n"
                       INDENT1 "else return -9876\n", num,a);
            } else {
                printf(INDENT1 "return 0x54321\n");
            }

            printf("}\n");
            num++;
        }

        printf(
            "func SlulApp.main() -> SlulExitStatus\n"
            "{\n");
        for (i = 0; i < NUM_HAPPYPATH_FUNCS; i++) {
            printf(INDENT1 "f%d(%d)\n", i, i);
        }
        printf(
            INDENT1 "return .success\n"
            "}\n");
    } else {
        /* Just generate (deterministically) random tokens */
        for (i = -1; i < NUM_SYMBOLS; i++) {
            for (j = 0; j < NUM_SYMBOLS; j++) {
                for (k = 0; k < NUM_TEMPLATES; k++) {
                    for (l = 0; l < NUM_SYMBOLS; l++) {
                        printf(start_templates[k], num);
                        print_symbol(i);
                        print_symbol(j);
                        print_symbol(l);
                        print_symbol(m);
                        print_symbol(n);
                        printf(end_templates[k], num++);
                        num %= MAX_IDENTS;
                    }
                }
            }
        }
    }

    return 0;
}
