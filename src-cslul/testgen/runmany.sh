#!/bin/sh -eu
#
# Script to invoke testgen multiple times, with different input parameters
#
# Copyright © 2021-2023 Samuel Lidén Borell <samuel@kodafritt.se>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

srcdir="${srcdir:-.}"
make -s -f "$srcdir/Makefile" srcdir="$srcdir" testgen

mkdir -p from_stdin
ln -snf /dev/stdin from_stdin/main.slul

num_templates=$(./testgen --num-templates)
num_symbols=$(./testgen --num-symbols)
total_steps=$((num_symbols * num_symbols))
step=0

clear='\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b'
seq 0 $((num_symbols - 1)) | while read i; do
    substep=0
    seq 0 $((num_symbols - 1)) | while read j; do
        printf "${clear}%3d %% [step %2d/%d] [substep %2d/%d]" \
                $((100*(step*num_symbols + substep) / total_steps)) $step $num_symbols $substep $num_symbols >&2
        ./testgen "$i" "$j" | ( ../cslul --message-level=fatal from_stdin || if [ $? != 1 ]; then
            echo "FAILURE: $?"
            exit 1
        fi; )
        substep=$((substep + 1))
    done
    step=$((step + 1))
done
printf "$clear                               $clear" >&2

true
