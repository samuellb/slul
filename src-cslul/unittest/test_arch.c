/*

  Unit tests of arch.c

  Copyright © 2021-2023 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "../arch.c"
#include "unittest.h"
#include "alltests.h"
#include "testcommon.h"

#define TEST_BAD_TARGET(t) do { \
        cfg->has_errors = 0; \
        tsoftassert(!cslul_config_add_arches(cfg, (t))); \
        tsoftassert(cfg->has_errors); \
    } while (0)

#define TEST_GOOD_TARGET(t) do { \
        cfg->has_errors = 0; \
        tsoftassert(cslul_config_add_arches(cfg, (t))); \
        tsoftassert(!cfg->has_errors); \
    } while (0)

static struct CSlulConfig *create_config(void)
{
    struct CSlulConfig *cfg = tcfg(cslul_config_create(NULL));
    tassert(cfg != NULL);
    return cfg;
}

static void test_target_invalid(void)
{
    struct CSlulConfig *cfg = create_config();
    TEST_BAD_TARGET("");
    TEST_BAD_TARGET("-");
    TEST_BAD_TARGET("--");
    TEST_BAD_TARGET("---");
    TEST_BAD_TARGET("x");
    TEST_BAD_TARGET("-x");
    TEST_BAD_TARGET("x-");
    TEST_BAD_TARGET("x-x-x");
    TEST_BAD_TARGET("x86_64-");
    TEST_BAD_TARGET("x86_64-linux-");
    TEST_BAD_TARGET("x86_64-linux-gnu-");
    TEST_BAD_TARGET("x86_64--gnu");
    TEST_BAD_TARGET("x86_64-x");
    TEST_BAD_TARGET("x86_64-x-");
    TEST_BAD_TARGET(" x86_64-linux-gnu");
    TEST_BAD_TARGET(",");
    TEST_BAD_TARGET(",,");
    TEST_BAD_TARGET(",-");
    TEST_BAD_TARGET("-,");
    TEST_BAD_TARGET(",x86_64-linux-gnu");
    TEST_BAD_TARGET("x86_64-linux-gnu,,");
    TEST_BAD_TARGET("x86_64-linux-gnu,,aarch64-linux-gnu");
    TEST_BAD_TARGET("x86_64-linux-gnu,\n");
    TEST_BAD_TARGET("  ,x86_64-linux-gnu");
    /* Bad combinations */
    TEST_BAD_TARGET("ir-linux");
    TEST_BAD_TARGET("riscv32i-windows");
    TEST_BAD_TARGET("i386-windows-musl");
    /* Misspelled names */
    TEST_BAD_TARGET("x86_65-linux-gnu");
    TEST_BAD_TARGET("x86_64-linuz-gnu");
    TEST_BAD_TARGET("x86_64-linux-gnx");
    TEST_BAD_TARGET("arf-linux");
    TEST_BAD_TARGET("arm-linux-muzl");
    TEST_BAD_TARGET("i376-netbsd");
    TEST_BAD_TARGET("i386-netbzd");
    TEST_BAD_TARGET("aarch63-linux");
    TEST_BAD_TARGET("riscv31i-linux");
    TEST_BAD_TARGET("riscv65gc-linux");
    TEST_BAD_TARGET("powerpz65el-gnu");
    cslul_config_free(cfg);
}

static void test_target_single(void)
{
    struct CSlulConfig *cfg = create_config();
    TEST_GOOD_TARGET("x86_64-linux-gnu");
    tassert(cfg->targets);
    tsoftassert(!cfg->targets->next);
    tsoftassert(cfg->targets->cpu == X86_64);
    tsoftassert(cfg->targets->syscalls == LINUX);
    tsoftassert(cfg->targets->userabi == GNU);
    tsoftassert(!cfg->use_arch_dirs);
    tsoftassert(!cfg->targets->next);
    tsoftassert(cfg->targets->cpu == X86_64);
    tsoftassert(cfg->targets->syscalls == LINUX);
    tsoftassert(cfg->targets->userabi == GNU);
    cslul_config_free(cfg);
}

static void test_target_long(void)
{
    struct CSlulConfig *cfg = create_config();
    /* vendor attribute is ignored */
    TEST_GOOD_TARGET("x86_64-unknown-linux-gnu");
    tassert(cfg->targets);
    tsoftassert(!cfg->targets->next);
    tsoftassert(cfg->targets->cpu == X86_64);
    tsoftassert(cfg->targets->syscalls == LINUX);
    tsoftassert(cfg->targets->userabi == GNU);
    tsoftassert(!cfg->use_arch_dirs);
    cslul_config_free(cfg);
}

static void test_target_short1(void)
{
    struct CSlulConfig *cfg = create_config();
    TEST_GOOD_TARGET("x86_64-windows");
    tassert(cfg->targets);
    tsoftassert(!cfg->targets->next);
    tsoftassert(cfg->targets->cpu == X86_64);
    tsoftassert(cfg->targets->syscalls == WINDOWS);
    tsoftassert(cfg->targets->userabi == WINNT_DLLS);
    tsoftassert(!cfg->use_arch_dirs);
    tsoftassert(!cfg->targets->next);
    tsoftassert(cfg->targets->cpu == X86_64);
    tsoftassert(cfg->targets->syscalls == WINDOWS);
    tsoftassert(cfg->targets->userabi == WINNT_DLLS);
    cslul_config_free(cfg);
}

static void test_target_short2(void)
{
    struct CSlulConfig *cfg = create_config();
    TEST_GOOD_TARGET("aarch64-musl");
    tassert(cfg->targets);
    tsoftassert(!cfg->targets->next);
    tsoftassert(cfg->targets->cpu == AARCH64);
    tsoftassert(cfg->targets->syscalls == LINUX);
    tsoftassert(cfg->targets->userabi == MUSL);
    tsoftassert(!cfg->use_arch_dirs);
    cslul_config_free(cfg);
}

static void test_target_ir(void)
{
    struct CSlulConfig *cfg = create_config();
    TEST_GOOD_TARGET("ir");
    tassert(cfg->targets);
    tsoftassert(!cfg->targets->next);
    tsoftassert(cfg->targets->cpu == IR_DUMP);
    tsoftassert(cfg->targets->syscalls == SYSCALLS_IR_GENERIC);
    tsoftassert(cfg->targets->userabi == USERABI_IR_GENERIC);
    tsoftassert(!cfg->use_arch_dirs);
    cslul_config_free(cfg);
}

static void check_target_several(struct CSlulConfig *cfg)
{
    const struct Target *t;
    tassert(cfg->targets);
    t = cfg->targets;
    tsoftassert(t->cpu == AARCH64);
    tsoftassert(t->syscalls == LINUX);
    tsoftassert(t->userabi == MUSL);
    tassert(t->next != NULL);
    t = t->next;
    tsoftassert(t->cpu == X86_64);
    tsoftassert(t->syscalls == LINUX);
    tsoftassert(t->userabi == GNU);
    tsoftassert(t->next == NULL);
    tsoftassert(cfg->use_arch_dirs);
}

static void test_target_several1(void)
{
    struct CSlulConfig *cfg = create_config();
    TEST_GOOD_TARGET("x86_64-linux-gnu,aarch64-linux-musl");
    check_target_several(cfg);
    cslul_config_free(cfg);
}

static void test_target_several2(void)
{
    struct CSlulConfig *cfg = create_config();
    TEST_GOOD_TARGET("x86_64-linux-gnu,  aarch64-linux-musl");
    check_target_several(cfg);
    cslul_config_free(cfg);
}

static void test_target_wrongorder(void)
{
    struct CSlulConfig *cfg = create_config();
    TEST_BAD_TARGET("aarch64-aarch64-linux-gnu");
    TEST_BAD_TARGET("aarch64-linux-linux-gnu");
    TEST_BAD_TARGET("aarch64-linux-gnu-gnu");
    TEST_BAD_TARGET("aarch64-gnu-linux");
    TEST_BAD_TARGET("linux-aarch64-gnu");
    TEST_BAD_TARGET("gnu-aarch64-linux");
    TEST_BAD_TARGET("aarch64-unknown-unknown-linux-gnu");
    cslul_config_free(cfg);
}

static void test_target_badcombo(void)
{
    struct CSlulConfig *cfg = create_config();
    TEST_BAD_TARGET("x86_64-openbsd6.8-gnu");
    TEST_BAD_TARGET("risc32i-windows");
    cslul_config_free(cfg);
}

const TestFunctionInfo tests_arch[] = {
    TEST_INFO(test_target_invalid)
    TEST_INFO(test_target_single)
    TEST_INFO(test_target_long)
    TEST_INFO(test_target_short1)
    TEST_INFO(test_target_short2)
    TEST_INFO(test_target_ir)
    TEST_INFO(test_target_several1)
    TEST_INFO(test_target_several2)
    TEST_INFO(test_target_wrongorder)
    TEST_INFO(test_target_badcombo)
    TEST_END
};
