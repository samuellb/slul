/*

  oommain.c -- Main entry point for out-of-memory tests

  Copyright © 2016-2023 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#define CSLUL_IN_TESTS
#include "testalloc.h"

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#define TEST_MAIN
#include "unittest.h"
#include "testcommon.h"
#include "alltests.h"
TESTCALLS_START
#undef TEST_D
#define TEST_D TEST_D2
#include "alltests.h"
TESTCALLS_END

/* We expect a lot of errors to occur when we simulate out-of-memory */
#define MAX_FUNCTION_ERRORS 1000

int char_by_char = 0;

static int num_allocs, failing_alloc, ignored_allocs;
static struct CSlulConfig *alloced_cfg;
static struct CSlul *alloced_ctx;
#define MAX_TMALLOCS 100
static void *tmalloc_ptrs[MAX_TMALLOCS];
static int not_freed = 0;
static int has_early_failures = 0;

static int run_with_oom(void)
{
    int total_allocs;
    /* Determine the number of allocations */
    failing_alloc = 0;
    num_allocs = 0;
    errors_testcase = 0;
    alloced_cfg = NULL;
    alloced_ctx = NULL;
    not_freed = 0;
    run_test(); /* First run */
    assert(not_freed == 0);
    if (testcase_aborted) {
        if (!quiet) {
            fprintf(stderr, "SKIPPING FAILING TEST %s\n", current_function);
        }
        has_early_failures = 1;
        return 0;
    }
    total_allocs = num_allocs;
    /* Test with allocation failure after x allocs */
    for (failing_alloc = ignored_allocs+1;
            failing_alloc <= total_allocs; failing_alloc++) {
        if (verbose == 2) {
            fprintf(stderr, "--- testing OOM at allocation %2d (of %d) ---\n", failing_alloc, total_allocs);
        } else if (verbose && (failing_alloc & 0x3f) == 0) {
            fputc('.', stderr);
        }
        num_allocs = 0;
        errors_testcase = 0;
        alloced_cfg = NULL;
        alloced_ctx = NULL;
        not_freed = 0;
        run_test(); /* Run with limited allocations */
        if (testcase_aborted) {
            int i;
            cslul_free(alloced_ctx);
            cslul_config_free(alloced_cfg);
            for (i = 0; i < not_freed; i++) {
                free(tmalloc_ptrs[i]);
            }
        }
    }
    if (verbose == 1 && ignored_allocs+63 <= total_allocs) {
        fputc('\n', stderr);
    }
    return 1;
}

void test_handler(void)
{
    char_by_char = 0;
    if (run_with_oom()) {
        char_by_char = 1;
        run_with_oom();
    }
}

/** Returns 1 if an allocation failure should be simulated */
static int simulate_alloc_failure(void)
{
    num_allocs++;
    /* First run is for counting allocations, so don't fail */
    if (!failing_alloc) return 0;
    return num_allocs >= failing_alloc;
}

void *test_malloc(size_t size)
{
    if (simulate_alloc_failure()) return NULL;
    return malloc(size);
}

void *test_realloc(void *p, size_t newsize)
{
    if (simulate_alloc_failure()) return NULL;
    return realloc(p, newsize);
}

void *aalloc(struct CSlul *ctx, size_t size, size_t align)
{
    if (simulate_alloc_failure()) {
        ctx_outofmem(ctx);
        return NULL;
    }
    return real_aalloc(ctx, size, align);
}

struct CSlulConfig *tcfg(struct CSlulConfig *cfg)
{
    return (alloced_cfg = cfg);
}

struct CSlul *tctx(struct CSlul *ctx)
{
    return (alloced_ctx = ctx);
}

void *tmalloc(size_t size)
{
    void *p = malloc(size);
    if (!p) return NULL;
    assert(not_freed < MAX_TMALLOCS);
    tmalloc_ptrs[not_freed++] = p;
    return p;
}

void tfree(void *p)
{
    assert(not_freed > 0);
    assert(p == tmalloc_ptrs[not_freed-1]);
    not_freed--;
    free(p);
}

const char *error_extra_text(void)
{
    return char_by_char ? "(char by char)" : "";
}

const char *help_extra_text(void)
{
    return
        "This program tests out-of-memory handling. Each test will be run in\n"
        "several iterations; in the first one the first allocation fails, in\n"
        "the second one the second allocation fails, etc.\n";
}

const char *verbose_extra_text(void)
{
    return "OoM-test";
}


int main(int argc, char **argv)
{
    int status;
    crash_test_only = 1;
    status = test_main(argc, argv);
    return status != EXIT_SUCCESS ? status :
            (has_early_failures ? EXIT_FAILURE : EXIT_SUCCESS);
}
