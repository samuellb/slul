/*

  Unit tests of platform.c

  Copyright © 2021-2023 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "../platform.c"
#include "unittest.h"
#include "alltests.h"
#include "testcommon.h"

static struct InterfaceDir *assert_dir(struct InterfaceDir *d, const char *s)
{
    tassert(d != NULL);
    if (!tsoftassert(!strncmp(d->path, s, d->pathlen)) ||
        !tsoftassert(d->pathlen == strlen(s))) {
        tprintf("Unexpected interface dir. "
                "Expected \"%s\" but was \"%.*s\"\n", s, d->pathlen, d->path);
    }
    return d->next;
}

static void test_platform_iface_envvar(void)
{
    struct InterfaceDir *dir;
    struct CSlulConfig *cfg = tcfg(cslul_config_create(NULL));
    struct CSlul *ctx = tctx(cslul_create(cfg));
    tassert(ctx != NULL);
    tsoftassert(add_iface_dirs_from_envvar(cfg, "/test/xyz"));
    tsoftassert(add_iface_dirs_from_envvar(cfg, "\\test\\abc;/home/u/123;/opt/abc/xyz/"));
    dir = ctx->cfg->iface_dirs;
    dir = assert_dir(dir, "\\test\\abc");
    dir = assert_dir(dir, "/home/u/123");
    dir = assert_dir(dir, "/opt/abc/xyz/");
    dir = assert_dir(dir, "/test/xyz");
    tassert(dir == NULL);
    cslul_free(ctx);
    cslul_config_free(cfg);
}

const TestFunctionInfo tests_platform[] = {
    TEST_INFO(test_platform_iface_envvar)
    TEST_END
};
