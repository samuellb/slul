/*

  Unit tests of funcchk.c

  Copyright © 2022-2023 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "../funcchk.c"
#define SLULPARSE
#include "parsecommon.h"
#include "chkcommon.h"
#include "unittest.h"
#include "alltests.h"

static void test_funcchk_return_ok(void)
{
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    TEST_SOURCE("func f() -> bool {\n"
                "    return true\n"
                "}\n", 1);
    DO_VERIFY(ctx);
    free_ctx(ctx);
}

static void test_funcchk_return_wrongtype(void)
{
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    TEST_SOURCE("func f() -> bool {\n"
                "    return \"no\"\n"
                "}\n", 1);
    expect_error(CSLUL_E_INCOMPATIBLETYPES, 2, 12);
    DO_VERIFY(ctx);
    free_ctx(ctx);
}

static struct CtlFor *get_for_loop_in(struct CSlul *ctx, const char *func)
{
    struct IdentDecl *f;
    struct Stmt *s;
    f = (struct IdentDecl *)lookup(
            ctx, ctx->impl.idents_root, func);
    tassert(f != NULL);
    tassert(f->u.funcbody != NULL);
    s = &f->u.funcbody->stmtblock.stmt;
    tassert(s->type == S_FOR);
    tassert(s->u.forstm != NULL);
    return s->u.forstm;
}

static void test_funcchk_for_arrayliteral(void)
{
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    struct CtlFor *forstm;
    TEST_SOURCE("func f() {\n"
                "    for int i in [1,2,3] { }\n"
                "}\n", 1);
    DO_VERIFY(ctx);
    tassert(!ctx->has_fatal_errors);
    forstm = get_for_loop_in(ctx, "f");
    tassert(forstm->looptype == LT_ARRAY);
    free_ctx(ctx);
}

static void test_funcchk_for_arrayident(void)
{
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    struct CtlFor *forstm;
    TEST_SOURCE("func f([3]int arr) {\n"
                "    for int i in arr { }\n"
                "}\n", 1);
    DO_VERIFY(ctx);
    tassert(!ctx->has_fatal_errors);
    forstm = get_for_loop_in(ctx, "f");
    tassert(forstm->looptype == LT_ARRAY);
    free_ctx(ctx);
}

const TestFunctionInfo tests_funcchk[] = {
    TEST_INFO(test_funcchk_return_ok)
    TEST_INFO(test_funcchk_return_wrongtype)
    TEST_INFO(test_funcchk_for_arrayliteral)
    TEST_INFO(test_funcchk_for_arrayident)
    TEST_END
};
