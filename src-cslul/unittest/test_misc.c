/*

  Unit tests of misc.c

  Copyright © 2021-2022 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "../misc.c"
#include "unittest.h"
#include "alltests.h"

#include <string.h>

static void test_unbase64_empty(void)
{
    static const char input[1] = { 0 };
    unsigned char output[1];
    tsoftassert(unbase64(&input[1], 0, &output[1], 0) == 1);
}

static void test_unbase64_tooshort(void)
{
    static const char input[1] = "A"; /* 6 bits only */
    unsigned char output[3];
    tsoftassert(unbase64(input, 1, output, 3) == 0);
}

static void test_unbase64_badchar(void)
{
    static const char input[4] = "AAA!";
    unsigned char output[3];
    tsoftassert(unbase64(input, 4, output, 3) == 0);
}

static void test_unbase64_padding(void)
{
    static const char input[4] = "AAA="; /* Padding is not allowed */
    unsigned char output[3];
    tsoftassert(unbase64(input, 4, output, 3) == 0);
}

static void test_unbase64_one0(void)
{
    static const char input[4] = "AAAA";
    unsigned char output[3];
    tassert(unbase64(input, 4, output, 3) == 1);
    tsoftassert(output[0] == 0x00);
    tsoftassert(output[1] == 0x00);
    tsoftassert(output[2] == 0x00);
}

static void test_unbase64_one1(void)
{
    static const char input[4] = "AAAB";
    unsigned char output[3];
    tassert(unbase64(input, 4, output, 3) == 1);
    tsoftassert(output[0] == 0x00);
    tsoftassert(output[1] == 0x00);
    tsoftassert(output[2] == 0x01);
}

static void test_unbase64_one2(void)
{
    static const char input[4] = "/A/A";
    unsigned char output[3];
    tassert(unbase64(input, 4, output, 3) == 1);
    tsoftassert(output[0] == 0xFC);
    tsoftassert(output[1] == 0x0F);
    tsoftassert(output[2] == 0xC0);
}

static void test_unbase64_multiple(void)
{
    static const char input[16] = "VGVzdCBtZXNzYWdl";
    unsigned char output[12];
    tassert(unbase64(input, 16, output, 12) == 1);
    tsoftassert(!memcmp(output, "Test message", 12));
}

static void test_unbase64_partial1(void)
{
    static const char input[2] = "YQ";
    unsigned char output[1];
    tassert(unbase64(input, 2, output, 1) == 1);
    tsoftassert(!memcmp(output, "a", 1));
}

static void test_unbase64_partial2(void)
{
    static const char input[3] = "YWI";
    unsigned char output[2];
    tassert(unbase64(input, 3, output, 2) == 1);
    tsoftassert(!memcmp(output, "ab", 2));
}

static void test_unbase64_partial_long(void)
{
    static const char input[42] = "VGhpcyBpcyBhIGxvbmcgbWVzc2FnZSEgQUJDMTIzCg";
    unsigned char output[31];
    tassert(unbase64(input, 42, output, 31) == 1);
    tsoftassert(!memcmp(output, "This is a long message! ABC123\n", 31));
}

static void test_unbase64_noextrabits1a(void)
{
    static const char input[2] = "/A";
    unsigned char output[1];
    tassert(unbase64(input, 2, output, 1) == 1);
    tsoftassert(output[0] == 0xfc);
}

static void test_unbase64_noextrabits1b(void)
{
    static const char input[2] = "/Q";
    unsigned char output[1];
    tassert(unbase64(input, 2, output, 1) == 1);
    tsoftassert(output[0] == 0xfd);
}

static void test_unbase64_noextrabits2a(void)
{
    static const char input[3] = "//A";
    unsigned char output[2];
    tassert(unbase64(input, 3, output, 2) == 1);
    tsoftassert(output[0] == 0xff);
    tsoftassert(output[1] == 0xf0);
}

static void test_unbase64_noextrabits2b(void)
{
    static const char input[3] = "//E";
    unsigned char output[2];
    tassert(unbase64(input, 3, output, 2) == 1);
    tsoftassert(output[0] == 0xff);
    tsoftassert(output[1] == 0xf1);
}

/** Tests a base64 encoded string with an extranous bit in
    a 2-character trailer */
static void test_unbase64_extrabits1(void)
{
    static const char input[2] = "/B";
    unsigned char output[2];
    tassert(unbase64(input, 2, output, 2) == 0);
}

/** Tests a base64 encoded string with an extranous bit in
    a 3-character trailer */
static void test_unbase64_extrabits2(void)
{
    static const char input[3] = "//B";
    unsigned char output[2];
    tassert(unbase64(input, 2, output, 2) == 0);
}

struct RangeTestItem {
    uint32 start;
    uint32 end;
    unsigned script;
};

static void test_unicodescript_ranges(void)
{
    static const struct RangeTestItem tests[] = {
        {  0xA1, 0x24F, SCRIPT_LATIN },
        { 0x250, 0x36F, SCRIPT_OTHER },
        { 0x370, 0x3E1, SCRIPT_GREEK },
        { 0x3E2, 0x3EF, SCRIPT_OTHER },
        { 0x3F0, 0x3FF, SCRIPT_GREEK },
        { 0x400, 0x52F, SCRIPT_CYRILLIC },
        { 0x530, 0x58F, SCRIPT_OTHER },
        { 0x590, 0x86F, SCRIPT_RTL },
        { 0x870, 0x89F, SCRIPT_OTHER },
        { 0x8A0, 0x8FF, SCRIPT_RTL },
        { 0x900, 0x10FF, SCRIPT_OTHER },
        { 0x1100, 0x11FF, 0 },
        { 0x1200, 0x1C7F, SCRIPT_OTHER },
        { 0x1C80, 0x1C8F, SCRIPT_CYRILLIC },
        { 0x1C90, 0x1DFF, SCRIPT_OTHER },
        { 0x1E00, 0x1EFF, SCRIPT_LATIN },
        { 0x1F00, 0x1FFF, SCRIPT_GREEK },
        { 0x2010, 0x201F, SCRIPT_SPECIALS },
        { 0x2070, 0x20CF, 0 },
        { 0x20D0, 0x218F, SCRIPT_SPECIALS },
        { 0x2190, 0x23FF, 0 },
        { 0x2400, 0x24FF, SCRIPT_SPECIALS },
        { 0x2501, 0x27FF, 0 }, /* 01 because 00 is an exception */
        { 0x2800, 0x28FF, SCRIPT_OTHER },
        { 0x2900, 0x2BFF, 0 },
        { 0x2C00, 0x2C5F, SCRIPT_OTHER },
        { 0x2C60, 0x2C7F, SCRIPT_LATIN },
        { 0x2C80, 0x2DDF, SCRIPT_OTHER },
        { 0x2DE0, 0x2DFF, SCRIPT_CYRILLIC },
        { 0x2E00, 0x30FF, 0 },
        { 0x3100, 0x312F, SCRIPT_OTHER },
        { 0x3130, 0x318F, 0 },
        { 0x3190, 0x31BF, SCRIPT_OTHER },
        { 0x31C0, 0x32CB, 0 },
        { 0x32CC, 0x32CF, SCRIPT_OTHER },
        { 0x32D0, 0x9FFF, 0 },
        { 0xA000, 0xA63F, SCRIPT_OTHER },
        { 0xA640, 0xA69F, SCRIPT_CYRILLIC },
        { 0xA6A0, 0xA71F, SCRIPT_OTHER },
        { 0xA720, 0xA7FF, SCRIPT_LATIN },
        { 0xA800, 0xA95F, SCRIPT_OTHER },
        { 0xA960, 0xA97F, 0 },
        { 0xA980, 0xAB2F, SCRIPT_OTHER },
        { 0xAB30, 0xAB6F, SCRIPT_LATIN },
        { 0xAB70, 0xABFF, SCRIPT_OTHER },
        { 0xAC00, 0xD7FF, 0 },
        { 0xE000, 0xF8FF, SCRIPT_SPECIALS },
        { 0xF900, 0xFAFF, 0 },
        { 0xFB00, 0xFB1C, SCRIPT_OTHER },
        { 0xFB1D, 0xFDFF, SCRIPT_RTL },
        { 0xFE00, 0xFE1F, SCRIPT_SPECIALS },
        { 0xFE20, 0xFE2F, SCRIPT_OTHER },
        { 0xFE30, 0xFE4F, 0 },
        { 0xFE50, 0xFE6F, SCRIPT_SPECIALS },
        { 0xFE70, 0xFEFC, SCRIPT_RTL },
        { 0xFF00, 0xFFFF, SCRIPT_SPECIALS },
        { 0x10000, 0x1018F, SCRIPT_OTHER },
        { 0x10190, 0x101CF, SCRIPT_SPECIALS },
        { 0x101D0, 0x107FF, SCRIPT_OTHER },
        { 0x10800, 0x10D3F, SCRIPT_RTL },
        { 0x10D40, 0x10E7F, SCRIPT_OTHER },
        { 0x10E80, 0x10FFF, SCRIPT_RTL },
        { 0x11000, 0x1D0FF, SCRIPT_OTHER },
        { 0x1D100, 0x1D1FF, SCRIPT_SPECIALS },
        { 0x1D200, 0x1D3FF, SCRIPT_OTHER },
        { 0x1D400, 0x1D7FF, SCRIPT_SPECIALS },
        { 0x1D800, 0x1EDFF, SCRIPT_OTHER },
        { 0x1EE00, 0x1EEFF, SCRIPT_RTL },
        { 0x1F000, 0x1F0FF, SCRIPT_OTHER },
        { 0x1F100, 0x1F2FF, SCRIPT_SPECIALS },
        { 0x1F300, 0x1F6FF, 0 },
        { 0x1F700, 0x1F77F, SCRIPT_SPECIALS },
        { 0x1F780, 0x1F7A0, 0 },
        { 0x1F7A1, 0x1F7BA, SCRIPT_SPECIALS },
        { 0x1F7BB, 0x1FB6F, 0 },
        { 0x1FB70, 0x1FB7B, SCRIPT_SPECIALS },
        { 0x1FB7C, 0x1FBFF, 0 },
        { 0x1FC00, 0x1FFFF, SCRIPT_OTHER },
        { 0x20000, 0x3134F, 0 },
        { 0xE0000, 0x10FFFF, SCRIPT_SPECIALS }
    };
    const struct RangeTestItem *range = &tests[0];
    for (;;) {
        if (!tsoftassert(get_unicode_script(range->start) == range->script)) {
            tprintf("- codepoint %x (start of range)\n", range->start);
            tprintf("- was %x\n", get_unicode_script(range->start));
        }
        if (!tsoftassert(get_unicode_script(range->end) == range->script)) {
            tprintf("- codepoint %x (end of range)\n", range->end);
        }
        if (range->end == 0x10FFFF) break;
        range++;
    }
}

static void test_unicodescript_exceptions1(void)
{
    tsoftassert(get_unicode_script(0x200E) == SCRIPT_RTL);
    tsoftassert(get_unicode_script(0x2024) == SCRIPT_SPECIALS);
    tsoftassert(get_unicode_script(0x33FF) == SCRIPT_OTHER);
    tsoftassert(get_unicode_script(0x132) == SCRIPT_OTHER);
    tsoftassert(get_unicode_script(0x133) == SCRIPT_OTHER);
}

static void test_unicodescript_exceptions2(void)
{
    const uint32 *exc = unicode_exceptions;
    int numleft = sizeof(unicode_exceptions) / sizeof(uint32);
    for (; numleft--; exc++) {
        unsigned script = get_unicode_script(*exc);
        /* There are no exceptions with latin or 0 */
    if (!tsoftassert(script != SCRIPT_LATIN && script != 0)) {
            tprintf("- codepoint %x\n", *exc);
        }
    }
}

#define NUM_EXC (sizeof(unicode_exceptions) / sizeof(uint32))
static void test_unicodescript_exceptions3(void)
{
    const struct CharRange *r = &unicode_scripts[0];
    unsigned lower_bound = 0;
    char matched[NUM_EXC] = { 0 };
    int num_exc = 0;
    for (;;) {
        const uint32 *exc;
        int numleft;
        unsigned ind;
        if (!r->except_end) goto skip;
        if (!tsoftassert(r->upper_bound >= lower_bound)) {
            tprintf("- range is not sorted: %x (prev. %x)\n",
                    r->upper_bound, lower_bound);
        }
        tsoftassert(r->except_script > 0);
        tassert(r->except_end >= r->except_start);
        tassert(r->except_end < NUM_EXC);
        exc = &unicode_exceptions[r->except_start];
        numleft = r->except_end - r->except_start + 1;
        ind = r->except_start;
        while (numleft--) {
            uint32 code;
            tassert(ind < NUM_EXC);
            /* Check that the exception entries are not used more than once */
            tsoftassert(matched[ind] == 0);
            matched[ind] = 1;
            /* Check that the exception entries are in range */
            code = *exc;
            if (!tsoftassert(code >= lower_bound) ||
                !tsoftassert(code <= r->upper_bound)) {
                tprintf("- codepoint %x,  range %x-%x\n",
                        code, lower_bound, r->upper_bound);
            }
            num_exc++;
            exc++;
            ind++;
        }
      skip:
        if (r->upper_bound == 0x10FFFF) break;
        lower_bound = r->upper_bound+1;
        r++;
    }
    /* Check that all exception entries have been referenced */
    tsoftassert(num_exc == NUM_EXC);
}

static void test_versioncmp(void)
{
    #define VERCMP(a, b) do { \
            tsoftassert(versioncmp((a), strlen(a), (b), strlen(b)) < 0); \
            tsoftassert(versioncmp((b), strlen(b), (a), strlen(a)) > 0); \
        } while (0)
    tsoftassert(versioncmp("1", 1, "1", 1) == 0);
    VERCMP("0", "1");
    VERCMP("1", "2");
    VERCMP("9", "10");
    VERCMP("0.0", "0.1");
    VERCMP("0.8", "0.9");
    VERCMP("0.1", "0.10");
    VERCMP("0.9", "0.10");
    VERCMP("0.1", "0.11");
    VERCMP("0.9", "0.11");
    VERCMP("0.1.9", "0.2.0");
    VERCMP("0.123123", "0.123124");
    VERCMP("0.1231239", "0.1231241");
    VERCMP("0.1231231.1", "0.1231241.1");
    VERCMP("0.11111111111111111111111111111",
           "0.11111111111111111111111111112");
    VERCMP("0.99999999999999999999999999999",
           "0.100000000000000000000000000000");
    VERCMP("0.11111111111111111111111111111.0",
           "0.11111111111111111111111111112.0");
    VERCMP("0.99999999999999999999999999999.0",
           "0.100000000000000000000000000000.0");
    VERCMP("8.1", "9.0");
    VERCMP("123.49", "123.51");
    VERCMP("10~alpha1", "10");
    VERCMP("10~beta1", "10a");
    VERCMP("10~beta1", "10a~a");
    VERCMP("10.101020203030404~a", "10.101020203030404");
    VERCMP("1.10~alpha1", "1.10.1");
    VERCMP("1.10~beta1", "1.10.a");
    VERCMP("10", "10a");
    VERCMP("10a", "10b");
    VERCMP("10a", "12");
    VERCMP("1", "10a");
    VERCMP("1b", "10a");
    VERCMP("9.10", "9.10a");
    VERCMP("9.10a", "9.10b");
    VERCMP("9.10a", "9.12");
    VERCMP("1.10.0", "1.11");
    VERCMP("1.9", "1.10.0");
    VERCMP("1.10~a", "1.10.0");
    VERCMP("1.10~9", "1.10.0");
    VERCMP("1.10~0", "1.10~a");
    VERCMP("1.10~a", "1.10~b");
    VERCMP("1.10~alpha", "1.10~beta");
    VERCMP("1.10~rc9", "1.10~rc11");
    VERCMP("1.10~rc9.99", "1.10~rc11");
    VERCMP("1.10~rc9.99", "1.10~rc11.1");
    VERCMP("1.10~rc11.1", "1.10~rc99.9");
    /* Strange cases - Please do not mix version formats like this! */
    VERCMP("1.10", "1.10.0");
    VERCMP("1.1", "1.0001");
    VERCMP("1.1", "1.0002");
    VERCMP("1.0001", "1.2");
    VERCMP("1.00", "1.000");
    VERCMP("1.1.2", "1.0001.2");
    VERCMP("1.10~rc01", "1.10~rc001");
    VERCMP("2021.3", "2021.04");
    VERCMP("2021.4", "2021.04");
    VERCMP("2021.04", "2021.5");
    VERCMP("2021.3.05", "2021.04.05");
    VERCMP("2021.4.05", "2021.04.05");
    VERCMP("2021.04.05", "2021.5.05");
    #undef VERCMP
}

const TestFunctionInfo tests_misc[] = {
    TEST_INFO(test_unbase64_empty)
    TEST_INFO(test_unbase64_tooshort)
    TEST_INFO(test_unbase64_badchar)
    TEST_INFO(test_unbase64_padding)
    TEST_INFO(test_unbase64_one0)
    TEST_INFO(test_unbase64_one1)
    TEST_INFO(test_unbase64_one2)
    TEST_INFO(test_unbase64_multiple)
    TEST_INFO(test_unbase64_partial1)
    TEST_INFO(test_unbase64_partial2)
    TEST_INFO(test_unbase64_partial_long)
    TEST_INFO(test_unbase64_noextrabits1a)
    TEST_INFO(test_unbase64_noextrabits1b)
    TEST_INFO(test_unbase64_noextrabits2a)
    TEST_INFO(test_unbase64_noextrabits2b)
    TEST_INFO(test_unbase64_extrabits1)
    TEST_INFO(test_unbase64_extrabits2)
    TEST_INFO(test_unicodescript_ranges)
    TEST_INFO(test_unicodescript_exceptions1)
    TEST_INFO(test_unicodescript_exceptions2)
    TEST_INFO(test_unicodescript_exceptions3)
    TEST_INFO(test_versioncmp)
    TEST_END
};
