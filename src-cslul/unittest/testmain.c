/*

  testmain.c -- Main entry point for unit tests

  Copyright © 2016-2023 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#define CSLUL_IN_TESTS
#include "testalloc.h"

#include <stdlib.h>
#include <stdio.h>

#define TEST_MAIN
#include "unittest.h"
#include "testcommon.h"
#include "alltests.h"
TESTCALLS_START
#undef TEST_D
#define TEST_D TEST_D2
#include "alltests.h"
TESTCALLS_END

void test_handler(void)
{
    /* Each test is ran twice, first in normal mode,
       and then in char-by-char mode */
    char_by_char = 0;
    for (;;) {
        num_ctxs = 0;
        run_test(); /* Run! */
        if (testcase_aborted) break;
        if (num_ctxs) {
            num_ctxs = 0;
            tsoftfail("Memory leak in test");
            break;
        }
        if (errors_testcase) break;
        /* Next mode */
        if (!char_by_char) char_by_char = 1;
        else break;
    }
}

const char *error_extra_text(void)
{
    return char_by_char ? "(char by char)" : "";
}

const char *help_extra_text(void)
{
    return "This executable contains the unit tests.\n";
}

const char *verbose_extra_text(void)
{
    return NULL;
}

/* Each test case is run in normal mode an in char-by-char mode. */
int char_by_char = 0;

/* Overrides of various functions.
   "oommain.c" has different overrides, for tracking memory handling. */
void *test_malloc(size_t size) { return malloc(size); }
void *test_realloc(void *p, size_t newsize) { return realloc(p, newsize); }
void *aalloc(struct CSlul *ctx, size_t size, size_t align) {
    return real_aalloc(ctx, size, align);
}

struct CSlul *tctx(struct CSlul *ctx) { return ctx; }
struct CSlulConfig *tcfg(struct CSlulConfig *cfg) { return cfg; }
void *tmalloc(size_t size) { return malloc(size); }
void tfree(void *p) { free(p); }

int main(int argc, char **argv)
{
    return test_main(argc, argv);
}

