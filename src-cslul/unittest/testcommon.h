/*

  testcommon.h -- Definitions of various common functions for cslul tests

  Copyright © 2016-2023 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/
#ifndef CSLUL_TESTCOMMON_H
#define CSLUL_TESTCOMMON_H

#include "unittest.h"
#define CSLUL_IN_TESTS
#include "../internal.h"
#include <stdio.h> /* several tests print error details using fprintf */

/** Whether the current test should do parsing character-by-character(=1)
    or in buffers(=0) controlled by the test. */
extern int char_by_char;

/* Memory allocation functions */
/** Calls malloc and then registers the allocation, so it can be free'd */
void *tmalloc(size_t size);
/** De-registers and frees the last allocation from tmalloc */
void tfree(void *p);
/** Registers a CSlulConfig object so it can be free'd */
struct CSlulConfig *tcfg(struct CSlulConfig *cfg);
/** Registers a CSlul object so it can be free'd */
struct CSlul *tctx(struct CSlul *ctx);

#endif

