/*

  alltests.h -- List of the unit tests for the compiler frontend

  Copyright © 2021-2023 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

TEST_D(arch)
TEST_D(arena)
TEST_D(build)
TEST_D(builtins)
TEST_D(bwrapper)
TEST_D(chkutil)
TEST_D(config)
TEST_D(context)
TEST_D(errors)
TEST_D(exprchk)
TEST_D(funcchk)
TEST_D(ir)
TEST_D(mhparse)
TEST_D(mhtoken)
TEST_D(misc)
TEST_D(parse)
TEST_D(platform)
TEST_D(token)
TEST_D(tree)
TEST_D(tlverify)
TEST_D(typechk)
TEST_D(typecompat)
#ifdef CSLUL_WINDOWS
TEST_D(winlibc)
#endif
