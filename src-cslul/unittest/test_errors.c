/*

  Unit tests of errors.c

  Copyright © 2021-2022 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "../errors.c"
#include "unittest.h"
#include "alltests.h"

#include <string.h>

static void test_firstmsg(void)
{
    tsoftassert(!strcmp(cslul_lookup_message(CSLUL_E_NOERROR),
                "No error. This is a bug in the compiler"));
}

static void test_secondmsg(void)
{
    tsoftassert(!strcmp(cslul_lookup_message(CSLUL_E_OUTOFMEMORY),
                "Out of memory"));
}

static void test_lastmsg(void)
{
    tsoftassert(cslul_lookup_message(CSLUL_E_LAST) == NULL);
}

static void test_internalerror(void)
{
    tsoftassert(!strcmp(cslul_lookup_message(MAKE_INTERR(1, INTERRBASE_ARENA)),
                "arena"));
}

static void test_errorlevel(void)
{
    tsoftassert(lookup_error_level(CSLUL_E_OUTOFMEMORY) == CSLUL_L_FATAL);
    tsoftassert(lookup_error_level(CSLUL_E_INVALIDCHAR) == CSLUL_L_ERROR);
    tsoftassert(lookup_error_level(CSLUL_E_TAB) == CSLUL_L_STYLE);
    tsoftassert(lookup_error_level(CSLUL_E_BADATTRNAME) == CSLUL_L_WARNING);
    tsoftassert(lookup_error_level(CSLUL_E_LAST)==(enum CSlulMessageLevel)-1);
}

const TestFunctionInfo tests_errors[] = {
    TEST_INFO(test_firstmsg)
    TEST_INFO(test_secondmsg)
    TEST_INFO(test_lastmsg)
    TEST_INFO(test_internalerror)
    TEST_INFO(test_errorlevel)
    TEST_END
};
