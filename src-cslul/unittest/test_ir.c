/*

  Unit tests of ir.c

  Copyright © 2021-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#define DISABLE_CSBE /* Do not actually test CSBE. It has it's own tests. */
#include "../ir.c"
#define SLULPARSE
#include "parsecommon.h"
#include "chkcommon.h"
#include "unittest.h"
#include "alltests.h"

static void test_get_dimension_infos(void)
{
    struct IdentDecl *ai;
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    TEST_SOURCE("data [2,3,5]int arr = [\n"
                "    [\n"
                "        [  0,  1,  2,  3,  4],\n"
                "        [ 10, 11, 12, 13, 14],\n"
                "        [ 20, 21, 22, 23, 24],\n"
                "    ],\n"
                "    [\n"
                "        [100,101,102,103,104],\n"
                "        [110,111,112,113,114],\n"
                "        [120,121,122,123,124],\n"
                "    ],\n"
                "]\n", 1);
    DO_VERIFY(ctx);
    tassert(!ctx->has_fatal_errors);
    ai = (struct IdentDecl *)lookup(ctx, ctx->impl.idents_root, "arr");
    tassert(ai);
    tassert(ai->u.initval);
    tassert(ai->u.initval->root);

    tassert(get_dimension_infos(ctx, ai->u.initval->root, 1));
    tassert(ctx->ir_dims_capacity == 1);
    tsoftassert(ctx->ir_dims_buffer[0].rowlen == 2*3*5);
    tsoftassert(ctx->ir_dims_buffer[0].elemlen == 3*5);
    memset(ctx->ir_dims_buffer, 0xAA, sizeof(struct DimensionInfo));

    tassert(get_dimension_infos(ctx, ai->u.initval->root, 2));
    tassert(ctx->ir_dims_capacity == 2);
    tsoftassert(ctx->ir_dims_buffer[0].rowlen == 2*3*5);
    tsoftassert(ctx->ir_dims_buffer[0].elemlen == 3*5);
    tsoftassert(ctx->ir_dims_buffer[1].rowlen == 3*5);
    tsoftassert(ctx->ir_dims_buffer[1].elemlen == 5);
    memset(ctx->ir_dims_buffer, 0xAA, 2*sizeof(struct DimensionInfo));

    tassert(get_dimension_infos(ctx, ai->u.initval->root, 3));
    tassert(ctx->ir_dims_capacity == 3);
    tsoftassert(ctx->ir_dims_buffer[0].rowlen == 2*3*5);
    tsoftassert(ctx->ir_dims_buffer[0].elemlen == 3*5);
    tsoftassert(ctx->ir_dims_buffer[1].rowlen == 3*5);
    tsoftassert(ctx->ir_dims_buffer[1].elemlen == 5);
    tsoftassert(ctx->ir_dims_buffer[2].rowlen == 5);
    tsoftassert(ctx->ir_dims_buffer[2].elemlen == 1);

    free_ctx(ctx);
}

const TestFunctionInfo tests_ir[] = {
    TEST_INFO(test_get_dimension_infos)
    TEST_END
};
