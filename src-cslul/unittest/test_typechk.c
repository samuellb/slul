/*

  Unit tests of typechk.c

  Copyright © 2022-2023 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "../typechk.c"
#define SLULPARSE
#include "parsecommon.h"
#include "chkcommon.h"
#include "unittest.h"
#include "alltests.h"


static void assert_enumval(struct EnumValueEntry **valptr, int num)
{
    struct EnumValueEntry *v = *valptr;
    unsigned absnum;
    unsigned flags;
    tassert(v != NULL);
    tassert(v->e.vd.decl.u.initval != NULL);
    tassert(v->e.vd.decl.u.initval->root != NULL);
    tassert(v->e.vd.decl.u.initval->root->exprtype == E_INTEGER);
    if (num >= 0) {
        absnum = num;
        flags = 0;
    } else {
        absnum = -num;
        flags = EIF_NEGATIVE;
    }
    tassert(v->e.vd.decl.u.initval->root->a.intval == absnum);
    tassert(v->e.vd.decl.u.initval->root->b.intflags == flags);
    *valptr = v->next;
}

static void test_typechk_enum_increment(void)
{
    struct TypeDecl *td;
    struct EnumValueEntry *val;
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    TEST_SOURCE("type TestEnum = enum int {\n"
                "    a = -2\n"
                "    b\n" /* -1 */
                "    c\n" /*  0 */
                "    d\n" /*  1 */
                "}\n", 1);
    DO_VERIFY(ctx);
    td = (struct TypeDecl *)lookup(
            ctx, ctx->impl.types_root, "TestEnum");
    tassert(td != NULL);
    tassert(td->type.u.enu != NULL);
    val = td->type.u.enu->values;
    assert_enumval(&val, -2);
    assert_enumval(&val, -1);
    assert_enumval(&val, 0);
    assert_enumval(&val, 1);
    free_ctx(ctx);
}

/* Combined testing of enum values and constexpr evaluation */
static void test_typechk_enum_constexpr_addsub(void)
{
    struct TypeDecl *td;
    struct EnumValueEntry *val;
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    TEST_SOURCE("type TestEnum = enum int {\n"
                "    add1 =  1 +  2\n"
                "    add2 =  2 +  1\n"
                "    add3 =  2 + -1\n"
                "    add4 = -2 +  1\n"
                "    add5 =  1 + -2\n"
                "    add6 = -1 +  2\n"
                "    add7 = -1 + -2\n"
                "    sub1 =  1 -  2\n"
                "    sub2 =  2 -  1\n"
                "    sub3 =  2 - -1\n"
                "    sub4 = -2 -  1\n"
                "    sub5 =  1 - -2\n"
                "    sub6 = -1 -  2\n"
                "    sub7 = -1 - -2\n"
                "}\n", 1);
    DO_VERIFY(ctx);
    td = (struct TypeDecl *)lookup(
            ctx, ctx->impl.types_root, "TestEnum");
    tassert(td != NULL);
    tassert(td->type.u.enu != NULL);
    val = td->type.u.enu->values;
    assert_enumval(&val, 3); /* add1 =  1 +  2 */
    assert_enumval(&val, 3); /* add2 =  2 +  1 */
    assert_enumval(&val, 1); /* add3 =  2 + -1 */
    assert_enumval(&val,-1); /* add4 = -2 +  1 */
    assert_enumval(&val,-1); /* add5 =  1 + -2 */
    assert_enumval(&val, 1); /* add6 = -1 +  2 */
    assert_enumval(&val,-3); /* add7 = -1 + -2 */
    assert_enumval(&val,-1); /* sub1 =  1 -  2 */
    assert_enumval(&val, 1); /* sub2 =  2 -  1 */
    assert_enumval(&val, 3); /* sub3 =  2 - -1 */
    assert_enumval(&val,-3); /* sub4 = -2 -  1 */
    assert_enumval(&val, 3); /* sub5 =  1 - -2 */
    assert_enumval(&val,-3); /* sub6 = -1 -  2 */
    assert_enumval(&val, 1); /* sub7 = -1 - -2 */
    free_ctx(ctx);
}

static void test_typechk_enum_constexpr_mul(void)
{
    struct TypeDecl *td;
    struct EnumValueEntry *val;
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    TEST_SOURCE("type TestEnum = enum int {\n"
                "    mul1 =  2 *  3\n"
                "    mul2 =  3 *  2\n"
                "    mul3 =  3 * -2\n"
                "    mul4 = -3 *  2\n"
                "    mul5 =  2 * -3\n"
                "    mul6 = -2 *  3\n"
                "    mul7 = -2 * -3\n"
                "    mul8 = -2 *  0\n"
                "}\n", 1);
    DO_VERIFY(ctx);
    td = (struct TypeDecl *)lookup(
            ctx, ctx->impl.types_root, "TestEnum");
    tassert(td != NULL);
    tassert(td->type.u.enu != NULL);
    val = td->type.u.enu->values;
    assert_enumval(&val, 6); /* mul1 =  2 *  3 */
    assert_enumval(&val, 6); /* mul2 =  3 *  2 */
    assert_enumval(&val,-6); /* mul3 =  3 * -2 */
    assert_enumval(&val,-6); /* mul4 = -3 *  2 */
    assert_enumval(&val,-6); /* mul5 =  2 * -3 */
    assert_enumval(&val,-6); /* mul6 = -2 *  3 */
    assert_enumval(&val, 6); /* mul7 = -2 * -3 */
    assert_enumval(&val, 0); /* mul8 = -2 *  0 */
    free_ctx(ctx);
}

static void test_typechk_enum_constexpr_div(void)
{
    struct TypeDecl *td;
    struct EnumValueEntry *val;
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    TEST_SOURCE("type TestEnum = enum int {\n"
                "    div1 =  3 /  2\n"
                "    div2 =  4 /  2\n"
                "    div3 =  7 / -1\n"
                "    div4 = -3 /  2\n"
                "    div5 =  3 / -2\n"
                "    div6 = -3 / -2\n"
                "    div7 = -2 / -3\n"
                "    div8 =  0 /  3\n"
                "    div9 =  0 / -3\n"
                "}\n", 1);
    DO_VERIFY(ctx);
    td = (struct TypeDecl *)lookup(
            ctx, ctx->impl.types_root, "TestEnum");
    tassert(td != NULL);
    tassert(td->type.u.enu != NULL);
    val = td->type.u.enu->values;
    assert_enumval(&val, 1); /* div1 =  3 /  2 */
    assert_enumval(&val, 2); /* div2 =  4 /  2 */
    assert_enumval(&val,-7); /* div3 =  7 / -1 */
    assert_enumval(&val,-1); /* div4 = -3 /  2 */
    assert_enumval(&val,-1); /* div5 =  3 / -2 */
    assert_enumval(&val, 1); /* div6 = -3 / -2 */
    assert_enumval(&val, 0); /* div7 = -2 / -3 */
    assert_enumval(&val, 0); /* div8 =  0 /  3 */
    assert_enumval(&val, 0); /* div9 =  0 / -3 */
    free_ctx(ctx);
}

static void test_typechk_enum_constexpr_mod(void)
{
    struct TypeDecl *td;
    struct EnumValueEntry *val;
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    TEST_SOURCE("type TestEnum = enum int {\n"
                "    mod1 =  5 mod  3\n"
                "    mod2 =  4 mod  2\n"
                "    mod3 =  7 mod -1\n"
                "    mod4 = -5 mod  3\n"
                "    mod5 =  5 mod -3\n"
                "    mod6 = -5 mod -3\n"
                "    mod7 = -3 mod -5\n"
                "    mod8 =  0 mod  5\n"
                "    mod9 =  0 mod -5\n"
                "}\n", 1);
    DO_VERIFY(ctx);
    td = (struct TypeDecl *)lookup(
            ctx, ctx->impl.types_root, "TestEnum");
    tassert(td != NULL);
    tassert(td->type.u.enu != NULL);
    val = td->type.u.enu->values;
    assert_enumval(&val, 2); /* mod1 =  5 mod  3 */
    assert_enumval(&val, 0); /* mod2 =  4 mod  2 */
    assert_enumval(&val, 0); /* mod3 =  7 mod -1 */
    assert_enumval(&val, 1); /* mod4 = -5 mod  3 */
    assert_enumval(&val,-1); /* mod5 =  5 mod -3 */
    assert_enumval(&val,-2); /* mod6 = -5 mod -3 */
    assert_enumval(&val,-3); /* mod7 = -3 mod -5 */
    assert_enumval(&val, 0); /* mod8 =  0 mod  5 */
    assert_enumval(&val, 0); /* mod9 =  0 mod -5 */
    free_ctx(ctx);
}

const TestFunctionInfo tests_typechk[] = {
    TEST_INFO(test_typechk_enum_increment)
    TEST_INFO(test_typechk_enum_constexpr_addsub)
    TEST_INFO(test_typechk_enum_constexpr_mul)
    TEST_INFO(test_typechk_enum_constexpr_div)
    TEST_INFO(test_typechk_enum_constexpr_mod)
    TEST_END
};
