/*

  Unit tests of context.c

  Copyright © 2021-2023 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include <stdlib.h>

#define CSLUL_IN_TESTS
/* Override malloc/realloc in context.c so errors can be injected (used by
   oomtest, and affects unit tests in all files, not just this one) */
#include "testalloc.h"
#define MALLOC(n) test_malloc(n)
#define REALLOC(p,n) test_realloc(p,n)
#include "../context.c"
#undef MALLOC
#undef REALLOC

#include "unittest.h"
#include "alltests.h"
#include "testcommon.h"

static void test_msghandler_null(void)
{
    struct CSlul *ctx;
    ctx = tctx(cslul_create(NULL));
    tassert(ctx != NULL);
    tsoftassert(!ctx->has_errors);
    error_linecol(ctx, CSLUL_E_OPENFILE, 0, 0); /* should not attempt to
                                                   call NULL msghandler */
    tsoftassert(ctx->has_errors);
    cslul_free(ctx);
}

const TestFunctionInfo tests_context[] = {
    TEST_INFO(test_msghandler_null)
    TEST_END
};
