/*

  Unit tests of build.c

  Copyright © 2021-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "../build.c"
#include "unittest.h"
#include "alltests.h"
#include "testcommon.h"

#include <stdlib.h>


static struct CSlulConfig *cfg;
static enum CSlulErrorCode expected_error;
static int error_reported;

static void msghandler(const struct CSlulState *state)
{
    if (state->errorcode == expected_error) {
        error_reported = 1;
    } else {
        char s[200];
        sprintf(s, "Unexpected error %d was reported at %d.%d.",
                (int)state->errorcode,
                state->locs[0].line, state->locs[0].column);
        tsoftfail(s);
        if (!quiet) {
            tprintf("- error description: %-40s\n",
                    cslul_lookup_message(state->errorcode));
        }
    }
}

#define DUMMYFILE ((CSlulFile)111)
#define OUTPUTFILE ((CSlulFile)222)

static const char *expected_name;
static size_t expected_nmemb;
static CSlulFile ret_fopen;
static int ret_ferror;
static size_t ret_fread;
static int fclose_called, remove_called;
static const char *mocked_data;

static CSlulFile mocked_fopen(const char *name, const char *mode)
{
    tsoftassert(!strcmp(expected_name, name));
    tsoftassert(!strcmp("rb", mode));
    return ret_fopen;
}

static CSlulFile mocked_createexec(const char *filename)
{
    tsoftassert(filename != NULL);
    return OUTPUTFILE;
}

static int mocked_fclose(CSlulFile file)
{
    tsoftassert(file == DUMMYFILE || file == OUTPUTFILE);
    fclose_called = 1;
    return 0;
}

static int mocked_ferror(CSlulFile file)
{
    tsoftassert(file == DUMMYFILE);
    return ret_ferror;
}

static int mocked_remove(const char *filename)
{
    tsoftassert(filename != NULL);
    remove_called = 1;
    return 0;
}

static size_t mocked_fread(void *buffer, size_t size, size_t nmemb,
                           CSlulFile file)
{
    size_t num_read;
    tassert(buffer != NULL);
    tsoftassert(size == 1);
    tsoftassert(nmemb == expected_nmemb);
    tsoftassert(file == DUMMYFILE);

    num_read = ret_fread;
    if (num_read > nmemb) { /* handle large buffers */
        num_read = nmemb;
    }
    ret_fread -= num_read;
    if (mocked_data) {
        memcpy(buffer, mocked_data, num_read);
        mocked_data += num_read;
    }
    return num_read;
}

static size_t mocked_fwrite(const void *buffer, size_t size, size_t nmemb, CSlulFile file)
{
    tsoftassert(buffer != NULL);
    tsoftassert(size == 1);
    tsoftassert(nmemb >= 1);
    tsoftassert(file == OUTPUTFILE);
    return nmemb;
}

static struct CSlul *create_ctx(void)
{
    static const struct CSlulInitParams prm = {
        sizeof(struct CSlulInitParams), '#', /* using # as dirsep */
        NULL, NULL, NULL, /* malloc, free, realloc */
        mocked_fopen, mocked_createexec, mocked_fclose, mocked_ferror,
        mocked_remove, mocked_fwrite, mocked_fread,
        NULL, NULL /* mkdir, dropprivs */
    };
    struct CSlul *ctx;

    cfg = tcfg(cslul_config_create(&prm));
    tassert(cfg != NULL);
    cslul_config_set_message_handler(cfg, msghandler);
    cslul_config_add_arch(cfg, "aarch64-linux-gnu");
    cfg->skip_implicit_slulrt_dep = 1; /* We're not testing the runtime */

    ctx = tctx(cslul_create(cfg));
    tassert(ctx != NULL);
    /* We're not testing the runtime. Skip the check for SlulApp.main */
    ctx->has_app_main = 1;
    return ctx;
}

static void before_test(void)
{
    error_reported = 0;
    fclose_called = 0;
    remove_called = 0;
}

static void test_fopen_failure(void)
{
    struct CSlul *ctx = create_ctx();
    ret_fopen = NULL;
    expected_error = CSLUL_E_OPENFILE;

    expected_name = "main.slul";
    tassert(!cslul_build(ctx, NULL));
    tassert(error_reported);
    tassert(!fclose_called);
    tassert(!remove_called);

    expected_name = "some_dir#main.slul";
    before_test();
    tassert(!cslul_build(ctx, "some_dir"));
    tassert(error_reported);
    tassert(!fclose_called);
    tassert(!remove_called);
    cslul_free(ctx);
    cslul_config_free(cfg);
}

static void test_fread_failure(void)
{
    struct CSlul *ctx = create_ctx();
    expected_name = "main.slul";
    ret_fopen = DUMMYFILE;
    expected_nmemb = PARSEBUFFER;
    ret_fread = 0;
    ret_ferror = 1;
    expected_error = CSLUL_E_READFILE;
    mocked_data = NULL;

    tassert(!cslul_build(ctx, NULL));
    tassert(error_reported);
    tassert(fclose_called);
    tassert(!remove_called);
    cslul_free(ctx);
    cslul_config_free(cfg);
}

static void test_deletion_on_error(void)
{
    static const char source[] =
        "\\slul 0.0.0\n"
        "\\name test\n"
        "\n"
        "type type\n";
    struct CSlul *ctx = create_ctx();
    expected_name = "main.slul";
    ret_fopen = DUMMYFILE;
    expected_nmemb = PARSEBUFFER;
    ret_fread = sizeof(source)-1;
    ret_ferror = 0;
    expected_error = CSLUL_E_IDENTIFIEREXPECTED;
    mocked_data = source;

    tassert(!cslul_build(ctx, NULL));
    tassert(error_reported);
    tassert(fclose_called);
    tassert(remove_called);
    cslul_free(ctx);
    cslul_config_free(cfg);
}

static int check_long_source(const char *modheader, size_t mhsize,
                             const char *otherfunc, size_t ofsize)
{
    char *data;
    int ret;
    struct CSlul *ctx = create_ctx();
    expected_name = "main.slul";
    ret_fopen = DUMMYFILE;
    expected_nmemb = PARSEBUFFER;
    ret_fread = PARSEBUFFER + ofsize - 2;
    ret_ferror = 0;

    /* Create a buffer which has a "func" keyword spanning over two buffers */
    data = tmalloc(ret_fread);
    memcpy(data, modheader, mhsize);
    memset(&data[mhsize], '\n', PARSEBUFFER-mhsize-2);
    memcpy(&data[PARSEBUFFER-2], otherfunc, ofsize);
    mocked_data = data;

    error_reported = 0;
    fclose_called = 0;
    ret = cslul_build(ctx, NULL);
    tassert(fclose_called);
    tassert(!remove_called);
    tfree(data);
    cslul_free(ctx);
    cslul_config_free(cfg);
    return ret;
}

/** Test of module of application type with source longer than BUFFERSIZE */
static void test_long_app(void)
{
    static const char modheader[] =
        "\\slul 0.0.0\n"
        "\\name test\n"
        "\n"
        "func f() -> int\n"
        "{\n"
        "    return 1\n"
        "}\n"
        "\n";
    static const char otherfunc[] =
        "func g()\n"
        "{\n"
        "}\n"
        "\n";
    int build_ret = check_long_source(modheader, sizeof(modheader)-1,
                                      otherfunc, sizeof(otherfunc)-1);
    tsoftassert(build_ret);
}

/** Test of module of library type with source longer than BUFFERSIZE */
static void test_long_library(void)
{
    static const char modheader[] =
        "\\slul 0.0.0\n"
        "\\name test\n"
        "\\type library\n"
        "\n"
        "func f() -> int\n"
        "\n";
    static const char otherfunc[] =
        "func g()\n"
        "\n";
    int build_ret;
    expected_error = CSLUL_E_MISSINGSOURCE;
    build_ret = check_long_source(modheader, sizeof(modheader)-1,
                                  otherfunc, sizeof(otherfunc)-1);
    tsoftassert(!build_ret);
    tsoftassert(error_reported);
}

static void test_add_sources(void)
{
    struct CSlul *ctx = create_ctx();
    struct CSlulSrcIter srciter;

    cslul_ll_start_phase(ctx, CSLUL_P_MODULEHEADER);
    srciter.filename = NULL;
    tsoftassert(!cslul_ll_implsource_iter(ctx, &srciter));

    /* Add first file */
    ctx->case_insens = 1;
    tsoftassert(build_add_source(ctx, 11, 6, "a.slul") == CSLUL_E_NOERROR);
    tassert(ctx->first_srcfile != NULL);
    tsoftassert(ctx->last_srcfile == ctx->first_srcfile);
    tsoftassert(ctx->first_srcfile->next == NULL);
    tsoftassert(ctx->first_srcfile->node.hashcode == 11);

    srciter.filename = NULL;
    tsoftassert(cslul_ll_implsource_iter(ctx, &srciter));
    tassert(srciter.namelen == 6);
    tassert(srciter.filename != NULL);
    tsoftassert(!strncmp("a.slul", srciter.filename, 6));
    tsoftassert(!cslul_ll_implsource_iter(ctx, &srciter));

    /* Add second file */
    tsoftassert(build_add_source(ctx, 22, 6, "b.slul") == CSLUL_E_NOERROR);
    tassert(ctx->first_srcfile != NULL);
    tassert(ctx->last_srcfile != NULL);
    tsoftassert(ctx->last_srcfile != ctx->first_srcfile);
    tsoftassert(ctx->first_srcfile->next == ctx->last_srcfile);
    tsoftassert(ctx->first_srcfile->node.hashcode == 11);
    tsoftassert(ctx->last_srcfile->node.hashcode == 22);

    /* Add third file */
    tsoftassert(build_add_source(ctx, 33, 6, "c.slul") == CSLUL_E_NOERROR);
    tassert(ctx->first_srcfile != NULL);
    tassert(ctx->last_srcfile != NULL);
    tsoftassert(ctx->last_srcfile != ctx->first_srcfile);
    tsoftassert(ctx->first_srcfile->node.hashcode == 11);
    tassert(ctx->first_srcfile->next != NULL);
    tsoftassert(ctx->first_srcfile->next->node.hashcode == 22);
    tassert(ctx->first_srcfile->next->next == ctx->last_srcfile);
    tsoftassert(ctx->last_srcfile->node.hashcode == 33);

    srciter.filename = NULL;
    tsoftassert(cslul_ll_implsource_iter(ctx, &srciter));
    tassert(srciter.namelen == 6);
    tassert(srciter.filename != NULL);
    tsoftassert(!strncmp("a.slul", srciter.filename, 6));
    tsoftassert(cslul_ll_implsource_iter(ctx, &srciter));
    tassert(srciter.namelen == 6);
    tassert(srciter.filename != NULL);
    tsoftassert(!strncmp("b.slul", srciter.filename, 6));
    tsoftassert(cslul_ll_implsource_iter(ctx, &srciter));
    tassert(srciter.namelen == 6);
    tassert(srciter.filename != NULL);
    tsoftassert(!strncmp("c.slul", srciter.filename, 6));
    tsoftassert(!cslul_ll_implsource_iter(ctx, &srciter));
    cslul_free(ctx);
    cslul_config_free(cfg);
}

const TestFunctionInfo tests_build[] = {
    TEST_BEFORE(before_test)
    TEST_INFO(test_fopen_failure)
    TEST_INFO(test_fread_failure)
    TEST_INFO(test_deletion_on_error)
    TEST_INFO(test_long_app)
    TEST_INFO(test_long_library)
    TEST_INFO(test_add_sources)
    TEST_END
};
