/*

  Unit tests of config.c

  Copyright © 2021-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include <stdlib.h>

#define CSLUL_IN_TESTS
#include "../config.c"

#include "unittest.h"
#include "alltests.h"
#include "testcommon.h"

#ifdef CSLUL_WINDOWS
#define PLATFORM_DIRSEP '\\'
#else
#define PLATFORM_DIRSEP '/'
#endif

static void test_initparams_null(void)
{
    struct CSlulConfig *cfg = tcfg(cslul_config_create(NULL));
    tassert(cfg != NULL);
    tsoftassert(cfg->params.dirsep == PLATFORM_DIRSEP);
    tsoftassert( ! cfg->params.fptr_malloc);
    tsoftassert( ! cfg->params.fptr_free);
    tsoftassert( ! cfg->params.fptr_realloc);
    tsoftassert( ! cfg->params.fptr_fopen);
    tsoftassert( ! cfg->params.fptr_fclose);
    tsoftassert( ! cfg->params.fptr_ferror);
    tsoftassert( ! cfg->params.fptr_remove);
    tsoftassert( ! cfg->params.fptr_fread);
    tsoftassert( ! cfg->params.fptr_fwrite);
    cslul_config_free(cfg);
}

static void test_initparams_size0(void)
{
    struct CSlulInitParams prm;
    struct CSlulConfig *cfg;
    memset(&prm, 123, sizeof(prm));
    prm.size = 0;
    prm.dirsep = 'A';
    cfg = tcfg(cslul_config_create(&prm));
    tassert(cfg != NULL);
    tsoftassert(cfg->params.dirsep == PLATFORM_DIRSEP);
    tsoftassert( ! cfg->params.fptr_malloc);
    tsoftassert( ! cfg->params.fptr_free);
    tsoftassert( ! cfg->params.fptr_realloc);
    tsoftassert( ! cfg->params.fptr_fopen);
    tsoftassert( ! cfg->params.fptr_fclose);
    tsoftassert( ! cfg->params.fptr_ferror);
    tsoftassert( ! cfg->params.fptr_remove);
    tsoftassert( ! cfg->params.fptr_fread);
    tsoftassert( ! cfg->params.fptr_fwrite);
    cslul_config_free(cfg);
    tsoftassert(prm.size == 0);
    tsoftassert(prm.dirsep == 'A');
}

static void test_initparams_size2(void)
{
    struct CSlulInitParams prm;
    struct CSlulConfig *cfg;
    memset(&prm, 123, sizeof(prm));
    prm.size = offsetof(struct CSlulInitParams, fptr_malloc);
    prm.dirsep = 'A';
    cfg = tcfg(cslul_config_create(&prm));
    tassert(cfg != NULL);
    tsoftassert(cfg->params.dirsep == 'A');
    tsoftassert( ! cfg->params.fptr_malloc);
    tsoftassert( ! cfg->params.fptr_free);
    tsoftassert( ! cfg->params.fptr_realloc);
    tsoftassert( ! cfg->params.fptr_fopen);
    tsoftassert( ! cfg->params.fptr_fclose);
    tsoftassert( ! cfg->params.fptr_ferror);
    tsoftassert( ! cfg->params.fptr_remove);
    tsoftassert( ! cfg->params.fptr_fread);
    tsoftassert( ! cfg->params.fptr_fwrite);
    cslul_config_free(cfg);
    tsoftassert(prm.size == offsetof(struct CSlulInitParams, fptr_malloc));
    tsoftassert(prm.dirsep == 'A');
}

static void *dummy_malloc(size_t size)
{
    return malloc(size);
}

static size_t dummy_fwrite(const void *buffer, size_t size, size_t nmemb, CSlulFile file)
{
    (void)buffer;
    (void)size;
    (void)nmemb;
    (void)file;
    return -1;
}

static void test_initparams_full(void)
{
    struct CSlulInitParams prm;
    struct CSlulConfig *cfg;
    memset(&prm, 0, sizeof(prm));
    prm.size = sizeof(prm);
    prm.dirsep = 'A';
    prm.fptr_malloc = dummy_malloc;
    prm.fptr_fwrite = dummy_fwrite;
    cfg = tcfg(cslul_config_create(&prm));
    tassert(cfg != NULL);
    tsoftassert(cfg->params.dirsep == 'A');
    tsoftassert(cfg->params.fptr_malloc);
    tsoftassert( ! cfg->params.fptr_free);
    tsoftassert( ! cfg->params.fptr_realloc);
    tsoftassert( ! cfg->params.fptr_fopen);
    tsoftassert( ! cfg->params.fptr_fclose);
    tsoftassert( ! cfg->params.fptr_ferror);
    tsoftassert( ! cfg->params.fptr_remove);
    tsoftassert( ! cfg->params.fptr_fread);
    tsoftassert(cfg->params.fptr_fwrite);
    cslul_config_free(cfg);
    tsoftassert(prm.size == sizeof(prm));
    tsoftassert(prm.dirsep == 'A');
    tsoftassert(prm.fptr_malloc);
    tsoftassert(!prm.fptr_free);
    tsoftassert(prm.fptr_fwrite);
}

const TestFunctionInfo tests_config[] = {
    TEST_INFO(test_initparams_null)
    TEST_INFO(test_initparams_size0)
    TEST_INFO(test_initparams_size2)
    TEST_INFO(test_initparams_full)
    TEST_END
};
