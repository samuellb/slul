/*

  Unit tests of exprchk.c

  Copyright © 2022-2023 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "../exprchk.c"
#define SLULPARSE
#include "parsecommon.h"
#include "chkcommon.h"
#include "unittest.h"
#include "alltests.h"

#include <string.h>

static void test_exprchk_badconstexpr(void)
{
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    TEST_SOURCE("data int x = 1\n"
                "data int y = x=2\n", 1);
    expect_error(CSLUL_E_CONSTEXPRBADOP, 2, 15);
    DO_VERIFY(ctx);
    free_ctx(ctx);
}

static void test_exprchk_cyclic(void)
{
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    TEST_SOURCE("data int x = y\n"
                "data int y = x\n", 1);
    /* The error message could happen on line 1 or 2, column 10 or 14.
       This line/column is simply for the current implementation: */
    expect_error(CSLUL_E_CYCLICDECL, 2, 10);
    DO_VERIFY(ctx);
    free_ctx(ctx);
}

static void verify_recursion_test(int depth)
{
    static const char prefix[] = "data int x = 1";
    char buff[1024];
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    int i;
    char *cp = &buff[sizeof(prefix)-1];
    memcpy(buff, prefix, sizeof(prefix)-1);
    for (i = 0; i < depth; i++) {
        *(cp++) = '+';
        *(cp++) = '1';
    }
    *(cp++) = '\n';
    test_source(ctx, buff, cp-buff, 1);
    if (depth >= MAX_VFY_RECURSION_DEPTH) {
        expect_error(CSLUL_E_VERIFYTOODEEP, 1, 15);
    }
    DO_VERIFY(ctx);
    free_ctx(ctx);
}

static void test_exprchk_recursiondepth_ok(void)
{
    sourceline = __LINE__;
    verify_recursion_test(MAX_VFY_RECURSION_DEPTH-1);
}

static void test_exprchk_recursiondepth_bad(void)
{
    sourceline = __LINE__;
    verify_recursion_test(MAX_VFY_RECURSION_DEPTH);
}

/** Tests referencing of an identifier that is constant but not known at
    compile-time (to clients of the module). This should give an error */
static void test_exprchk_ident_noncompiletime(void)
{
    struct CSlul *ctx = create_ctx(CSLUL_P_IFACE);
    /* y is run-time constant, but not compile-time constant */
    cslul_ll_set_current_filename(ctx, "main.slul");
    TEST_SOURCE("since 1.0\n"
                "data int x = y\n"
                "since 1.1\n"
                "data int y\n", 1);
    tassert(cslul_ll_start_phase(ctx, CSLUL_P_IMPL));
    cslul_ll_set_current_filename(ctx, "impl.slul");
    TEST_SOURCE("data int y = 123\n", 1);
    errors_in("main.slul");
    expect_error(CSLUL_E_IDENTNOTCOMPILETIME, 2, 14);
    DO_VERIFY(ctx);
    free_ctx(ctx);
}

/** Checks the error handling of type identifiers without a "target type".
    Type identifiers can only appear where there is a "target type"
    (i.e. when the type can be detected from the containing expression). */
static void test_exprchk_typeident_notarget1(void)
{
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    TEST_SOURCE("data bool b = .typeident == 2\n", 1);
    expect_error(CSLUL_E_TYPESCOPENOTARGET, 1, 16);
    DO_VERIFY(ctx);
    free_ctx(ctx);
}

static void test_exprchk_typeident_notarget2(void)
{
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    expect_error(CSLUL_E_BADSTMTLINESTART, 2, 5); /* error from parser */
    TEST_SOURCE("func f() {\n"
                "    .typeident\n"
                "}\n", 1);
    DO_VERIFY(ctx);
    free_ctx(ctx);
}

static void test_exprchk_typeident_notarget3(void)
{
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    TEST_SOURCE("func f() {\n"
                "    bool b = (.typeident == 1)\n"
                "}\n", 1);
    expect_error(CSLUL_E_TYPESCOPENOTARGET, 2, 16);
    DO_VERIFY(ctx);
    free_ctx(ctx);
}

static void test_exprchk_typeident_notypescope(void)
{
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    TEST_SOURCE("data int x = .thing\n"
                "data [2]int y = .thing\n", 1);
    expect_error(CSLUL_E_NOTYPESCOPE, 1, 15);
    expect_error(CSLUL_E_NOTYPESCOPE, 2, 18);
    DO_VERIFY(ctx);
    free_ctx(ctx);
}

static void test_exprchk_typeident_notfound(void)
{
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    TEST_SOURCE("type T = struct { int x }\n"
                "data T y = .thing\n", 1);
    expect_error(CSLUL_E_TYPEIDENTNOTFOUND, 2, 13);
    DO_VERIFY(ctx);
    free_ctx(ctx);
}

static void assert_uint_decl(struct IdentDecl *decl, uint64 expectednum,
                             unsigned type, unsigned exprtype, int line)
{
    struct ExprNode *e;
    if (tsoftassert_line(line, decl != NULL) &&
            tsoftassert_line(line, decl->type.type == T_ELMNTRY) &&
            tsoftassert_line(line, decl->type.u.builtin == type) &&
            tsoftassert_line(line, decl->u.initval != NULL) &&
            tsoftassert_line(line, (e = decl->u.initval->root) != NULL) &&
            tsoftassert_line(line, e->exprtype == exprtype)) {
        tsoftassert_line(line, e->a.intval==expectednum);
        tsoftassert_line(line, decl->u.initval->rpn == e);
        tsoftassert_line(line, e->rpnnext == NULL);
    }
}

static void assert_uint(struct CSlul *ctx, const char *ident,
                        uint64 expectednum, unsigned type, unsigned exprtype,
                        int line)
{
    struct IdentDecl *decl = (struct IdentDecl *)lookup(
            ctx, ctx->impl.idents_root, ident);
    assert_uint_decl(decl, expectednum, type, exprtype, line);
}

static void assert_uint_local(struct CSlul *ctx,
        struct FuncBody *func, const char *ident,
        uint64 expectednum, unsigned type, unsigned exprtype,
        int line)
{
    struct IdentDecl *decl = (struct IdentDecl *)lookup(
            ctx, func->stmtblock.idents, ident);
    assert_uint_decl(decl, expectednum, type, exprtype, line);
}

#define ASSERT_BOOL(ident, expectednum) \
        assert_uint(ctx, ident, expectednum, BT_Bool, E_BOOL, __LINE__)
#define ASSERT_L_BOOL(func, ident, expectednum) \
        assert_uint_local(ctx, func, ident, \
                          expectednum, BT_Bool, E_BOOL, __LINE__)
#define ASSERT_L_INT(func, ident, expectednum) \
        assert_uint_local(ctx, func, ident, \
                          expectednum, BT_Int, E_INTEGER, __LINE__)

static void test_exprchk_constexpr_bool(void)
{
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    TEST_SOURCE("data bool tr = true\n"
                "data bool fa = false\n"
                "data bool fa1 = not true\n"
                "data bool fa2 = not tr\n"
                "data bool fa3 = false or fa6\n"
                "data bool fa4 = false or not true\n"
                "data bool fa5 = fa4 and true\n"
                "data bool fa6 = (true or fa4) and fa\n"
                "data bool tr1 = tr and tr\n"
                "data bool tr2 = fa2 or not fa3\n", 1);
    DO_VERIFY(ctx);
    ASSERT_BOOL("tr", 1);
    ASSERT_BOOL("fa", 0);
    ASSERT_BOOL("fa1", 0);
    ASSERT_BOOL("fa2", 0);
    ASSERT_BOOL("fa3", 0);
    ASSERT_BOOL("fa4", 0);
    ASSERT_BOOL("fa5", 0);
    ASSERT_BOOL("fa6", 0);
    ASSERT_BOOL("tr1", 1);
    ASSERT_BOOL("tr2", 1);
    free_ctx(ctx);
}

static void test_exprchk_rpnfixup_full(void)
{
    struct IdentDecl *f;
    struct FuncBody *fb;
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    TEST_SOURCE("func rpnfixup() {\n"
                "    int fully11 = -1\n"
                "    int fully12 = (1 + 2) + 3\n"
                "    int fully13 = -1 * (2 + 3)\n"
                "    bool fully21 = not false\n"
                "    bool fully22 = false and true\n"
                "    bool fully23 = not (true and false)\n"
                "    bool fully24 = (not true) and false\n"
                "    bool fully25 = true and not false\n"
                "    bool fully26 = not (true and not false)\n"
                "    int fully31 = 2*-fully13 + 5\n"
                "}\n", 1);
    DO_VERIFY(ctx);
    f = (struct IdentDecl *)lookup(ctx, ctx->impl.idents_root, "rpnfixup");
    tassert(f != NULL);
    fb = f->u.funcbody;
    tassert(fb != NULL);
    ASSERT_L_INT(fb, "fully11", 1); /* stored as amplitude + sign */
    ASSERT_L_INT(fb, "fully12", 6);
    ASSERT_L_INT(fb, "fully13", 5); /* stored as amplitude + sign */
    ASSERT_L_BOOL(fb, "fully21", 1);
    ASSERT_L_BOOL(fb, "fully22", 0);
    ASSERT_L_BOOL(fb, "fully23", 1);
    ASSERT_L_BOOL(fb, "fully24", 0);
    ASSERT_L_BOOL(fb, "fully25", 1);
    ASSERT_L_BOOL(fb, "fully26", 0);
    ASSERT_L_INT(fb, "fully31", 15);
    free_ctx(ctx);
}

static void test_exprchk_rpnfixup_partial(void)
{
    struct IdentDecl *f;
    struct Stmt *s;
    struct ExprRoot *e;
    struct ExprNode *rpn;
    struct CSlul *ctx = create_ctx(CSLUL_P_IMPL);
    /* multiplication is evaluated 10, and the expression becomes:
        10 + x, and in RPN form:  10 x + */
    TEST_SOURCE("func rpnfixup(int x) -> int {\n"
                "    return -2*5 + x\n"
                "}\n", 1);
    DO_VERIFY(ctx);
    f = (struct IdentDecl *)lookup(ctx, ctx->impl.idents_root, "rpnfixup");
    tassert(f != NULL);
    tassert(f->u.funcbody != NULL);
    s = &f->u.funcbody->stmtblock.stmt;
    tassert(s->type == S_RETURN);
    e = s->u.expr;
    tassert(e != NULL);
    tassert(e->root != NULL);
    rpn = e->rpn;
    tassert(rpn != NULL);
    tassert(rpn->exprtype == E_INTEGER);
    tsoftassert(rpn->a.intval == 10);
    tsoftassert(INTLITERAL_IS_NEGATIVE(*rpn));
    rpn = rpn->rpnnext;
    tassert(rpn != NULL);
    tassert(rpn->exprtype == E_IDENT);
    rpn = rpn->rpnnext;
    tassert(rpn != NULL);
    tassert(rpn->exprtype == E_BINARYOP);
    tsoftassert(rpn->op == OP_ADD);
    tsoftassert(rpn->rpnnext == NULL);
    free_ctx(ctx);
}

const TestFunctionInfo tests_exprchk[] = {
    TEST_INFO(test_exprchk_badconstexpr)
    TEST_INFO(test_exprchk_cyclic)
    TEST_INFO(test_exprchk_recursiondepth_ok)
    TEST_INFO(test_exprchk_recursiondepth_bad)
    TEST_INFO(test_exprchk_ident_noncompiletime)
    TEST_INFO(test_exprchk_typeident_notarget1)
    TEST_INFO(test_exprchk_typeident_notarget2)
    TEST_INFO(test_exprchk_typeident_notarget3)
    TEST_INFO(test_exprchk_typeident_notypescope)
    TEST_INFO(test_exprchk_typeident_notfound)
    TEST_INFO(test_exprchk_constexpr_bool)
    TEST_INFO(test_exprchk_rpnfixup_full)
    TEST_INFO(test_exprchk_rpnfixup_partial)
    TEST_END
};
