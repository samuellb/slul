/*

  bwrapper.c -- Wrapper around the backend (CSBE) and IR generator

  Copyright © 2021-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include <assert.h>
#include <limits.h>
#include "internal.h"
#include "backend.h"

#ifndef DISABLE_CSBE
#include "csbe.h"
#define INTERR_BWRAPPER(errnum) MAKE_INTERR(errnum, INTERRBASE_BWRAPPER)
#define INTERR_INITBACKEND  INTERR_BWRAPPER(0x01)
#define INTERR_OUTPUT       INTERR_BWRAPPER(0x02)


/* TODO remove the CSLUL types {CpuArch, UserABI, Syscalls, OutputFormat} and use CSBE types only? */

struct CpuType {
    enum CsbeCpu cpu;
    enum CsbeEndianness endianness;
};
static const struct CpuType cpu_types[NUM_CPU_ARCHES] = {
    /* IR_DUMP */  { CSBEC_IR_DUMP, 0 },
    /* I386 */  { CSBEC_I386, CSBEEN_LITTLE_ENDIAN },
    /* X86_64 */  { CSBEC_X86_64, CSBEEN_LITTLE_ENDIAN },
    /* ARM */  { 0/* TODO */, CSBEEN_LITTLE_ENDIAN },
    /* AARCH64 */  { CSBEC_AARCH64, CSBEEN_LITTLE_ENDIAN },
    /* RV32 */  { 0/* TODO */, CSBEEN_LITTLE_ENDIAN },
    /* RV64GC */  { 0/* TODO */, CSBEEN_LITTLE_ENDIAN },
    /* PPC64EL */  { 0/* TODO */, CSBEEN_LITTLE_ENDIAN },
    /* MIPSEL */  { 0/* TODO */, CSBEEN_LITTLE_ENDIAN }
};

static const enum CsbeOutputFormat outformats[NUM_OUTFORMATS] = {
    /* OF_RAW */ CSBEOF_RAW,
    /* OF_ELFEXE */ CSBEOF_ELF_EXE,
    /* OF_ELFOBJFILE */ CSBEOF_ELF_OBJ, /* FIXME this is actually two types, non-PIC and PIC. (this applies to a lesser degree to executables also) */
    /* OF_ELFDYNLIB */ CSBEOF_ELF_DYNLIB,
    /* OF_ELFSTATLIB */ 1/*TODO */,
    /* OF_PECLI */ 0/*TODO */,
    /* OF_PEGUI */ 0/*TODO */,
    /* OF_COFFOBJFILE */ CSBEOF_PE_OBJ,
    /* OF_PEDYNLIB */ 0/*TODO */,
    /* OF_COFFSTATLIB */ 0/*TODO */,
    /* OF_CHEADER */ 0
};

static enum CsbeOsType get_ostype(const struct Target *target)
{
    /* TODO merge enum UserABI and enum SysCalls? */
    static const enum CsbeOsType userabi_to_csbe[] = {
        /* USERABI_IR_GENERIC */0,
        /* GNU */CSBEOS_LINUX_GLIBC, /* TODO could also be GNU/HURD */
        /* MUSL */CSBEOS_LINUX_MUSL,
        /* OPENBSD68_LIBC */0, /* TODO */
        /* FREEBSD_LIBC */0,
        /* NETBSD_LIBC */0,
        /* WINNT_DLLS */CSBEOS_WINDOWS,
        /* SLULIB */0
    };
    if (target->cpu == IR_DUMP) return 0;
    return userabi_to_csbe[target->userabi];
}

int interr_csbe(struct CSlul *ctx, enum CsbeErr csbe_e, unsigned cslul_e)
{
    if (csbe_e == CSBEE_OUT_OF_MEM) {
        error_linecol(ctx, CSLUL_E_OUTOFMEMORY, 0, 0);
    } else {
        internal_error(ctx, cslul_e);
    }
    return 0;
}

static void *aalloc_wrapper(void *userctx, size_t size, size_t align)
{
    struct CSlul *ctx = (struct CSlul *)userctx;
    CHECK_STRUCT(*ctx);
    return aalloc(ctx, size, align);
}

static void *lowlvl_alloc_wrapper(void *userctx, size_t size)
{
    struct CSlulConfig *cfg = (struct CSlulConfig *)userctx;
    CHECK_STRUCT(*cfg);
    return lowlvl_alloc(cfg, size);
}

static struct Csbe *initialize_backend(struct CSlul *ctx)
{
    struct CsbeConfig *cfg;
    struct Csbe *csbe;
    struct CsbeFuncPtrs funcptrs;
    enum CsbeErr e;
    unsigned arch_id;
    struct Output *output;

    funcptrs.fptr_alloc = aalloc_wrapper;
    cfg = csbe_config_new(CSBE_API_VERSION_0, &funcptrs, ctx);
    if (!cfg) goto report_oom;

    arch_id = 0;
    for (output = ctx->outputs; output; output = output->next) {
        const struct CpuType *cpu = &cpu_types[output->target->cpu];
        ECHK(csbe_config_add_arch(cfg, cpu->cpu, cpu->endianness,
                                  get_ostype(output->target),
                                  outformats[output->format]));
        output->arch_id = arch_id++;
    }

    csbe = csbe_new(cfg);
    if (UNLIKELY(!csbe)) goto report_oom;
    csbe_set_input_name(csbe, aalloc_memzdup(ctx, ctx->module.name,
                                             ctx->module.namelen));
    return csbe;
  err_csbe:
    if (e == CSBEE_OUT_OF_MEM) goto report_oom;
    internal_error(ctx, INTERR_INITBACKEND);
    return NULL;
  report_oom:
    error_linecol(ctx, CSLUL_E_OUTOFMEMORY, 0, 0);
    return NULL;
}

static int output_file(struct CSlul *ctx, struct Output *output,
                       struct Csbe *csbe, int arch_id)
{
    enum CsbeErr e;
    unsigned char *buffer = NULL;
    size_t size;

    ECHK(csbe_output(csbe, arch_id, &buffer, &size, lowlvl_alloc_wrapper,
                     (void*)ctx->cfg));
    if (size != 0) {
        size = ctx_fwrite(ctx, buffer, size, 1, output->file);
        if (UNLIKELY(size != 1)) goto err_write;
    }
    lowlvl_free(ctx->cfg, buffer);
    return 1;
  err_write:
    error_linecol(ctx, CSLUL_E_WRITEFILE, 0, 0);
    goto err_end;
  err_csbe:
    interr_csbe(ctx, e, INTERR_OUTPUT);
    goto err_end;
  err_end:
    if (buffer) lowlvl_free(ctx->cfg, buffer);
    return 0;
}

/** Generates all output files */
static int output_all(struct CSlul *ctx, struct Csbe *csbe)
{
    int ok = 1;
    struct Output *output = ctx->outputs;
    for (; output; output = output->next) {
        ctx->current_filename = output->filename;
        ok &= output_file(ctx, output, csbe, output->arch_id);
        if (ctx_fclose(ctx, output->file)) ok = 0;
    }
    return ok;
}

/** Builds IR and generates all output files */
int backend_output(struct CSlul *ctx)
{
    struct Csbe *csbe = initialize_backend(ctx);
    ZCHK(csbe);
    ZCHK(generate_ir(ctx, csbe));
    return output_all(ctx, csbe);
}

#else /* DISABLE_CSBE */
int backend_output(struct CSlul *ctx)
{
    (void)ctx;
    return 1;
}
#endif
