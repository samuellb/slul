/*

  funcchk.c -- Checking of function bodies

  Copyright © 2022-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "internal.h"
#include "ast.h"
#include <assert.h>
#include <string.h>
#define INTERR_FUNCCHK(errnum) MAKE_INTERR(errnum, INTERRBASE_FUNCCHK)
#define INTERR_BADSTMTTYPE      INTERR_FUNCCHK(0x01)
#define INTERR_MISSINGFCERROR   INTERR_FUNCCHK(0x02)

static int check_stmtblock(struct CSlul *ctx, struct FuncBody *func,
                           struct StmtBlock *block);
static int check_goto_varstates(struct CSlul *ctx, struct FuncBody *func);

static void varstate_range_from_type(struct CSlul *ctx,
                                     struct VarStateEntry *v,
                                     const struct TypeRef *from_tr)
{
    struct TypeRef tr;
    /* Get nullability from type */
    CHK(real_tr(ctx, from_tr, &tr));
    if (tr.type->type == T_REF) {
        v->not_none = !IS_REF_OPTIONAL(*tr.type);
    } else if (tr.type->type == T_FUNC) {
        v->not_none = !IS_FUNCREF_OPTIONAL(*tr.type);
    } else {
        v->not_none = 1;
    }
    /* Get maximum range for type */
    real_tr(ctx, &tr, &tr);
    /* TODO */
    return;
  assert_error:
    assert(ctx->has_errors);
}

/** Initializes variable state tracking. Function parameters start in
    assigned state, and all other variables start in unassigned state. */
static int init_varstates(struct CSlul *ctx, struct FuncBody *func)
{
    struct VarStates *varstates;
    varstates = aallocp(ctx, sizeof(struct VarStates));
    if (!varstates) return 0;
    PROTECT_STRUCT(*varstates);
    varstates->refcount = 0;
    if (func->num_variables) {
        size_t remaining;
        size_t size = sizeof(struct VarStateEntry)*func->num_variables;
        struct VarStateEntry *vars, *v;
        const struct Type *functype;
        const struct FieldOrParamEntry *param;
        vars = aallocp64(ctx, size);
        if (!vars) return 0;
        varstates->vars = vars;
        memset(vars, 0, size);
        CHK(func->ident);
        functype = funcdecl_real_type(&func->ident->type);
        v = vars;
        CHK(functype->u.func);
        if (functype->type == T_METHOD) {
            /* "this"-parameter */
            v->assigned = 1;
            v->not_none = 1;
            v++;
        }
        remaining = functype->u.func->params.count;
        assert(remaining <= func->num_variables);
        param = functype->u.func->params.first;
        while (remaining--) {
            const struct TypeRef tr = root_tr_const(&param->f.vardef.decl.type);
            varstate_range_from_type(ctx, v, &tr);
            v->assigned = 1;
    /*        v->owned = TODO;
            v->owned_by = TODO;
            v->min = 0; TODO
            v->max = UINT64_MAX; TODO */
            v++;
            param = param->next;
        }
    } else {
        varstates->vars = NULL;
    }
    ctx->varstates = varstates;
    return 1;
  assert_error:
    if (!ctx->has_errors) {
        internal_error(ctx, INTERR_MISSINGFCERROR);
    }
    return 0;
}

/**
 * Copies a varstates object if refcount != 1.
 * Returns 0 on fatal errors only.
 */
int varstates_copyonwrite(struct CSlul *ctx)
{
    size_t numvars = ctx->funcbody->num_variables;
    if (!numvars) return 1;
    assert(ctx->varstates->refcount >= 0);
    if (ctx->varstates->refcount) {
        size_t size = sizeof(struct VarStateEntry)*numvars;
        struct VarStateEntry *vars;
        struct VarStates *varstates =
                    aallocp(ctx, sizeof(struct VarStates));
        if (!varstates) return 0;
        PROTECT_STRUCT(*varstates);
        vars = aallocp64(ctx, size);
        if (!vars) return 0;
        varstates->refcount = 0;
        varstates->vars = memcpy(vars, ctx->varstates->vars, size);
        ctx->varstates = varstates;
    }
    return 1;
}

/**
 * Determines the common variable states for two VarStates:
 * - Variables are considered assigned if they are assigned in both.
 */
static int common_varstates(struct CSlul *ctx,
                            struct VarStates *a, struct VarStates *b)
{
    size_t numvars = ctx->funcbody->num_variables;
    size_t size;
    struct VarStates *merged;
    struct VarStateEntry *vars, *av, *bv;
    assert(a->refcount >= 0);
    assert(b->refcount >= 0);
    if (!numvars) return 1;
    if (a == b) {
        ctx->varstates = b;
        return 1;
    }
    size = sizeof(struct VarStateEntry)*numvars;
    if (!memcmp(a->vars, b->vars, size)) {
        ctx->varstates = b;
        return 1;
    }
    /* Not equal. Create a merged list. */
    merged = aallocp(ctx, sizeof(struct VarStates));
    if (!merged) return 0;
    PROTECT_STRUCT(*merged);
    vars = aallocp64(ctx, size);
    if (!vars) return 0;
    memset(vars, 0, size);
    merged->refcount = 0;
    merged->vars = vars;
    av = a->vars;
    bv = b->vars;
    while (numvars--) {
        vars->assigned = av->assigned & bv->assigned;
        vars->not_none = av->not_none & bv->not_none;
        /* TODO track values, ownership and bounds also */
        vars++; av++; bv++;
    }
    ctx->varstates = merged;
    return 1;
}

/**
 * Called when a variable has been assigned
 * Returns 0 on fatal errors only.
 */
int var_assigned(struct CSlul *ctx, struct VarDef *vardef,
                  struct ExprNode *value)
{
    struct VarStateEntry *v;
    if (!varstates_copyonwrite(ctx)) return 0;
    v = &ctx->varstates->vars[vardef->var_id];
    v->assigned = 1;
    SOFTCHK(value);
    varstate_range_from_type(ctx, v, &value->u.tr);
    /* TODO set other attributes */
    return 1;
  soft_assert_error:
    assert(ctx->has_errors);
    /* Assume "best case" to suppress further errors */
    v->not_none = 1;
    return 1;
}

/**
 * Requires that a variable is assigned.
 *
 * \param ctx       Compilation context.
 * \param vardef    Variable definition.
 * \param identexpr Subexpression where the identifier is used.
 * \param target_tr Expected target type at place of subexpression.
 * \param varstate_out Current var-state of variable is placed here.
 */
void require_assigned(struct CSlul *ctx, struct VarDef *vardef,
                      const struct ExprNode *identexpr,
                      const struct VarStateEntry **varstate_out)
{
    const struct VarStateEntry *v = &ctx->varstates->vars[vardef->var_id];
    *varstate_out = v;
    if (UNLIKELY(!v->assigned)) {
        error_expr(ctx, CSLUL_E_VARNOTASSIGNED,
                   ctx->current_exprroot, identexpr);
        return;
    }
}

/**
 * Attempts to constrain the varstates, e.g. nullability and integer range.
 *
 * \param ctx       Compilation context
 * \param condexpr  Condition expression to check
 * \param is_true   Whether the condition is checked for true or false.
 * \param dry_run   1 to just perform a check.
 * \return 1 if varstates could be constrained
 */
static int constrain(struct CSlul *ctx, const struct ExprNode *condexpr,
                     int is_true, int dry_run)
{
    SOFTCHK(condexpr);
    if (condexpr->exprtype == E_BINARYOP) {
        const struct ExprNode *a = condexpr->a.expr;
        const struct ExprNode *b = condexpr->b.expr;
        switch ((int)condexpr->op) {
        case OP_AND:
            if (is_true) {
                return constrain(ctx, a, is_true, dry_run) &&
                       constrain(ctx, b, is_true, dry_run);
            }
            break;
        case OP_OR:
            if (!is_true) {
                return constrain(ctx, a, !is_true, dry_run) &&
                       constrain(ctx, b, !is_true, dry_run);
            }
            break;
        case OP_EQ:
        case OP_NOTEQ: {
            if (b && b->exprtype == E_NONE && a && a->exprtype == E_IDENT) {
                const struct IdentDecl *decl =
                        (const struct IdentDecl *)a->a.ident;
                SOFTCHK(decl);
                if (IS_IDENT_LOCAL(decl)) {
                    if (!dry_run) {
                        unsigned id = ((const struct VarDef *)decl)->var_id;
                        struct VarStateEntry *v = &ctx->varstates->vars[id];
                        v->not_none = (condexpr->op == OP_EQ) ^ is_true;
                    }
                    return 1;
                }
            }
            break; }
        /* TODO constrain integer range as well */
        }
    } else if (condexpr->exprtype == E_UNARYOP) {
        if (condexpr->op == OP_NOT) {
            return constrain(ctx, condexpr->a.expr, !is_true, dry_run);
        }
    }
    return 0;
  soft_assert_error:
    assert(ctx->has_errors);
    return 0;
}

static int constrain_varstates(struct CSlul *ctx,
                               struct ExprNode *condexpr,
                               int is_true)
{
    if (constrain(ctx, condexpr, is_true, 1/* dry_run */)) {
        if (!varstates_copyonwrite(ctx)) return 0;
        constrain(ctx, condexpr, is_true, 0);
    }
    return 1;
}

int check_funcbody(struct CSlul *ctx, struct FuncBody *func)
{
    int ok = 1;
    struct TopLevelIdent *tlident = (struct TopLevelIdent *)func->ident;
    if (tlident->class_ && !tlident->is_typeident) {
        ctx->this_tr = root_tr_const(&tlident->class_->type);
    } else {
        ctx->this_tr.type = NULL;
    }
    ctx->funcbody = func;
    if (!init_varstates(ctx, func)) return 0;
    ok &= check_stmtblock(ctx, func, &func->stmtblock);
    ok &= check_goto_varstates(ctx, func);
    if (HAS_RETURN(&func->ident->type) && !func->stmtblock.f.f.always_ret) {
        message_set_ident(ctx, 0, CSLUL_LT_MAIN, &func->ident->ident);
        message_final(ctx, CSLUL_E_FUNCMISSINGRETURN);
    }
    ctx->current_assign_lvalue = NULL;
    ctx->current_nonecheck_expr = NULL;
    return ok;
}

static void error_goto_varstate(struct CSlul *ctx, struct FuncBody *func,
                                struct GotoIdent *gi, unsigned var_id)
{
    (void)var_id;
    message_set_stmt(ctx, 0, CSLUL_LT_MAIN, func, gi->target_stmt);
    /* TODO look up variable */
    message_final(ctx, CSLUL_E_VARNOTASSIGNGOTO);
}

static int check_goto_varstates(struct CSlul *ctx, struct FuncBody *func)
{
    struct GotoIdent *gi;
    for (gi = func->gotoidents; gi; gi = gi->next) {
        struct VarStateEntry *jumpv, *targetv;
        unsigned remaining, var_id;
        if (UNLIKELY(!gi->jump_vs) || UNLIKELY(!gi->target_vs)) {
            assert(ctx->has_errors);
            continue;
        }
        jumpv = gi->jump_vs->vars;
        targetv = gi->target_vs->vars;
        var_id = 0;
        for (remaining = func->num_variables; remaining; remaining--) {
            if (UNLIKELY(targetv->assigned && !jumpv->assigned)) {
                error_goto_varstate(ctx, func, gi, var_id);
            }
            /* TODO track values, ownership and bounds also */
            jumpv++; targetv++; var_id++;
        }
    }
    return 1;
}

static void detect_always_truefalse(struct CSlul *ctx, struct ExprRoot *expr)
{
    if (UNLIKELY(expr->root->exprtype == E_BOOL)) {
        error_expr(ctx, expr->root->a.intval ?
                       CSLUL_E_ALWAYSTRUE : CSLUL_E_ALWAYSFALSE,
                   expr, expr->root);
    }
}

static void mark_have_outbound_goto(struct StmtBlock *block, unsigned goto_id)
{
    do {
        if (block->f.f.has_outbound_goto) break;
        block->f.f.has_outbound_goto = 1;
        block = block->base;
    } while (block && (goto_id <= block->f.f.prev_goto_id ||
                       goto_id > block->last_goto_id));
}

static int require_useful_expr(struct CSlul *ctx,
                                const struct ExprRoot *expr)
{
    const struct ExprNode *n = expr->root;
    if (UNLIKELY(!n)) goto assert_error;
    switch (n->exprtype) {
    case E_UNARYOP:
    case E_CONDITIONAL:
    case E_CONDCHOICES:
    case E_IDENT:
    case E_INTEGER:
    case E_FLOAT:
    case E_STRING:
    case E_NONE:
    case E_UNDEF:
    case E_ARRAY:
    case E_STRUCT:
    case E_INDEX:
    case E_FIELD:
    case E_THIS:
    case E_BOOL:
        goto meaningless;
    case E_BINARYOP:
        if (!IS_ASSIGN_OP(n->op)) goto meaningless;
        else return 1;
    case E_CALL:
        /* TODO check for pure functions here ( => needs to be moved to funcchk) */
        return 1;
    case E_TYPEIDENT:
    case E_FIELDINIT:
        goto assert_error;
    }
    assert(0);
  meaningless:
    error_expr(ctx, CSLUL_E_OPNOEFFECT, expr, expr->root);
    return 0;
  assert_error:
    assert(ctx->has_errors);
    return 0;
}

static int check_stmtblock(struct CSlul *ctx, struct FuncBody *func,
                           struct StmtBlock *block)
{
    struct Stmt *sm;
    struct StmtBlock *if_base_block = NULL;
    struct VarStates *pre_if_vs = NULL, *true_vs = NULL;
    unsigned if_always_ret = INVALID_BOOL;
  deeper_block:
    if (UNLIKELY(block == NULL)) goto assert_error;
    for (sm = &block->stmt; sm; sm = sm->next) {
        switch (sm->type) {
        case S_BLOCK:
            SOFTCHK(sm->u.block);
            CHK(check_stmtblock(ctx, func, sm->u.block));
            block->f.f.always_ret = sm->u.block->f.f.always_ret;
            if (block->f.f.always_ret) goto handle_always_return;
            break;
        case S_DECL: {
            struct IdentDecl *decl;
            struct TypeRef tr;
            int ok;
            SOFTCHK(sm->u.vardef);
            assert(!ctx->varstates->vars[sm->u.vardef->var_id].assigned);
            decl = &sm->u.vardef->decl;
            /* FIXME can these nasty casts be avoided? */
            ok = check_type(ctx, (struct TypeDecl *)decl,
                                 (struct TypeDecl *)decl, &decl->type,
                                 LTLocalVar);
            if (UNLIKELY(!ok)) {
                CHK(var_assigned(ctx, sm->u.vardef, NULL));
            }
            decl->ident.state = TNS_DONE;
            assert(!IS_FUNC_DECL(decl));
            if (decl->u.initval != NULL) {
                if (LIKELY(ok)) {
                    tr = root_tr(&decl->type);
                    CHK(check_expr(ctx, &tr, decl->u.initval, LEFuncBodyExpr));
                    CHK(var_assigned(ctx, sm->u.vardef, decl->u.initval->root));
                }
                SOFTCHK(!decl->u.initval->root ||
                        decl->u.initval->root->u.tr.type);
            }
            break; }
        case S_EXPR:
            if (!require_useful_expr(ctx, sm->u.expr)) break;
            CHK(check_expr(ctx, NULL, sm->u.expr, LEFuncBodyExpr));
            break;
/*        case S_ASSIGN:*/
        case S_RETURN: {
            struct TypeRef tr;
            struct Stmt *next;
            SOFTCHK(func->ident && func->ident->type.u.func);
            if (HAS_RETURN(&func->ident->type)) {
                tr = root_tr(&func->ident->type.u.func->returntype);
                CHK(check_expr(ctx, &tr, sm->u.expr, LEFuncBodyExpr));
                /* TODO check lifetime of returned expr */
            }
            /* TODO check that no owned refs are leaked */
          handle_always_return:
            next = sm->next;
            if (next && next->type == S_TARGET) {
                /* The goto target has no predecessor, so instead the common
                   varstates from the preceeding goto jumps are used
                   instead. */
                sm = next;
                block->f.f.always_ret = 0;
                sm->u.gotoident->target_vs = next->u.gotoident->jump_vs;
                sm->u.gotoident->target_stmt = sm;
                if (LIKELY(next->u.gotoident->jump_vs)) {
                    ctx->varstates = next->u.gotoident->jump_vs;
                } else {
                    error_stmt(ctx, CSLUL_E_NOGOTOPREDECESSOR, func, sm);
                }
                break;
            } else if (UNLIKELY(next && next->type != S_TARGET &&
                     !(next->type == S_FOR &&
                            next->u.forstm->l.block.f.f.has_gototarget) &&
                     !((next->type == S_WHILE || next->type == S_DOWHILE) &&
                            next->u.whilestm->l.block.f.f.has_gototarget))) {
                enum CSlulErrorCode errcode;
                struct Stmt *errstmt = sm;
                if (sm->type==S_RETURN) {
                    if (ctx->has_errors && !HAS_RETURN(&func->ident->type) &&
                                sm->line == next->line) {
                        errcode = 0;
                    } else {
                        errcode = CSLUL_E_STMTSAFTERRETURN;
                    }
                } else if (sm->type==S_GOTO)  errcode = CSLUL_E_STMTSAFTERGOTO;
                else if (sm->type==S_WHILE)   errcode = CSLUL_E_AFTERWHILETRUE;
                else if (sm->type==S_DOWHILE) errcode=CSLUL_E_AFTERDOWHILETRUE;
                else {
                    errcode = CSLUL_E_UNREACHABLESTMT;
                    errstmt = next;
                }
                if (errcode) {
                    /* FIXME report error on the unreachable statement instead? and include reference to the preceeding stmt? */
                    error_stmt(ctx, errcode, func, errstmt);
                }
            }
            block->f.f.always_ret = 1;
            break; }
        case S_BREAK:
            break;
        case S_CONT:
            /* XXX save start state at start of loop block, and handle this like goto? */
            break;
        case S_GOTO: {
            unsigned goto_id;
            SOFTCHK(sm->u.gotoident);
            goto_id = sm->u.gotoident->goto_id;
            if (goto_id <= block->f.f.prev_goto_id ||
                    goto_id > block->last_goto_id) {
                mark_have_outbound_goto(block, goto_id);
            }
            /* The common subset of defined variables/variable states
               is tracked in gotoident->jump_vs */
            if (!sm->u.gotoident->jump_vs) {
                ctx->varstates->refcount++;
                sm->u.gotoident->jump_vs = ctx->varstates;
            } else {
                struct VarStates *save_vs = ctx->varstates;
                CHK(common_varstates(ctx, sm->u.gotoident->jump_vs,
                                     ctx->varstates));
                sm->u.gotoident->jump_vs = ctx->varstates;
                ctx->varstates = save_vs;
            }
            goto handle_always_return; }
        case S_IF: {
            /* This is a bit messy because there's a lot of special handling:
                - else-if is optimized to prevent stack overflow
                - Always true/always false is reported as an error
                - The "if", "else if" and "else" branches are checked for
                  variable states:
                    - These use the same starting state, so it must be saved
                    - The final state is the common subset. Variables that
                      are assigned in ALL of the if/elseif/else branches are
                      considered assigned (note: only if else is present).
            */
            int literal_bool;
            SOFTCHK(sm->u.ifstm);
            SOFTCHK(sm->u.ifstm->cond);
            SOFTCHK(sm->u.ifstm->cond->root);
            literal_bool = (sm->u.ifstm->cond->root->exprtype == E_BOOL);
            CHK(check_expr(ctx, &ctx->basedefs->builtin_tr[BT_Bool],
                            sm->u.ifstm->cond, LEFuncBodyExpr));
            if (!literal_bool) { /* "if true" is useful for debugging */
                detect_always_truefalse(ctx, sm->u.ifstm->cond);
            }
            if (!pre_if_vs) { /* don't save again in else if */
                pre_if_vs = ctx->varstates;
                pre_if_vs->refcount++;
                if_always_ret = 1;
                true_vs = NULL;
                if_base_block = block;
            }
            assert(pre_if_vs);
            CHK(constrain_varstates(ctx, sm->u.ifstm->cond->root, 1));
            CHK(check_stmtblock(ctx, func, &sm->u.ifstm->true_block));
            if (sm->u.ifstm->false_block) {
                if (true_vs) {
                    true_vs->refcount--;
                    if (!sm->u.ifstm->true_block.f.f.always_ret) {
                        CHK(common_varstates(ctx, true_vs, ctx->varstates));
                    }
                }
                if (!sm->u.ifstm->true_block.f.f.always_ret) {
                    true_vs = ctx->varstates;
                    true_vs->refcount++;
                }
                ctx->varstates = pre_if_vs;
                assert(if_always_ret != INVALID_BOOL);
                if_always_ret &= sm->u.ifstm->true_block.f.f.always_ret;
                CHK(constrain_varstates(ctx, sm->u.ifstm->cond->root, 0));
                if (!sm->next) {
                    /* Avoid recursion in repeated elseif's */
                    block = sm->u.ifstm->false_block;
                    goto deeper_block;
                } else {
                    /* Process "else" block */
                    CHK(check_stmtblock(ctx, func, sm->u.ifstm->false_block));
                    if_always_ret &= sm->u.ifstm->false_block->f.f.always_ret;
                    if (sm->u.ifstm->false_block->f.f.always_ret) {
                        ctx->varstates = true_vs ? true_vs : pre_if_vs;
                    } else if (true_vs) {
                        true_vs->refcount--;
                        assert(true_vs->refcount >= 0);
                        CHK(common_varstates(ctx, true_vs, ctx->varstates));
                    }
                  end_of_ifelse:
                    pre_if_vs->refcount--;
                    assert(pre_if_vs->refcount >= 0);
                    pre_if_vs = NULL;
                    if (if_always_ret) goto handle_always_return;
                }
            } else { /* No "else" */
                ctx->varstates = pre_if_vs;
                if (true_vs) {
                    true_vs->refcount--;
                    assert(true_vs->refcount >= 0);
                    true_vs = NULL;
                }
                if_always_ret = 0;
                goto end_of_ifelse;
            }
            break; }
        case S_WHILE:
        case S_DOWHILE: {
            /* FIXME the definedness checking is simplified (in particular,
                     do-while loops always execute at least once) */
            struct VarStates *start_vs;
            int literal_bool, while_true = 0;
            SOFTCHK(sm->u.whilestm);
            SOFTCHK(sm->u.whilestm->cond);
            SOFTCHK(sm->u.whilestm->cond->root);
            literal_bool = sm->u.whilestm->cond->root->exprtype == E_BOOL;
            CHK(check_expr(ctx, &ctx->basedefs->builtin_tr[BT_Bool],
                            sm->u.whilestm->cond, LEFuncBodyExpr));
            if (!literal_bool) { /* "if true" is useful for debugging */
                detect_always_truefalse(ctx, sm->u.whilestm->cond);
            }
            start_vs = ctx->varstates;
            start_vs->refcount++;
            if (sm->type == S_WHILE) {
                CHK(constrain_varstates(ctx, sm->u.whilestm->cond->root, 1));
            }
            CHK(check_stmtblock(ctx, func, &sm->u.whilestm->l.block));
            ctx->varstates = start_vs;
            /* Special handling for "while true" loops */
            if (literal_bool && !sm->u.whilestm->l.has_break &&
                    sm->u.whilestm->cond->root->a.intval) {
                block->f.f.always_ret = 1; /* similar effect */
                while_true = 1;
                if (UNLIKELY(sm->u.whilestm->l.empty_block)) {
                    error_stmt(ctx, CSLUL_E_ENDLESSWITHLOOPEMPTY, func,
                               &sm->u.whilestm->l.empty_block->stmt);
                }
                if (!sm->u.whilestm->l.block.f.f.has_outbound_goto) {
                    /* TODO mark loop as endless, and use this
                            fact in optimizations, etc. */
                }
            }
            /* loopempty/loopend */
            CHK(!sm->u.whilestm->l.empty_block ||
                 check_stmtblock(ctx, func, sm->u.whilestm->l.empty_block));
            ctx->varstates = start_vs;
            CHK(!sm->u.whilestm->l.end_block ||
                 check_stmtblock(ctx, func, sm->u.whilestm->l.end_block));
            start_vs->refcount--;
            assert(start_vs->refcount >= 0);
            ctx->varstates = start_vs;
            if (while_true) goto handle_always_return;
            break; }
        case S_FOR: {
            struct VarStates *start_vs;
            struct IdentDecl *loopvar;
            int ok;
            /* Check loop variable */
            SOFTCHK(sm->u.forstm);
            loopvar = &sm->u.forstm->loopvar;
            SOFTCHK(IS_IDENT_DEFINED(loopvar));
            /* FIXME can these nasty casts be avoided? */
            ok = check_type(ctx, (struct TypeDecl *)loopvar,
                                 (struct TypeDecl *)loopvar, &loopvar->type,
                                 LTLocalVar);
            loopvar->ident.state = TNS_DONE;
            assert(!IS_FUNC_DECL(loopvar));
            /* Check loop expression */
            if (LIKELY(ok)) {
                struct ExprRoot *expr = sm->u.forstm->loopexpr;
                SOFTCHK(expr);
                SOFTCHK(expr->root);
                if (expr->root->exprtype == E_ARRAY) {
                    struct TypeRef arrtr;
                    struct Type *arrtype;
                    /* Expected type: Array of loopvar->type */
                    arrtype = aallocp(ctx, sizeof(struct Type));
                    SOFTCHK(arrtype);
                    PROTECT_STRUCT(*arrtype);
                    arrtr.type = arrtype;
                    arrtr.quals = 0;
                    arrtr.prm = NULL;
                    PROTECT_STACK_STRUCT(arrtr);
                    arrtype->defflags = 0;
                    arrtype->quals = 0;
                    arrtype->misc = M_ANY_LENGTH;
                    arrtype->type = T_ARRAY;
                    arrtype->column = 0;
                    arrtype->line = 0;
                    arrtype->u.nested = &loopvar->type;
                    CHK(check_expr(ctx, &arrtr, expr, LEFuncBodyExpr));
                    sm->u.forstm->looptype = LT_ARRAY;
                } else {
                    struct TypeRef tr;
                    struct Type *t;
                    CHK(check_expr(ctx, NULL, expr, LEFuncBodyExpr));
                    t = real_deref_tr(ctx, &expr->root->u.tr, expr->root, &tr);
                    SOFTCHK(t);
                    if (t->type == T_ARRAY) {
                        sm->u.forstm->looptype = LT_ARRAY;
                    } else {
                        /* TODO: iterators and ranges:
                           - iterator expressions: get_iterator()
                           - iterable expressions: get_list()
                           - exclusive range expressions: 1..<5
                           - inclusive range expressions: 1..=5 (or 1..&5)
                        */
                    }
                    /* TODO if "out" parameters are added, then they will be quite confusing in this case (only set once) */
                }
            }
            /* Check loop body */
            start_vs = ctx->varstates;
            start_vs->refcount++;
            /* FIXME var_id for loop variables? */
            /*assert(!ctx->varstates->vars[sm->u.vardef->var_id].assigned);*/
            CHK(check_stmtblock(ctx, func, &sm->u.forstm->l.block));
            ctx->varstates = start_vs;
            /* loopempty/loopend */
            CHK(!sm->u.forstm->l.empty_block ||
                 check_stmtblock(ctx, func, sm->u.forstm->l.empty_block));
            ctx->varstates = start_vs;
            CHK(!sm->u.forstm->l.end_block ||
                 check_stmtblock(ctx, func, sm->u.forstm->l.end_block));
            start_vs->refcount--;
            assert(start_vs->refcount >= 0);
            ctx->varstates = start_vs;
            break; }
        case S_SWITCH: { /* TODO enforce exhaustive switches? */
            struct VarStates *start_vs, *common_vs;
            struct SwitchCase *sc;
            struct TypeRef *tr;
            SOFTCHK(sm->u.switchstm);
            SOFTCHK(sm->u.switchstm->cond);
            SOFTCHK(sm->u.switchstm->cond->root);
            CHK(check_expr(ctx, NULL, sm->u.switchstm->cond, LEFuncBodyExpr));
            tr = &sm->u.switchstm->cond->root->u.tr;
            start_vs = ctx->varstates;
            start_vs->refcount++;
            common_vs = NULL;
            for (sc = sm->u.switchstm->cases; sc; sc = sc->next) {
                struct CaseValue *val;
                /* Case values */
                for (val = &sc->values; val; val = val->next) {
                    if (!val->expr) continue; /* default case */
                    SOFTCHK(val->expr->root);
                    /* TODO support range expressions? */
                    CHK(check_expr(ctx, tr, val->expr, LEFuncBodyExpr));
                    /* TODO check that value is possible */
                }
                /* Block */
                ctx->varstates = start_vs;
                /* TODO should set varstate range here */
                CHK(check_stmtblock(ctx, func, &sc->block));
                if (!sc->block.f.f.always_ret) {
                    if (common_vs) {
                        CHK(common_varstates(ctx, common_vs, ctx->varstates));
                    }
                    common_vs = ctx->varstates;
                    common_vs->refcount++;
                }
            }
            start_vs->refcount--;
            assert(start_vs->refcount >= 0);
            /* TODO also include case where all possible values are matched */
            if (sm->u.switchstm->has_default) {
                if (!common_vs) goto handle_always_return;
                ctx->varstates = common_vs;
            } else {
                if (common_vs) {
                    common_vs->refcount--;
                    assert(common_vs->refcount >= 0);
                }
                ctx->varstates = start_vs;
            }
            break; }
        case S_ASSERT: {
            int literal_bool;
            SOFTCHK(sm->u.expr);
            SOFTCHK(sm->u.expr->root);
            literal_bool = (sm->u.expr->root->exprtype == E_BOOL);
            if (literal_bool && sm->u.expr->root->a.intval == 1) {
                error_expr(ctx, CSLUL_E_ASSERTTRUE,
                           sm->u.expr, sm->u.expr->root);
            }
            CHK(check_expr(ctx, &ctx->basedefs->builtin_tr[BT_Bool],
                            sm->u.expr, LEFuncBodyExpr));
            if (!literal_bool && /* not an explicit "assert false", which is useful */
                sm->u.expr->root->exprtype == E_BOOL &&
                sm->u.expr->root->a.intval == 0) {
                error_expr(ctx, CSLUL_E_ALWAYSFALSE,
                           sm->u.expr, sm->u.expr->root);
            }
            CHK(constrain_varstates(ctx, sm->u.expr->root, 1));
            break; }
        case S_TARGET:
            SOFTCHK(sm->u.gotoident);
            block->f.f.always_ret = 0;
            ctx->varstates->refcount++;
            sm->u.gotoident->target_vs = ctx->varstates;
            sm->u.gotoident->target_stmt = sm;
            break;
        case S_SUBCASE:
            /* TODO */
            break;
        case S_UNREACH:
            /* TODO */
            break;
        case S_NOP:
            break;
        default:
            internal_error(ctx, INTERR_BADSTMTTYPE);
            return 1;
        }
      recover: ;
    }
    if (pre_if_vs) { /* Leaving "else if" block */
        if (true_vs && !block->f.f.always_ret) {
            CHK(common_varstates(ctx, true_vs, ctx->varstates));
        }
        if (if_always_ret && block->f.f.always_ret) {
            /* A sequence of if-elseif-else where all blocks return a value.
               Set the always_ret flag all the way up to the main if/else. */
            struct StmtBlock *baseblk = block;
            assert(if_base_block);
            while (baseblk) {
                baseblk->f.f.always_ret = 1;
                if (baseblk == if_base_block) break;
                baseblk = baseblk->base;
            }
            if_base_block->f.f.always_ret = 1;
        } else if (block->f.f.always_ret) {
            ctx->varstates = true_vs ? true_vs : pre_if_vs;
        }
        pre_if_vs->refcount--;
        assert(pre_if_vs->refcount >= 0);
    }
    return 1;
  soft_assert_error:
    if (!ctx->has_errors) goto missing_error;
    goto recover;
  assert_error:
    if (!ctx->has_errors) goto missing_error;
    return 0;
  missing_error:
    internal_error(ctx, INTERR_MISSINGFCERROR);
    return 1;
}
