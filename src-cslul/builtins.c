/*

  builtins.c -- Sets up built-in definitions

  Copyright © 2022-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "internal.h"
#include "ast.h"
#define INTERR_BUILTINS(errnum) MAKE_INTERR(errnum, INTERRBASE_BUILTINS)

#define MAX_7 0x7f
#define MIN_7 0x80
#define MAX_8 0xff
#define MAX_15 0x7fff
#define MIN_15 0x8000
#define MAX_16 0xffff
#define MAX_31 0x7fffffff
#define MIN_31 0x80000000UL
#define MAX_32 0xffffffffUL
#define MAX_63 0x7fffffffffffffffULL
#define MIN_63 0x8000000000000000ULL
#define MAX_64 0xffffffffffffffffULL
const struct BuiltinInfo builtin_infos[BT_NUMTYPES] = {
                   /*   Type   Wrap Sys NegMin  Max */
    /* BT_Bool     */{ BI_OTHER, 0, 0,  0,      0      },
    /* BT_USize    */{ BI_INT,   0, 1,  0,      MAX_32 },
    /* BT_SSize    */{ BI_INT,   0, 1,  MIN_31, MAX_31 },
    /* BT_FileOffs */{ BI_INT,   0, 1,  MIN_31, MAX_31 },
    /* BT_String   */{ BI_OTHER, 0, 0,  0,      0      },
    /* BT_Int8     */{ BI_INT,   0, 0,  MIN_7,  MAX_7  },
    /* BT_Byte     */{ BI_INT,   0, 0,  0,      MAX_8  },
    /* BT_WUInt8   */{ BI_INT,   1, 0,  0,      MAX_8  },
    /* BT_Int16    */{ BI_INT,   0, 0,  MIN_15, MAX_15 },
    /* BT_UInt16   */{ BI_INT,   0, 0,  0,      MAX_16 },
    /* BT_WUInt16  */{ BI_INT,   1, 0,  0,      MAX_16 },
    /* BT_Int      */{ BI_INT,   0, 0,  MIN_31, MAX_31 },
    /* BT_UInt     */{ BI_INT,   0, 0,  0,      MAX_32 },
    /* BT_WUInt    */{ BI_INT,   1, 0,  0,      MAX_32 },
    /* BT_Int32    */{ BI_INT,   0, 0,  MIN_31, MAX_31 },
    /* BT_UInt32   */{ BI_INT,   0, 0,  0,      MAX_32 },
    /* BT_WUInt32  */{ BI_INT,   1, 0,  0,      MAX_32 },
    /* BT_Int64    */{ BI_INT,   0, 0,  MIN_63, MAX_63 },
    /* BT_UInt64   */{ BI_INT,   0, 0,  0,      MAX_64 },
    /* BT_WUInt64  */{ BI_INT,   1, 0,  0,      MAX_64 }
};


int builtins_init(struct CSlul *ctx)
{
    int i;
    /* TODO move directly into ctx without allocation/pointer */
    struct BaseDefs *basedefs = aallocp(ctx, sizeof(struct BaseDefs));
    if (!basedefs) return 0;
    PROTECT_STRUCT(*basedefs);
    for (i = 0; i < BT_NUMTYPES; i++) {
        struct Type *t = &basedefs->builtin_type[i];
        struct TypeRef *tr;
        t->defflags = D_DEFINED;
        t->type = T_ELMNTRY;
        t->quals = 0;
        t->misc = 0;
        t->column = 0;
        t->line = 0;
        t->u.builtin = (enum BuiltinType)i;
        tr = &basedefs->builtin_tr[i];
        tr->type = t;
        tr->quals = 0;
        tr->prm = NULL;
        PROTECT_STRUCT(*tr);
        PROTECT_STRUCT(*t);
    }
    for (i = 0; i < IT_NUMTYPES; i++) {
        struct Type *t = &basedefs->internal_type[i];
        struct TypeRef *tr;
        t->defflags = D_DEFINED;
        t->quals = 0;
        t->misc = 0;
        t->type = T_INTERNAL;
        t->column = 0;
        t->line = 0;
        t->u.internal = (enum InternalType)(IT_FIRST+i);
        tr = &basedefs->internal_tr[i];
        tr->type = t;
        tr->quals = 0;
        tr->prm = NULL;
        PROTECT_STRUCT(*tr);
        PROTECT_STRUCT(*t);
    }
    ctx->basedefs = basedefs;
    return 1;
}
