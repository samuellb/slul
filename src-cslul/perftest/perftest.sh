#!/bin/sh -eu
#
# Script for doing basic performance testing
#
# Copyright © 2021-2022 Samuel Lidén Borell <samuel@kodafritt.se>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

# except main.c and testperf.c
files="arch.c arena.c backend.c build.c builtins.c context.c errors.c
      mhparse.c misc.c parse.c platform.c tree.c tlverify.c"

if [ -e "../mhparse.c" ]; then
    cd ..
elif [ -e "src-cslul/mhparse.c" ]; then
    cd src-cslul
fi

gcc -pg -O2 -Wall -Wextra -D_FILE_OFFSET_BITS=64 -D_POSIX_C_SOURCE=200112L $files perftest/perftest.c -o perftest/perftest

perftest/perftest 20000 --message-level=error ../testexec/mainapp -I ../testexec/slul-interfaces/

gprof perftest/perftest gmon.out > perftest/perftest.txt
