/*

  perftest.c -- Performance test for C-SLUL

  Copyright © 2021 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "../cslul.h"
#include "../internal.h"
#include <errno.h>
#include <stdio.h>
#include <time.h>

#define main tested_main
#include "../main.c"
#undef main

int main(int argc, char **argv)
{
    char **targv, **cargv;
    char *targs, *cargs;
    size_t argssize;
    int itercount, i;
    clock_t start, time_taken;
    if (argc <= 1) {
      bad_syntax:
        fprintf(stderr, "usage: %s <ITERATION COUNT> [C-SLUL ARGS...]\n",
                argv[0]);
        return 1;
    }
    errno = 0;
    itercount = strtol(argv[1], NULL, 10);
    if (errno != 0) goto bad_syntax;
    argc--;
    argv++;
    /* Prepare argv for copying */
    argv[0] = "/dummy/testperf-cslul";
    argssize = 0;
    for (i = 0; i < argc; i++) {
        argssize += strlen(argv[i])+1;
    }
    targs = malloc(argssize);
    cargs = malloc(argssize);
    targv = malloc(sizeof(char *)*(argc+1));
    cargv = malloc(sizeof(char *)*(argc+1));
    argssize = 0;
    for (i = 0; i < argc; i++) {
        size_t thissize = strlen(argv[i])+1;
        memcpy(&targs[argssize], argv[i], thissize);
        targv[i] = &cargs[argssize];
        argssize += thissize;
    }
    targv[argc] = NULL;
    /* Do the testing */
    start = clock();
    for (i = 0; i < itercount; i++) {
        memcpy(cargv, targv, sizeof(char *)*(argc+1));
        memcpy(cargs, targs, argssize);
        tested_main(argc, cargv);
    }
    time_taken = clock() - start;
    fprintf(stderr, "time ticks: %lu\n", (unsigned long)time_taken);
    return 0;
}
