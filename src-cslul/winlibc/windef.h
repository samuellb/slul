/*

  windef.h -- Part of basic (incomplete) C library for Windows

  Copyright © 2022 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef _CSLULWINLIBC_WINDEF_H
#define _CSLULWINLIBC_WINDEF_H

#include <stddef.h>
#include "winlibc_internal.h"

#ifndef WINAPI
#    define WINAPI __stdcall
#endif

/* WinAPI definitions - Types */
typedef int BOOL;
typedef unsigned short USHORT;
typedef unsigned short WORD;
typedef unsigned short WCHAR;
typedef unsigned UINT;
typedef unsigned DWORD;
typedef unsigned UINT32;
typedef unsigned long long UINT64;

typedef void *PVOID;
typedef void *LPVOID;
typedef const void *LPCVOID;
typedef WCHAR *LPWSTR;
typedef const WCHAR *LPCWSTR;
typedef const char *LPCCH;
typedef DWORD *LPDWORD;
#define HANDLE _CSlulWinLibC_HANDLE
#define HINSTANCE HANDLE
#define HMODULE HANDLE

/* WinAPI definitions - Constants */
#define FALSE 0
#define TRUE  1
#define INVALID_HANDLE_VALUE ((HANDLE)-1)
#define STD_OUTPUT_HANDLE ((DWORD)-11)
#define STD_ERROR_HANDLE ((DWORD)-12)
#define GENERIC_READ  0x80000000L
#define GENERIC_WRITE 0x40000000L
#define FILE_SHARE_READ     0x1
#define FILE_SHARE_WRITE    0x2
#define FILE_SHARE_DELETE   0x4
#define CREATE_NEW    1
#define CREATE_ALWAYS 2
#define OPEN_EXISTING 3
#define OPEN_ALWAYS   4
#define TRUNCATE_EXISTING 5
#define FILE_ATTRIBUTE_READONLY   0x1
#define FILE_ATTRIBUTE_DIRECTORY  0x10
#define FILE_ATTRIBUTE_ARCHIVE    0x20
#define FILE_ATTRIBUTE_NORMAL     0x80
#define FILE_FLAG_DELETE_ON_CLOSE 0x4000000
#define FILE_FLAG_SEQUENTIAL_SCAN 0x8000000
#define FILE_FLAG_RANDOM_ACCESS   0x10000000
#define FILE_FLAG_NO_BUFFERING    0x20000000
#define FILE_FLAG_WRITE_THROUGH   0x80000000
#define CP_UTF8 65001
#define MB_ERR_INVALID_CHARS 0x8
#define FORMAT_MESSAGE_ALLOCATE_BUFFER  0x100
#define FORMAT_MESSAGE_FROM_SYSTEM      0x1000
#define FORMAT_MESSAGE_ARGUMENT_ARRAY   0x2000
#define MAKELANGID(l, sl) ((((USHORT)(sl)) << 10) | (USHORT)(l))
#define LANG_NEUTRAL    0x0
#define SUBLANG_DEFAULT 0x1
#define MAX_PATH 260

#define ERROR_SUCCESS 0

#endif
