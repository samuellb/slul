/*

  stdio.h -- Part of basic (incomplete) C library for Windows

  Copyright © 2021-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef _CSLULWINLIBC_STDIO_H
#define _CSLULWINLIBC_STDIO_H

#include <stdarg.h>
#define _CSLULWINLIBC_NEED_NULL
#define _CSLULWINLIBC_NEED_SIZE_T
#include "winlibc_internal.h"

/* TODO hide/prefix these fields */
typedef struct {
    _CSlulWinLibC_HANDLE hfile;
    unsigned error : 1;
    unsigned is_unbuffered : 1;
    unsigned never_flush : 1;
    unsigned is_console : 1;
    unsigned special_init : 2;
    char *buff, *buffp, *buffend;
} _CSlulWinLibC_FILE;

#define FILE _CSlulWinLibC_FILE

typedef _CSlulWinLibC_uint64_t off_t;

#define putc(c, f) \
        (LIKELY((f)->buff && (f)->buffp != (f)->buffend && \
                !(f)->is_unbuffered && (c) != '\n') ? \
            (int)(unsigned char)(*((f)->buffp++) = (unsigned char)(c)) : \
            fputc(c, f))

extern FILE *stdout;
extern FILE *stderr;

#define EOF -1

int vfprintf(FILE *f, const char *fmt, va_list ap);
int fputc(int c, FILE *f);
int fputs(const char *s, FILE *f);
int fprintf(FILE *f, const char *fmt, ...);
int printf(const char *fmt, ...);
int vsprintf(char *s, const char *fmt, va_list ap);
int sprintf(char *s, const char *fmt, ...);
int puts(const char *s);
void perror(const char *s);
FILE *fopen(const char *filename, const char *mode);
int fclose(FILE *file);
int fflush(FILE *file);
int ferror(FILE *file);
size_t fread(void *buffer, size_t size, size_t nmemb, FILE *file);
size_t fwrite(const void *buffer, size_t size, size_t nmemb, FILE *file);
int remove(const char *filename);

#endif
