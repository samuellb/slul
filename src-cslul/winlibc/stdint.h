/*

  stdint.h -- Part of basic (incomplete) C library for Windows

  Copyright © 2022-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef _CSLULWINLIBC_STDINT_H
#define _CSLULWINLIBC_STDINT_H

#include "winlibc_internal.h"

/* Some of the types below are also used in other headers, that
   should not include stdint.h. Hence the usage of internal types. */
typedef _CSlulWinLibC_int8_t int8_t;
typedef _CSlulWinLibC_int8_t int_fast8_t;
typedef _CSlulWinLibC_uint8_t uint8_t;
typedef _CSlulWinLibC_uint8_t uint_fast8_t;
typedef _CSlulWinLibC_int16_t int16_t;
typedef _CSlulWinLibC_int16_t int_fast16_t;
typedef _CSlulWinLibC_uint16_t uint16_t;
typedef _CSlulWinLibC_int16_t uint_fast16_t;
typedef _CSlulWinLibC_int32_t int32_t;
typedef _CSlulWinLibC_int32_t int_fast32_t;
typedef _CSlulWinLibC_uint32_t uint32_t;
typedef _CSlulWinLibC_uint32_t uint_fast32_t;
typedef _CSlulWinLibC_int64_t int64_t;
typedef _CSlulWinLibC_int64_t int_fast64_t;
typedef _CSlulWinLibC_uint64_t uint64_t;
typedef _CSlulWinLibC_uint64_t uint_fast64_t;

/* Some compilers/libc's define these in stddef.h, but
   according to ANSI C they should be in stdint.h */
typedef _CSlulWinLibC_intptr_t intptr_t;
typedef _CSlulWinLibC_uintptr_t uintptr_t;

#define INT8_MIN   -128
#define INT8_MAX   127
#define UINT8_MAX  255U
#define INT16_MIN  -32768
#define INT16_MAX  32767
#define UINT16_MAX 65535U
#define INT32_MIN  -2147483648
#define INT32_MAX  2147483647
#define UINT32_MAX 4294967295U
#define UINT64_MAX ((uint64_t)~((uint64)0))

#endif
