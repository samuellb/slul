/*

  winlibc_internal.h -- Part of basic (incomplete) C library for Windows

  Copyright © 2022-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef _CSLULWINLIBC_CAN_USE_MAIN_IDENTIFIER
/* Very ugly hack. Needed because compilers give special treatment to any
   function named "main", so we need to give our "main" a different name */
#    undef main
#    undef NAME_OF_MAIN
#    define NAME_OF_MAIN _CSlulWinLibC_main
#    define main(argc, argv) NAME_OF_MAIN(argc, argv)
#endif


#ifndef _CSLULWINLIBC_INTERNAL_H
#define _CSLULWINLIBC_INTERNAL_H

#if defined(__aarch64__) || defined(_M_ARM64)
    #define _CSLULWINLIBC_AARCH64
#elif defined(__amd64__) || defined(__amd64) || \
      defined(__x86_64__) || defined(__x86_64) || \
      defined(_M_AMD64) || defined(_M_X64)
    #define _CSLULWINLIBC_X86_64
#elif defined(__i386__) || defined(__i386) || \
      defined(_X86_) || defined(__X86__) || defined(_M_IX86)
    #define _CSLULWINLIBC_I386
#else
    #error Unknown CPU architecture. Please define it in winlibc_internal.h
#endif

#if defined(_CSLULWINLIBC_AARCH64) || defined(_CSLULWINLIBC_X86_64)
#    define _CSLULWINLIBC_64BIT
#endif

#if defined(__GNUC__) || defined(__clang__)
#    define _CSLULWINLIBC_NORETURN __attribute__((__noreturn__))
#else
#    define _CSLULWINLIBC_NORETURN
#endif

typedef signed char _CSlulWinLibC_int8_t;
typedef short _CSlulWinLibC_int16_t;
typedef int _CSlulWinLibC_int32_t;
typedef long long _CSlulWinLibC_int64_t;

typedef unsigned char _CSlulWinLibC_uint8_t;
typedef unsigned short _CSlulWinLibC_uint16_t;
typedef unsigned _CSlulWinLibC_uint32_t;
typedef unsigned long long _CSlulWinLibC_uint64_t;

typedef void *_CSlulWinLibC_HANDLE;
typedef unsigned _CSlulWinLibC_mode_t;

/* These appear to be defined by the compiler in MinGW,
   except in very old versions */
#ifdef _CSLULWINLIBC_64BIT
typedef long long _CSlulWinLibC_intptr_t;
typedef unsigned long long _CSlulWinLibC_uintptr_t;
#else
typedef long _CSlulWinLibC_intptr_t;
typedef unsigned long _CSlulWinLibC_uintptr_t;
#endif
#define _CSlulWinLibC_size_t _CSlulWinLibC_uintptr_t

#ifndef _CSLULWINLIBC_CAN_USE_MAIN_IDENTIFIER
/* Ensure that there is a prototype for main */
extern int NAME_OF_MAIN(int argc, char **argv);
#endif

#endif /* end of include guard */

/* Note: These have to go outside the include guard! */

#ifdef _CSLULWINLIBC_NEED_NULL
    #ifndef _CSLULWINLIBC_HAVE_NULL
        #define _CSLULWINLIBC_HAVE_NULL
        #define NULL ((void *)0)
    #endif
    #undef _CSLULWINLIBC_NEED_NULL
#endif

#ifdef _CSLULWINLIBC_NEED_SIZE_T
    #ifndef _CSLULWINLIBC_HAVE_SIZE_T
        #define _CSLULWINLIBC_HAVE_SIZE_T
        typedef _CSlulWinLibC_size_t size_t;
    #endif
    #undef _CSLULWINLIBC_NEED_SIZE_T
#endif

#ifdef _CSLULWINLIBC_NEED_WCHAR_T
    #ifndef _CSLULWINLIBC_HAVE_WCHAR_T
        #define _CSLULWINLIBC_HAVE_WCHAR_T
        typedef _CSlulWinLibC_uint32_t wchar_t;
    #endif
    #undef _CSLULWINLIBC_NEED_WCHAR_T
#endif
