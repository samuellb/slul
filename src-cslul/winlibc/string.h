/*

  string.h -- Part of basic (incomplete) C library for Windows

  Copyright © 2021-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef _CSLULWINLIBC_STRING_H
#define _CSLULWINLIBC_STRING_H

#define _CSLULWINLIBC_NEED_NULL
#define _CSLULWINLIBC_NEED_SIZE_T
#include "winlibc_internal.h"

void *memcpy(void *dest, const void *source, size_t size);
void *memset(void *dest, int value, size_t size);
int memcmp(const void *a, const void *b, size_t size);
void *memchr(const void *buff, int c, size_t n);
#if defined(_GNU_SOURCE) || defined(_CSLULWINLIBC_SOURCE)
void *memrchr(const void *buff, int c, size_t n);
#endif
char *strchr(const char *s, int c);
char *strrchr(const char *s, int c);
int strcmp(const char *a, const char *b);
int strncmp(const char *a, const char *b, size_t len);
size_t strlen(const char *s);
#if (defined(_POSIX_C_SOURCE) && _POSIX_C_SOURCE >= 200809L) || \
     defined(_CSLULWINLIBC_SOURCE)
size_t strnlen(const char *s, size_t max);
#endif
size_t strcspn(const char *s, const char *reject);
size_t strspn(const char *s, const char *accept);
#if (defined(_POSIX_C_SOURCE) && _POSIX_C_SOURCE >= 200809L) || \
     defined(_CSLULWINLIBC_SOURCE)
char *strdup(const char *s);
char *strndup(const char *s, size_t max);
#endif

#endif
