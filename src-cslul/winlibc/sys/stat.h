/*

  sys/stat.h -- Part of basic (incomplete) C library for Windows

  Copyright © 2022-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef _CSLULWINLIBC_SYS_STAT_H
#define _CSLULWINLIBC_SYS_STAT_H

#include "../winlibc_internal.h"

#define S_IRWXU 0700
#define S_IRWXG 0070
#define S_IRWXO 0007

int mkdir(const char *filename, _CSlulWinLibC_mode_t mode);

#ifdef _CSLULWINLIBC_SOURCE
/**
 * Returns the root directory of the application,
 * or optionally a subdir/filename in that directory.
 *
 * For example, if exe_subdir=Bin, append=SubDir,
 * and this EXE file is running in:
 *    C:\Apps\Test\Bin\example.exe
 * then it returns:
 *    C:\Apps\Test\SubDir
 */
char *cslulwinlibc_getappdir(const char *exe_subdir, const char *append);
#endif

#endif
