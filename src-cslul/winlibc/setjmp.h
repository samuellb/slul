/*

  setjmp.h -- Part of basic (incomplete) C library for Windows

  Copyright © 2022-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#ifndef _CSLULWINLIBC_SETJMP_H
#define _CSLULWINLIBC_SETJMP_H

#include "winlibc_internal.h"

/* NOTE: Works with GCC/clang only */

#undef jmp_buf
#undef setjmp
#undef longjmp

/* The largest jmp_buf size on any platform according to
   setjmp.h in mingw-w64  (33 longs / 2) */
typedef void *jmp_buf[17];

#if defined(__GNUC__) || defined(__clang__)
    #if !defined(_CSLULWINLIBC_AARCH64) /* at least unavailable in clang */
        #define _CSLULWINLIBC_HAVE_SETJMP 1
        #define longjmp __builtin_longjmp
        #define setjmp  __builtin_setjmp
    #endif
#endif

#ifndef _CSLULWINLIBC_HAVE_SETJMP
    /* Provide dummy implementation that aborts on longjmp */
    #define longjmp _CSlulWinLibC_longjmp
    _CSLULWINLIBC_NORETURN void _CSlulWinLibC_longjmp(jmp_buf env, int val);
    #define setjmp(env) 0
#endif

#endif
