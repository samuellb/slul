/*

  errors.c -- Builds table for error message strings

  Copyright © 2014-2022 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/

#include "internal.h"
#include <stddef.h>

/* Error descriptions.
   Based on the method developed by Bruno Haible in Ulrich Drepper's
   "How To Write Shared Libraries" manual. */
static const union {
    struct DescriptionEntries {
        #define MSGDEF(lvl, n, s) char n[sizeof(s)];
        #include "errors.h"
        #undef MSGDEF
        char dummy;
    } entries;
    char data[sizeof(struct DescriptionEntries)];
} errormsgs = {
    {
        #define MSGDEF(lvl, n, s) s,
        #include "errors.h"
        #undef MSGDEF
        0
    }
};

struct ErrorInfo {
    unsigned int msgoffset;
    enum CSlulMessageLevel level;
};
static const struct ErrorInfo errormsg_indices[] = {
    #define MSGDEF(lvl, n, s) { offsetof(struct DescriptionEntries, n), lvl },
    #define F CSLUL_L_FATAL
    #define E CSLUL_L_ERROR
    #define W CSLUL_L_WARNING
    #define N CSLUL_L_NOTICE
    #define S CSLUL_L_STYLE
    #include "errors.h"
    #undef S
    #undef N
    #undef W
    #undef E
    #undef F
    #undef MSGDEF
    { 0, 0 }
};

const char *lookup_error_message(enum CSlulErrorCode errorcode)
{
    if (errorcode < CSLUL_E_LAST) {
        return &errormsgs.data[errormsg_indices[errorcode].msgoffset];
    } else {
        return NULL;
    }
}

enum CSlulMessageLevel lookup_error_level(enum CSlulErrorCode errorcode)
{
    if (errorcode < CSLUL_E_LAST) {
        return errormsg_indices[errorcode].level;
    } else if (CSLUL_IS_INTERNAL_ERR(errorcode)) {
        return CSLUL_L_FATAL;
    } else {
        return (enum CSlulMessageLevel)-1;
    }
}

#ifdef CHECK_ERROR_INDICES
enum ErrorIndexCheckEnum {
#define MSGDEF(lvl, n, s) MSGINDEXCHECK_##n,
#include "errors.h"
#undef MSGDEF
MSGINDEXCHECK_END
};

#define MSGDEF(lvl, n, s) typedef char ErrorIndexCheck_##n[(int)n == (int)MSGINDEXCHECK_##n ? 1 : -1];
#include "errors.h"
#undef MSGDEF

typedef char ErrorIndexCountCheck_[CSLUL_E_LAST == MSGINDEXCHECK_END ? 1 : -1];
#endif
