/*

  slulrt.h -- Definitions for interfacing with the SLUL runtime

  Copyright © 2023-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  -----------------------------------------------------------------------------

  As an exception from the license above, permission is hereby granted to
  omit the copyright notice and permission notice otherwise required by the
  license, and to use this file without any requirement of attribution.

*/

#ifndef SLULRT_H

#ifdef SLULRT
    #if defined(_WIN32) || defined(__CYGWIN__)
    #define SLULRTAPI __declspec(dllexport)
    #else
        #define SLULRTAPI
    #endif
#elif defined(_WIN32) || defined(__CYGWIN__)
    #define SLULRTAPI __declspec(dllimport)
#elif defined(__has_attribute)
    #if __has_attribute(visibility)
        #define SLULRTAPI __attribute__((visibility("default")))
    #else
        #define SLULRTAPI
    #endif
#elif __GNUC__ >= 4
    #define SLULRTAPI __attribute__((visibility("default")))
#else
    #define SLULRTAPI
#endif


struct SlulArena;
struct SlulApp;
typedef int SlulExitStatus;

/**
 * Initializes a root arena for an application, and returns a new
 * SlulApp instance in the new arena.
 *
 * The arena will have full capability to call system; all system functions
 * will call the libc.
 *
 * \param argc  Argument count, or 0 if this information is not provided by
 *              the OS at startup (e.g. on Windows).
 * \param argv  Argument vector, or NULL if not provided by the OS at startup.
 * \return  A new SlulApp, allocated in a new root arena.
 */
extern SLULRTAPI struct SlulApp *SlulApp_init_full(int argc, char **argv);
/* TODO add a function to create an unrestricted SlulArena from argc/argv? */

/**
 * Creates a new SlulApp within the given arena.
 *
 * The command-line arguments are taken from the arena.
 */
extern SLULRTAPI struct SlulApp *SlulApp_init_restricted(struct SlulArena *arena);

/**
 * Frees a SlulApp instance.
 */
extern SLULRTAPI void SlulApp_finalize(struct SlulApp *app);


#endif
