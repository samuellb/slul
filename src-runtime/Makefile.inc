#
# Makefile definitions for the base runtime for SLUL
#
# Copyright © 2023-2024 Samuel Lidén Borell <samuel@kodafritt.se>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

RUNTIME_TEST_OBJECTS = \
    $(RUNTIME_EXTRA_TEST_OBJECTS) \
    $(build_runtime)/unittest/test_rtarena.o \
    $(build_runtime)/unittest/test_rtinit.o \
    $(COMMON_TEST_OBJECTS)
RUNTIME_UNITTEST_OBJECTS = \
    $(RUNTIME_TEST_OBJECTS) \
    $(build_runtime)/unittest/testmain.o
RUNTIME_ALL_TEST_OBJECTS = \
    $(RUNTIME_TEST_OBJECTS) \
    $(build_runtime)/unittest/testmain.o
RUNTIME_UNITTEST_C_FILES_WITHOUT_EXTERNALS = \
    $(src_runtime)/unittest/test_rtarena.c \
    $(src_runtime)/unittest/test_rtinit.c \
    $(src_runtime)/unittest/testmain.c
RUNTIME_UNITTEST_C_FILES = \
    $(RUNTIME_EXTRA_UNITTEST_C_FILES) \
    $(RUNTIME_UNITTEST_C_FILES_WITHOUT_EXTERNALS) \
    $(COMMON_TEST_C_FILES)
RUNTIME_OBJECTS = \
    $(RUNTIME_EXTRA_OBJECTS) \
    $(build_runtime)/rtarena.pio \
    $(build_runtime)/rtinit.pio
RUNTIME_C_FILES = \
    $(RUNTIME_EXTRA_C_FILES) \
    $(src_runtime)/rtarena.c \
    $(src_runtime)/rtinit.c
RUNTIME_HEADERS = \
    $(src_runtime)/include/slulrt.h \
    $(src_runtime)/rt_internal.h \
    $(src_runtime)/unittest/alltests.h \
    $(src_common)/unittest/unittest.h \
    $(RUNTIME_EXTRA_HEADERS)


runtime-all: $(build_runtime)/libslulrt.so

$(build_runtime)/rtarena.pio: $(src_runtime)/include/slulrt.h $(src_runtime)/rt_internal.h
$(build_runtime)/rtinit.pio: $(src_runtime)/include/slulrt.h $(src_runtime)/rt_internal.h

$(build_runtime)/unittest/testcommon.o: $(src_runtime)/include/slulrt.h $(src_runtime)/rt_internal.h $(src_runtime)/unittest/testcommon.h $(src_common)/unittest/unittest.h
$(build_runtime)/unittest/test_rtarena.o: $(src_runtime)/rtarena.c $(src_runtime)/include/slulrt.h $(src_runtime)/rt_internal.h $(src_common)/unittest/unittest.h
$(build_runtime)/unittest/test_rtinit.o: $(src_runtime)/rtinit.c $(src_runtime)/include/slulrt.h $(src_runtime)/rt_internal.h $(src_common)/unittest/unittest.h
$(build_runtime)/unittest/testmain.o: $(src_runtime)/unittest/alltests.h $(src_common)/unittest/unittest.h

$(build_runtime)/libslulrt.so: $(RUNTIME_OBJECTS)
	$(CC) $(SOLIB_CFLAGS) $(INTERNCFLAGS) $(CFLAGS) \
	    $(SOLIB_LDFLAGS) -Wl,-soname,libslulrt.so $(RUNTIME_OBJECTS) -o $@

# TODO WINE/Windows .dll


$(build_runtime)/unittest-normal: $(RUNTIME_UNITTEST_OBJECTS)
	$(CC) $(INTERNCFLAGS) $(CFLAGS) $(LDFLAGS) $(DEBUG_DEFINES) $(RUNTIME_UNITTEST_OBJECTS) $(RUNTIME_EXTRA_LIBS) -o $@
$(build_runtime)/unittest-tcc-boundscheck: $(RUNTIME_UNITTEST_C_FILES) $(RUNTIME_C_FILES) $(RUNTIME_HEADERS)
	tcc -b -bt24 $(INTERNCFLAGS) $(LDFLAGS) $(DEBUG_DEFINES) $(RUNTIME_UNITTEST_C_FILES) $(RUNTIME_EXTRA_LIBS) -o $@
$(build_runtime)/unittest-gcov: $(RUNTIME_UNITTEST_C_FILES) $(RUNTIME_C_FILES) $(RUNTIME_HEADERS)
	gcc --coverage $(INTERNCFLAGS) $(LDFLAGS) $(DEBUG_DEFINES) $(RUNTIME_UNITTEST_C_FILES) $(RUNTIME_EXTRA_LIBS) -o $@
$(build_runtime)/unittest-gprof: $(RUNTIME_UNITTEST_C_FILES) $(RUNTIME_C_FILES) $(RUNTIME_HEADERS)
	gcc -pg $(INTERNCFLAGS) $(LDFLAGS) $(DEBUG_DEFINES) $(RUNTIME_UNITTEST_C_FILES) $(RUNTIME_EXTRA_LIBS) -o $@

runtime-check: runtime-check-unit

runtime-check-unit: $(build_runtime)/unittest-normal
	$(build_runtime)/unittest-normal $(UNITTEST_OPTS)

runtime-check-unit-valgrind: $(build_runtime)/unittest-normal
	valgrind -q --leak-check=full $(VALGRIND_OPTS) $(build_runtime)/unittest-normal $(UNITTEST_OPTS)

runtime-tcc-boundscheck: $(build_runtime)/unittest-tcc-boundscheck
	TCC_BOUNDS_WARN_POINTER_ADD=1 $(build_runtime)/unittest-tcc-boundscheck $(UNITTEST_OPTS)

runtime-gcc-coverage: $(build_runtime)/unittest-gcov
	$(build_runtime)/unittest-gcov $(UNITTEST_OPTS)

runtime-clang-analyze:
	clang $(DEFINES) $(INCLUDES) --analyze $(RUNTIME_C_FILES)
	clang $(DEFINES) $(INCLUDES) --analyze $(RUNTIME_UNITTEST_C_FILES)

runtime-gcc-analyze:
	gcc $(GCC_FANALYZE_FLAGS) $(DEFINES) $(INCLUDES) \
	        $(RUNTIME_UNITTEST_C_FILES) \
	        -o $(build_runtime)/fanalyzer_dummy

runtime-sparse-analyze:
	sparse $(DEFINES) $(INCLUDES) $(RUNTIME_C_FILES)
	sparse $(DEFINES) $(INCLUDES) $(RUNTIME_UNITTEST_C_FILES)

# For now, we run all tests for a specific platform only,
# in order to reduce the number of permutations.
# The "threadsafety" addon is used to find missing/incorrect
# usage of static and/or const keywords.
runtime-cppcheck:
	cppcheck $(CPPCHECK_FLAGS) -rp $(src_runtime) \
	    --suppressions-list=$(src_runtime)/misc/cppcheck_suppressions.txt \
	    -DSLULRT \
	    $(RUNTIME_C_FILES) $(RUNTIME_UNITTEST_C_FILES)

gdb-runtime-unit: $(build_runtime)/unittest-normal
	gdb --args $(build_runtime)/unittest-normal $(UNITTEST_OPTS)

runtime-outdirs:
	$(MKDIR_P) $(build_runtime)/unittest

runtime-clean:
	$(RM_F) $(RUNTIME_OBJECTS) $(RUNTIME_ALL_TEST_OBJECTS) \
	        $(build_runtime)/libslulrt.so \
	        $(build_runtime)/unittest-normal \
	        $(build_runtime)/unittest-oom \
	        $(build_runtime)/unittest-tcc-boundscheck \
	        $(build_runtime)/unittest-gcov $(build_runtime)/unittest-gprof \
	        $(build_runtime)/fanalyzer_dummy
runtime-distclean: runtime-clean

runtime-install: $(build_runtime)/libslulrt.so
	$(INSTALL_DIR) $(DESTDIR)$(libdir)
	$(INSTALL_DIR) $(DESTDIR)$(libdir)$(multiarch_)
	$(INSTALL_PROGRAM) $(build_runtime)/libslulrt.so $(DESTDIR)$(libdir)$(multiarch_)/libslulrt.so
	$(INSTALL_DIR) $(DESTDIR)$(includedir)
	$(INSTALL_DATA) $(src_runtime)/include/slulrt.h $(DESTDIR)$(includedir)/slulrt.h
	$(INSTALL_DIR) $(DESTDIR)$(slulinterfacesdir)
	$(INSTALL_DATA) $(src_runtime)/slul-interfaces/slulrt.slul $(DESTDIR)$(slulinterfacesdir)/slulrt.slul
runtime-uninstall:
	$(RM) $(DESTDIR)$(libdir)$(multiarch_)/libslulrt.so
	$(RM) $(DESTDIR)$(includedir)/slulrt.h
	$(RM) $(DESTDIR)$(slulinterfacesdir)/slulrt.slul


.PHONY: runtime-all runtime-clean runtime-distclean runtime-outdirs \
        runtime-check runtime-check-unit \
        runtime-check-unit-valgrind \
        runtime-tcc-boundscheck runtime-gcc-coverage runtime-clang-analyze \
        runtime-sparse-analyze runtime-cppcheck \
        gdb-runtime-unit \
        runtime-install runtime-uninstall
