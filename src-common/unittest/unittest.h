/*

  unittest.h -- Simple "library" for unit tests

  Copyright © 2016-2024 Samuel Lidén Borell <samuel@kodafritt.se>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

*/
#ifndef UNITTEST_H
#define UNITTEST_H

#include <stddef.h>
#include <stdio.h> /* several tests print error details using fprintf */

/*
  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~

  Simple unit testing "library". Please see README.md for an overview

  ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~
*/


#if defined(__GNUC__) || defined(__clang__) || defined(__cppcheck__)
#    define UNITTEST_NORETURN __attribute__((__noreturn__))
#else
#    define UNITTEST_NORETURN
#endif


/* =======================================================================
   ||||||||||  Internal Definitions. Should be used via macros  ||||||||||
   ======================================================================= */

/** Function type for test cases, and for before/after functions */
typedef void TestFunction(void);

/**
 * Information entry of a given test case.
 * Each test file should have an array of this, named test_xyz where the
 * filename should be test_xyz.c (the "test_" part is required).
 */
typedef struct {
    /** Test/function name. It may also point to either before_test_ptrval or
        after_test_ptrval to indicate that the function should be run before
        or after each test case, respectively. */
    const char *name;
    /** Pointer to test function */
    TestFunction *function;
} TestFunctionInfo;

/** Special sentinel value for the name in TestFunctionInfo.
    Use the TEST_BEFORE macro instead. */
extern char before_test_ptrval[];
/** Special sentinel value for the name in TestFunctionInfo.
    Use the TEST_AFTER macro instead. */
extern char after_test_ptrval[];

/** Called from test_all(). Runs an array of test cases, that should be
    terminated with TEST_END */
void test_file(const char *filename, const TestFunctionInfo *tests);


/* =======================================================================
   ||||||||||||||||||||||  Definition of Test Cases  |||||||||||||||||||||
   ======================================================================= */

/** Test function info in test source files */
#define TEST_INFO(func) { #func, &(func) },
/** Function to run before each test case */
#define TEST_BEFORE(func) { before_test_ptrval, &(func) },
/** Function to run after each test case */
#define TEST_AFTER(func) { after_test_ptrval, &(func) },
/** Sentinel value at end of list of tests */
#define TEST_END { NULL, NULL }


/* =======================================================================
   ||||||||||||||||||  Assertion and Printing Functions  |||||||||||||||||
   ======================================================================= */

/** Like printf, but respects the "quiet" variable */
void tprintf(const char *format, ...);

/** Reports error at the given filename/line, and aborts the test.
    Use the tfail() macro instead to report at the current line. */
UNITTEST_NORETURN void tfail_(const char *filename, int line, const char *msg);
/** Reports error at the given filename/line, but continues.
    Use the tsoftfail() macro instead to report at the current line. */
int tsoftfail_(const char *filename, int line, const char *msg, int ok);

/** Evaluates the expression, and aborts the test with an error if false */
#define tassert(expr) do { \
        if (!(expr)) { tfail_(__FILE__, __LINE__, #expr); } \
    } while (0)
/** Evaluates the expression, and reports an error (but continues) if false */
#define tsoftassert(expr) \
    ((expr) || (tsoftfail_(__FILE__, __LINE__, #expr, 0), 0))

/** Like tassert(), but allows the source line to be overridden */
#define tassert_line(line, expr) do { \
        if (!(expr)) { tfail_(__FILE__, line, #expr); } \
    } while (0)
/** Like tsoftassert(), but allows the source line to be overridden */
#define tsoftassert_line(line, expr) \
    ((expr) || (tsoftfail_(__FILE__, line, #expr, 0), 0))

/** Aborts the test with an error message */
#define tfail(message) tfail_(__FILE__, __LINE__, message)
/** Reports an error, but allows the test to continue */
#define tsoftfail(message) tsoftfail_(__FILE__, __LINE__, message, 0)


/* =======================================================================
   |||||||||||||||||||||||||||  Configuration  |||||||||||||||||||||||||||
   ======================================================================= */

/**
 * Whether to disable printing of error messages / tprintf.
 *
 * This is set by the -q option.
 */
extern int quiet;

/**
 * Whether to enable printing of the name of the running test case.
 *
 * This is set by the -v option.
 */
extern int verbose;

/**
 * Ignore test results, just check that there is no crash.
 * If 1: Don't print errors, don't run TEST_AFTER functions.
 *
 * This is useful for testing out-of-memory handling (which will certainly
 * generate error, but should still not crash).
 */
extern int crash_test_only;


/* =======================================================================
   |||||||||||||||||||||  Project-Defined Functions  |||||||||||||||||||||
   ======================================================================= */

/**
 * This function MUST be defined, and is run once per test case.
 *
 * In the most simple case it can be a one-liner that just calls run_test().
 *
 * Optionally, it can iterate through test configurations and perform
 * multiple calls to run_test(), and/or perform additional before-test or
 * after-test code.
 */
void test_handler(void);

/**
 * This function MUST be defined, and is run when printing an error.
 *
 * In the most simple case it can be a one-liner that just returns an
 * empty string.
 *
 * Optionally, it can return some form of identifier for the current
 * test configuration.
 */
const char *error_extra_text(void);

/**
 * This function MUST be defined, and is run when printing the command
 * usage / help text.
 *
 * It should return NULL if there is no extra help text.
 *
 * If non-NULL, it should have lines shorter than 80 characters and
 * a newline "\n" at the end of each line.
 */
const char *help_extra_text(void);

/**
 * This function MUST be defined, and is run when printing test names
 * in verbose mode.
 *
 * It should return NULL if there is no extra text.
 */
const char *verbose_extra_text(void);


/* =======================================================================
   ||||||||||||  Functions that should be called from project  |||||||||||
   ======================================================================= */

/**
 * Must be called by test_handler() to run the test (possibly multiple times).
 * (If not called, the test will appear to run, while it in fact gets skipped)
 */
void run_test(void);

/**
 * Processes command-line arguments, and then runs specified tests.
 * Should be called from main().
 */
int test_main(int argc, char **argv);

/* =======================================================================
   ||||||||||||||  Information about the Current Test Case  ||||||||||||||
   ======================================================================= */

/**
 * File name of the current test file (without path)
 */
extern const char *current_file;

/**
 * Name of the current test case/function.
 */
extern const char *current_function;

/**
 * Number of errors that have occurred in the current test case.
 */
extern int errors_testcase;

/**
 * Whether the last test case was aborted (1) or ran to the end (0).
 */
extern int testcase_aborted;

/* =======================================================================
   |||||||||||||||||||||  Listing of All Test Cases  |||||||||||||||||||||
   ======================================================================= */

/*

    You should have a file called "alltests.h" which should contain a
    sequence of TEST_D macro calls, for each test file, for example:

        TEST_D(wheels)
        TEST_D(pedals)
        TEST_D(chain)

    Each test file should be named test_xyz.c, for example test_wheels.c.
    In each test file, please include unittest.h like this:

        #include "unittest.h"
        #include "alltests.h"

    In the main file, please include unittest.h like this:

        #define TEST_MAIN
        #include "unittest.h"

        #include "alltests.h"
        TESTCALLS_START
        #undef TEST_D
        #define TEST_D TEST_D2
        #include "alltests.h"
        TESTCALLS_END

*/

void test_all(void);
#ifndef TEST_MAIN
#    define TEST_D(name) extern const TestFunctionInfo tests_##name[];
#else
#    define TESTCALLS_START void test_all(void) {
/* This is nicer, but gives warnings */
/*#    define TEST_D(name) { \
            extern const TestFunctionInfo tests_##name[]; \
            test_file(#name ".c", tests_##name); \
        }*/
#    define TEST_D1(name) extern const TestFunctionInfo tests_##name[];
#    define TEST_D2(name) test_file(#name ".c", tests_##name);
#    define TESTCALLS_END }
#    define TEST_D TEST_D1
#endif


#endif

