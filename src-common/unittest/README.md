Simple Unit-Testing "Library"
=============================

License: MIT

This is a simple unit-testing system that can be included in a C project
to handle unit testing.

Requirements
------------
* C89 compiler
* longjmp/setjmp support is required for recovering from errors or running
  in "crash-test-only" mode.

Usage
-----
The unit tested project should contain these parts:

* alltests.h  - a list of all tests
* testmain.c  - containing various definitions, and the main function
* test_xyz.c  - test functions + list of tests

The file called "alltests.h" which should contain a sequence of TEST_D
macro calls, for each test file, for example:

    TEST_D(wheels)
    TEST_D(pedals)
    TEST_D(chain)

A minimal testmain.c looks like this:

    #define TEST_MAIN
    #include "unittest.h"

    #include "alltests.h"
    TESTCALLS_START
    #undef TEST_D
    #define TEST_D TEST_D2
    #include "alltests.h"
    TESTCALLS_END


    /* This function is called for each test case, and should call run_test().
       It can be expanded with multiple test configuration (and multiple calls
       to run_test()), as well as test initialization, cleanup and checks. */
    void test_handler(void)
    {
        run_test();
    }

    /* These functions are used to add extra details in messages */
    void error_extra_text(void)   { return NULL; }
    void help_extra_text(void)    { return NULL; }
    void verbose_extra_text(void) { return NULL; }

    int main(int argc, char **argv)
    {
        return test_main(argc, argv);
    }


Each test file should be named test_xyz.c, for example test_wheels.c.
Here is an example:

    #include "unittest.h"
    #include "alltests.h"

    #include "../wheel.c"

    static void test_wheel_rotate(void)
    {
        wheel_axis_rotate(180);
        tsoftassert(get_wheel_rotation() == 180);
        wheel_axis_rotate(180);
        tassert(get_wheel_rotation() == 360);
    }

    static void test_freewheel(void)
    {
        wheel_axis_rotate(-180);
        tassert(get_wheel_rotation() == 0);
    }

    const TestFunctionInfo tests_wheels[] = {
        TEST_INFO(test_wheel_rotate)
        TEST_INFO(test_freewheel)
        TEST_END
    };
