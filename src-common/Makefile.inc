#
# Common Makefile definitions
#
# Copyright © 2021-2024 Samuel Lidén Borell <samuel@kodafritt.se>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

CFLAGS ?= -g
SOLIB_CFLAGS ?= -fpic
SOLIB_LDFLAGS ?= -shared -Wl,-z,defs,-z,relro,-z,now,-z,text

DEFINES += -D_POSIX_C_SOURCE=200112L -I$(src_common)/unittest
DEBUG_DEFINES += -DSLUL_DEBUG
# -Wno-long-long is necessary when building in "C89" mode.
# (64 bit ints are requried, but not other C99 features)
WARNINGS += -Wall -Wextra -pedantic -Wcast-align -Wdate-time \
    -Wmissing-declarations -Wmissing-prototypes -Wnested-externs \
    -Wnull-dereference -Wold-style-definition -Wshadow -Wshift-negative-value \
    -Wshift-overflow -Wstrict-aliasing -Wstrict-prototypes -Wswitch-enum \
    -Wundef \
    -Wno-long-long
# -Wunused-macros gives warnings on unimplemented code and is not enabled.
# GCC specific warnings:
#   -Walloc-zero -Wc90-c99-compat -Wcast-align=strict -Wduplicated-cond
#   -Wlogical-op -Wshift-overflow=2 -Wformat-signedness
# clang specific warnings
#   -Wdocumentation -Wextra-semi-stmt -Wconditional-uninitialized
# With -Weverything + exclusions:
#WARNINGS += -Weverything -Wno-padded -Wno-conversion -Wno-sign-conversion -Wno-covered-switch-default -Wno-format-nonliteral -Wno-implicit-fallthrough
# On BSD you may need to build with CSLUL_LIBS=-lintl

prefix ?= /usr/local
exec_prefix ?= $(prefix)
bindir ?= $(exec_prefix)/bin
datarootdir ?= $(prefix)/share
datadir ?= $(datarootdir)
docdir ?= $(datarootdir)/doc
htmldir ?= $(docdir)
libdir ?= $(exec_prefix)/lib$(multilib_suffix)
includedir ?= $(prefix)/include
slulinterfacesdir ?= $(datarootdir)/slul-interfaces
localedir ?= $(datarootdir)/locale
mandir ?= $(datarootdir)/man
man1dir ?= $(mandir)/man1
man1ext ?= .1
multiarch_ = $(multiarch:%=/%)
# If you use srcdir: Please run "make srcdir=xxx outdirs" first,
# to create the necessary directory structure for output files.
srcdir ?= .

DIRS = -DLOCALEDIR=\"$(localedir)\" -DDATADIR=\"$(datadir)\" -DSLULIFACEDIR=\"$(slulinterfacesdir)\"
INCLUDES = -I$(src_backend)/include
INTERNCFLAGS = $(DIRS) $(INCLUDES) $(DEFINES) -std=c89 -fvisibility=hidden $(WARNINGS)
VPATH ?= $(srcdir)

INSTALL ?= install
INSTALL_DIR ?= $(INSTALL) -d -m 755
INSTALL_PROGRAM ?= $(INSTALL) -m 755
INSTALL_DATA ?= $(INSTALL) -m 644
MKDIR_P ?= mkdir -p
RM ?= rm
RM_F ?= rm -f
ELFLINT ?= eu-elflint

COMMON_TEST_OBJECTS = $(build_common)/unittest/unittest.o
COMMON_TEST_C_FILES = $(src_common)/unittest/unittest.c

# gcc -fanalyze runs in -O2 mode, because some of the traditional warnings
# options (like -Wmaybe-uninitialized) can give more results in -O2 mode.
GCC_FANALYZE_FLAGS = \
    -fanalyzer $(WARNINGS) -O2 $(GCC_FANALYZER_OPTS)

CPPCHECK_FLAGS = \
    -q --language=c --std=c89 --enable=all \
    --addon=threadsafety --inconclusive -ijunk \
    $(DEFINES) -D__linux__ -U_WIN32 -U_WIN64 -U_WINDOWS -D__i386__ \
    -D__cppcheck__ $(CPPCHECK_OPTS)

.SUFFIXES: .po .mo .c .o .pio .rawout .actual

# override is needed to get the same behaviour in both bmake and GNU make.
# bmake does not include -o, which leads to the toplevel directory being used
.c.o:
	$(CC) $(INTERNCFLAGS) $(CFLAGS) -c $< -o $@
# .pio = made up file extension for position-independent object file
.c.pio:
	$(CC) $(INTERNCFLAGS) $(CFLAGS) $(SOLIB_CFLAGS) -c $< -o $@

.po.mo:
	msgfmt $< -o $@

common-outdirs:
	$(MKDIR_P) $(build_common)/unittest

.PHONY: common-outdirs
