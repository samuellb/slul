Field with multi-threaded access
================================

Problem 1:

    type Thing = struct {
        threaded byte a
        threaded byte b
    }

    - The CPU instruction(s) that write to a might also touch b.
    - Solution: Add padding between a and b, and after b.

Problem 2:

    type B = struct {
        threaded byte b
    }
    type Thing = struct {
        threaded byte a
        threaded struct B b
    }

    - Same problem as above.
    - Padding can be added.

Problem 3:
- Should atomic operations be allowed on threaded items? (Probably yes)
- Which atomic operations to support?
    - Do all multi-threading capable CPUs support all atomic operations? (Probably not)
    - Do some CPUs require special formatting (such as an extra word) to support atomic operations? (Hopefully not)
