SLUL threading API
==================

Thread creation:

    # Information for creating a thread
    type ThreadInfo<t> = struct versioned {
        size structsize

        ref T initparam
        # If this threadfunc is set, the thread will be spawned with a private
        # arena for allocations. Exceptions will only affect the spawned
        # thread, and not the thread of the caller.
        func? ThreadFunc<T> threadfunc
        # If this threadfunc is set, the thead will re-use the arena of the
        # caller (but does not necessarilly need to have a reference to it).
        # Any exceptions will cause both the thread and the thread of the
        # caller to be killed*.
        #
        # * and when a thread is killed, then all threads that it has spawned
        #   will also be killed.
        func? ThreadFunc_ReusedArena<T> threadfunc_reused
        # Thread func that allows the same arena to be shared (also for
        # allocations) among multiple threads. The thread also has a private
        # arena for efficient thread-private allocations.
        #
        # If ANY of the threads that have an arena reference to the concurrent
        # arena is killed, then ALL of those threads will be killed.
        func? ThreadFunc_ConcurrentArena<T> threadfunc_concurrent
        func? ThreadFunc_ReusedConcurrentArena<T> threadfunc_reusedconcurrent

        # Thread attributes, if any
        ref? ThreadAttrs attrs
    }

    # T can be "threaded" or "var" (or perhaps even "writeonly"). Otherwise it is immutable.
    # "arena" parameters will be unreachable.
    # XXX default qualifier for struct members? or immutable by default?
    type ThreadFunc<T> = func(arena any x, ref T initparam) -> bool
    type ThreadFunc_ReusedArena<T> = func(ref T initparam) -> bool

    # Thread funcs that allow arenas to be shared.
    type ThreadFunc_ConcurrentArena<T> = func(arena any x, threaded arena T initparam) -> bool
    type ThreadFunc_ReusedConcurrentArena<T> = func(threaded arena T initparam) -> bool


    type ThreadAttrs = private

    type ThreadResult = enum {
        OK
        Failed
        Crashed
        Killed
    }

    func .create<T>(arena ThreadInfo<T> info) -> own Thread
    func own Thread.join() -> enum ThreadResult
    func own Thread.kill() -> enum ThreadResult
    func own Thread.background() -> void

    func .new(arena any x) -> arena ThreadAttrs
    func arena ThreadAttrs.set_title(string title)
        lifetime title >= this
    func arena ThreadAttrs.set_stacksize(size stacksize)
    func arena ThreadAttrs.set_arenaconfig(ref ArenaConfig acfg)
        lifetime acfg >= this

Multi-threaded arenas:

    func threaded_subarena(arena any x) -> threaded arena any
    func threaded_aalloc<T>(threaded arena any the_arena, size length) -> var threaded arena T

