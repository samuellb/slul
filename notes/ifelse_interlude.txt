if-then-else interlude
======================

It is common to have a sequence of else-if that depend on a common value
that can't be obtained at the beginning of the if's.

One way is to use nested if-else's, but then the indentation gets deep quickly.

Solution with nested if-else's
------------------------------

This can lead to deep indentations.

    if read_type() == .greyscale {
        int v = read_number()
        ...
    } else {
        int r = read_number()
        int g = read_number()
        int b = read_number()
        if (r > 128 && g < r) {
            ...
        } else if (b > 128 && g < b) {
            ...
        } else {
            ...
        }
    }


Solution with "interlude" clause
--------------------------------

    if read_type() == .greyscale {
        int v = read_number()
        ...
    } interlude {
        int r = read_number()
        int g = read_number()
        int b = read_number()
    } else if (r > 128 && g < r) {
        ...
    } else if (b > 128 && g < b) {
        ...
    } else {
        ...
    }


Solution with "with" statement
------------------------------

    if read_type() == .greyscale {
        int v = read_number()
        ...
    } else
    with int r = read_number()
    with int g = read_number()
    with int b = read_number()
    if (r > 128 && g < r) {
        ...
    } else if (b > 128 && g < b) {
        ...
    } else {
        ...
    }

As a bonus, this can also be used with loops:

    with Sponge sp = get_sponge()
    while sp.squeeze() {
        ...
    }
    # sp goes out of scope here.
    # no risk of accidental usage after loop.
