A resource created in one arena might only work with that arena,
since the I/O functions are tied to the arena (and hence handles etc.).
