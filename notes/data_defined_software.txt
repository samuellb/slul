"Data Defined" Software
=======================

In C programs, it is common to see patterns like this
(error handling omitted):

    Window *win = window_new();
    Button *btn1 = button_new();
    Label *label1 = label_new();
    VBox *vbox = vbox_new("OK");
    button_set_default(TRUE);
    container_add(button_as_container(btn1), label_as_control(label1));
    vbox_add(vbox, button_as_control(btn1), FALSE);
    container_add(window_as_container(win), vbox_as_control(vbox));

    window_show(win);
    eventloop_run();

In languages with built in OOP syntax it might look a bit better,
but still ugly:

    Window win = Window.new()
    Button btn = Button.new()
    button.add(Label.new("OK"))
    button.set_default(true)
    VBox vbox = VBox.new()
    vbox.add(button, false);
    win.add(vbox)
    win.show()
    eventloop.run()

Many UI libraries have turned to "data defined" UIs instead, that are
defined by (for example) external XML files.

It would be nice if the same could be acheived with SLUL, but
with a built-in syntax.

Syntax idea 1
-------------

    data namedarray<UIDef> uidef = [
        mainwindow = .main_window(
                responsive(
                    .case(.either(.screen, .highprecision),
                        .vbox(
                            .label("Label 1"),
                            .button("OK")
                        )
                    )
                )
            )
    ]

Syntax idea 2
-------------

    data dataarray<UIDef> = [
        mainwindow =
            <main_window>
                <responsive>
                    <case>
                        <either>
                            <media name="screen"/>
                            <media name="highprecision"/>
                        </either>
                    </case>
                    <then>
                        <vbox>
                            <label>Label 1</label>
                            <button>OK</button>
                        </vbox>
                    </then>
                </responsive>
            </main_window>
    ]

Syntax idea 3
-------------
In main.slul:

    \slul 0.0.0
    \name example
    \depends slului
    \source aaa.slul
    \source bbb.slul
    \xml SlulUI my_uidef1 my_uidef1.xml

In my_uidef1.xml:

    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE something SYSTEM "something.dtd">
    <something xmlns="https://example.com/something">
        <someelement name="xyz">
            <other thing="abc" />
            <other thing="123" />
            <text>Abc 123 &amp;</text>
        </someelement>
    </something>

    Restrictions:
    - xml version and encoding MUST be present
    - Double or single quotes can be used
    - Encoding must be UTF-8 (case insensitive)
    - Should <!DOCTYPE> or xmlns be used?
    - No custom XML entity definitions allowed.
    - No <? or <! tags allowed besides <?xml (and maybe <!DOCTYPE)

In slului/main.slul:

    ...
    TODO
