Specifying language version per \api_def
========================================

Syntax idea 1
-------------

For libraries:

    \slul
    \name somelib
    \type library
    \api_def 1.0 xxxxxxxxxxxxxxx langver 0.0.0
    \api_def 1.1 xxxxxxxxxxxxxxx
    \api_def 1.2 xxxxxxxxxxxxxxx langver 0.1.0


Syntax idea 2
-------------

For libraries:

    \slul 0.2.0 upto 0.3.0
    \langver 0.0.0 since 1.0
    \langver 0.1.0 since 1.2
    \name somelib
    \type library
    \api_def 1.0 xxxxxxxxxxxxxxx
    \api_def 1.1 xxxxxxxxxxxxxxx
    \api_def 1.2 xxxxxxxxxxxxxxx

It should be required to have at least one \langver for modules
with stable API.

For applications:

    \slul 0.2.0 upto 0.3.0
    \name myapp

Syntax idea 3
-------------

For libraries:

    \slul
    \langver 0.0.0 since 1.0
    \langver 0.1.0 since 1.2
    \langver impl 0.2.0 upto 0.3.0
    \name somelib
    \type library
    \api_def 1.0 xxxxxxxxxxxxxxx
    \api_def 1.1 xxxxxxxxxxxxxxx
    \api_def 1.2 xxxxxxxxxxxxxxx

For applications:

    \slul
    \langver 0.2.0 upto 0.3.0
    \name myapp
