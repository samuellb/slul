x86 (32-bit)
------------
Apparently 32-bit x86 has different struct passing on different platforms:

* SysV (Linux) always passes struct returns as a pointer to the stack.
  (this includes unions, even unions of non-structs)
* BSDs pass by register if possible.
* Windows?

Multi-ABI support
-----------------
This could be useful both for multi-OS-ABI (SysV vs. BSD) support and for
multi-ABI-version support (e.g. SLUL version A, SLUL version B).

This can be implemented by adding extra letter(s) to the symbol names
or versions. See "SLUL ABI and emulation" in versioning.txt.

For example:
* "S0" for SysV ABI version 0
* "S1" for SysV ABI version 1 (e.g. if a bug would be found in v. 0)
* "B0" for BSD ABI version 0
* "W0" for Windows ABI version 0
