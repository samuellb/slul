Deprecations/Archival
=====================

It would be nice to have a way to deprecate and/or archive "old" things:

* old functions/types/constants
* old versions / \api_defs

On the implementation side, it is easy. Just add a new file called
"old.slul" with all deprecated function bodies (or a directory with
multiple files).

On the interface side, it will require some special handling. But
it does not have to be complex! Here is a fairly simple solution:

    \slul 0.0.0
    ...
    \api_def 1.2 AbC123dEf...
    ...
    # not sure if the filename should be possible to override
    \deprecated deprecated.slul
    \source impl_stuff1.slul
    \source impl_stuff2.slul
    ...
    \source deprecated/impl1.slul

The \deprecated part of the interface would only have to be parsed in
two cases:

* When the module is the main module being compiled.
* When the a non-existent version is parsed.

When installing the module interface to the slul-interfaces directory,
the deprecated files would have to go into either:

* a combined file, perhaps "MODULE_deprecated.slul"
    - a bit confusing when the filename is in the attribute
* a directory containing the files with deprecated interface parts
  (e.g. "MODULE-deprecated/")
    - this could result in old files being present in "MODULE-deprecated/"
