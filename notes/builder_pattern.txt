Builder pattern
===============

The builder pattern could be avoided by having default values in functions,
AND allowing versioned function declarations.

For example, something like this:

    # Versioned function.
    # Makes the "builder pattern" unnecessary in many (but not all) cases.
    func .new(
            arena,
            int x,
            int y,
            bool b = false since 1.1,
            ?ref Thing thing = none since 1.2) -> arena SomeObject
                since 1.0
                versioned

    # Unversioned function.
    # It's not possible to add parameters.
    func SomeObject.set_x(int x)
            since 1.0
            closed


Note that there are still some cases were the builder pattern is useful,
e.g. when you want to break out configuration to multiple functions that
each provide some parameters to the builder object.


ABI and C compatibility
-----------------------

There are several tricky parts:
- How to interoperate with C
- How to handle backports (and multiple since-versions)


Implementation possibilities - using varargs:

1. Add a version parameter.
2. Add a parameter count parameter.
3. Add a sentinel value (incompatible with int64 -1 etc.)

Implementation possibilities - not using varargs:

1. Pass the versioned parameters in a struct, that is passed by reference.
   e.g. internally this:

    func .new(
            arena,
            int x,
            int y,
            ?ref VersionedParamsForNew params) # <-- internal parameter/type
            since 1.0
            closed

    type VersionedParamsForNew = struct {
        size structsize
        bool b              since 1.1
        ?ref Thing thing    since 1.2
        string s            since 1.0.4
    }
