Usability issues with arena allocation
======================================

In this case, we *probably* want to return an arena-owned pointer:

    func f(arena T x) -> ref U

In this case, we *probably* want to allocate the lower-cased string
in the arena of "a":

    func f(arena T a, arena T b)
    {
        a.set_name(b.get_name().to_lowercase())
    }

This would generate an error. We need to copy b.
But what about other types, e.g. user-defined ones?
We don't want to require everything to have a copy() method.

    func f(arena T a, arena T b)
    {
        a.set_name(b.get_name())
        # need this instead:
        #a.set_name(b.get_name().copy())
    }

---

Should we put arenas on a separate line instead?

    func f(arena T a, ref U b) -> arena V
    # to this:
    func f(ref T a, ref U b) -> ref V
    arena a
    arena return

    - yes: exception break *all* reachable areanas, so in that way it makes
           sense.
    - no: reduces flexibility. cannot copy or compare data between arenas.

Or, use a thread variable (can be a register, an implicit function parameter
or a thread local variable). Then only "arena" function should be allowed
to 1) allocate in an arena 2) perform possibly exception-throwing operations
or 3) call other "arena" functions.

    func f(ref T a, ref U b) arena -> ref V
    # ...but we still need to track the lifetime of the return value
    func f(ref T a, ref U b) arena -> arena V
    
